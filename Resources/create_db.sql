/*SQL-������ ��� �������� ��������� ��������� ��*/

--����� ��������� ��������� ����������
CREATE SCHEMA Objects;

/*������� ���������� ����������
	Id - �������������
	Kind - ��� ���������
	Status - ���������
	ReleaseMark - ������� �� ����������
	DeleteMark - ������� �� ��������
	TranslatesId - ������������� ��������� ��������� � ������� Translates
	Script - ����������� ��� ���������
	Flags - ����������� ������� ��� ���������� �������������	
*/
CREATE TABLE Objects.Documents(
	Id SERIAL PRIMARY KEY,	
	Kind INT,
	Status INT,
	ReleaseMark BOOLEAN,
	DeleteMark BOOLEAN,
	TranslatesId INT,
	Script TEXT,
	Flags INT
);

/*������� ���������� ���������� ����������
	Id - ������������� ���������
	TypeId - ������������� ������� ��������� � �������, ������������ ����� TypeKind
	TypeKind - ������������� ���� ��������� ������������ ����� ������� SysTypes
	Kind - ��� ���������
	TablePartId - ������������� ��������� �����, ����������� �������������� ���������
				  ��������� ��������� ����� � ������� TableParts;
				  -1 ��������, ��� ��� �������� ����� ���������
	TranslatesId - ������������� ��������� ��������� � ������� Translates
	OwnerId - ������������� ��������� ��������� � ������� Documents
*/
CREATE TABLE Objects.Accessories(
	Id SERIAL PRIMARY KEY,		
	TypeId INT,
	TypeKind INT,
	Kind INT,
	TablePartId INT,	
	TranslatesId INT,
	OwnerId INT
);

/*������� ����������� ��������� �����
	Id - ������������� ���������� ����:
		 1 - ����� TypeDefinitionTableName = Numbers
		 2 - ������ TypeDefinitionTableName = Strings
		 3 - ��������� TypeDefinitionTableName = DateTimes
		 4 - ���������� TypeDefinitionTableName = Logicals
		 5 - ��������� TypeDefinitionTableName = Numerators
		 6 - �������������� TypeDefinitionTableName = ImageLinks
		 7 - �������������� TypeDefinitionTableName = Documents
	TypeDefinitionTableName - ��� �������, ������� �������� ����������� ����
	TranslatesId - ������������� ��������� ���������� ���� � ������� Translates
*/
CREATE TABLE Objects.SysTypes(
	Id SERIAL PRIMARY KEY,
	TypeDefinitionTableName VARCHAR(256),	
	TranslatesId INT	
);

/*������� ����������� �������� ���� �����
	Id - ������������� �������
	Length - ����� �����
	Precision - ���������� ������ ����� �������
	Min - ����������� ��������
	Max - ������������ ��������
	DefaultValue - �������� �� ���������
	Required - ������� ������������� ���������� ��������
*/
CREATE TABLE Objects.Numbers(
	Id SERIAL PRIMARY KEY,
	Length INT,	
	Precision INT,
	Min INT,
	Max INT,
	DefaultValue INT,--Number(100, 100),
	Required BOOLEAN
);

/*������� ����������� �������� ���� �����
	Id - ������������� �������	
	Min - ����������� ��������
	Max - ������������ ��������
	DefaultValue - �������� �� ���������
	Required - ������� ������������� ���������� ��������
*/
CREATE TABLE Objects.Integers(
	Id SERIAL PRIMARY KEY,	
	Min INT,
	Max INT,
	DefaultValue INT,
	Required BOOLEAN
);

/*������� ����������� �������� ���� ������������
	Id - ������������� �������	
	Min - ����������� ��������
	Max - ������������ ��������
	DefaultValue - �������� �� ���������
	Required - ������� ������������� ���������� ��������
*/
CREATE TABLE Objects.Floats(
	Id SERIAL PRIMARY KEY,	
	Min FLOAT8,
	Max FLOAT8,
	DefaultValue FLOAT8,
	Required BOOLEAN
);

/*������� ����������� �������� ���� ������
	Id - ������������� �������
	Length - ����� ������	
	DefaultValue - �������� �� ���������
	Required - ������� ������������� ���������� ��������
*/
CREATE TABLE Objects.Strings(
	Id SERIAL PRIMARY KEY,
	Length INT,	
	DefaultValue VARCHAR(4096),
	Required BOOLEAN
);

/*������� ����������� �������� ���� ���������
	Id - ������������� �������		
	DefaultValue - �������� �� ���������
	Required - ������� ������������� ���������� ��������
*/
CREATE TABLE Objects.DateTimes(
	Id SERIAL PRIMARY KEY,	
	DefaultValue TIMESTAMP,
	Required BOOLEAN
);

/*������� Images*/
/*CREATE TABLE Objects.Images(
	id INT PRIMARY KEY,
	img OID
);*/

/*������� ����������� �������� ���� ����������
	Id - ������������� �������		
	DefaultValue - �������� �� ���������
	Required - ������� ������������� ���������� ��������
*/
CREATE TABLE Objects.Logicals(
	Id SERIAL PRIMARY KEY,		
	DefaultValue BOOLEAN,
	Required BOOLEAN
);

/*������� ����������� �������� ���� ���������
	Id - ������������� �������		
	Editable - ������� ����������� �������������� ��������
	Init - ��������� �������� ��� ����������
	Generator - ��������� ������
*/
CREATE TABLE Objects.Numerators(
	Id SERIAL PRIMARY KEY,
	Editable BOOLEAN,
	Init INT,
	Generator TEXT	
);

/*������� ����������� �������� ���� ������
	Id - ������������� �������		
	Flag - ��������� ����
	LinkedTableName - ��� ������� �� ������� ��������� ���������� ������
	LinkedTableRecordId - ������������� ������ � �������, �� ������� ��������� ������
*/
CREATE TABLE Objects.Links(
	Id SERIAL PRIMARY KEY,
	Flag INT,
	LinkedTableName VARCHAR(1024),
	LinkedTableRecordId INT
);

/*������� ���������� ��������� ������ ���������
	Id - ������������� �������		
	TranslatesId - ������������� ��������� ��������� ����� � ������� Translates
	OwnerId - ������������� ��������� ��������� ����� � ������� Documents
*/
CREATE TABLE Objects.TableParts(
	Id SERIAL PRIMARY KEY,
	TranslatesId INT,
	OwnerId INT
);

/*������� ���������� ���� ���������
	Id - ������������� �������
	Kind - ��� �����:
		   0 - ���������� �����
		   1 - �������� �����
	Body - ��� �������� �����
	Module - ����������� ��� ��������� � ������ ������ 
	TranslatesId - ������������� ��������� ����� � ������� Translates
	OwnerId - ������������� ��������� ����� � ������� Documents
*/
CREATE TABLE Objects.Forms(
	Id SERIAL PRIMARY KEY,
	Kind INT,
	Body TEXT,
	Module TEXT,
	TranslatesId INT,
	OwnerId INT
);

/*������� ���������� ������ ��������� ���� ������������
	Id - ������������� �������		
	TranslatesId - ������������� ��������� �������� ������������ � ������� Translates
	OwnerId - ������������� ��������� �������� � ������� Documents
*/
CREATE TABLE Objects.EnumData(
	Id SERIAL PRIMARY KEY,
	TranslatesId INT,
	OwnerId INT
);

/*������� ���������� ���������� �������� ������������
	Id - ������������� �������		
	TranslatesId - ������������� ��������� ����� ������� � ������� Translates
	Body - ������������� �������
*/
CREATE TABLE Objects.Scripts(
	Id SERIAL PRIMARY KEY,
	Body TEXT,
	TranslatesId INT,
	OwnerId INT
);

/*������� ����������� ������ ������ 
	Id - ������������� �����
	Name - �������� �����
*/
CREATE TABLE Objects.LangDefs(
	Id SERIAL PRIMARY KEY,
	Name VARCHAR(128)	
);

/*������� ���������� ��������� 
	Id - ������������� ��������	
	Language(LangDefs.LangId) - ������� �� �����, ������� ��������� � ������� LangDefs;
							����� ����� ��������� ����������� ����������� � �������������
							�� �������� language + ������������� ����� �� ������� LangDefs
*/
CREATE TABLE Objects.Translates(
	Id SERIAL PRIMARY KEY,	
	Language1 VARCHAR(4096),--���������� ����
	Language2 VARCHAR(4096) --������� ����
);

/*������� ����������� ��������� ���������
	Id - ������������� ���������:
		 0 - �������������
		 1 - �����������
		 2 - ���������			
	TranslatesId - ������������� ��������� ��������� � ������� Translates	
*/
CREATE TABLE Objects.DocStatus(
	Id SERIAL PRIMARY KEY,
	TranslatesId INT
);

/*������� ����������� ����� ���������
	Id - ������������� ����:
		 0 - ��������
		 1 - ����������
		 2 - �����
		 3 - ������ ����������
		 4 - �������
		 5 - ������������
		 6 - ���������			
	TranslatesId - ������������� ��������� ���� � ������� Translates	
*/
CREATE TABLE Objects.DocKinds(
	Id SERIAL PRIMARY KEY,
	TranslatesId INT
);

/*��������� ������������*/
CREATE SCHEMA Header;

/*������� ������������� ������������
	Id - �������������
	TranslatesId - ������������� ��������� ��������� � ������� Translates
	Password - ������
	Permission - ��������� ����� ���������� ������������
*/
CREATE TABLE Header.Users(-- �������� ���� ������������
	Id SERIAL PRIMARY KEY,
	Name VARCHAR(256),
	Password VARCHAR(1024),
	Permission INT
);

/*������� ����� ������������
	Id - �������������
	Name - ���
*/
CREATE TABLE Header.Roles(
	Id SERIAL PRIMARY KEY,	
	Name VARCHAR(256)
);

/*������� ���������� ������������
	Id - �������������
	Name - ���
*/
CREATE TABLE Header.Privileges(
	Id SERIAL PRIMARY KEY,	
	Name VARCHAR(256)
);

CREATE TABLE Header.Info(
	Id SERIAL PRIMARY KEY,
	ExternalId VARCHAR(64),
	Name VARCHAR(256),
	Author VARCHAR(256),
	Version VARCHAR(32),
	CreationDateTime TIMESTAMP,
	LastModificationDateTime TIMESTAMP,
	LastModificationUserName VARCHAR(256),
	UserId INT,
	UserDataBaseId INT	
);

/*C�������� ��������� ��������� ����������*/
CREATE SCHEMA Archive;

--������� Doc_Archive
CREATE TABLE Archive.ArchiveDocuments(
	Id SERIAL PRIMARY KEY,
	DocumentId INT,	
	DocumentXML TEXT
);

/*
/--������� ActionJournal
CREATE TABLE Archive.ActionJournal(
	Id INT PRIMARY KEY,
	Action TEXT
);

/--������� ActionType
CREATE TABLE Archive.ActionType(-- ���� �������� ��� �������� ������������
	Id INT PRIMARY KEY,
	TranslatesId INT
);
*/
