CREATE DATABASE CommonDataStorage
  WITH OWNER = %1
       ENCODING = 'UTF8'
       TABLESPACE = pg_default;

CREATE TABLE Configurations
(
  Id SERIAL PRIMARY KEY, 
  Host varchar(256),
  Port varchar(5),
  DataBaseName varchar(256),
  UserName varchar(256),
  Password varchar(256)
) 
WITHOUT OIDS;
ALTER TABLE Configurations OWNER TO %1;
