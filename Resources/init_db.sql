--���������� ������ �� ���������� ����������

--������� LangDefs
INSERT INTO Objects.LangDefs(Name)
VALUES ('English');

INSERT INTO Objects.LangDefs(Name)
VALUES ('Russian');

--������� Translates
INSERT INTO Objects.Translates(Language1)
VALUES ('Numeric');

INSERT INTO Objects.Translates(Language1)
VALUES ('Integer');

INSERT INTO Objects.Translates(Language1)
VALUES ('Float');

INSERT INTO Objects.Translates(Language1)
VALUES ('String');

INSERT INTO Objects.Translates(Language1)
VALUES ('DateTime');

INSERT INTO Objects.Translates(Language1)
VALUES ('Boolean');

INSERT INTO Objects.Translates(Language1)
VALUES ('Numerator');

INSERT INTO Objects.Translates(Language1)
VALUES ('Link');

INSERT INTO Objects.Translates(Language1)
VALUES ('Image');

INSERT INTO Objects.Translates(Language1)
VALUES ('UnCompiled');

INSERT INTO Objects.Translates(Language1)
VALUES ('Compiled');

INSERT INTO Objects.Translates(Language1)
VALUES ('Deleted');

INSERT INTO Objects.Translates(Language1)
VALUES ('Document');

INSERT INTO Objects.Translates(Language1)
VALUES ('Dictionary');

INSERT INTO Objects.Translates(Language1)
VALUES ('Report');

INSERT INTO Objects.Translates(Language1)
VALUES ('DocumentsJournal');

INSERT INTO Objects.Translates(Language1)
VALUES ('Register');

INSERT INTO Objects.Translates(Language1)
VALUES ('Enumeration');

INSERT INTO Objects.Translates(Language1)
VALUES ('Constatnt');

INSERT INTO Objects.Translates(Language1)
VALUES ('Form');

--������� DocStatus
INSERT INTO Objects.DocStatus(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'UnCompiled';

INSERT INTO Objects.DocStatus(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Compiled';

INSERT INTO Objects.DocStatus(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Deleted';

--������� DocKinds
INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Document';

INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Dictionary';

INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Report';

INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'DocumentsJournal';

INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Register';

INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Enumeration';

INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Constant';

INSERT INTO Objects.DocKinds(TranslatesId)
SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Form';

--������� SysTypes
INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Numbers', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Numeric'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Integers', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Integer'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Floats', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Float'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Strings', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'String'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('DateTimes', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'DateTime'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Images', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Image'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Logicals', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Boolean'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Numerators', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Numerator'));

INSERT INTO Objects.SysTypes(TypeDefinitionTableName, TranslatesId)
VALUES ('Links', (SELECT Id FROM Objects.Translates translates WHERE translates.language1 = 'Link'));
