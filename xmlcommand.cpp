#include "xmlcommand.h"

XMLCommand::XMLCommand(const XMLCommandType& commandType)
{  
  id = -1;
  type = commandType;  
}

XMLCommand::XMLCommand(const int commandId, const XMLCommandType& commandType)
{  
  id = commandId;
  type = commandType;   
}

int XMLCommand::getId()
{
  return id;
}

XMLCommand::XMLCommandType XMLCommand::getType()
{
  return type;
}

XMLCommand::XMLCommandResultType XMLCommand::getResultType()
{
  return resultType;
}

QString XMLCommand::toXML()
{
  //������� ���������
  xmlCommand.clear();

  QDomElement root = xmlCommand.createElement("command");
  root.setAttribute("id", id);

  switch (type)
  {
    case GetConfigurationList:
    {
      root.setAttribute("type", 1);
    }
    break;

    case GetConfiguration:
    {
      root.setAttribute("type", 2);
    }
    break;

    case SetConfiguration:
    {
      root.setAttribute("type", 3);
    }
    break;

    case ReleaseConfiguration:
    {
      root.setAttribute("type", 4);
    }
    break;
  }

  xmlCommand.appendChild(root);

  QDomElement ne = xmlCommand.createElement("parameters");
  root.appendChild(ne);
  
  //���������� ����������
  for (int i = 0; i < parameters.size(); i++)
  {
    XMLParameter xmlParam = parameters.value(i);

    ne = xmlCommand.createElement("parameter");
    ne.setAttribute("name", xmlParam.name);
    
    int paramType = xmlParam.value.type;
    
    switch (xmlParam.value.type)
    {
      case TVoid:
      {
        //ne.setAttribute("type", 0);
      }
      break;

      case TInt:
      {     
        if ( xmlParam.value.value.type() == QVariant::Int )
        {
          ne.setAttribute("value", xmlParam.value.value.toInt());          
        }
        else
        {
          ne.setAttribute("err", "Type mismatch");
          paramType = -1;          
        }
      }
      break;

      case TString:
      {        
        if ( xmlParam.value.value.type() == QVariant::String )
        {
          ne.setAttribute("value", xmlParam.value.value.toString());
        }
        else
        {
          ne.setAttribute("err", "Type mismatch");
          paramType = -1;          
        }
      }
      break;

      case TBool:
      {        
        if (xmlParam.value.value.type() == QVariant::Bool)
        {
          ne.setAttribute("value", xmlParam.value.value.toBool());
        }
        else
        {
          ne.setAttribute("err", "Type mismatch");
          paramType = -1;          
        }
      }
      break;

      case TDateTime:
      {        
        if ( xmlParam.value.value.type() == QVariant::DateTime)
        {
          ne.setAttribute("value", xmlParam.value.value.toDateTime().toString());
        }
        else
        {
          ne.setAttribute("err", "Type mismatch");
          paramType = -1;          
        }
      }
      break;    
    }

    ne.setAttribute("type", paramType);

    getElement(xmlCommand, "/command/parameters").appendChild(ne);
  }

  //���������� ���������� �������
  //������������ ����������
  ne = xmlCommand.createElement("result");
  root.appendChild(ne);

  if (resultType == Error)
  {
    //���������� ������
    for (int i = 0; i < errors.size(); i++)
    {
      XMLError xmlError = errors.value(i);
      
      ne = xmlCommand.createElement("error");
      ne.setAttribute("code", xmlError.code);
      ne.setAttribute("msg", xmlError.msg);

      getElement(xmlCommand, "/command/result").appendChild(ne);
    }
  }
  else
  {
    if (resultType == ReturnedValue)
    {
      //���������� ������������ ��������
      for (int i = 0; i < returnedValues.size(); i++)
      {
        XMLValue xmlValue = returnedValues.value(i);

        ne = xmlCommand.createElement("returnedvalue");
        ne.setAttribute("id", i);
        
        int valType = xmlValue.type;
        switch (xmlValue.type)
        {
          case TVoid:
          {
            
          }
          break;

          case TInt:
          {                   
            if (xmlValue.value.type() == QVariant::Int)
            {
              ne.setAttribute("value", xmlValue.value.toInt());
            }
            else
            {
              ne.setAttribute("err", "Type mismatch");
              valType = -1;
            }
          }
          break;

          case TString:
          {
            ne.setAttribute("type", 2);
            if (xmlValue.value.type() == QVariant::String)
            {
              ne.setAttribute("value", xmlValue.value.toString());
            }
            else
            {
              ne.setAttribute("err", "Type mismatch");
              valType = -1;
            }
          }
          break;

          case TBool:
          {
            ne.setAttribute("type", 3);
            if (xmlValue.value.type() == QVariant::Bool)
            {
              ne.setAttribute("value", xmlValue.value.toBool());
            }
            else
            {
              ne.setAttribute("err", "Type mismatch");
              valType = -1;
            }
          }
          break;

          case TDateTime:
          {
            ne.setAttribute("type", 4);
            if (xmlValue.value.type() == QVariant::DateTime)
            {
              ne.setAttribute("value", xmlValue.value.toDateTime().toString());
            }
            else
            {
              ne.setAttribute("err", "Type mismatch");
              valType = -1;
            }
          }
          break;    
        }
        
        ne.setAttribute("type", valType);

        getElement(xmlCommand, "/command/result").appendChild(ne);   
      }
    }
  }
  
  return xmlCommand.toString();
}

bool XMLCommand::fromXML(const QString& xml)
{
  //������� ���������
  xmlCommand.clear();

  //�������� ���������
  if ( !xmlCommand.setContent(xml) )
  {
    return false;
  }
  
  //��������� ��������� kind � id �������� /command
  QDomElement fe = getElement(xmlCommand, "/command");
  id = fe.attribute("id").toInt();
  int cmdType = fe.attribute("type").toInt();
  //enum XMLCommand::XMLCommandType typeCmd =  fe.attribute("type").toInt();
  switch (cmdType)
  {
    case 1:
    {
      type = GetConfigurationList;
    }
    break;

    case 2:
    {
      type = GetConfiguration;
    }
    break;

    case 3:
    {
      type = SetConfiguration;
    }
    break;

    case 4:
    {
      type = ReleaseConfiguration;
    }
    break;
  }

  //��������� �������� /command/parameters
  fe = getElement(xmlCommand, "/command/parameters").firstChild().toElement();
  //������ ���������� � ������������ �������� �������� ����������
  while ( !fe.isNull() )
  {
    XMLParameter curParameter;
    curParameter.name = fe.attribute("name");
    XMLValue curParamValue;

    int paramType = fe.attribute("type").toInt();    
    switch (paramType)
    {
      case 0:
      {
        curParamValue.type = TVoid;
      }
      break;

      case 1:
      {
        curParamValue.type = TInt;
        curParamValue.value.setValue(fe.attribute("value").toInt());
      }
      break;

      case 2:
      {
        curParamValue.type = TString;
        curParamValue.value.setValue(fe.attribute("value"));
      }
      break;      

      case 3:
      {
        curParamValue.type = TBool;
        curParamValue.value.setValue((bool)fe.attribute("value").toInt());
       /* if (fe.attribute("value").toInt() == 0)
        {
          curParamValue.value.setValue(false);
        }
        else
        {
          curParamValue.value.setValue(true);
        }        */
      }
      break; 

      case 4:
      {
        curParamValue.type = TDateTime;
        curParamValue.value.setValue(QDateTime::fromString(fe.attribute("value"), ""));
      }
      break;
    }

    curParameter.value = curParamValue;
    parameters.append(curParameter);

    //������� � ���������� ���������
    fe = fe.nextSibling().toElement();
  }

  //������ �������� � ������������ �������� �������� ����������  
  if (getElement(xmlCommand, "/command/result/error").isNull())
  {
    resultType = ReturnedValue;
    fe = getElement(xmlCommand, "/command/result/returnedvalue").toElement();
  }
  else
  {
    resultType = Error;
    fe = getElement(xmlCommand, "/command/result/error").toElement();
  }
  
  while (!fe.isNull())
  {
    if (resultType == Error)
    {
      XMLError curError;
      curError.code = fe.attribute("code").toInt();
      curError.msg = fe.attribute("msg");
      errors.append(curError);
    }
    else
    {
      if (resultType == ReturnedValue)
      {
        XMLValue curValue;
      
        int valType = fe.attribute("type").toInt();
        switch (valType)
        {
          case 0:
          {
            curValue.type = TVoid;
          }
          break;

          case 1:
          {
            curValue.type = TInt;
            curValue.value.setValue(fe.attribute("value").toInt());
          }
          break;

          case 2:
          {
            curValue.type = TString;
            curValue.value.setValue(fe.attribute("value"));
          }
          break;          

          case 3:
          {
            curValue.type = TBool;
            curValue.value.setValue((bool)fe.attribute("value").toInt());
            /*if (fe.attribute("value").toInt() == 0)
            {
              curValue.value.setValue(false);
            }
            else
            {
              curValue.value.setValue(true);
            }     */   
          }
          break;

          case 4:
          {
            curValue.type = TDateTime;
            curValue.value.setValue(QDateTime::fromString(fe.attribute("value"), ""));
          }
          break;
        }       

        returnedValues.append(curValue);
      }
    }

    //������� � ���������� ��������
    fe = fe.nextSibling().toElement();
  }
  
  //�������� ���������
  return true;
}

void XMLCommand::addParameter(const XMLParameter& newParameter)
{
  XMLParameter xmlParam;
  xmlParam.name = newParameter.name;  
  xmlParam.value = newParameter.value;  

  parameters.append(xmlParam);  
}

void XMLCommand::removeParameter(const int index)
{
  if ((index < 0) || (index >= parameters.size()))
  {
    return;
  }

  parameters.removeAt(index);
}

XMLCommand::XMLParameter XMLCommand::getParameter(const int index)
{
  if ((index >= 0) && (index < parameters.size()))
  {
    return parameters.value(index);
  }
  else
  {
    //������� ������� ��������
    XMLParameter xmlParam;
    xmlParam.name = "";
    XMLValue xmlValue;
    xmlValue.type = TVoid;
    xmlValue.value = QVariant();    
    xmlParam.value = xmlValue;
    return xmlParam;
  }
}

int XMLCommand::getParametersCount()
{
  return parameters.size();
}

void XMLCommand::parametersClear()
{
  parameters.clear();
}

void XMLCommand::addError(const XMLError& newError)
{
  //����������� ������� ����� ���� ���� ������, ���� ���������� ��������
  //������� ��� ������������ ������ ���� ���������� ������������� �������� ����������
  returnedValues.clear();
  resultType = Error;

  XMLError xmlError;
  xmlError.code = newError.code;
  xmlError.msg = newError.msg;

  errors.append(xmlError);
}

XMLCommand::XMLError XMLCommand::getError(const int index)
{
  if ((index >= 0) && (index < errors.size()))
  {
    return errors.value(index);
  }
  else
  {
    //������� ������� ��������
    XMLError xmlError;
    xmlError.code = -1;
    xmlError.msg = "";    
    return xmlError;
  }
}

void XMLCommand::removeError(const int index)
{
  if ((index < 0) || (index >= errors.size()))
  {
    return;
  }

  errors.removeAt(index);
}

int XMLCommand::getErrorsCount()
{
  return errors.size();
}

void XMLCommand::errorsClear()
{
  errors.clear();
}

void XMLCommand::addReturnedValue(const XMLValue& newValue)
{
  errors.clear();
  resultType = ReturnedValue;
  
  XMLValue xmlValue;
  xmlValue.type = newValue.type;
  xmlValue.value = newValue.value;

  returnedValues.append(xmlValue);
}

void XMLCommand::removeReturnedValue(const int index)
{

  if ( (index < 0) || (index >= returnedValues.size()) )
  {
    return;
  }

  returnedValues.removeAt(index);
}

XMLCommand::XMLValue XMLCommand::getReturnedValue(const int index)
{
  if ( (index >= 0) && (index < returnedValues.size()) )
  {
    return returnedValues.value(index);
  }
  else
  {
    //������� ������� ��������
    XMLValue xmlValue;
    xmlValue.type = TVoid;
    xmlValue.value = QVariant();
     
    return xmlValue;
  }
}

int XMLCommand::getReturnedValuesCount()
{
  return returnedValues.size();
}

void XMLCommand::returnedValuesClear()
{
  returnedValues.clear();
}
