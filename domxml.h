/***********************************************************************
* Module:   domxml.h
* Author:   LGP
* Modified: 29 ������� 2006 �.
* Purpose:  ����� ��������� xml ��������� ������ DOM
***********************************************************************/

#ifndef DOM_XML_H
#define DOM_XML_H

#include <QString>
#include <QStringList>
#include <QDomDocument>
#include <QDomElement>
#include <QDomAttr>

namespace ConfiguratorObjects
{
  namespace Documents
  {
    class DomXml
    {
    public:
      DomXml();
      virtual ~DomXml();

      //����� ��������� ����� �������� �� ��������� ����, ������� � ���������� �������� 
      QDomElement findElement(const QDomElement&, QStringList&);

      //����� ��������� ����� �������� �� ��������� ���� � ��������� �
      //���������� ��� � ������ ��������� ������, ����� ���������� ������ �������
      QDomElement getElement(const QDomDocument&, const QString&);

      //����� ��������� ����� �������� �� ��������� ���� ������������ ��������� �������� �
      //���������� ��� � ������ ��������� ������, ����� ���������� ������ �������
      QDomElement getElement(const QDomElement&, const QString&); 
    };
  }
}

#endif
