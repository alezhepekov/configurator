#ifndef NEW_USER_DATABASE_FORM_H
#define NEW_USER_DATABASE_FORM_H

#include <QDialog>
#include "generatedfiles/ui_newuserdatabaseform.h"
#include "configurationheader.h"

namespace ConfiguratorObjects
{
  class NewUserDatabaseForm : public QDialog, Ui_NewUserDatabaseForm
  {
    Q_OBJECT

  public:
    NewUserDatabaseForm(QWidget *parent = 0);
    ~NewUserDatabaseForm();

    ConnectionInfo getConnectionInfo();

  protected:
    bool event(QEvent *);
  };
}

#endif
