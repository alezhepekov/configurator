#include "dbcontrol.h"

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;

DBControl::DBControl()
{  
}

DBControl::~DBControl()
{
  disconnect();
}

bool DBControl::createDatabase(ConnectionInfo& connectionInfo)
{  
  QSqlDatabase newDb = QSqlDatabase::addDatabase("QPSQL", connectionInfo.dbName);
  
  //��������� ���������� �����������
  newDb.setHostName(connectionInfo.host);    
  newDb.setDatabaseName("template1");
  newDb.setUserName(connectionInfo.userName);
  newDb.setPassword(connectionInfo.password);

  QSqlQuery query(newDb);
  QString sql;

  if ( !newDb.open() )
  {   
    lastError = newDb.lastError().text();
    return false;
  }

  //�������� � ���������� ������� �� �������� �������� ��   
  QFile fileCreateDb(":/Configurator/create_db.sql");
  QFile fileInitDb(":/Configurator/init_db.sql");
  if ( !fileCreateDb.open(QIODevice::ReadOnly | QIODevice::Text) || !fileInitDb.open(QIODevice::ReadOnly | QIODevice::Text) )
  {
    newDb.close();
    return false;
  }
  
  //���������� ������� �� �������� �������� ��
  sql = QString("CREATE DATABASE %1 WITH OWNER = %2 ENCODING = 'UTF8' TABLESPACE = pg_default TEMPLATE template0").arg(connectionInfo.dbName).arg(connectionInfo.userName);
  query.prepare(sql);
  if ( !query.exec() )
  {   
    lastError = query.lastError().text();
    return false;
  }

  //���������� �� �� template1
  newDb.close();

  //����������� � ��������� ��
  newDb.setDatabaseName(connectionInfo.dbName);
  if ( !newDb.open() )
  {
    lastError = newDb.lastError().text();
    return false;
  }  

  //���������� �������� 
  bool runScriptError = false;
  if ( newDb.transaction() )
  {   
    QTextStream streamDbCreate(&fileCreateDb);    
    query.prepare(streamDbCreate.readAll());
    if ( !query.exec() )
    {     
      lastError = newDb.lastError().text();
      newDb.rollback();
      runScriptError = true;
    }

    if ( !runScriptError )
    {
      //�������� � ���������� ������� �� ������������� ������    
      QTextStream streamDbInit(&fileInitDb);    
      query.prepare(streamDbInit.readAll());
      if ( !query.exec() )
      {        
        lastError = newDb.lastError().text();
        newDb.rollback();
        runScriptError = true;       
      }
    }    

    fileCreateDb.close();
    fileInitDb.close();
  }
  
  if ( !runScriptError )
  {
    newDb.commit();
    newDb.close();
    return true;
  }
    
  //���������� ������� �� �������� ��
  newDb.close();
  newDb.setDatabaseName("template1");
  if ( newDb.open() )
  {
    sql = QString("DROP DATABASE %1").arg(connectionInfo.dbName);
    query.prepare(sql);
    if ( !query.exec() )
    {      
      lastError = newDb.lastError().text();
    }
    newDb.close();
  }
  
  return false; 
}

bool DBControl::connect(ConnectionInfo& connectionInfo)
{  
  currentDatabase = QSqlDatabase::addDatabase("QPSQL", connectionInfo.dbName);
  currentDatabase.setHostName(connectionInfo.host);
  currentDatabase.setDatabaseName(connectionInfo.dbName);
  currentDatabase.setUserName(connectionInfo.userName);
  currentDatabase.setPassword(connectionInfo.password);
  
  dbConnectionInfo = connectionInfo;

  if ( !currentDatabase.open() )
  {   
    lastError = currentDatabase.lastError().text();
    return false;
  }  

  return true;    
}

int DBControl::recordCount(QSqlQuery &query, const QString& tableName)
{
  QString sql = "SELECT Count(*) FROM " + tableName;
  query.prepare(sql);

  if (!query.exec())
  {    
    return -1;
  }

  query.first(); 
  return query.value(0).toInt();
}

int DBControl::getRecordCount(const QString& tableName)
{
  //��������� ����� ������� ������� tableName
  QSqlQuery query(currentDatabase);
  return recordCount(query, tableName);  
}

int DBControl::getRecordCount(QSqlDatabase &database, const QString &tableName)
{
  //��������� ����� ������� ������� tableName
  QSqlQuery query(database);
  return recordCount(query, tableName);
}

int DBControl::sequenceMaxValue(QSqlQuery &query, const QString &tableName)
{  
  QString sql = QString("SELECT setval('%1_id_seq', MAX(Id)) FROM %1").arg(tableName);
  query.prepare(sql);

  if ( !query.exec() )
  {    
    std::cout << query.lastError().text().toLocal8Bit().data() << std::endl;
    return -1;
  }

  query.first();
  return query.value(0).toInt();
}

int DBControl::getSequenceMaxValue(const QString& tableName)
{
  QSqlQuery query(currentDatabase);
  return sequenceMaxValue(query, tableName);
}

int DBControl::getSequenceMaxValue(QSqlDatabase &database, const QString& tableName)
{
  QSqlQuery query(database);
  return sequenceMaxValue(query, tableName);
}

int DBControl::fillSequence(QSqlQuery &query, const QString& tableName)
{
  QString sql = QString("SELECT Id FROM %1 ORDER BY Id").arg(tableName);
  query.prepare(sql);

  if (!query.exec())
  {    
    return false;
  }

  QSqlQuery subQuery(query);

  int i = 1;
  bool qr = query.first();
  while ( qr )
  {    
    if ( query.record().field("Id").value().toInt() != i )
    {
      sql = QString("UPDATE %1 SET Id = :NewId WHERE Id = :Id").arg(tableName);
      subQuery.prepare(sql);
      subQuery.bindValue(":Id", query.record().field("Id").value().toInt());
      subQuery.bindValue(":NewId", i);

      if ( !subQuery.exec() )
      {        
        return false;
      }
    }    

    i++;
    qr = query.next();
  }

  sql = QString("ALTER SEQUENCE %1_id_seq RESTART WITH :NewStartValue").arg(tableName);  

  query.prepare(sql);
  query.bindValue(":NewStartValue", i);
  if (!query.exec())
  {    
    return false;
  }

  return true;
}

bool DBControl::fillIndex(const QString& tableName)
{
  QSqlQuery query(currentDatabase);
  return fillSequence(query, tableName);
}

bool DBControl::fillIndex(QSqlDatabase &database, const QString& tableName)
{
  QSqlQuery query(database);
  return fillSequence(query, tableName);
}

QString DBControl::getLastError() const
{
  return lastError;
}

ConnectionInfo DBControl::getConnectionInfo()
{
  return dbConnectionInfo;
}

QSqlDatabase* DBControl::getDatabase()
{
  return &currentDatabase;
}

BaseDocument* DBControl::getDocument(int docId)
{  
  if ( !currentDatabase.isOpen() ) return NULL;
    
  QSqlQuery query(currentDatabase);
  QString sql;
  
  //��������� �� ������� Documents ��������� � ��������������� docId
  sql = "SELECT Kind FROM Objects.Documents WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", docId);
  if ( !query.exec() ) return NULL;
    
  //���� ������� ������, �� �������� � ���� �� ���������
  if ( !query.first() ) return NULL;

  //���������� ���������
  switch ( query.record().field("Kind").value().toInt() )
  {
    case DOC_KIND_DOCUMENT:
    {
      Document *newDocument = new Document();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }     

      return newDocument;      
    }
  	break;

    case DOC_KIND_DICTIONARY:
    {
      Dictionary *newDocument = new Dictionary();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;      
    }
    break;

    case DOC_KIND_REPORT:
    {
      Report *newDocument = new Report();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;  
    }
    break;

    case DOC_KIND_DOCUMENTS_JOURNAL:
    {
      DocumentsJournal *newDocument = new DocumentsJournal();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;    
    }
    break;

    case DOC_KIND_REGISTER:
    {     
      Register *newDocument = new Register();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_ENUMERATION:
    {      
      Enumeration *newDocument = new Enumeration();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_CONSTANT:
    {      
      Constant *newDocument = new Constant();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_FORM:
    {      
      DForm *newDocument = new DForm();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_TABLE:
    {      
      DTable *newDocument = new DTable();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_QUERY:
    {      
      DQuery *newDocument = new DQuery();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_CLASS_MODULE:
    {
      DClassModule *newDocument = new DClassModule();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_GLOBAL_THREAD:
    {
      DGlobalThread *newDocument = new DGlobalThread();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_GLOBAL_CONTAINER:
    {
      DGlobalContainer *newDocument = new DGlobalContainer();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_TRIGGER:
    {
      DTrigger *newDocument = new DTrigger();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    case DOC_KIND_TIME_MANAGER:
    {
      DTimeManager *newDocument = new DTimeManager();
      newDocument->setId(docId);

      if ( !loadContentForBaseDocument(newDocument) || !loadContentForDocument(newDocument) )
      {
        delete newDocument;
        return NULL;
      }    

      return newDocument;
    }
    break;

    default:
      return NULL;
    break;
  }
}

bool DBControl::setDocument(BaseDocument* document)
{  
  if ( !document ) return false; 
  
  //������ � ��������� ���� ������ ���������  
  QSqlQuery query(currentDatabase);
  QString sql;

  //�������� ������������ �������������� ���������
  sql = "SELECT Id FROM Objects.Documents WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", document->getId());

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
  }

  if ( query.first() )
  {
    //������������� ��� ������������
    lastError = QString("Document id=%1 unique value failed!").arg(document->getId());
    return false;
  }

  switch ( document->getKind() )
  {
    case DOC_KIND_DOCUMENT:
    {
      Document* curDocument = static_cast<Document*> (document);

      //���������� �������� ������� ���������
      if ( !appendContentForDocument(curDocument) )
      {       
        return false;
      }            
    }
    break;

    case DOC_KIND_DICTIONARY:
    {
      Dictionary* curDocument = static_cast<Dictionary*> (document);
     
      //���������� �������� ������� ���������
      if ( !appendContentForDocument(curDocument) )
      {
        //currentDatabase.rollback();
        return false;
      }
    }
    break;

    case DOC_KIND_REPORT:
    {
      Report* curDocument = static_cast<Report*> (document);     

      //���������� �������� ������� ���������
      if ( !appendContentForDocument(curDocument) )
      {       
        return false;
      }
    }
    break;

    case DOC_KIND_DOCUMENTS_JOURNAL:
    {
      DocumentsJournal* curDocument = static_cast<DocumentsJournal*> (document);     

      //���������� �������� ������� ���������
      if ( !appendContentForDocument(curDocument) )
      {     
        return false;
      }
    }
    break;

    case DOC_KIND_REGISTER:
    {
      Register* curDocument = static_cast<Register*> (document);     

      //���������� �������� ������� ���������
      if ( !appendContentForDocument(curDocument) )
      {      
        return false;
      }
    }
    break;

    case DOC_KIND_ENUMERATION:
    {
      Enumeration* curDocument = static_cast<Enumeration*> (document);     

      //���������� �������� ������� ���������
      if ( !appendContentForDocument(curDocument) )
      {       
        return false;
      }
    }
    break;

    case DOC_KIND_CONSTANT:
    {
      Constant* curDocument = static_cast<Constant*> (document);     

      //���������� �������� ������� ���������
      if ( !appendContentForDocument(curDocument) )
      {       
        return false;
      }
    }
    break;

    case DOC_KIND_FORM:
    {
      DForm* curDocument = static_cast<DForm*> (document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {       
        return false;
      }
    }
    break;

    case DOC_KIND_TABLE:
    {
      DTable* curDocument = static_cast<DTable*> (document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {       
        return false;
      }
    }
    break;

    case DOC_KIND_QUERY:
    {
      DQuery *curDocument = static_cast<DQuery*>(document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {
        return false;
      }
    }
    break;

    case DOC_KIND_CLASS_MODULE:
    {
      DClassModule *curDocument = static_cast<DClassModule*>(document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {
        return false;
      }
    }
    break;

    case DOC_KIND_GLOBAL_THREAD:
    {
      DGlobalThread *curDocument = static_cast<DGlobalThread*>(document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {
        return false;
      }
    }
    break;

    case DOC_KIND_GLOBAL_CONTAINER:
    {
      DGlobalContainer *curDocument = static_cast<DGlobalContainer*>(document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {
        return false;
      }
    }
    break;

    case DOC_KIND_TRIGGER:
    {
      DTrigger *curDocument = static_cast<DTrigger*>(document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {
        return false;
      }
    }
    break;

    case DOC_KIND_TIME_MANAGER:
    {
      DTimeManager *curDocument = static_cast<DTimeManager*>(document);     

      //���������� ����������� ���������
      if ( !appendContentForDocument(curDocument) )
      {
        return false;
      }
    }
    break;

    default:
    {     
      return false;
    }      
    break;
  }

  //���������� ��������� 
  int translatesId = 0;
  if ( !appendToDatabase(document->getTranslates(), translatesId) )
  {   
    return false;
  }  

  sql = "INSERT INTO Objects.Documents("
        "Id, Kind, Status, ReleaseMark, DeleteMark, TranslatesId, Script, Flags) "
        "VALUES (:Id, :Kind, :Status, :ReleaseMark, :DeleteMark, :TranslatesId, :Script, :Flags)";

  query.prepare(sql);
  query.bindValue(":Id", document->getId());
  query.bindValue(":Kind", document->getKind());
  query.bindValue(":Status", document->getStatus());
  query.bindValue(":ReleaseMark", document->getReleaseMark());
  query.bindValue(":DeleteMark", document->getDeleteMark());
  query.bindValue(":TranslatesId", translatesId);
  query.bindValue(":Script", document->getScript()->getBody());
  query.bindValue(":Flags", document->getFlags());
  
  if ( !query.exec() )
  {   
    lastError =  query.lastError().text();
    return false;
  }
 
  return true;
}

bool DBControl::deleteDocument(int docId)
{
  if (!currentDatabase.isOpen() || docId > getSequenceMaxValue("Objects.Documents") || docId < 1)
  {
    return false;
  }

  QSqlQuery query(currentDatabase);
  QString sql;

  //������ �� ������� ������ ������� documents ��� ������� Id = docId
  sql = "SELECT Kind FROM Objects.Documents WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", docId);

  if (!query.exec())
  {
    return false;
  }

  //���� ������� ������
  if (!query.first())
  {
    return false;
  }  

  //����������� ���� ���������
  int kind = query.record().field("Kind").value().toInt(); 
  switch ( kind )
  {
    case DOC_KIND_DOCUMENT:
    {              
      if (!deleteContentForDocumentDocument(docId))
      {        
        return false;
      }           
    }
  	break;

    case DOC_KIND_DICTIONARY:
    {              
      if (!deleteContentForDocumentDictionary(docId))
      {       
        return false;
      }           
    }
    break;

    case DOC_KIND_REPORT:
    {              
      if (!deleteContentForDocumentReport(docId))
      {       
        return false;
      }           
    }
    break;

    case DOC_KIND_DOCUMENTS_JOURNAL:
    {              
      if (!deleteContentForDocumentDocumentsJournal(docId))
      {       
        return false;
      }           
    }
    break;

    case DOC_KIND_REGISTER:
    {              
      if (!deleteContentForDocumentRegister(docId))
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_ENUMERATION:
    {              
      if (!deleteContentForDocumentEnumeration(docId))
      {       
        return false;
      }           
    }
    break;

    case DOC_KIND_CONSTANT:
    {              
      if (!deleteContentForDocumentConstant(docId))
      {       
        return false;
      }           
    }
    break;

    case DOC_KIND_FORM:
    {              
      if (!deleteContentForDocumentForm(docId))
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_TABLE:
    {              
      if ( !deleteContentForDocumentTable(docId) )
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_QUERY:
    {              
      if ( !deleteContentForDocumentQuery(docId) )
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_CLASS_MODULE:
    {              
      if ( !deleteContentForDocumentClassModule(docId) )
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_GLOBAL_THREAD:
    {              
      if ( !deleteContentForDocumentGlobalThread(docId) )
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_GLOBAL_CONTAINER:
    {              
      if ( !deleteContentForDocumentGlobalContainer(docId) )
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_TRIGGER:
    {              
      if ( !deleteContentForDocumentTrigger(docId) )
      {      
        return false;
      }           
    }
    break;

    case DOC_KIND_TIME_MANAGER:
    {              
      if ( !deleteContentForDocumentTimeManager(docId) )
      {      
        return false;
      }           
    }
    break;

    default:
    {    
      return false;
    }      
    break;
  }

  //�������� ���������
  sql = "DELETE FROM Objects.Documents WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", docId);

  if (!query.exec())
  {   
    return false;
  }
 
  return true;
}

QList<Script*> DBControl::getConfigurationScripts()
{
  QSqlQuery query(currentDatabase);
  QString sql;

  QList<Script*> scriptList;

  //���������� ���������� � ��������
  sql = "SELECT * FROM Objects.Scripts WHERE OwnerId = 0";
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
  }

  Script *newScript = NULL;  
  bool qr = query.first();
  while ( qr )
  {
    newScript = new Script(query.record().field("Body").value().toString());
    newScript->setId(query.record().field("Id").value().toInt());
   
    int trId = query.record().field("TranslatesId").value().toInt();

    //�������� ��������� ����� �������
    takeFromDatabase(newScript->getTranslates(), trId);

    scriptList.append(newScript);

    qr = query.next();
  }

  return scriptList;
}

bool DBControl::setConfigurationScripts(const QList<Script*> &scriptList)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  //�������� ������ � ������������ ���������� �������� ������������
  removeScripts(0); 

  //������ ������
  for ( QList<Script*>::const_iterator i = scriptList.constBegin(); i != scriptList.constEnd(); i++ )
  {
    int trId;
    if ( !appendToDatabase((*i)->getTranslates(), trId) )
    {
      lastError = query.lastError().text();      
      return false;
    }

    //���������� ���������� � ��������
    sql = "INSERT INTO Objects.Scripts(Body, TranslatesId, OwnerId) "
          "VALUES (:Body, :TrId, 0)";
    query.prepare(sql);
    query.bindValue(":TrId", trId);
    query.bindValue(":Body", (*i)->getBody());

    if ( !query.exec() )
    {
      lastError = query.lastError().text();     
      return false;
    }
  }

  return true;
}

ConfigurationHeader DBControl::getHeader()
{
  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT * FROM Header.Info";
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();    
    return ConfigurationHeader();
  }

  if ( !query.first() )
  {
    //������� �����!!!    
    return ConfigurationHeader();
  }

  ConfigurationHeader newConfigurationHeader;
  //���������� ����� ����� ��������� 
  newConfigurationHeader.setName(query.record().field("Name").value().toString());
  newConfigurationHeader.setAuthor(query.record().field("Author").value().toString());
  newConfigurationHeader.setVersion(query.record().field("Version").value().toString());
  newConfigurationHeader.setCreationDateTime(query.record().field("CreationDateTime").value().toDateTime());
  newConfigurationHeader.setLastModificationDateTime(query.record().field("LastModificationDateTime").value().toDateTime());
  newConfigurationHeader.setLastModificationUserName(query.record().field("LastModificationUserName").value().toString());
  newConfigurationHeader.setUserDatabaseId(query.record().field("UserDataBaseId").value().toInt()); 

  //���������� �����
  QList<Role*> roles;

  sql = "SELECT * FROM Header.Roles";
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();   
    return ConfigurationHeader();
  }

  bool qr = query.first();
  while ( qr )
  {
    Role *newRole = new Role();  
    newRole->setName(query.record().field("Name").value().toString());

    roles.append(newRole);

    qr = query.next();
  }

  newConfigurationHeader.setRoles(roles);

  //���������� ����������
  QList<Privilege*> privileges;

  sql = "SELECT * FROM Header.Privileges";
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return ConfigurationHeader();
  }

  qr = query.first();
  while ( qr )
  {
    Privilege *newPrivilege = new Privilege();   
    newPrivilege->setName(query.record().field("Name").value().toString());

    privileges.append(newPrivilege);

    qr = query.next();
  }

  newConfigurationHeader.setPriveleges(privileges);

  return newConfigurationHeader;
}

bool DBControl::setHeader(const ConfigurationHeader &newHeader)
{
  QSqlQuery query(currentDatabase);
  QString sql; 

  //������� ������
  sql = "DELETE FROM Header.Info;\n"
        "ALTER SEQUENCE Header.Info_id_seq RESTART WITH 1;"
        "DELETE FROM Header.Roles;\n"
        "ALTER SEQUENCE Header.Roles_id_seq RESTART WITH 1;"
        "DELETE FROM Header.Privileges;\n"
        "ALTER SEQUENCE Header.Privileges_id_seq RESTART WITH 1;"
        "DELETE FROM Header.Users;\n"
        "ALTER SEQUENCE Header.Users_id_seq RESTART WITH 1;";

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  sql = "INSERT INTO Header.Info "
        "(Name, Author, Version, CreationDateTime, LastModificationDateTime, LastModificationUserName, UserDataBaseId) "
        "VALUES "
        "(:Name, :Author, :Version, :CreationDateTime, :LastModificationDateTime, :LastModificationUserName, :UserDataBaseId)";

  query.prepare(sql);
  query.bindValue(":Name", newHeader.getName());
  query.bindValue(":Author", newHeader.getAuthor());
  query.bindValue(":Version", newHeader.getVersion());
  query.bindValue(":CreationDateTime", newHeader.getCreationDateTime());
  query.bindValue(":LastModificationDateTime", newHeader.getLastModificationDateTime());
  query.bindValue(":LastModificationUserName", newHeader.getLastModificationUserName());
  query.bindValue(":UserDataBaseId", newHeader.getUserDatabaseId()); 

  if ( !query.exec() )
  {
    lastError = query.lastError().text();   
    return false;
  }  

  for ( QList<Role*>::const_iterator i = newHeader.getRoles()->constBegin(); i != newHeader.getRoles()->constEnd(); i++ )
  {
    sql = "INSERT INTO Header.Roles (ExternalId, Name) "
      "VALUES (:ExternalId, :Name)";

    query.prepare(sql);
    query.bindValue(":ExternalId", (*i)->getExternalId());
    query.bindValue(":Name", (*i)->getName());

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return false;
    }
  }

  for ( QList<Privilege*>::const_iterator i = newHeader.getPrivileges()->constBegin(); i != newHeader.getPrivileges()->constEnd(); i++ )
  {
    sql = "INSERT INTO Header.Privileges (ExternalId, Name) "
      "VALUES (:ExternalId, :Name)";

    query.prepare(sql);
    query.bindValue(":ExternalId", (*i)->getExternalId());
    query.bindValue(":Name", (*i)->getName());

    if ( !query.exec() )
    {
      lastError = query.lastError().text();     
      return false;
    }
  }

  return true;
}

bool DBControl::clear()
{
  //�������� � ���������� ������� �� �������� �������� ��
  QFile inFile(":/Configurator/clear_db.sql");
  if ( !inFile.open(QIODevice::ReadOnly | QIODevice::Text) )
  {         
    return false;
  }

  QSqlQuery query(currentDatabase);
  QString sql;
 
  QTextStream in(&inFile);
  sql = in.readAll();

  query.prepare(sql);
  if ( !query.exec() )
  {
    return false;
  }
 
  inFile.close();

  return true;
}

bool DBControl::addArchiveDocument(int docId, const QString& xmlDoc)
{
  QSqlQuery query(currentDatabase);

  QString sql = "INSERT INTO Archive.ArchiveDocuments(DocumentId, DocumentXML) "
                "VALUES (:DocumentId, :DocumentXML)";
  query.prepare(sql);
  query.bindValue(":DocumentId:", docId);
  query.bindValue(":DocumentXML", xmlDoc);  

  return query.exec();
}

QStringList DBControl::getArchiveDocuments(int docId)
{
  return QStringList();
}

bool DBControl::appendToDatabase(const QMap<int, QString> *translates, int& recNo)
{  
  QSqlQuery query(currentDatabase);
  //���������� ������ �������� � ������� Translates
  QString sql = "INSERT INTO Objects.Translates(Id) "
                "VALUES(DEFAULT)";
  query.prepare(sql);
  if (!query.exec())
  {    
    //std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
    return false;
  }  

  //��������� �������������� ���������� ������������ ��������
  recNo = getSequenceMaxValue("Objects.Translates");

  //��������� ������ � ������ ������� 
  sql = "SELECT Id FROM Objects.LangDefs";
  query.prepare(sql);
  if (!query.exec())
  {
    //std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
    return false;
  }

  //������ ���� translates �� ���������� ���������� ��������������� ������  
  QMap<int, QString>::const_iterator i = translates->constBegin(); 
  int langId = 0;
  while ( i != translates->constEnd() )
  {
    //��������� ��������������� ������
    bool flag = query.first();
    bool bf = false;
    while (flag && !bf)
    {      
      langId = query.value(0).toInt();
      if (langId == i.key())
      {
        //������������� ����� ���������        
        bf = true;
      }
      flag = query.next();
    }

    if (!bf)
    {
      //��������� ������������ ������������� �����
      //std::cerr << QString("Not allowed language id").toLocal8Bit().data() << std::endl;
      return false;
    }
    else
    {
      //�������� ��������� � ������� translates        
      QSqlQuery query1(query);
      sql = QString("UPDATE Objects.Translates SET Language%1 = :LangText "
                    "WHERE Id = :Id").arg(i.key());
      query1.prepare(sql);      
      //query1.bindValue(":LangId", QString("Language%1").arg(i.key()));
      query1.bindValue(":LangText", i.value());
      query1.bindValue(":Id", recNo);

      if (!query1.exec())
      {        
        //std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;          
        return false;
      }         
    }
    //��������� ���������� �������� ���������
    ++i;
  }

  return true;  
}

bool DBControl::appendToDatabase(const QList<Accessory*> *accessories, const int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  for ( QList<Accessory*>::const_iterator i = accessories->constBegin(); i != accessories->constEnd(); i++ )
  {
    //������ ��������� ���������
    int newTranslatesId = 0;    
    if ( !appendToDatabase((*i)->getTranslates(), newTranslatesId) )
    {      
      return false;
    }
   
    //������ ���� ���������
    int accTypeId = 0;
    int accTypeKind = 0;
    if ( (*i)->getType() )
    {
      accTypeKind = (*i)->getType()->getTypeKind();

      BaseType* type = (*i)->getType();    
      switch ( type->getTypeKind() )
      {
        case DATA_TYPE_NUMBER:
        {
          Number *accTypeNumber = dynamic_cast<Number*>(type);

          sql = "INSERT INTO Objects.Numbers(Length, Precision, Min, Max, DefaultValue, Required) "
                "VALUES(:Length, :Precision, :Min, :Max, :DefaultValue, :Required)";
          query.prepare(sql);
          query.bindValue(":Length", accTypeNumber->getLength());
          query.bindValue(":Precision", accTypeNumber->getPrecision());
          query.bindValue(":Min", accTypeNumber->getMin());
          query.bindValue(":Max", accTypeNumber->getMax());
          query.bindValue(":DefaultValue", accTypeNumber->getDefault());
          query.bindValue(":Required", accTypeNumber->getRequired());
          if ( !query.exec() )
          {
            lastError =  query.lastError().text();
            return false;
          }

          //��������� ����� ������� ������� Numbers          
          accTypeId = getRecordCount("Objects.Numbers");
        }
        break;

        case DATA_TYPE_INTEGER:
        {
          Integer *accTypeInteger = dynamic_cast<Integer*>(type);

          sql = "INSERT INTO Objects.Integers(Min, Max, DefaultValue, Required) "
                "VALUES(:Min, :Max, :DefaultValue, :Required)";
          query.prepare(sql);          
          query.bindValue(":Min", accTypeInteger->getMin());
          query.bindValue(":Max", accTypeInteger->getMax());
          query.bindValue(":DefaultValue", accTypeInteger->getDefault());
          query.bindValue(":Required", accTypeInteger->getRequired());
          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return false;
          }

          //��������� ����� ������� ������� Numbers          
          accTypeId = getRecordCount("Objects.Integers");
        }
        break;

        case DATA_TYPE_FLOAT:
        {
          Float *accTypeFloat = dynamic_cast<Float*>(type);

          sql = "INSERT INTO Objects.Floats(Min, Max, DefaultValue, Required) "
                "VALUES(:Min, :Max, :DefaultValue, :Required)";
          query.prepare(sql);          
          query.bindValue(":Min", accTypeFloat->getMin());
          query.bindValue(":Max", accTypeFloat->getMax());
          query.bindValue(":DefaultValue", accTypeFloat->getDefault());
          query.bindValue(":Required", accTypeFloat->getRequired());
          if (!query.exec())
          {
            lastError = query.lastError().text();
            return false;
          }

          //��������� ����� ������� ������� Numbers          
          accTypeId = getRecordCount("Objects.Floats");
        }
        break;

        case DATA_TYPE_STRING:
        {
          String *accTypeString = dynamic_cast<String*>(type);

          sql = "INSERT INTO Objects.Strings(Length, DefaultValue, Required) "
                "VALUES(:Length, :DefaultValue, :Required)";
          query.prepare(sql);
          query.bindValue(":Length", accTypeString->getLength());          
          query.bindValue(":DefaultValue", accTypeString->getDefault());
          query.bindValue(":Required", accTypeString->getRequired());
          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return false;
          }

          //��������� ����� �������
          accTypeId = getRecordCount("Objects.Strings");
        }
        break;

        case DATA_TYPE_DATETIME:
        {
          DateTime *accTypeDateTime = dynamic_cast<DateTime*>(type);

          sql = "INSERT INTO Objects.DateTimes(DefaultValue, Required) "
                "VALUES(:DefaultValue, :Required)";
          query.prepare(sql);                   
          query.bindValue(":DefaultValue", accTypeDateTime->getDefault());         
          query.bindValue(":Required", accTypeDateTime->getRequired());
          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return false;
          }

          //��������� ����� �������         
          accTypeId = getRecordCount("Objects.DateTimes");
        }
        break;

        case DATA_TYPE_BOOLEAN:
        {
          Logical *accTypeLogical = dynamic_cast<Logical*>(type);

          sql = "INSERT INTO Objects.Logicals(DefaultValue, Required) "
                "VALUES(:DefaultValue, :Required)";
          query.prepare(sql);                   
          query.bindValue(":DefaultValue", accTypeLogical->getDefault());
          query.bindValue(":Required", accTypeLogical->getRequired());
          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return false;
          }

          //��������� ����� �������          
          accTypeId = getRecordCount("Objects.Logicals");
        }
        break;

        case DATA_TYPE_NUMERATOR:
        {
          Numerator *accTypeNumerator = dynamic_cast<Numerator*>(type);

          sql = "INSERT INTO Objects.Numerators(Editable, Init, Generator) "
                "VALUES(:Editable, :Init, :Generator)";
          query.prepare(sql);                   
          query.bindValue(":Editable", accTypeNumerator->getEditable());
          query.bindValue(":Init", accTypeNumerator->getInit());
          query.bindValue(":Generator", accTypeNumerator->getGenerator());
          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return false;
          }

          //��������� ����� �������          
          accTypeId = getRecordCount("Objects.Numerators");
        }
        break;

        case DATA_TYPE_LINK:
        {
          Link *accTypeLink = dynamic_cast<Link*>(type);
          sql = "INSERT INTO Objects.Links(Flag, LinkedTableName, LinkedTableRecordId) "
                "VALUES (:Flag, :LinkedTableName, :LinkedTableRecordId)";          
          query.prepare(sql);                   
          query.bindValue(":Flag", accTypeLink->getFlag());
          query.bindValue(":LinkedTableName", accTypeLink->getLinkedTableName());
          query.bindValue(":LinkedTableRecordId", accTypeLink->getLinkedTableRecordId());
          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return false;
          }

          //��������� ����� �������          
          accTypeId = getRecordCount("Objects.Links");
        }
        break;
      }
    }
    else
    {      
      accTypeId = -1;
    }

    //���������� ������ � ������� Accessories
    sql = "INSERT INTO Objects.Accessories(TypeId, TypeKind, Kind, TablePartId, TranslatesId, OwnerId) "
          "VALUES(:TypeId, :TypeKind, :Kind, :TablePartId, :TranslatesId, :OwnerId)";
    query.prepare(sql);
    query.bindValue(":TypeId", accTypeId);
    query.bindValue(":TypeKind", accTypeKind);
    query.bindValue(":Kind", (*i)->getFlags());
    query.bindValue(":TablePartId", (*i)->getTablePartId());
    query.bindValue(":TranslatesId", newTranslatesId);
    query.bindValue(":OwnerId", ownerId);
    if (!query.exec())
    {
      lastError = query.lastError().text();
      return false;
    }
  }

  //for (int i = 0; i < accList.size(); i++)
  //{
  //  //������ ��������� ���������
  //  int newTranslatesId = 0;    
  //  if (!appendToDatabase(*(accList[i]->getTranslates()), newTranslatesId))
  //  {      
  //    return false;
  //  }
  //  
  //  int accTypeKind, accTypeId;
  //  //������ ���� ���������
  //  if (accList[i]->getType() != NULL)
  //  {
  //    BaseType* accType = accList[i]->getType();
  //    accTypeKind = accType->getTypeKind();
  //    accTypeId = 0;
  //    switch(accTypeKind)
  //    {
  //      case DATA_TYPE_NUMBER:
  //      {
  //        Number* accTypeNumber = dynamic_cast<Number*> (accType);
  //        sql = "INSERT INTO Objects.Numbers(Length, Precision, Min, Max, DefaultValue, Required) "
  //              "VALUES(:Length, :Precision, :Min, :Max, :DefaultValue, :Required)";
  //        query.prepare(sql);
  //        query.bindValue(":Length", accTypeNumber->getLength());
  //        query.bindValue(":Precision", accTypeNumber->getPrecision());
  //        query.bindValue(":Min", accTypeNumber->getMin());
  //        query.bindValue(":Max", accTypeNumber->getMax());
  //        query.bindValue(":DefaultValue", accTypeNumber->getDefault());
  //        query.bindValue(":Required", accTypeNumber->getRequired());
  //        if (!query.exec())
  //        {
  //          std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� ������� ������� Numbers          
  //        accTypeId = getRecordCount("Objects.Numbers");
  //      }
  //      break;

  //      case DATA_TYPE_INTEGER:
  //      {
  //        Integer *accTypeInteger = dynamic_cast<Integer*> (accType);
  //        sql = "INSERT INTO Objects.Integers(Min, Max, DefaultValue, Required) "
  //              "VALUES(:Min, :Max, :DefaultValue, :Required)";
  //        query.prepare(sql);          
  //        query.bindValue(":Min", accTypeInteger->getMin());
  //        query.bindValue(":Max", accTypeInteger->getMax());
  //        query.bindValue(":DefaultValue", accTypeInteger->getDefault());
  //        query.bindValue(":Required", accTypeInteger->getRequired());
  //        if (!query.exec())
  //        {
  //          std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� ������� ������� Numbers          
  //        accTypeId = getRecordCount("Objects.Integers");
  //      }
  //      break;

  //      case DATA_TYPE_FLOAT:
  //      {
  //        Float *accTypeFloat = dynamic_cast<Float*> (accType);
  //        sql = "INSERT INTO Objects.Floats(Min, Max, DefaultValue, Required) "
  //              "VALUES(:Min, :Max, :DefaultValue, :Required)";
  //        query.prepare(sql);          
  //        query.bindValue(":Min", accTypeFloat->getMin());
  //        query.bindValue(":Max", accTypeFloat->getMax());
  //        query.bindValue(":DefaultValue", accTypeFloat->getDefault());
  //        query.bindValue(":Required", accTypeFloat->getRequired());
  //        if (!query.exec())
  //        {
  //          std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� ������� ������� Numbers          
  //        accTypeId = getRecordCount("Objects.Floats");
  //      }
  //      break;

  //      case DATA_TYPE_STRING:
  //      {
  //        String* accTypeString = dynamic_cast<String*> (accType);
  //        sql = "INSERT INTO Objects.Strings(Length, DefaultValue, Required) "
  //              "VALUES(:Length, :DefaultValue, :Required)";
  //        query.prepare(sql);
  //        query.bindValue(":Length", accTypeString->getLength());          
  //        query.bindValue(":DefaultValue", accTypeString->getDefault());
  //        query.bindValue(":Required", accTypeString->getRequired());
  //        if (!query.exec())
  //        {
  //          //std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� �������
  //        accTypeId = getRecordCount("Objects.Strings");
  //      }
  //      break;

  //      case DATA_TYPE_DATETIME:
  //      {
  //        DateTime* accTypeDateTime = dynamic_cast<DateTime*> (accType);
  //        sql = "INSERT INTO Objects.DateTimes(DefaultValue, Required) "
  //              "VALUES(:DefaultValue, :Required)";
  //        query.prepare(sql);                   
  //        query.bindValue(":DefaultValue", accTypeDateTime->getDefault());
  //        query.bindValue(":Required", accTypeDateTime->getRequired());
  //        if (!query.exec())
  //        {
  //          //std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� �������         
  //        accTypeId = getRecordCount("Objects.DateTimes");
  //      }
  //      break;

  //      case DATA_TYPE_BOOLEAN:
  //      {
  //        Logical* accTypeLogical = dynamic_cast<Logical*> (accType);
  //        sql = "INSERT INTO Objects.Logicals(DefaultValue, Required) "
  //              "VALUES(:DefaultValue, :Required)";
  //        query.prepare(sql);                   
  //        query.bindValue(":DefaultValue", accTypeLogical->getDefault());
  //        query.bindValue(":Required", accTypeLogical->getRequired());
  //        if (!query.exec())
  //        {
  //          std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� �������          
  //        accTypeId = getRecordCount("Objects.Logicals");
  //      }
  //      break;

  //      case DATA_TYPE_NUMERATOR:
  //      {
  //        Numerator* accTypeNumerator = dynamic_cast<Numerator*> (accType);
  //        sql = "INSERT INTO Objects.Numerators(Editable, Init, Generator) "
  //              "VALUES(:Editable, :Init, :Generator)";
  //        query.prepare(sql);                   
  //        query.bindValue(":Editable", accTypeNumerator->getEditable());
  //        query.bindValue(":Init", accTypeNumerator->getInit());
  //        query.bindValue(":Generator", accTypeNumerator->getGenerator());
  //        if (!query.exec())
  //        {
  //          //std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� �������          
  //        accTypeId = getRecordCount("Objects.Numerators");
  //      }
  //      break;

  //      case DATA_TYPE_LINK:
  //      {
  //        Link* accTypeLink = dynamic_cast<Link*> (accType);
  //        sql = "INSERT INTO Objects.Links(Flag, LinkedTableName, LinkedTableRecordId) "
  //              "VALUES (:Flag, :LinkedTableName, :LinkedTableRecordId)";          
  //        query.prepare(sql);                   
  //        query.bindValue(":Flag", accTypeLink->getFlag());
  //        query.bindValue(":LinkedTableName", accTypeLink->getLinkedTableName());
  //        query.bindValue(":LinkedTableRecordId", accTypeLink->getLinkedTableRecordId());
  //        if (!query.exec())
  //        {
  //          //std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //          return false;
  //        }

  //        //��������� ����� �������          
  //        accTypeId = getRecordCount("Objects.Links");
  //      }
  //      break;
  //    }
  //  }
  //  else
  //  {
  //    accTypeKind = -1;
  //    accTypeId = -1;
  //  }
  //  
  //  //���������� ������ � ������� Accessories
  //  sql = "INSERT INTO Objects.Accessories(TypeId, TypeKind, Kind, TablePartId, TranslatesId, OwnerId) "
  //        "VALUES(:TypeId, :TypeKind, :Kind, :TablePartId, :TranslatesId, :OwnerId)";
  //  query.prepare(sql);
  //  query.bindValue(":TypeId", accTypeId);
  //  query.bindValue(":TypeKind", accTypeKind);
  //  query.bindValue(":Kind", accList[i]->getFlags());
  //  query.bindValue(":TablePartId", accList[i]->getTablePartId());
  //  query.bindValue(":TranslatesId", newTranslatesId);
  //  query.bindValue(":OwnerId", accList[i]->getOwnerId());
  //  if (!query.exec())
  //  {
  //    //std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
  //    return false;
  //  }
  //}

  return true;
}

bool DBControl::appendToDatabase(const QList<TablePart*> *tableParts, const int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql;
  for ( QList<TablePart*>::const_iterator i = tableParts->constBegin(); i != tableParts->constEnd(); i++ )
  {
    //������ ��������� ��������� �����
    int newTranslatesId = 0;    
    if ( !appendToDatabase((*i)->getTranslates(), newTranslatesId) )
    {      
      return false;
    }

    //���������� ������ � ������� TableParts
    sql = "INSERT INTO Objects.TableParts(TranslatesId, OwnerId) "
          "VALUES(:TranslatesId, :OwnerId)";
    query.prepare(sql);    
    query.bindValue(":TranslatesId", newTranslatesId);
    query.bindValue(":OwnerId", ownerId);
    if (!query.exec())
    {
      lastError = query.lastError().text();
      return false;
    }
  }

  return true;
}

bool DBControl::appendToDatabase(const QList<Form*> *forms, const int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql;
  for ( QList<Form*>::const_iterator i = forms->constBegin(); i != forms->constEnd(); i++ )
  {
    //������ ��������� ��������� �����
    int newTranslatesId = 0;      
    if ( !appendToDatabase((*i)->getTranslates(), newTranslatesId) )
    {      
      return false;
    }

    //���������� ������ � ������� TableParts
    sql = "INSERT INTO Objects.Forms(Kind, Body, Module, TranslatesId, OwnerId) "
          "VALUES(:Kind, :Body, :Module, :TranslatesId, :OwnerId)";
    query.prepare(sql);    
    query.bindValue(":Kind", (*i)->getKind());
    query.bindValue(":Body", (*i)->getBody()->getBody());
    query.bindValue(":Module", (*i)->getModule()->getBody());
    query.bindValue(":TranslatesId", newTranslatesId);
    query.bindValue(":OwnerId", ownerId);
    if (!query.exec())
    {
      lastError = query.lastError().text();
      return false;
    }
  }

  return true;
}

bool DBControl::appendToDatabase(const QList<Script*> *scripts, const int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  for ( QList<Script*>::const_iterator i = scripts->constBegin(); i != scripts->constEnd(); i++ )
  {
    //������ ���������
    int newTranslatesId = 0;  
    if ( !appendToDatabase((*i)->getTranslates(), newTranslatesId) )
    {      
      return false;
    }

    //���������� ������ � ������� Scripts
    sql = "INSERT INTO Objects.Scripts(Body, TranslatesId, OwnerId) "
          "VALUES(:Body, :TranslatesId, :OwnerId)";
    query.prepare(sql);   
    query.bindValue(":Body", (*i)->getBody());    
    query.bindValue(":TranslatesId", newTranslatesId);
    query.bindValue(":OwnerId", ownerId);
    
    if ( !query.exec() )
    {
      lastError =  query.lastError().text();
      return false;
    }
  }  

  return true;
}

bool DBControl::takeFromDatabase(QMap<int, QString> *translates, const int translatesId)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT * FROM Objects.Translates WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", translatesId);
  if (!query.exec())
  {
    std::cerr << query.lastError().text().toLocal8Bit().data() << std::endl;
    return false;
  }

  //���� ������� ������, �� �������� � ���� �� ����������
  bool br = query.first();
  if (!br)
  {
    return true;
    //return false;
  }

  //������� ������ �������
  while (br)
  {    
    //��������� ������� ������
    QSqlRecord curRec = query.record();
    //��������� ������ ���������
    for (int i = 1; i < curRec.count(); i++)
    {
      translates->insert(i, curRec.field(i).value().toString());
    }

    br = query.next();
  }

  return true;
}

bool DBControl::takeFromDatabase(QList<Accessory*> *accessories, const int ownerId)
{  
  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT * FROM Objects.Accessories WHERE OwnerId = :Id ORDER BY Id";
  query.prepare(sql);
  query.bindValue(":Id", ownerId);
  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  //���� ������� ������, �� ��������� � ���� �� ����������
  bool br = query.first();
  //������� ������ �������
  
  int i = 1;
  while (br)
  {  
    //��������� ������� ������
    QSqlRecord curRec = query.record();
    Accessory *curAcc = new Accessory();
    //curAcc.setId(curRec.field("Id").value().toInt());
    curAcc->setId(i);

    //�������� ���� ���������
    QSqlQuery query1(query);
    int accTypeKind = curRec.field("TypeKind").value().toInt();
    int accTypeId = curRec.field("TypeId").value().toInt();    
    switch ( accTypeKind )
    {
      case DATA_TYPE_NUMBER:
      {
        sql = "SELECT * FROM Objects.Numbers WHERE Id = :Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          Number* accTypeNumber = new Number;
          accTypeNumber->setId(curRec1.field("Id").value().toInt());
          accTypeNumber->setLength(curRec1.field("Length").value().toInt());
          accTypeNumber->setPrecision(curRec1.field("Precision").value().toInt());
          accTypeNumber->setMin(curRec1.field("Min").value().toInt());
          accTypeNumber->setMax(curRec1.field("Max").value().toInt());
          accTypeNumber->setDefault(curRec1.field("DefaultValue").value().toInt());
          accTypeNumber->setRequired(curRec1.field("Required").value().toBool());

          curAcc->setType(accTypeNumber);
        }        
      }
      break;

      case DATA_TYPE_INTEGER:
      {
        sql = "SELECT * FROM Objects.Integers WHERE Id = :Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          Integer *accTypeInteger = new Integer();
          accTypeInteger->setId(curRec1.field("Id").value().toInt());         
          accTypeInteger->setMin(curRec1.field("Min").value().toInt());
          accTypeInteger->setMax(curRec1.field("Max").value().toInt());
          accTypeInteger->setDefault(curRec1.field("DefaultValue").value().toInt());
          accTypeInteger->setRequired(curRec1.field("Required").value().toBool());

          curAcc->setType(accTypeInteger);
        }        
      }
      break;

      case DATA_TYPE_FLOAT:
      {
        sql = "SELECT * FROM Objects.Floats WHERE Id = :Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          Float *accTypeFloat = new Float();
          accTypeFloat->setId(curRec1.field("Id").value().toInt());         
          accTypeFloat->setMin(curRec1.field("Min").value().toDouble());
          accTypeFloat->setMax(curRec1.field("Max").value().toDouble());
          accTypeFloat->setDefault(curRec1.field("DefaultValue").value().toDouble());
          accTypeFloat->setRequired(curRec1.field("Required").value().toBool());

          curAcc->setType(accTypeFloat);
        }        
      }
      break;

      case DATA_TYPE_STRING:
      {
        sql = "SELECT * FROM Objects.Strings WHERE Id = :Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          String* accTypeString = new String;
          accTypeString->setId(curRec1.field("Id").value().toInt());
          accTypeString->setLength(curRec1.field("Length").value().toInt());          
          accTypeString->setDefault(curRec1.field("DefaultValue").value().toString());
          accTypeString->setRequired(curRec1.field("Required").value().toBool());

          curAcc->setType(accTypeString);
        }
      }
      break;

      case DATA_TYPE_DATETIME:
      {
        sql = "SELECT * FROM Objects.DateTimes WHERE Id = :Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          DateTime* accTypeDateTime = new DateTime;
          accTypeDateTime->setId(curRec1.field("Id").value().toInt());                    
          accTypeDateTime->setDefault(curRec1.field("DefaultValue").value().toDateTime());
          accTypeDateTime->setRequired(curRec1.field("Required").value().toBool());

          curAcc->setType(accTypeDateTime);
        }
      }
      break;

      case DATA_TYPE_BOOLEAN:
      {
        sql = "SELECT * FROM Objects.Logicals WHERE Id = :Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          Logical* accTypeLogical = new Logical;
          accTypeLogical->setId(curRec1.field("Id").value().toInt());                    
          accTypeLogical->setDefault(curRec1.field("DefaultValue").value().toBool());
          accTypeLogical->setRequired(curRec1.field("Required").value().toBool());

          curAcc->setType(accTypeLogical);
        }
      }
      break;

      case DATA_TYPE_NUMERATOR:
      {
        sql = "SELECT * FROM Objects.Numerators WHERE Id = :Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          Numerator* accTypeNumerator = new Numerator;
          accTypeNumerator->setId(curRec1.field("Id").value().toInt());
          accTypeNumerator->setEditable(curRec1.field("Editable").value().toBool());          
          accTypeNumerator->setInit(curRec1.field("Init").value().toString());
          accTypeNumerator->setGenerator(curRec1.field("Generator").value().toString());

          curAcc->setType(accTypeNumerator);
        }
      }
      break;

      case DATA_TYPE_LINK:
      {
        sql = "SELECT * FROM Objects.Links WHERE Id = :Id ORDER BY Id";
        query1.prepare(sql);
        query1.bindValue(":Id", accTypeId);
        if (!query1.exec())
        {
          //std::cerr << query1.lastError().text().toLocal8Bit().data() << std::endl;
          return false;
        }

        //��� � ����� ��������������� � ������� �� ���������
        if (query1.first())
        {
          QSqlRecord curRec1 = query1.record();

          Link* accTypeLink = new Link;
          accTypeLink->setId(curRec1.field("Id").value().toInt());
          accTypeLink->setFlag(curRec1.field("Flag").value().toInt());          
          accTypeLink->setLinkedTableName(curRec1.field("LinkedTableName").value().toString());
          accTypeLink->setLinkedTableRecordId(curRec1.field("LinkedTableRecordId").value().toInt());

          curAcc->setType(accTypeLink);
        }
      }
      break;
    }

    curAcc->setFlags(curRec.field("Kind").value().toInt());
    curAcc->setTablePartId(curRec.field("TablePartId").value().toInt());   

    takeFromDatabase(curAcc->getTranslates(), curRec.field("TranslatesId").value().toInt());

    accessories->append(curAcc);
    
    i++;
    br = query.next();
  }

  return true;
}

bool DBControl::takeFromDatabase(QList<TablePart*> *tableParts, const int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT * FROM Objects.TableParts WHERE OwnerId = :Id";
  query.prepare(sql);
  query.bindValue(":Id", ownerId);
  if (!query.exec())
  {
    lastError =  query.lastError().text();
    return false;
  }

  //���� ������� ������, �� ��������� ����� � ���� �� ����������
  bool qr = query.first(); 

  //������� ������ ������� 
  while ( qr )
  {    
    //��������� ������� ������
    QSqlRecord curRec = query.record();
    TablePart *curTP = new TablePart();
    curTP->setId(curRec.field("Id").value().toInt());
   
    takeFromDatabase(curTP->getTranslates(), curRec.field("TranslatesId").value().toInt());

    tableParts->append(curTP);

    qr = query.next();
  }

  return true;
}

bool DBControl::takeFromDatabase(QList<Form*> *forms, const int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT * FROM Objects.Forms WHERE OwnerId = :Id";
  query.prepare(sql);
  query.bindValue(":Id", ownerId);
  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  //���� ������� ������, �� ����� � ���� �� ����������
  bool br = query.first();
  if (!br)
  {
    //return false;
    return true;
  }

  //������� ������ ������� 
  while (br)
  {    
    //��������� ������� ������
    QSqlRecord curRec = query.record();
    Form *curForm = new Form();
    curForm->setId(curRec.field("Id").value().toInt());
    curForm->setKind(curRec.field("Kind").value().toInt());
    Script curScript(curRec.field("Body").value().toString());   
    curForm->setBody(curScript);
    curScript.setBody(curRec.field("Module").value().toString());
    curForm->setModule(curScript);
  
    takeFromDatabase(curForm->getTranslates(), curRec.field("TranslatesId").value().toInt());

    forms->append(curForm);

    br = query.next();
  }

  return true;
}

bool DBControl::takeFromDatabase(QList<Script*> *scripts, const int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT * FROM Objects.Scripts WHERE OwnerId = :OwnerId";
  query.prepare(sql);
  query.bindValue(":OwnerId", ownerId);
  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }  
  
  bool qr = query.first();
  while ( qr )
  {    
    //��������� ������� ������
    QSqlRecord curRec = query.record();

    Script *newScript = new Script(curRec.field("Body").value().toString());
    newScript->setId(curRec.field("Id").value().toInt());
    
    takeFromDatabase(newScript->getTranslates(), curRec.field("TranslatesId").value().toInt());

    scripts->append(newScript);

    qr = query.next();
  }

  return true;
}

bool DBControl::removeTranslates(int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql = "SELECT TranslatesId FROM Objects.Documents WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", ownerId);
  
  if (!query.exec())
  {
    return false;
  }

  //���� ������� ������
  if (!query.first())
  {
    return true;
  }

  int translatesId = query.record().field("TranslatesId").value().toInt();

  sql = "DELETE FROM Objects.Translates WHERE "
        "(Id = :TranslatesId) "
        "OR (Id IN (SELECT TranslatesId FROM Objects.Accessories WHERE OwnerId = :OwnerId)) "
        "OR (Id IN (SELECT TranslatesId FROM Objects.TableParts  WHERE OwnerId = :OwnerId)) "
        "OR (Id IN (SELECT TranslatesId FROM Objects.Forms       WHERE OwnerId = :OwnerId))";
  query.prepare(sql);
  query.bindValue(":OwnerId", ownerId);
  query.bindValue(":TranslatesId", translatesId);
 
  return query.exec();
}

bool DBControl::removeAccessories(int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql = "SELECT TypeId, TypeKind FROM Objects.Accessories WHERE OwnerId = :OwnerId";
  query.prepare(sql);
  query.bindValue(":OwnerId", ownerId);

  if ( !query.exec() )
  {
    return false;
  }

  bool qr = query.first();
  //������� ����������
  while ( qr )
  {
    QSqlQuery subQuery(query);
    QString tableName;          

    int typeKind = query.record().field("TypeKind").value().toInt();
    int typeId = query.record().field("TypeId").value().toInt();
    bool typeNotDefined = false;
    switch (typeKind)
    {
      case DATA_TYPE_NUMBER:
      {
        tableName = "Numbers";
      }
      break;

      case DATA_TYPE_STRING:
      {
        tableName = "Strings";
      }
      break;

      case DATA_TYPE_DATETIME:
      {
        tableName = "DateTimes";
      }
      break;

      case DATA_TYPE_BOOLEAN:
      {
        tableName = "Logicals";
      }
      break;

      case DATA_TYPE_NUMERATOR:
      {
        tableName = "Numerators";
      }
      break;

      case DATA_TYPE_LINK:
      {
        tableName = "Links";
      }
      break;

      default:
        typeNotDefined = true;
      break;
    }

    //�������� ���� ���������
    if ( !typeNotDefined )
    {
      sql = QString("DELETE FROM Objects.%1 WHERE Id = :Id").arg(tableName);
      subQuery.prepare(sql);
      subQuery.bindValue(":Id", typeId);

      if (!subQuery.exec())
      {       
        return false;
      }

      //�������������� ��������
      if ( !fillIndex("Objects." + tableName) )
      {      
        return false;
      }
    }          

    qr = query.next();
  }

  //�������� ������ �� ������� Accessories
  sql = "DELETE FROM Objects.Accessories WHERE OwnerId = :OwnerId";//!!!���������� �������� ��������� � ���������� � ���� 
  query.prepare(sql);
  query.bindValue(":OwnerId", ownerId);

  if ( !query.exec() )
  {    
    return false;
  }

  if ( !fillIndex("Objects.Accessories") )
  {  
    return false;
  }

  return true;
}

bool DBControl::removeTableParts(int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql = "DELETE FROM Objects.Translates WHERE Id IN "
                "(SELECT TranslatesId FROM Objects.TableParts WHERE OwnerId = :OwnerId);\n"  
                "DELETE FROM Objects.TableParts WHERE OwnerId = :OwnerId;";
  query.prepare(sql);
  query.bindValue(":OwnerId", ownerId);

  if (!query.exec())
  {
    //currentDatabase.rollback();
    return false;
  }

  if (!fillIndex("Objects.TableParts"))
  {
    //currentDatabase.rollback();
    return false;
  }

  return true;
}

bool DBControl::removeForms(int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql = "DELETE FROM Objects.Translates WHERE Id IN "
                "(SELECT TranslatesId FROM Objects.Forms WHERE OwnerId = :OwnerId);\n"  
                "DELETE FROM Objects.Forms WHERE OwnerId = :OwnerId;";
  query.prepare(sql);
  query.bindValue(":OwnerId", ownerId);

  if (!query.exec())
  {
    //currentDatabase.rollback();
    return false;
  }

  if (!fillIndex("Objects.Forms"))
  {
    //currentDatabase.rollback();
    return false;
  }

  return true;
}

bool DBControl::removeScripts(int ownerId)
{
  QSqlQuery query(currentDatabase);
  QString sql = "DELETE FROM Objects.Translates WHERE Id IN "
                "(SELECT TranslatesId FROM Objects.Scripts WHERE OwnerId = :OwnerId);\n"  
                "DELETE FROM Objects.Scripts WHERE OwnerId = :OwnerId;";
  query.prepare(sql);
  query.bindValue(":OwnerId", ownerId);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  if ( !fillIndex("Objects.Scripts") )
  {
    lastError = query.lastError().text();
    return false;
  }

  return true;
}

bool DBControl::deleteContentForDocumentDocument(int docId)
{
  //�������� ���������
  if (!removeTranslates(docId))
  {    
    return false;
  }        

  //�������� ����������
  if (!removeAccessories(docId))
  {    
    return false;
  }       

  //�������� ��������� ������
  if (!removeTableParts(docId))
  {    
    return false;
  }

  //�������� ����
  if (!removeForms(docId))
  {    
    return false;
  }

  return true;
}

bool DBControl::deleteContentForDocumentDictionary(int docId)
{
  return deleteContentForDocumentDocument(docId);
}

bool DBControl::deleteContentForDocumentReport(int docId)
{
  return deleteContentForDocumentDocument(docId);
}

bool DBControl::deleteContentForDocumentDocumentsJournal(int docId)
{
  return removeAccessories(docId);  
}

bool DBControl::deleteContentForDocumentRegister(int docId)
{
  return removeAccessories(docId);
}

bool DBControl::deleteContentForDocumentEnumeration(int docId)
{
  QSqlQuery query(currentDatabase);
  QString(sql);

  //�������� ��������� ���������
  sql = "DELETE FROM Objects.Translates "
        "WHERE Id IN (SELECT TranslatesId FROM Objects.EnumData WHERE OwnerId = :OwnerId)";
  
  query.prepare(sql);
  query.bindValue(":OwnerId", docId);
  if (!query.exec())
  {
    return false;
  }

  //�������� ���������
  sql = "DELETE FROM Objects.EnumData "
        "WHERE OwnerId = :OwnerId";

  query.prepare(sql);
  query.bindValue(":OwnerId", docId);
  if (!query.exec())
  {
    return false;
  }
  
  return true;
}

bool DBControl::deleteContentForDocumentConstant(int docId)
{
  return removeAccessories(docId);
}

bool DBControl::deleteContentForDocumentForm(int docId)
{
  return removeForms(docId);
}

bool DBControl::deleteContentForDocumentTable(int docId)
{
  return removeAccessories(docId);
}

bool DBControl::deleteContentForDocumentQuery(int docId)
{
  return removeScripts(docId);
}

bool DBControl::deleteContentForDocumentClassModule(int docId)
{
  return removeScripts(docId);
}

bool DBControl::deleteContentForDocumentGlobalThread(int docId)
{
  return removeScripts(docId);
}

bool DBControl::deleteContentForDocumentGlobalContainer(int docId)
{
  return removeScripts(docId);
}

bool DBControl::deleteContentForDocumentTrigger(int docId)
{
  return true;
}

bool DBControl::deleteContentForDocumentTimeManager(int docId)
{
  return true;
}

bool DBControl::appendContentForDocument(Document *document)
{
  if ( !document ) return false; 

  //������������� ����� ��������� �����   
  int newTablePartId = getSequenceMaxValue("Objects.TableParts");
 
  //���������� �������������� ��������� ����� �� �������������� ��������  
  for ( int i = 0; i < document->getAccessories()->size(); i++ )
  {
    //�������� ����������� ��������� �����    
    if ( document->getAccessories()->value(i)->getTablePartId() > 0 )   
    {
      document->getAccessories()->value(i)->setTablePartId(document->getAccessories()->value(i)->getTablePartId() + newTablePartId);
    }    
  }

  //������ ����������      
  if ( !appendToDatabase(document->getAccessories(), document->getId()) ) return false; 

  //������ ��������� ������ ���������       
  if ( !appendToDatabase(document->getTableParts(), document->getId()) ) return false;
  
  //������ ���� ���������        
  if ( !appendToDatabase(document->getForms(), document->getId()) ) return false;

  return true;
}

bool DBControl::appendContentForDocument(Dictionary* dictionary)
{
  return appendContentForDocument((Document*)dictionary);
}

bool DBControl::appendContentForDocument(Report* report)
{
  return appendContentForDocument((Document*)report);
}

bool DBControl::appendContentForDocument(DocumentsJournal* documentsJournal)
{
  //������ ������ �� ���������
  /*QSqlQuery query(currentDatabase);
  QString sql;*/

  /*sql = "INSERT INTO Objects.Links(Flag, LinkedTableName, LinkedTableRecordId) "
        "VALUES (:Flag, :LinkedTableName, :LinkedTableRecordId)";*/

  if (documentsJournal == NULL)
  {
    return false;
  }

  QList<Link>* links = documentsJournal->transform(*documentsJournal->getRegisteredDocuments());

  QList<Accessory*> accList;
  Link* newLink = NULL;

  for (int i = 0; i < links->size(); i++)
  {
    Accessory *newAccessory = new Accessory();
    //newAccessory->setOwnerId(documentsJournal->getId());

    newLink = new Link();
    newLink->setFlag(links->value(i).getFlag());
    newLink->setLinkedTableName(links->value(i).getLinkedTableName());
    newLink->setLinkedTableRecordId(links->value(i).getLinkedTableRecordId());   

    newAccessory->setType(newLink);
    accList.append(newAccessory);
  }  

  appendToDatabase(&accList, documentsJournal->getId());

  accList.clear();

  QList<Link>* accLinks = documentsJournal->transform(*documentsJournal->getRegisteredAccessories());

  for (int i = 0; i < accLinks->size(); i++)
  {
    Accessory *newAccessory = new Accessory();    
    //newAccessory->setOwnerId(documentsJournal->getId());

    newLink = new Link();  

    newLink->setFlag(accLinks->value(i).getFlag());
    newLink->setLinkedTableName(accLinks->value(i).getLinkedTableName());
    newLink->setLinkedTableRecordId(accLinks->value(i).getLinkedTableRecordId());   

    newAccessory->setType(newLink);
    accList.append(newAccessory);
  }
  
  return appendToDatabase(&accList, documentsJournal->getId()); 
}

bool DBControl::appendContentForDocument(Register* docRegister)
{
  if (docRegister == NULL)
  {
    return false;
  }

  QList<Link>* links = docRegister->transform(*docRegister->getDimensions());

  QList<Accessory*> accList;
  Link* newLink = NULL;

  for (int i = 0; i < links->size(); i++)
  {
    Accessory *newAccessory = new Accessory();
    //newAccessory->setOwnerId(docRegister->getId());

    newLink = new Link();
    newLink->setFlag(links->value(i).getFlag());
    newLink->setLinkedTableName(links->value(i).getLinkedTableName());
    newLink->setLinkedTableRecordId(links->value(i).getLinkedTableRecordId());    

    newAccessory->setType(newLink);
    accList.append(newAccessory);
  }

  //���������� ���������
  appendToDatabase(&accList, docRegister->getId());  

  //������� ������ ����������
  accList.clear();

  //���������� ������ ��� ���������� ��������
  /*for (int i = 0; i < docRegister->getFacts()->size(); i++)
  {
    docRegister->getFacts()->value(i)->setOwnerId(docRegister->getId());
  }*/
  appendToDatabase(docRegister->getFacts(), docRegister->getId());  

  return true;  
}

bool DBControl::appendContentForDocument(Enumeration* enumeration)
{
  if (enumeration == NULL)
  {
    return false;
  }

  QSqlQuery query(currentDatabase);
  QString sql;

  //��������� ��������� ������������
  for (int i = 0; i < enumeration->getItems()->size(); i++)
  {
    int newTranslatesId = 0;
    if ( !appendToDatabase(enumeration->getItems()->value(i)->getTranslates(), newTranslatesId) )
    {      
      return false;
    }

    //���������� ������ � ������� EnumData
    sql = "INSERT INTO Objects.EnumData(TranslatesId, OwnerId) "
          "VALUES(:TranslatesId, :OwnerId)";

    query.prepare(sql);
    query.bindValue(":TanslatesId", newTranslatesId);
    query.bindValue(":OwnerId", enumeration->getId());

    if (!query.exec())
    {
      std::cout << query.lastError().text().toStdString() << std::endl;
      return false;
    }
  }

  return true;
}

bool DBControl::appendContentForDocument(Constant* constant)
{
  if (constant == NULL)
  {
    return false;
  }

  QList<Accessory*> accList;
  
  if (constant->getType() != NULL)
  {
    //���������� ���� ���������
    Accessory *newAccessory = new Accessory(); 
    newAccessory->setType(constant->getType());
    accList.append(newAccessory);
  }  

  return appendToDatabase(&accList, constant->getId()); 
}

bool DBControl::appendContentForDocument(DForm *document)
{
  if ( !document ) return false;  

  return appendToDatabase(document->getForms(), document->getId());  
}

bool DBControl::appendContentForDocument(DTable *document)
{
  if ( !document ) return false;  

  return appendToDatabase(document->getAccessories(), document->getId());  
}

bool DBControl::appendContentForDocument(DQuery *document)
{
  if ( !document ) return false;  

  return appendToDatabase(document->getScripts(), document->getId());
}

bool DBControl::appendContentForDocument(DClassModule *document)
{
  if ( !document ) return false;  

  return appendToDatabase(document->getScripts(), document->getId());
}

bool DBControl::appendContentForDocument(DGlobalThread *document)
{
  if ( !document ) return false;  

  return appendToDatabase(document->getScripts(), document->getId());
}

bool DBControl::appendContentForDocument(DGlobalContainer *document)
{
  if ( !document ) return false;

  return appendToDatabase(document->getScripts(), document->getId());
}

bool DBControl::appendContentForDocument(DTrigger *document)
{
  if ( !document ) return false;
  return true;
}

bool DBControl::appendContentForDocument(DTimeManager *document)
{
  if ( !document ) return false;
  return true;
}

bool DBControl::loadContentForBaseDocument(BaseDocument *document)
{
  QSqlQuery query(currentDatabase);
  QString sql;
  //��������� �� ������� Documents ��������� � ��������������� docId
  sql = "SELECT * FROM Objects.Documents WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", document->getId());  
  if ( !query.exec() ) 
  {
    lastError = query.lastError().text();
    return false; 
  }

  //���� ������� ������, �� �������� � ���� �� ���������
  if ( !query.first() ) return true; 

  document->setId(query.record().field("Id").value().toInt()); 
  document->setStatus(query.record().field("Status").value().toInt());
  document->setReleaseMark(query.record().field("ReleaseMark").value().toBool());
  document->setDeleteMark(query.record().field("DeleteMark").value().toBool());
  Script script(query.record().field("Script").value().toString());
  document->setScript(script);
  document->setFlags(query.record().field("Flags").value().toInt());
  int translatesId = query.record().field("TranslatesId").value().toInt();

  //�������� ��������� ���������
  if ( !takeFromDatabase(document->getTranslates(), translatesId) ) return false;

  //���������� ����� ��������� ���������
  /*for ( QMap<int, QString>::iterator i = document->getTranslates()->begin(); i != document->getTranslates()->end(); i++ )
  {
    std::cout << QString("bool DBControl::loadContentForBaseDocument(BaseDocument *document)\nkey=%1\nvalue=%2").arg(i.key()).arg(i.value()).toLocal8Bit().data() << std::endl;
  }*/

  return true;
}

bool DBControl::loadContentForDocument(Document *document)
{
  if ( !document ) return false; 

  //�������� ����������
  if ( !takeFromDatabase(document->getAccessories(), document->getId()) ) return false;
 
  //�������� ��������� ������
  if ( !takeFromDatabase(document->getTableParts(), document->getId()) ) return false;
  
  //�������� ����
  if ( !takeFromDatabase(document->getForms(), document->getId()) ) return false;

  return true;
}

bool DBControl::loadContentForDocument(Dictionary* dictionary)
{
  return loadContentForDocument((Document*)dictionary);
}

bool DBControl::loadContentForDocument(Report* report)
{
  return loadContentForDocument((Document*)report);
}

bool DBControl::loadContentForDocument(DocumentsJournal *document)
{
  if ( !document ) return false;    

  //�������� ����������
  QList<Accessory*> accList;

  if ( !takeFromDatabase(&accList, document->getId()) ) return false;
 
  //�������� ������������ ���������� �� �������
  QList<Link> documentLinkList;
  QList<Link> accessoryLinkList;
  //�������������� ���������� � ������ ������
  for (int i = 0; i < accList.size(); i++)
  {
    Link newLink;    
    if ( accList[i]->getType() != NULL )
    {
      if ( accList[i]->getType()->getTypeKind() == DATA_TYPE_LINK )
      {
        newLink.setFlag(((Link*)accList[i]->getType())->getFlag());
        newLink.setLinkedTableName(((Link*)accList[i]->getType())->getLinkedTableName());
        newLink.setLinkedTableRecordId(((Link*)accList[i]->getType())->getLinkedTableRecordId());

        if (newLink.getLinkedTableName().compare("Objects.Documents") == 0)
        {
          documentLinkList.append(newLink);
        }
        else
        {
          if (newLink.getLinkedTableName().compare("Objects.Accessories") == 0)
          {
            accessoryLinkList.append(newLink);
          }
        }
      }
    }
  }

  document->setRegisteredDocuments(*document->transform(documentLinkList));
  document->setRegisteredAccessories(*document->transform1(accessoryLinkList));

  return true;
}

bool DBControl::loadContentForDocument(Register *document)
{
  if ( !document ) return false;
 
  //�������� ���������� ��������
  QList<Accessory*> accList;

  if ( !takeFromDatabase(&accList, document->getId()) ) return false;

  QList<Link> dimensionLinkList;
  QList<Accessory*> factList;

  //������ ���������� ��������
  for (int i = 0; i < accList.size(); i++)
  {
    Link newLink;
    if ( accList[i]->getType() != NULL )
    {
      //������������� ������
      if ( accList[i]->getType()->getTypeKind() == DATA_TYPE_LINK )
      {
        newLink.setFlag(((Link*)accList[i]->getType())->getFlag());
        newLink.setLinkedTableName(((Link*)accList[i]->getType())->getLinkedTableName());
        newLink.setLinkedTableRecordId(((Link*)accList[i]->getType())->getLinkedTableRecordId());

        if (newLink.getLinkedTableName().compare("Objects.Documents") == 0)
        {
          dimensionLinkList.append(newLink);
        }        
      }
      //else
      //{
      //  //�������� ���������
      //  factList.append(accList[i]);
      //}
    }
    else
    {
      //�������� �����
      factList.append(accList[i]);
    }
  }  

  document->setDimensions(*document->transform(dimensionLinkList));
  document->setFacts(factList);

  return true;
}

bool DBControl::loadContentForDocument(Enumeration* document)
{
  if ( !document )return false;

  QSqlQuery query(currentDatabase);
  QString sql;
    
  //�������� ��������� ������������
  sql = "SELECT * FROM Objects.EnumData WHERE OwnerId = :OwnerId";
  query.prepare(sql);
  query.bindValue(":OwnerId", document->getId());
  if ( !query.exec() ) return false;
  
  //������� ��������� �������
  bool qr = query.first();  
  while ( qr )
  {
    EnumItem *newEnumItem = new EnumItem();
    if ( !takeFromDatabase(newEnumItem->getTranslates(), query.record().field("TranslatesId").value().toInt()) ) return false;
   
    document->getItems()->append(newEnumItem);
    qr = query.next();
  } 

  return true;
}

bool DBControl::loadContentForDocument(Constant* document)
{
  if ( !document ) return false;
   
  //�������� ����������
  QList<Accessory*> accList;
  if ( !takeFromDatabase(&accList, document->getId()) ) return false;  

  //��������� �� ����� ����� ����� ������ ���������
  //���� �������� �� ��������, �� ���������� ��� ��������� ���������� 
  //if ( (accList.size() > 1) || (accList.size() == 0) ) return false;
  //if ( accList.size() > 1 ) return false;

  if ( accList.size() )
  {
    document->setType(accList.value(0)->getType());
  }  
  
  return true;
}

bool DBControl::loadContentForDocument(DForm* document)
{
  if ( !document ) return false;

  //�������� ����
  if ( !takeFromDatabase(document->getForms(), document->getId()) ) return false;
 
  return true;
}

bool DBControl::loadContentForDocument(DTable *document)
{
  if ( !document ) return false;

  //�������� ����������
  if ( !takeFromDatabase(document->getAccessories(), document->getId()) ) return false;

  return true;
}

bool DBControl::loadContentForDocument(DQuery *document)
{
  if ( !document ) return false;

  //�������� ��������
  if ( !takeFromDatabase(document->getScripts(), document->getId()) ) return false;

  return true;
}

bool DBControl::loadContentForDocument(DClassModule *document)
{
  if ( !document ) return false;

  //�������� ��������
  if ( !takeFromDatabase(document->getScripts(), document->getId()) ) return false;

  return true;
}

bool DBControl::loadContentForDocument(DGlobalThread *document)
{
  if ( !document ) return false;

  //�������� ��������
  if ( !takeFromDatabase(document->getScripts(), document->getId()) ) return false;

  return true;
}

bool DBControl::loadContentForDocument(DGlobalContainer *document)
{
  if ( !document ) return false;

  //�������� ��������
  if ( !takeFromDatabase(document->getScripts(), document->getId()) ) return false;

  return true;
}

bool DBControl::loadContentForDocument(DTrigger *document)
{
  if ( !document ) return false;
  return true;
}

bool DBControl::loadContentForDocument(DTimeManager *document)
{
  if ( !document ) return false;
  return true;
}
