#include "matrix.h"
#include <new>

Matrix::Matrix(int x, int y)
{
  try
  {
    //������������ �������
    rowCount = x;
    colCount = y;
    data = new int[rowCount * colCount];
  }
  catch (std::bad_alloc xa)
  {
  }  
}

Matrix::Matrix(const Matrix& oldMatrix)
{
  //����������� �����������
  rowCount = oldMatrix.rowCount;
  colCount = oldMatrix.colCount;

  //��������� ������
  data = new int[rowCount * colCount];
  //����������� ������
  for (int i = 0; i < rowCount * colCount; i++)
  {
    data[i] = oldMatrix.data[i];
  }
}

Matrix::~Matrix()
{ 
  delete[] data;  
}

Matrix & Matrix::operator= (const Matrix &op)
{
  //this->

  return *this;
}

bool Matrix::setDimension(int x, int y)
{
  try
  {
    if (data == NULL)
    {
      rowCount = x;
      colCount = y;
      data = new int[rowCount * colCount];
      return true;
    }

    //���������� ����������� �� ���������� �����
    if (x > rowCount)
    {
      for (; rowCount < x; rowCount++)
      {
        int rn = addRow();
      }
      //rowCount = x;
    }
    else
    {
      for (; rowCount > x; rowCount--)
      {
        deleteRow(rowCount - 1);
      }     
    }

    //���������� ����������� �� ���������� ��������
    if (y > colCount)
    {
      for (; colCount < y; colCount++)
      {
        int cn = addColumn();
      }      
    }
    else
    {
      for (; colCount > y; colCount--)
      {
        deleteColumn(colCount - 1);
      }     
    }

    return true;
  }
  catch (...)
  {
    rowCount = 0;
    colCount = 0;
    data = NULL;
    return false;
  }  
}

int Matrix::addColumn()
{
  try
  {
    //���������� ����� �������
    colCount++;

    //���������� ����������� �������
    int memSize = rowCount * colCount;
    //��������� ������
    int* data_new = new int[memSize];

    //����������� ���������� �������
    for (int i = 0; i < rowCount; i++)
    {
      for (int j = 0; j < colCount - 1; j++)
      {
        data_new[i * colCount + j] = data[i * colCount + j];
      }
    }

    //������������ ������
    delete[] data;

    //��������� ������ ������ �������
    data = data_new;

    //������������ ������ ����������� ������� �������
    return colCount - 1;
  }
  catch (std::bad_alloc xa)
  {
    return -1;
  } 
}

bool Matrix::deleteColumn(int index)
{
  try
  {
    if (index < 0 || index >= colCount)
    {
      return false;
    }

    //���������� ����������� �������
    int memSize = rowCount * (colCount - 1);
    //��������� ������
    int* data_new = new int[memSize];

    //����������� ���������� �������
    int offset = 0;
    for (int i = 0; i < rowCount; i++)
    {
      for (int j = 0; j < colCount; j++)
      {
        if (j == index)
        {
          offset--;
        }
        else
        {
          data_new[i * colCount + j + offset] = data[i * colCount + j];
        }        
      }
    }

    colCount--;

    //������������ ������
    delete[] data;

    //��������� ������ ������ �������
    data = data_new;

    return true;
  }
  catch (std::bad_alloc xa)
  {
    return false;
  }  
}

int Matrix::addRow()
{
  try
  {
    //���������� ����� �����
    rowCount++;

    //���������� ����������� �������
    int memSize = rowCount * colCount;
    //��������� ������
    int* data_new = new int[memSize];

    //����������� ���������� �������
    for (int i = 0; i < rowCount - 1; i++)
    {
      for (int j = 0; j < colCount; j++)
      {
        data_new[i * colCount + j] = data[i * colCount + j];
      }
    }

    //������������ ������
    delete[] data;

    //��������� ������ ������ �������
    data = data_new;

    //������������ ������ ����������� ������ �������
    return rowCount - 1;
  }
  catch (std::bad_alloc xa)
  {
    return -1;
  }
}

bool Matrix::deleteRow(int index)
{
  try
  {
    if (index < 0 || index >= rowCount)
    {
      return false;
    }

    //���������� ����������� �������
    int memSize = (rowCount - 1) * colCount;
    //��������� ������
    int* data_new = new int[memSize];

    //����������� ���������� �������
    int offset = 0;
    for (int i = 0; i < rowCount; i++)
    {
      if (i == index)
      {
        offset--;
      }
      else
      {
        for (int j = 0; j < colCount; j++)
        {
          data_new[(i + offset) * colCount + j] = data[i * colCount + j];                 
        }
      }      
    }

    rowCount--;

    //������������ ������
    delete[] data;

    //��������� ������ ������ �������
    data = data_new;

    return true;
  }
  catch (std::bad_alloc xa)
  {
    return false;
  }  
}


void Matrix::setItem(int row, int col, int value)
{
  try
  {
    if (row > rowCount || col > colCount || row < 0 || col < 0)
      return;

    data[row * colCount + col] = value;
  }
  catch (...)
  {
  }  
}

int Matrix::getItem(int row, int col)
{
  try
  {
    if (row > rowCount || col > colCount || row < 0 || col < 0)
      return -1;

    return data[row * colCount + col];
  }
  catch (...)
  {
  }  
}

void Matrix::clear()
{
  delete[] data;
  data = NULL;
  rowCount = 0;
  colCount = 0;
}

int Matrix::getRowCount()
{
  return rowCount;
}

int Matrix::getColCount()
{
  return colCount;
}
