#include "userdatabasemanager.h"

#include <QFile>
#include <QTextStream>
#include <QApplication>
#include <QRegExp>
#include <QDateTime>
#include <QStringList>

#include "punycode.h"

#include <iostream>

#include<algorithm>

using namespace ConfiguratorObjects;

UserDatabaseManager::UserDatabaseManager()
{
}

UserDatabaseManager::~UserDatabaseManager()
{
}

void UserDatabaseManager::putData1(DataBlock &data, const QString &s1)
{
  QString fileName = QString("%1//%2").arg(QApplication::applicationDirPath()).arg("datamanager.out");
  QFile of(fileName);
  if ( !of.open(QIODevice::WriteOnly | QIODevice::Truncate) )
  {
    return;    
  }

  QTextStream os(&of);

  os << QString("Source query: %1\r\n").arg(s1);
  os << QString("Processing query: %1\r\n").arg(debugOut);

  for ( DataBlock::iterator i = data.begin(); i != data.end(); i++ )
  {
    for ( QList<QPair<QString, QVariant> >::iterator j = i.value().begin(); j != i.value().end(); j++ )
    {
      QString s = (*j).first;
      QVariant v = (*j).second;

      QString t;
      switch ( v.type() )
      {
        case QVariant::Int:
        {
          t.append(QString("%1").arg(v.toInt()));         
        }
        break;

        case QVariant::Double:
        {
          t.append(QString("%1").arg(v.toDouble()));         
        }
        break;

        case QVariant::String:
        {
          t.append(QString("'%1'").arg(v.toString()));         
        }
        break;

        case QVariant::DateTime:
        {
          t.append(QString("'%1'").arg(v.toDateTime().toString("yyyy.MM.dd hh:mm:ss")));
        }
        break;

        case QVariant::Bool:
        {         
          if ( v.toBool() )
          {
            t.append(QString("%1").arg("TRUE"));
          }
          else
          {
            t.append(QString("%1").arg("FALSE"));
          }
        }
        break;

        default:;
      }

      os << QString("Field info: name %1 value %2\r\n").arg(s).arg(t);
      //of.write(.toLocal8Bit());
    }
  }

  of.close();
}

void UserDatabaseManager::putData(const QByteArray &data, const QString &s1)
{
  DataBlock db;
  QByteArray ba = data;
  QDataStream out(&ba, QIODevice::ReadOnly);

  inData(out, db);
  putData1(db, s1);
}

void UserDatabaseManager::selfTest()
{  
  /*std::string str = "s:t:r:i:n:g";
  const char symbol = ':';
  str.erase(remove(str.begin(), str.end(), symbol),str.end());

  std::cout << str << std::endl;
  return;*/

  /*const QString exp = "f1+((f1-f2)*f3)/(f1+f3)";
  QString sout = parseExpression(exp);*/
  //std::cout << sout.toLocal8Bit().data() << std::endl;

  /*const QString exps = "f1 + ((f1 - f2) * f3) / (f1 + f3)";
  sout = parseExpression(exps);*/
  //std::cout << sout.toLocal8Bit().data() << std::endl;

  //return;

 /* const QString s = "SELECT accessory1, documents.document1.accessory1 FROM documents.document1, table1";
  QDataStream ds;
  exec(s, ds);*/

  /*return;*/

  ConnectionInfo cInfo;
  cInfo.host = "127.0.0.1";
  cInfo.dbName = "udb1";
  cInfo.userName = "root";
  cInfo.password = "11111";

  if ( !connect(cInfo) )
  {    
    std::cout << currentDatabase.lastError().text().toLocal8Bit().data() << std::endl;
    return;
  } 

  QString sql;
  QByteArray ba0;
  QDataStream out0(&ba0, QIODevice::WriteOnly);

  sql = "SELECT Accessory1, Documents.Document1.Accessory1 FROM Documents.Document1, table1";
  //exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  //sql = "SELECT Documents.Document1.Accessory1, Accessory2 FROM Documents.Document1, Documents.Document2";
  sql = "SELECT Documents.Document1.Accessory1, Documents.Document2.Accessory2, Accessory3, Column1 FROM Documents.Document1, Documents.Document2, Table1";
  //sql = "SELECT Accessory1, Accessory2 FROM Documents.Document1, Documents.Document2";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  return;

  //sql = QString("SELECT Column1, Column2 FROM Table1 WHERE TRUE");
  sql = "SELECT ���������.Document1.Accessory1, Documents.Document1.Accessory2, Documents.Document1.Accessory4, Documents.Document1.Accessory3 "
        "FROM Documents.Document1 "
        "where Documents.Document1.Accessory1 / Documents.Document1.Accessory2 = 10 "
        "OR Documents.Document1.Accessory2 - Documents.Document1.Accessory1 = 5 "
        "AND Documents.Document1.Accessory1 * Documents.Document1.Accessory2 = 10 "
        "OR Documents.Document1.Accessory2 + Documents.Document1.Accessory1 = 5";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "SELECT Documents.Document1.TablePart1.Accessory1, Documents.Document1.TablePart1.Accessory2 "
        "FROM Documents.Document1.TablePart1 "
        "WHERE Documents.Document1.TablePart1.Accessory1/Documents.Document1.TablePart1.Accessory2 = 10 "
        "OR Documents.Document1.TablePart1.Accessory2-Documents.Document1.TablePart1.Accessory1= 5 "
        "AND Documents.Document1.TablePart1.Accessory1*Documents.Document1.TablePart1.Accessory2 = 10 "
        "OR Documents.Document1.TablePart1.Accessory2+Documents.Document1.TablePart1.Accessory1 = 5";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  //sql = QString("SELECT Document1.Accessory1, Document1.Accessory2 FROM Document1 WHERE Document1.Accessory1/Document1.Accessory2 = 10 OR Document1.Accessory2 = 5");
  //exec(sql, out0);
  //std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "SELECT Accessory1, Accessory2 FROM Document1 "
        "WHERE (Accessory1/Accessory2 = 10 "
        "OR (Accessory2-Accessory1 > 5 "
        "AND Accessory1*Accessory2 < 10) "
        "OR Accessory2+Accessory1 <> 5)";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  //sql = QString("SELECT Documents.Document1.TablePart1.Accessory1 FROM Documents.Document1.TablePart1 WHERE Documents.Document1.TablePart1.Accessory1=1");
  //sql = "UPDATE Documents.Document1.TablePart1 SET (Documents.Document1.TablePart1.Accessory1, Documents.Document1.TablePart1.Accessory2) = (1, 2)";
  //sql = "UPDATE Documents.Document1.TablePart1 SET (Accessory1, Accessory2) = (1, 2) WHERE Documents.Document1.TablePart1.Accessory1=1";
  //sql = "UPDATE Table1 SET (Column1, Column2) = (1, 2) WHERE Column1 = 1";
  //sql = "UPDATE Documents.Document1.TablePart1 SET (Accessory1, Accessory2) = (1, 2) WHERE Accessory1 = 1 AND Accessory2 = 2";
  
  sql = "UPDATE Documents.Document1.TablePart1 SET (Accessory1, Accessory2) = (1, 2) WHERE Accessory1 = 1 AND Accessory2 = 2";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "UPDATE Documents.Document1 SET (Accessory1, Accessory2) = (1, 2) WHERE Accessory1 = 1 AND Accessory2 = 2";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "UPDATE Document1 SET (Accessory1, Accessory2) = (1, 2) WHERE Accessory1 = 1 AND Accessory2 = 2";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "INSERT INTO Document1(Accessory1, Accessory2) VALUES (1, 2), (3, 4)";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "INSERT INTO Documents.Document1(Accessory1, Accessory2) VALUES (1, 2)";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "INSERT INTO Documents.Document1.TablePart1 (Accessory1, Accessory2) VALUES (1, 2)";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "DELETE FROM Document1 WHERE Accessory1 = 0";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "DELETE FROM Document1";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

  sql = "DELETE FROM Documents.Document1 WHERE Accessory1 = 0";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;  

  sql = "INSERT INTO Document1(Accessory1, Accessory2) VALUES (1, 2), (3, 4)";
  exec(sql, out0);
  std::cout << "-------------------------------------------------------------------------------" << std::endl;

 
  return;

  const QString docTableName = "Document1-";
  const int docId = 1;
  const QString acessoryName = "Accessory1-";
  const QString tpTableName  = "TablePart1-";

  bool br;
  //�������� ���������� �����
  br = checkPath(QString("%1.*").arg(docTableName));
  br = checkPath(QString("%1.%2").arg(docTableName).arg(acessoryName));
  br = checkPath(QString("%1.%2.*").arg(docTableName).arg(QString("%1%2").arg(docTableName).arg(tpTableName)));
  br = checkPath(QString("%1.%2.%3").arg(docTableName).arg(QString("%1%2").arg(docTableName).arg(tpTableName)).arg(acessoryName));

  //�������� ������������ �����
  br = checkPath(QString("%1_invalid.*").arg(docTableName));
  br = checkPath(QString("%1.%2_invalid").arg(docTableName).arg(acessoryName));
  br = checkPath(QString("%1.%2_invalid.*").arg(docTableName).arg(QString("%1%2").arg(docTableName).arg(tpTableName)));
  br = checkPath(QString("%1.%2.%3_invalid").arg(docTableName).arg(QString("%1%2").arg(docTableName).arg(tpTableName)).arg(acessoryName));

  //exec(QString("SELECT * FROM \"%1\"").arg(docTableName), ds);
  //QMap<int, QList<QPair<QString, QVariant> > >

  QSqlQuery query(currentDatabase);
  sql = QString("SELECT * FROM \"%1\" ORDER BY \"Id\"").arg(docTableName);

  query.prepare(sql);

  if ( !query.exec() )
  {
    std::cout << query.lastError().text().toLocal8Bit().data() << std::endl;
    return;
  }

  if ( !query.first() )
  {
    std::cout << "Query result is empty !!!" << std::endl;
    return;
  }

  QByteArray ba;
  QDataStream out(&ba, QIODevice::WriteOnly);
  outData(query, out);

  DataBlock data;
  QDataStream in(&ba, QIODevice::ReadOnly);
  inData(in, data);

  //putData(data);

  //������������ ���������� ������� � ���������
  int r = 0;
  QByteArray ba1;
  QDataStream out1(&ba1, QIODevice::WriteOnly);
  r = read(docTableName, docId, out1);

  //r = erase(docTableName, docId);

  QDataStream in1(&ba1, QIODevice::ReadOnly);
  r = write(docTableName, docId, in1);  
}

QString UserDatabaseManager::parsePath(const QString &path, const bool flag, const int k)
{
  if ( !flag )
  {
    return path;
  }

  //����: Documents.Table.Field[, ...]
  QStringList pathObjects = path.split(QChar('.'), QString::SkipEmptyParts);

  if ( !pathObjects.isEmpty() )
  {
    if ( pathObjects.value(0).toUpper().compare("DOCUMENTS") == 0 ||
         pathObjects.value(0).toUpper().compare("���������") == 0 )
    {
      pathObjects.removeFirst();

      int startPos = 0;

      //��������� ��������� � �� ���������   
      if ( pathObjects.size() == k )
      {
        PunyCode pc;
        QString documentName  = pc.encode(pathObjects.value(0));
        QString tablePartName = pc.encode(pathObjects.value(1));

        QString tableName = QString("\"%1\"").arg(documentName.append(tablePartName));//QString("%1%2").arg(pc.encode(documentName)).arg(pc.encode(tablePartName));

        //���������� ������ ���� ��������� ����
        pathObjects.removeFirst();
        pathObjects.removeFirst();

        pathObjects.insert(0, tableName);
        
        startPos = 1;
      }

      for ( int pathObjectLexemCounter = startPos; pathObjectLexemCounter < pathObjects.size(); pathObjectLexemCounter++ )
      {                
        QString lexem = pathObjects.value(pathObjectLexemCounter);

        PunyCode pc;
        QString systemName = QString("\"%1\"").arg(pc.encode(lexem));//��������� punycode-��� ��������
        pathObjects.replace(pathObjectLexemCounter, systemName);        
      }
    }
    else
    {      
      PunyCode pc;
      return QString("\"%1\"").arg(pc.encode(pathObjects.value(0)));
    }
  } 

  return pathObjects.join(".");
}

QString UserDatabaseManager::parsePathObjects(const QStringList &lexems, const ParseKind kind, const bool flag, const int k)
{
  if ( lexems.isEmpty() )
  {
    return QString();
  }

  for ( int lexemCounter = 0; lexemCounter < lexems.count(); lexemCounter++ )
  {
    QStringList subLexems = lexems.value(lexemCounter).split(QRegExp("[\\s+,]"), QString::SkipEmptyParts);

    for ( int selectSubLexemCounter = 0; selectSubLexemCounter < subLexems.size(); selectSubLexemCounter++ )
    {
      QString lexem = subLexems.value(selectSubLexemCounter);    

      if ( (kind == SetClause) && flag )
      {
        //���������� �������� � ������ ��������� � ������� ���������
        lexem.insert(0, QString("DOCUMENTS."));      
      }
     
      subLexems.replace(selectSubLexemCounter, parsePath(lexem, flag, k));
    }    
   
    return subLexems.join(",");    
  }

  return QString();
}

QString UserDatabaseManager::parseExpression(const QString &exp, const bool flag)
{  
  QString lexem;
  QString outLexem;  
  int c = 0;
  for ( int i = 0; i < exp.size(); i++ )
  {
    QChar token = exp[i];

    if ( token.isSpace() )
    {
      continue;
    }

    if ( token == QChar('(') )
    {
      if ( c )
      {
        lexem.append(token);       
      }
      else
      {
        outLexem.append(token);
      }
     
      c++;     
    }
    else
    {
      if ( token == QChar(')') )
      {
        c--;

        if ( c == 0 )
        {
          if ( !lexem.isEmpty() )
          {
            QString newLexem = parseExpression(lexem, flag);
            outLexem.append(newLexem);
            lexem.clear();           

            outLexem.append(token);
          }
        }
        else
        {
          lexem.append(token);
        }     
      }
      else
      {
        if ( token == QChar('*') )
        {          
          if ( c )
          {
            lexem.append(token);
          }
          else
          {
            if ( !lexem.isEmpty() )
            {
              QString newLexem = parsePath(lexem, flag);
              outLexem.append(newLexem);

              lexem.clear();                
            }

            lexem.clear();
            
            outLexem.append(token);
          }
        }
        else
        {
          if ( token == QChar('/') )
          {            
            if ( c )
            {
              lexem.append(token);
            }
            else
            {
              if ( !lexem.isEmpty() )
              {
                QString newLexem = parsePath(lexem, flag);
                outLexem.append(newLexem);

                lexem.clear();                
              }
              
              outLexem.append(token);              
            }
          }
          else
          {
            if ( token == QChar('+') )
            {              
              if ( c )
              {
                lexem.append(token);
              }
              else
              {
                if ( !lexem.isEmpty() )
                {
                  /*if ( lexem.indexOf(QChar('.')) != -1 )
                  {*/
                    QString newLexem = parsePath(lexem, flag);
                    outLexem.append(newLexem);
                  /*}*/

                  lexem.clear();   
                }

                outLexem.append(token);
              }
            }
            else
            {
              if ( token == QChar('-') )
              {               
                if ( c )
                {
                  lexem.append(token);
                }
                else
                {
                  if ( !lexem.isEmpty() )
                  {
                    QString newLexem = parsePath(lexem, flag);
                    outLexem.append(newLexem);

                    lexem.clear();                
                  }
                  
                  outLexem.append(token);
                }
              }
              else
              {
                lexem.append(token);
              }
            }
          }
        }
      }
    }
  }

  if ( !lexem.isEmpty() )
  {
    QString newLexem = parsePath(lexem, flag);
    outLexem.append(newLexem);
  }

  return outLexem;
}

void UserDatabaseManager::parseWhereLexem(QStringList &lexems, const bool flag)
{
  if ( lexems.isEmpty() )
  {
    return;
  }

  for ( int lexemCounter = 0; lexemCounter < lexems.count(); lexemCounter++ )
  { 
    QString lexem = lexems.value(lexemCounter);

    if ( lexem.indexOf(QChar('=')) != -1 )
    {
      //����������� ��������� ����� �� �������
      int count = lexem.indexOf(QChar('='));
      QString leftLexem = lexem.left(count);

      if ( !leftLexem.isEmpty() )
      {
        //��������� ������ ����� ������ ����� ������� =
        count = lexem.size() - lexem.indexOf(QChar('='));
        QString rightLexem = lexem.right(count);

        if ( leftLexem.indexOf(QChar('*')) != -1 ||
             leftLexem.indexOf(QChar('/')) != -1 ||
             leftLexem.indexOf(QChar('+')) != -1 ||
             leftLexem.indexOf(QChar('-')) != -1 )
        {
          leftLexem = parseExpression(leftLexem, flag);
        }
        else
        {
          leftLexem = parsePath(leftLexem, flag);
        }

        QString newLexem = leftLexem.append(rightLexem);      
        lexems.replace(lexemCounter, newLexem);
      }
    }
    else
    {     
      if ( lexem.indexOf(QString("<>")) != -1 )
      {
        //����������� ��������� ����� �� �������
        int count = lexem.indexOf(QString("<>"));
        QString leftLexem = lexem.left(count);

        if ( !leftLexem.isEmpty() )
        {
          //��������� ������ ����� ������ ����� ������� =
          count = lexem.size() - lexem.indexOf(QString("<>"));
          QString rightLexem = lexem.right(count);

          if ( leftLexem.indexOf(QChar('*')) != -1 ||
               leftLexem.indexOf(QChar('/')) != -1 ||
               leftLexem.indexOf(QChar('+')) != -1 ||
               leftLexem.indexOf(QChar('-')) != -1 )
          {
            leftLexem = parseExpression(leftLexem, flag);
          }
          else
          {
            leftLexem = parsePath(leftLexem, flag);
          }

          QString newLexem = leftLexem.append(rightLexem);      
          lexems.replace(lexemCounter, newLexem);
        }
      }
      else
      {
        if ( lexem.indexOf(QChar('<')) != -1 )          
        {
          //����������� ��������� ����� �� �������
          int count = lexem.indexOf(QChar('<'));
          QString leftLexem = lexem.left(count);

          if ( !leftLexem.isEmpty() )
          {
            //��������� ������ ����� ������ ����� ������� =
            count = lexem.size() - lexem.indexOf(QChar('<'));
            QString rightLexem = lexem.right(count);

            if ( leftLexem.indexOf(QChar('*')) != -1 ||
                 leftLexem.indexOf(QChar('/')) != -1 ||
                 leftLexem.indexOf(QChar('+')) != -1 ||
                 leftLexem.indexOf(QChar('-')) != -1 )
            {
              leftLexem = parseExpression(leftLexem, flag);
            }
            else
            {
              leftLexem = parsePath(leftLexem, flag);
            }

            QString newLexem = leftLexem.append(rightLexem);      
            lexems.replace(lexemCounter, newLexem);
          }
        }
        else
        {
          if ( lexem.indexOf(QChar('>')) != -1 )
          {
            //����������� ��������� ����� �� �������
            int count = lexem.indexOf(QChar('>'));
            QString leftLexem = lexem.left(count);

            if ( !leftLexem.isEmpty() )
            {
              //��������� ������ ����� ������ ����� ������� =
              count = lexem.size() - lexem.indexOf(QChar('>'));
              QString rightLexem = lexem.right(count);

              if ( leftLexem.indexOf(QChar('*')) != -1 ||
                   leftLexem.indexOf(QChar('/')) != -1 ||
                   leftLexem.indexOf(QChar('+')) != -1 ||
                   leftLexem.indexOf(QChar('-')) != -1 )
              {
                leftLexem = parseExpression(leftLexem, flag);
              }
              else
              {
                leftLexem = parsePath(leftLexem, flag);
              }

              QString newLexem = leftLexem.append(rightLexem);      
              lexems.replace(lexemCounter, newLexem);
            }
          }
          else
          {
            if ( lexem.indexOf(QChar('*')) != -1 )
            {
              //����������� ��������� ����� �� �������
              int count = lexem.indexOf(QChar('*'));
              QString leftLexem = lexem.left(count).trimmed();
              //��������� ������ ����� ������ ����� �������
              count = lexem.size() - lexem.indexOf(QChar('*')) - 1;
              QString rightLexem = lexem.right(count).trimmed();

              //��������� ��������� ��������� ����
              QString newLexem = QString("%1%2%3").arg(parsePath(leftLexem, flag)).arg(QChar('*')).arg(parsePath(rightLexem, flag));

              lexems.replace(lexemCounter, newLexem);
            }
            else
            {
              if ( lexem.indexOf(QChar('/')) != -1 )
              {
                //����������� ��������� ����� �� �������
                int count = lexem.indexOf(QChar('/'));
                QString leftLexem = lexem.left(count).trimmed();
                //��������� ������ ����� ������ ����� �������
                count = lexem.size() - lexem.indexOf(QChar('/')) - 1;
                QString rightLexem = lexem.right(count).trimmed();

                //��������� ��������� ��������� ����
                QString newLexem = QString("%1%2%3").arg(parsePath(leftLexem, flag)).arg(QChar('/')).arg(parsePath(rightLexem, flag));

                lexems.replace(lexemCounter, newLexem);
              }
              else
              {
                if ( lexem.indexOf(QChar('+')) != -1 )
                {
                  //����������� ��������� ����� �� �������
                  int count = lexem.indexOf(QChar('+'));
                  QString leftLexem = lexem.left(count).trimmed();
                  //��������� ������ ����� ������ ����� �������
                  count = lexem.size() - lexem.indexOf(QChar('+')) - 1;
                  QString rightLexem = lexem.right(count).trimmed();

                  //��������� ��������� ��������� ����
                  QString newLexem = QString("%1%2%3").arg(parsePath(leftLexem, flag)).arg(QChar('+')).arg(parsePath(rightLexem, flag));

                  lexems.replace(lexemCounter, newLexem);
                }
                else
                {
                  if ( lexem.indexOf(QChar('-')) != -1 )
                  {
                    //����������� ��������� ����� �� �������
                    int count = lexem.indexOf(QChar('-'));
                    QString leftLexem = lexem.left(count).trimmed();
                    //��������� ������ ����� ������ ����� �������
                    count = lexem.size() - lexem.indexOf(QChar('-')) - 1;
                    QString rightLexem = lexem.right(count).trimmed();

                    //��������� ��������� ��������� ����
                    QString newLexem = QString("%1%2%3").arg(parsePath(leftLexem, flag)).arg(QChar('-')).arg(parsePath(rightLexem, flag));

                    lexems.replace(lexemCounter, newLexem);
                  }
                  else
                  {
                    if ( lexem.indexOf(QChar('.')) != -1 )
                    {
                      //��������� ��������� ��������� ����
                      QString newLexem = parsePath(lexem, flag);
                      lexems.replace(lexemCounter, newLexem);
                    }
                    else
                    {
                      if ( !(lexem.toUpper().compare(QString("AND")) == 0 ||
                             lexem.toUpper().compare(QString("OR"))  == 0 ||
                             lexem.toUpper().compare(QString("NOT")) == 0) )
                      {
                        QString newLexem = parsePath(lexem, flag);
                        lexems.replace(lexemCounter, newLexem);
                      }  
                    }                                      
                  }
                }
              }              
            }
          }
        }
      }   
    }   
  }
}

void UserDatabaseManager::parseFromLexem(QStringList &fromLexems, QStringList &selectLexems)
{
  //
  QString lexem = fromLexems.value(0);
  QStringList subLexems = lexem.split(QRegExp("[\\s+,]"), QString::SkipEmptyParts);

  QMap<QString, QString> associations;

  for ( int i = 0; i < subLexems.size(); i++ )
  {
    lexem = subLexems.value(i);   
    
    bool flag = false;    
    if ( lexem.toUpper().startsWith("DOCUMENTS") ||
         lexem.toUpper().startsWith("���������") )
    {
      flag = true;

      QStringList pathObjects = lexem.split(QChar('.'), QString::SkipEmptyParts);

      if ( pathObjects.size() >= 2 )
      {
        QString documentName = pathObjects.value(1);

        if ( currentDatabase.isOpen() )
        {
          QSqlQuery query(currentDatabase);

          PunyCode pc;
          QString sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(pc.encode(documentName));

          query.prepare(sql);

          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return;
          }

          if ( !query.first() )
          {           
            return;
          }

          int ownerId = query.record().field("Id").value().toInt();

          QString tablePartName;
          if ( pathObjects.size() == 3 )//Documents.Document.TablePart
          {            
            sql = QString("SELECT Id, SystemName FROM SysInfo WHERE ParentId = %1 AND Kind = 'TablePart'").arg(ownerId);

            query.prepare(sql);

            if ( !query.exec() )
            {
              lastError = query.lastError().text();
              return;
            }

            if ( !query.first() )
            {             
              return;
            }

            ownerId = query.record().field("Id").value().toInt();
            tablePartName = query.record().field("SystemName").value().toString();
          }

          sql = QString("SELECT Id, SystemName FROM SysInfo WHERE ParentId = %1 AND Kind = 'Accessory'").arg(ownerId);

          query.prepare(sql);

          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return;
          }

          //��������� ���������� �������
          QStringList accessories;
          bool qr = query.first();
          while ( qr )
          {
            accessories.append(query.record().field("SystemName").value().toString());
            qr = query.next();
          }

          //��������� ��������� select � ���������� �������
          lexem = selectLexems.value(0);
          QStringList selectSubLexems = lexem.split(QRegExp("[\\s+,]"), QString::SkipEmptyParts);
          for ( int j = 0; j < selectSubLexems.size(); j++ )
          {
            //���� ���������
            //- Table.Column
            //- "Table"."Column"
            //- Column
            //- "Column"
            //- Documents.Document.Accessory
            //- Documents.Document.TablePart.Accessory

            QString subLexem = selectSubLexems.value(j);

            QString ownerName(documentName);
            if ( !tablePartName.isEmpty() )
            {
              ownerName.append(tablePartName);
            }

            if ( associations.contains(subLexem) )
            {
              QString s = associations.value(subLexem);

              bool flag = false;    
              if ( subLexem.toUpper().startsWith("DOCUMENTS") ||
                   subLexem.toUpper().startsWith("���������") )
              {
                flag = true;
                //��������� ��������� �� ����
                //QStringList pathObjects = subLexem.split(QChar('.'), QString::SkipEmptyParts);
                //subLexem = pathObjects.value(pathObjects.size() - 1);
              }
              else
              {
                if ( s.compare(ownerName) != 0 )
                {
                  //�������� ��������������� !!!
                  qDebug(QString("Ambiguous column %1").arg(subLexem).toLocal8Bit().data());                
                }
              }              
            }
            else
            {
              associations.insert(subLexem, ownerName);           
            }
          }
        }
      }
    }
  }

  qDebug(selectLexems.value(0).toLocal8Bit().data());
  qDebug(fromLexems.value(0).toLocal8Bit().data());

  //���������� ��������������� �������
  bool flag = true;//???
  fromLexems.replace(0, parsePathObjects(fromLexems, FromClause, flag, 2));

  lexem = selectLexems.value(0);
  subLexems = lexem.split(QRegExp("[\\s+,]"), QString::SkipEmptyParts);

  PunyCode pc;
  for ( int i = 0; i < subLexems.size(); i++ )
  {
    if ( associations.contains(subLexems.value(i)) )
    {
      bool flag = false;    
      if ( subLexems.value(i).toUpper().startsWith("DOCUMENTS") ||
           subLexems.value(i).toUpper().startsWith("���������") )
      {
        flag = true;
      }

      //QString name = QString("\"%1\"").arg(pc.encode(parsePath(subLexems.value(i), flag)));
      QString name = parsePath(subLexems.value(i), flag);

      subLexems.replace(i, name);
    }
  }

  QString newLexem(subLexems.join(","));

  selectLexems.replace(0, newLexem);
}

void UserDatabaseManager::parseSelectLexem(QStringList &lexems, const int currentIndex)
{
  QStringList selectLexems;
  //������ ������ ����������� SELECT
  bool stopProcessing = false;
  for ( int j = currentIndex + 1; (j < lexems.size()) && !stopProcessing; j++ )
  {       
    QString lexem = lexems.value(j);

    if ( lexem.toUpper().compare("FROM") == 0 )
    {
      //��������� ������ ����������� SELECT
      bool flag = false;    
      if ( selectLexems.value(0).toUpper().startsWith("DOCUMENTS") ||
           selectLexems.value(0).toUpper().startsWith("���������") )
      {
        flag = true;
      }

      //QString newSelectArguments = parsePathObjects(selectLexems, SelectClause, flag);

      //������ �������
      //lexems.replace(j - 1, newSelectArguments);

      //������ ������ ����������� FROM
      QStringList fromLexems;
      for ( int k = j + 1; (k < lexems.size()) && !stopProcessing; k++ )
      {
        lexem = lexems.value(k);

        if ( lexem.toUpper().compare("WHERE") == 0 || (k + 1) == lexems.size() )
        {
          //��������� ������ ����������� FROM
          if ( (k + 1) == lexems.size() )
          {
            fromLexems.append(lexem);
          }

          parseFromLexem(fromLexems, selectLexems);

          QString newSelectArguments = selectLexems.value(0);

          //������ �������
          lexems.replace(j - 1, newSelectArguments);

          QString newFromArguments = fromLexems.value(0);//parsePathObjects(fromLexems, FromClause, flag, 2);         

          //������ �������        
          if ( (k + 1) == lexems.size() )
          {
            lexems.replace(k, newFromArguments);
          }
          else
          {
            lexems.replace(k - 1 , newFromArguments);
          }                  

          //��������� ������ ����������� WHERE
          QStringList whereLexems;
          for ( int l = k + 1; l < lexems.size(); l++ )
          {
            lexem = lexems.value(l);

            whereLexems.append(lexem);

            //std::cout << lexem.toLocal8Bit().data() << std::endl;
          }     

          //�������� ������ � ������ ���������� ����������� WHERE
          for ( int c = lexems.count() - 1; c >= k + 1; c-- )
          {          
            lexems.removeLast();
          }

          parseWhereLexem(whereLexems, flag);           
         
          lexems.append(whereLexems.join(" "));

          //���������� ��������� !!!
          stopProcessing = true;
        }
        else
        {
          fromLexems.append(lexem);
        }
      }
    }
    else
    {          
      selectLexems.append(lexem);
    }
  }
}

void UserDatabaseManager::parseSetLexem(QStringList &lexems, const bool flag)
{
  if ( lexems.isEmpty() )
  {
    return;
  }

  for ( int lexemCounter = 0; lexemCounter < lexems.count(); lexemCounter++ )
  {    
    QString lexem = lexems.value(lexemCounter);

    if ( lexem.indexOf(QChar('=')) != -1 )
    {
      //����������� ��������� ����� �� �������
      int count = lexem.indexOf(QChar('='));
      QString leftLexem = lexem.left(count);

      if ( !leftLexem.isEmpty() )
      {
        //��������� ������ ����� ������ ����� ������� =
        count = lexem.size() - lexem.indexOf(QChar('='));
        QString rightLexem = lexem.right(count);

        leftLexem = leftLexem.trimmed();

        if ( leftLexem.startsWith(QChar('(')) && leftLexem.endsWith(QChar(')')) )
        {
          leftLexem.remove(0, 1);
          leftLexem.remove(leftLexem.size() - 1, 1);
        
          QStringList list(leftLexem);    
          QString newLexem = parsePathObjects(list, SetClause, flag);
          newLexem.insert(0, QChar('('));
          newLexem.append(QChar(')'));
          newLexem.append(rightLexem);

          lexems.replace(lexemCounter, newLexem);  
        }
      }
    }
  }
}

void UserDatabaseManager::parseUpdateLexem(QStringList &lexems, const int currentIndex)
{
  QStringList updateLexems;
  //������ ������
  bool stopProcessing = false;
  for ( int j = currentIndex + 1; (j < lexems.size()) && !stopProcessing; j++ )
  {       
    QString lexem = lexems.value(j);

    if ( lexem.toUpper().compare("SET") == 0 )
    {
      //��������� ������ ����������� UPDATE    
      bool flag = false;    
      if ( updateLexems.value(0).toUpper().startsWith("DOCUMENTS") ||
           updateLexems.value(0).toUpper().startsWith("���������") )
      {
        flag = true;
      }

      QString newUpdateArguments = parsePathObjects(updateLexems, UpdateClause, flag, 2);

      //������ �������
      lexems.replace(j - 1, newUpdateArguments);

      //������ ������ ����������� SET
      QStringList setLexems;
      for ( int k = j + 1; (k < lexems.size()) && !stopProcessing; k++ )
      {
        lexem = lexems.value(k);

        if ( lexem.toUpper().compare("WHERE") == 0 || (k + 1) == lexems.size() )
        {
          //��������� ������ ����������� SET
          if ( (k + 1) == lexems.size() )
          {
            setLexems.append(lexem);
          }

          parseSetLexem(setLexems, flag);
          QString newSetArguments = setLexems.join(" ");

          if ( (k + 1) == lexems.size() )
          {
            lexems.replace(k, newSetArguments);
          }
          else
          {
            lexems.replace(k - 1, newSetArguments); 
          }

          //��������� ������ ����������� WHERE
          QStringList whereLexems;
          for ( int l = k + 1; l < lexems.size(); l++ )
          {
            lexem = lexems.value(l);

            whereLexems.append(lexem);
          }     

          //�������� ������ � ������ ���������� ����������� WHERE
          for ( int c = lexems.count() - 1; c >= k + 1; c-- )
          {          
            lexems.removeLast();
          }

          parseWhereLexem(whereLexems, flag);
          
          lexems.append(whereLexems.join(" "));

          //���������� ��������� !!!
          stopProcessing = true;
        }
        else
        {
          setLexems.append(lexem);
        }
      }
    }
    else
    {          
      updateLexems.append(lexem);
    }
  }
}

void UserDatabaseManager::parseIntoLexem(QStringList &lexems, const bool flag)
{
  if ( lexems.isEmpty() )
  {
    return;
  }

  for ( int lexemCounter = 0; lexemCounter < lexems.count(); lexemCounter++ )
  {    
    QString lexem = lexems.value(lexemCounter);

    if ( lexem.startsWith(QChar('(')) && lexem.endsWith(QChar(')')) )
    {
      lexem.remove(0, 1);
      lexem.remove(lexem.size() - 1, 1);

      QStringList list(lexem);    
      QString newLexem = parsePathObjects(list, InsertClause, flag);
      newLexem.insert(0, QChar('('));
      newLexem.append(QChar(')'));

      lexems.replace(lexemCounter, newLexem);  
    }
  }
}

void UserDatabaseManager::parseInsertLexem(QStringList &lexems, const int currentIndex)
{  
  QStringList insertLexems;
  //������ ������
  bool stopProcessing = false;
  for ( int j = currentIndex + 1; (j < lexems.size()) && !stopProcessing; j++ )
  {       
    QString lexem = lexems.value(j);

    if ( lexem.toUpper().compare("VALUES") == 0 )
    {
      //��������� ������ ����������� INSERT
      bool flag = false;    
      if ( insertLexems.value(0).toUpper().startsWith("DOCUMENTS") ||
           insertLexems.value(0).toUpper().startsWith("���������") )
      {
        flag = true;
      }

      QString tableName = parsePath(insertLexems.value(0), flag, 2);
      lexems.replace(j - 2, tableName);

      QStringList intoLexems(insertLexems);
      intoLexems.removeFirst();    

      parseIntoLexem(intoLexems, flag);

      QString newLexem = intoLexems.join(" ");
      //������ �������
      lexems.replace(j - 1, newLexem);

      //���������� ��������� !!!
      stopProcessing = true;    
    }
    else
    {
      insertLexems.append(lexem);
    }
  }
}



void UserDatabaseManager::parseFromLexem(QStringList &lexems, const bool flag)
{
  if ( lexems.isEmpty() )
  {
    return;
  }

  for ( int lexemCounter = 0; lexemCounter < lexems.count(); lexemCounter++ )
  {    
    QString lexem = lexems.value(lexemCounter);
  }
}

void UserDatabaseManager::parseDeleteLexem(QStringList &lexems, const int currentIndex)
{
  //QString tableName = lexems.value(currentIndex + 1);
  //bool flag = false; 
  //if ( tableName.toUpper().startsWith("DOCUMENTS") ||
  //     tableName.toUpper().startsWith("���������") )
  //{
  //  flag = true;
  //}
  //
  //QString newLexem = parsePath(tableName, flag, 2);
  ////������ �������
  //lexems.replace(currentIndex + 1, newLexem);
  
  QStringList fromLexems;
  //������ ������
  bool stopProcessing = false;
  for ( int j = currentIndex + 1; (j < lexems.size()) && !stopProcessing; j++ )
  {       
    QString lexem = lexems.value(j);

    if ( lexem.toUpper().compare("WHERE") == 0 || (j + 1) == lexems.size() )
    {
      //��������� ������ ����������� FROM
      if ( (j + 1) == lexems.size() )
      {
        fromLexems.append(lexem);
      }

      QString tableName = fromLexems.value(0);
      bool flag = false; 
      if ( tableName.toUpper().startsWith("DOCUMENTS") ||
           tableName.toUpper().startsWith("���������") )
      {
        flag = true;
      }

      QString newLexem = parsePath(tableName, flag, 2);      

      ////��������� ������ ����������� FROM  
      if ( (j + 1) == lexems.size() )
      {
        lexems.replace(j, newLexem);
      }
      else
      {
        lexems.replace(j - 1, newLexem); 
      }

      //��������� ������ ����������� WHERE
      QStringList whereLexems;
      for ( int l = j + 1; l < lexems.size(); l++ )
      {
        lexem = lexems.value(l);
        whereLexems.append(lexem);
      }     

      //�������� ������ � ������ ���������� ����������� WHERE
      for ( int c = lexems.count() - 1; c >= j + 1; c-- )
      {
        lexems.removeLast();
      }

      parseWhereLexem(whereLexems, flag);

      lexems.append(whereLexems.join(" "));

      //���������� ��������� !!!
      stopProcessing = true;
    }
    else
    {
      fromLexems.append(lexem);
    }
  }
}

bool UserDatabaseManager::exec(const QString &sql, QDataStream &data)
{
  if ( sql.isEmpty() )
  {
    lastError = "WARNING: Query is empty !!!";
    return false;
  }

  //������ �����������
  QStringList lexems = sql.split(QRegExp("\\b\\s+"), QString::SkipEmptyParts);  

  for ( int i = 0; i < lexems.size(); i++ )
  {
    std::cout << lexems.value(i).toLocal8Bit().data() << std::endl;
  }
  
  bool stopProcessing = false; 
  for ( int i = 0; (i < lexems.size()) && !stopProcessing; i++ )
  {    
    QString lexem = lexems.value(i);

    if ( lexem.toUpper().compare("SELECT") == 0 )
    {
      parseSelectLexem(lexems, i);
      break;     
    }
    else 
    {
      if ( lexem.toUpper().compare("UPDATE") == 0 )
      {
        //�������������� ������ ������
        for ( int j = i; j < lexems.size(); j++ )
        {
          QString subLexem = lexems.value(j);
          if ( subLexem.indexOf(QString("WHERE")) != - 1 )
          {
            QString leftLexem = subLexem.left(subLexem.indexOf(QString("WHERE")));
            lexems.replace(j, leftLexem);
            lexems.insert(j + 1, QString("WHERE"));
            j++;
          }
        }

        parseUpdateLexem(lexems, i);
        break;
      }
      else
      {
        if ( lexem.toUpper().compare("INSERT") == 0 )
        {
          //�������������� ������ ������, ������� �� ��������� �����������
          //� ������� ������������� ����������� ���������
          //- Table(Column1, Column2) VALUES �
          //- (Column1, Column2) VALUES
          
          for ( int j = i; j < lexems.size(); j++ )
          {
            QString subLexem = lexems.value(j);

            if ( subLexem.indexOf(QString("VALUES")) != - 1 )
            {
              QString leftLexem = subLexem.left(subLexem.indexOf(QString("VALUES"))).trimmed();

              lexems.insert(j + 1, QString("VALUES"));
            
              bool separateLexemFlag = false;
              if ( leftLexem.indexOf(QChar('(')) > 0 )
              {              
                int count = subLexem.indexOf(QChar('('));
                QString tableName = leftLexem.left(count).trimmed();

                count = leftLexem.size() - leftLexem.indexOf(QChar('('));
                QString rightLexem = leftLexem.right(count).trimmed();
                lexems.insert(j + 1, rightLexem);
                
                leftLexem = tableName;
              
                separateLexemFlag = true;
                j++;
              }
              
              if ( separateLexemFlag )
              {
                lexems.replace(j - 1, leftLexem);
              }
              else
              {
                lexems.replace(j, leftLexem);
              }             

              j++;                          
            }
          }

          parseInsertLexem(lexems, i + 1);
          break;     
        }
        else 
        {
          if ( lexem.toUpper().compare("DELETE") == 0 )
          {
            parseDeleteLexem(lexems, i + 1);
            break;     
          }
          else 
          {
            //Other commands
            //1. DROP
            //2. CREATE
          }
        }
      }
    }
  }

  QString sout = lexems.join(QString(" "));
  std::cout << sout.toLocal8Bit().data() << std::endl;

  if ( !currentDatabase.isOpen() )
  {
    return false;
  }

  QSqlQuery query(currentDatabase); 
  query.prepare(sout);

  debugOut = sout;

  return DataManager::exec(sout, data);
}

int UserDatabaseManager::read(const QString &name, const int id, QDataStream &out)
{
  //����� ��������� ���������� ������ ��� ���������
  //��������� ������

  if ( !currentDatabase.isOpen() )
  {
    return -1;
  }

  QSqlQuery query(currentDatabase);

  QString sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(name);

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  if ( !query.first() )
  {
    //�������� �� ���������
    return -1;
  }

  int documentId = query.record().field("Id").value().toInt();

  //��������� ���-�� �� ���������
  sql = QString("SELECT COUNT(Id) FROM SysInfo WHERE ParentId = %1 AND Kind = 'TablePart'").arg(documentId);

  query.prepare(sql);

  if ( !query.exec() )
  {    
    lastError = query.lastError().text();
    return -1;
  }

  if ( !query.first() )
  {
    return -1;
  }

  int tpCount = query.value(0).toInt();

  out << tpCount + 1;

  out << name;

  //������� ���������� ����� ���������
  sql = QString("SELECT * FROM \"%1\" WHERE \"Id\" = %2").arg(name).arg(id);

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  //����� ������
  outData(query, out);

  //��������� ���������� � ��������� ������ ���������  
  sql = QString("SELECT SystemName FROM SysInfo WHERE ParentId = %1 AND Kind = 'TablePart'").arg(documentId);

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  //���������� �� ���������
  //out << query.size();

  bool qr = query.first();
  while ( qr )
  {
    QSqlRecord rec = query.record();

    QString sysName = rec.field("SystemName").value().toString();

    out << sysName;

    QSqlQuery subQuery(query);
    sql = QString("SELECT * FROM \"%1\" WHERE \"OwnerId\" = %2").arg(sysName).arg(id);

    subQuery.prepare(sql);

    if ( !subQuery.exec() )
    {
      lastError = subQuery.lastError().text();
      return -1;
    }

    //����� ������ �������
    outData(subQuery, out);

    qr = query.next();
  }

  return 0;
}

bool UserDatabaseManager::insertDataIntoTable(const QString &tableName, const QMap<int, QList<QPair<QString, QVariant> > > &data)
{
  if ( !currentDatabase.isOpen() || tableName.isEmpty() )
  {
    return false;
  }

  QSqlQuery query(currentDatabase);
  QString sql;

  for ( QMap<int, QList<QPair<QString, QVariant> > >::iterator i = data.begin(); i != data.end(); i++ )
  {   
    //������������ ������� �� ���������� ������
    QString fields;
    QString values;
    for ( QList<QPair<QString, QVariant> >::iterator j = i.value().begin(); j != i.value().end(); j++ )
    {
      QPair<QString, QVariant> p = *j;

      QString s = p.first;
      QVariant v = p.second;      

      QString t;
      switch ( v.type() )
      {
        case QVariant::Int:
        {
          t.append(QString("%1").arg(v.toInt()));         
        }
        break;

        case QVariant::Double:
        {
          t.append(QString("%1").arg(v.toDouble()));         
        }
        break;

        case QVariant::String:
        {
          t.append(QString("'%1'").arg(v.toString()));         
        }
        break;

        case QVariant::DateTime:
        {
          t.append(QString("'%1'").arg(v.toDateTime().toString("yyyy.MM.dd hh:mm:ss")));
        }
        break;

        case QVariant::Bool:
        {         
          if ( v.toBool() )
          {
            t.append(QString("%1").arg("TRUE"));
          }
          else
          {
            t.append(QString("%1").arg("FALSE"));
          }                
        }
        break;

        default:;
      }

      fields.append(QString("\"%1\"").arg(s));
      values.append(t);

      if ( j != (i.value().end() - 1) )
      {
        fields.append(QString(","));
        values.append(QString(","));
      }   
    }

    sql.append(QString("INSERT INTO \"%1\"(%2) VALUES (%3);").arg(tableName).arg(fields).arg(values));
  }

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  return true;
}

int UserDatabaseManager::write(const QString &name, const int id, QDataStream &in)
{
  if ( !currentDatabase.isOpen() )
  {
    return -1;
  }

  if ( erase(name, id) != 0 )
  {
    return -1;
  }

  QSqlQuery query(currentDatabase);

  QString sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(name);

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  if ( !query.first() )
  {
    //�������� �� ���������
    return -1;
  }

  int documentId = query.record().field("Id").value().toInt();

  int tpCount;
  in >> tpCount;

  QString tableName;
  in >> tableName;

  //������ ���������� �����
  QMap<int, QList<QPair<QString, QVariant> > > data; 
  inData(in, data);  

  if ( !currentDatabase.transaction() )
  {
    lastError = currentDatabase.lastError().text();
    return -1;
  }

  if ( !insertDataIntoTable(name, data) )
  {
    currentDatabase.rollback();
    return -2;
  }

  //������ ��
  for ( int i = 0; i < tpCount - 1; i++ )
  {
    QMap<int, QList<QPair<QString, QVariant> > > tpData; 

    QString tpName;
    in >> tpName;

    inData(in, tpData);
  
    if ( !insertDataIntoTable(tpName, tpData) )
    {
      currentDatabase.rollback();
      return -2;
    }
  }

  currentDatabase.commit();

  return 0;
}

int UserDatabaseManager::erase(const QString &name, const int id)
{
  if ( !currentDatabase.isOpen() )
  {
    return -1;
  }

  QSqlQuery query(currentDatabase);

  QString sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(name);

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  if ( !query.first() )
  {
    //�������� �� ���������
    return -1;
  }

  int documentId = query.record().field("Id").value().toInt();

  //��������� ���������� � ��������� ������ ���������
  sql = QString("SELECT SystemName FROM SysInfo WHERE ParentId = %1 AND Kind = 'TablePart'").arg(documentId);

  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  if ( !currentDatabase.transaction() )
  {
    lastError = currentDatabase.lastError().text();
    return -1;
  }  

  bool qr = query.first();
  while ( qr )
  {
    QSqlRecord rec = query.record();

    QString sysName = rec.field("SystemName").value().toString();    

    QSqlQuery subQuery(currentDatabase);
    sql = QString("DELETE FROM \"%1\" WHERE \"OwnerId\" = %2").arg(sysName).arg(id);

    subQuery.prepare(sql);

    if ( !subQuery.exec() )
    {
      lastError = subQuery.lastError().text();
      currentDatabase.rollback();
      return -1;
    }

    qr = query.next();
  }

  //�������� �������
  sql = QString("DELETE FROM \"%1\" WHERE \"Id\" = %2").arg(name).arg(id); 
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    currentDatabase.rollback();
    return -1;
  }

  currentDatabase.commit();

  return 0;
}

bool UserDatabaseManager::checkPath(const QString &path)
{
  if ( !currentDatabase.isOpen() || path.isEmpty() )
  {
    return false;
  }

  //�������� ������������ ���� ����� ������� ���������� ���������� SysInfo

  //���� �����:
  //1) ��������.�������� || ��������.*
  //2) ��������.��������������.�������� || ��������.��������������.*

  QStringList sl = path.split(QChar('.'));  

  QString documentName;
  QString tablePartName;
  QString accessoryName;

  int pathKind = 0;

  if ( sl.size() == 2 )
  {
    documentName  = sl.value(0);
    accessoryName = sl.value(1);
    pathKind = 1;
  }
  else
  {
    if ( sl.size() == 3 )
    {
      documentName  = sl.value(0);
      tablePartName = sl.value(1);
      accessoryName = sl.value(2);
      pathKind = 2;
    }
    else
    {
      //���������� ��������� ���� ������ �� ��������� !!!
      lastError = QString("ERROR: Path %1 invalid !!!");
      return false;
    }
  }

  QSqlQuery query(currentDatabase);
  QString sql;

  if ( pathKind && !documentName.isEmpty() )
  {
    sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(documentName);

    query.prepare(sql);

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return false;
    }

    if ( !query.first() )
    {
      //�������� �� ��������� !!!
      lastError = QString("ERROR: Document %1 not exists !!!").arg(documentName);
      return false;
    }

    int documentId = query.record().field("Id").value().toInt();

    if ( pathKind == 1 && !accessoryName.isEmpty() )
    {
      if ( accessoryName.compare(QString("*")) == 0 )
      {
        return true;
      }
      else
      {
        sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Accessory' AND ParentId = %2").arg(accessoryName).arg(documentId);

        query.prepare(sql);

        if ( !query.exec() )
        {
          lastError = query.lastError().text();
          return false;
        }

        if ( !query.first() )
        {
          //�������� ��������� �� ��������� !!!
          lastError = QString("ERROR: Document %1 Accessory %2 not exists !!!").arg(documentName).arg(accessoryName);
          return false;
        }

        return true;
      }
    }    

    if ( pathKind == 2 && !tablePartName.isEmpty() && !accessoryName.isEmpty() )
    {
      sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'TablePart' AND ParentId = %2").arg(tablePartName).arg(documentId);

      query.prepare(sql);

      if ( !query.exec() )
      {
        lastError = query.lastError().text();
        return false;
      }

      if ( !query.first() )
      {
        lastError = QString("ERROR: Document %1 TablePart %2 not exists !!!").arg(documentName).arg(tablePartName);
        return false;
      }

      int tablePartId = query.record().field("Id").value().toInt();

      if ( accessoryName.compare(QString("*")) == 0 )
      {
        return true;
      }
      else
      {
        sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Accessory' AND ParentId = %2").arg(accessoryName).arg(tablePartId);

        query.prepare(sql);

        if ( !query.exec() )
        {
          lastError = query.lastError().text();
          return false;
        }

        if ( !query.first() )
        {
          //�������� ��������� ����� ��������� �� ��������� !!!
          lastError = QString("ERROR: Document %1 TablePart %2 Accessory %3 not exists !!!").arg(documentName).arg(tablePartName).arg(accessoryName);
          return false;
        }

        return true;
      }
    }
  }

  return false;
}
