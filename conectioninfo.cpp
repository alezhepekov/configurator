#include "connectioninfo.h"

namespace ConfiguratorObjects
{
  QDataStream &operator<<(QDataStream &out, const ConnectionInfo &cInfo)
  {
    out << cInfo.host;
    out << cInfo.port;
    out << cInfo.dbName;
    out << cInfo.userName;
    out << cInfo.password;

    return out;
  }

  QDataStream &operator>>(QDataStream &in, ConnectionInfo &cInfo)
  {
    in >> cInfo.host;
    in >> cInfo.port;
    in >> cInfo.dbName;
    in >> cInfo.userName;
    in >> cInfo.password;

    return in;
  }
}