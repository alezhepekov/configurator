#include "configurationlistform.h"

#include "newconfigurationform.h"
#include "newuserdatabaseform.h"

#include "../core/IdeServerConnectionModule/ideserverconnectionmodule.h"

#include <QStringList>
#include <QDataStream>
#include <QByteArray>
#include <QFileDialog>
#include <QMessageBox>

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;

ConfigurationListForm::ConfigurationListForm(QWidget *parent) : 
QDialog(parent, Qt::WindowTitleHint|Qt::WindowSystemMenuHint),
currentConfigurationId(-1),
downloadConfigurationFlag(false)
{
  setWindowTitle(tr("Configuration manager"));
 
  actionGetConfigurationList = new QAction(this); 
  actionGetConfigurationList->setText(tr("Get configuration list"));
  actionGetConfigurationList->setStatusTip(tr("Get configuration list"));
  actionGetConfigurationList->setShortcut(tr("Ctrl+L"));
  actionGetConfigurationList->setIcon(QIcon(QString::fromUtf8(":/Configurator/configurationlist.png")));  
  actionGetConfigurationList->setEnabled(true);

  actionNewDatabase = new QAction(this); 
  actionNewDatabase->setText(tr("New database"));
  actionNewDatabase->setStatusTip(tr("Create a new database"));
  actionNewDatabase->setShortcut(tr("Ctrl+Alt+Insert"));
  actionNewDatabase->setIcon(QIcon(QString::fromUtf8(":/Configurator/newdatabase.png")));  
  actionNewDatabase->setEnabled(false);

  actionLoadConfiguration = new QAction(this); 
  actionLoadConfiguration->setText(tr("Load configuration"));
  actionLoadConfiguration->setStatusTip(tr("Load selected configuration"));
  actionLoadConfiguration->setShortcut(tr("Ctrl+Alt+L"));
  actionLoadConfiguration->setIcon(QIcon(QString::fromUtf8(":/Configurator/openconfiguration.png")));  
  actionLoadConfiguration->setEnabled(false);

  actionRemoveDatabase = new QAction(this); 
  actionRemoveDatabase->setText(tr("Remove database"));
  actionRemoveDatabase->setStatusTip(tr("Remove selected database"));
  actionRemoveDatabase->setShortcut(tr("Ctrl+Alt+Delete"));
  actionRemoveDatabase->setIcon(QIcon(QString::fromUtf8(":/Configurator/removedatabase.png")));
  actionRemoveDatabase->setEnabled(false);

  actionReleaseConfiguration = new QAction(this); 
  actionReleaseConfiguration->setText(tr("Release configuration"));
  actionReleaseConfiguration->setStatusTip(tr("Release selected configuration"));
  actionReleaseConfiguration->setShortcut(tr("Ctrl+R"));
  actionReleaseConfiguration->setIcon(QIcon(QString::fromUtf8(":/Configurator/releaseconfiguration.png")));
  actionReleaseConfiguration->setEnabled(false);
  
  actionUploadDatabase = new QAction(this); 
  actionUploadDatabase->setText(tr("Upload database"));
  actionUploadDatabase->setStatusTip(tr("Upload database"));
  //actionUploadDatabase->setShortcut(tr(""));
  actionUploadDatabase->setIcon(QIcon(QString::fromUtf8(":/Configurator/uploaddatabase.png")));
  actionUploadDatabase->setEnabled(false);

  actionDownloadDatabase = new QAction(this); 
  actionDownloadDatabase->setText(tr("Download database"));
  actionDownloadDatabase->setStatusTip(tr("Download database"));
  //actionDownloadDatabase->setShortcut(tr(""));
  actionDownloadDatabase->setIcon(QIcon(QString::fromUtf8(":/Configurator/downloaddatabase.png")));
  actionDownloadDatabase->setEnabled(false); 

  toolBar = new QToolBar();
  toolBar->setFixedHeight(34);
  toolBar->setOrientation(Qt::Horizontal);  
  toolBar->setIconSize(QSize(20, 20)); 

  toolBar->addAction(actionGetConfigurationList);
  toolBar->addAction(actionNewDatabase);
  toolBar->addAction(actionLoadConfiguration);
  toolBar->addAction(actionRemoveDatabase);
  toolBar->addAction(actionReleaseConfiguration); 
  toolBar->addSeparator();
  toolBar->addAction(actionUploadDatabase);
  toolBar->addAction(actionDownloadDatabase);  
 
  treeWidget = new QTreeWidget();
  treeWidget->setColumnCount(2);
  treeWidget->setColumnWidth(0, 250);
  treeWidget->setColumnWidth(1, 100);
  
  QStringList hLabels;
  hLabels.append(tr("Object/Property"));
  hLabels.append(tr("Value"));
  treeWidget->setHeaderLabels(hLabels);

  statusBar = new QStatusBar();

  layout = new QVBoxLayout();
  layout->setMargin(2);
  layout->setSpacing(0);
  layout->addWidget(toolBar);
  layout->addWidget(treeWidget);
  layout->addWidget(statusBar);
  setLayout(layout); 

  connect(treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)));
  
  connect(actionGetConfigurationList, SIGNAL(triggered()), this, SLOT(getConfigurationList()));
  connect(actionNewDatabase,          SIGNAL(triggered()), this, SLOT(newDatabase()));
  connect(actionLoadConfiguration,    SIGNAL(triggered()), this, SLOT(loadConfiguration()));
  connect(actionRemoveDatabase,       SIGNAL(triggered()), this, SLOT(removeDatabase()));
  connect(actionReleaseConfiguration, SIGNAL(triggered()), this, SLOT(releaseConfiguration()));  
  connect(actionUploadDatabase,       SIGNAL(triggered()), this, SLOT(uploadDatabase()));
  connect(actionDownloadDatabase,     SIGNAL(triggered()), this, SLOT(downloadDatabase()));  

  connect(IdeServerConnectionModule::instance(), SIGNAL(processCmdNewConfiguration(const qint32 &, const QByteArray &)), this, SLOT(processCmdNewConfiguration(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveConfigurationList(const qint32 &, const QByteArray &)),   this, SLOT(processCmdConfigurationList(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveConfiguration(const qint32 &, const QByteArray &)),       this, SLOT(processCmdConfiguration(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(configurationSaved(const qint32 &, const QByteArray &)),         this, SLOT(processCmdSaveConfiguration(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveRemoveConfiguration(const qint32 &, const QByteArray &)), this, SLOT(processCmdRemoveConfiguration(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(configurationReleased(const qint32 &, const QByteArray &)),      this, SLOT(processCmdReleaseConfiguration(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveUserDatabaseList(const qint32 &, const QByteArray &)),    this, SLOT(processCmdUserDatabaseList(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveNewUserDatabase(const qint32 &, const QByteArray &)),     this, SLOT(processCmdNewUserDatabase(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveRemoveUserDatabase(const qint32 &, const QByteArray &)),  this, SLOT(processCmdRemoveUserDatabase(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveDumpUserDatabase(const qint32 &, const QByteArray &)),  this, SLOT(processCmdDumpUserDatabase( const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(receiveRestoreUserDatabase(const qint32 &, const QByteArray &)),  this, SLOT(processCmdRestoreUserDatabase( const qint32 &, const QByteArray &)));
}

ConfigurationListForm::~ConfigurationListForm()
{
  delete toolBar;
  delete treeWidget;
  delete statusBar;
  delete layout;
  delete actionGetConfigurationList;
  delete actionNewDatabase;
  delete actionLoadConfiguration;
  delete actionRemoveDatabase;
  delete actionReleaseConfiguration;
  delete actionUploadDatabase;
  delete actionDownloadDatabase;  
}

bool ConfigurationListForm::event(QEvent *e)
{
  if ( e->type() == QEvent::LanguageChange )
    retranslateUi(this);

  return QWidget::event(e);
}

ConfigurationList * ConfigurationListForm::getConfigurations()
{
  return &configurationList;
}

void ConfigurationListForm::setConfigurations(const ConfigurationList &newConfigurationList)
{
  configurationList = newConfigurationList;
}

UserDatabaseList * ConfigurationListForm::getUserDatabases()
{
  return &userDatabaseList;
}

void ConfigurationListForm::setUserDatabases(const UserDatabaseList &newUserDatabaseList)
{
  userDatabaseList = newUserDatabaseList;
}

QTreeWidgetItem * ConfigurationListForm::appendConnectionInfo(QTreeWidgetItem *current, const ConnectionInfo &cInfo)
{
  QStringList itemData;
  CLItemData newCLItemData;
  QVariant d;

  itemData.append(tr("Host"));
  itemData.append(QString("%1").arg(cInfo.host));
  QTreeWidgetItem *newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  current->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Port"));
  itemData.append(QString("%1").arg(cInfo.port));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  current->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Database name"));
  itemData.append(QString("%1").arg(cInfo.dbName));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  current->addChild(newItem);

  itemData.clear();
  itemData.append(tr("User"));
  itemData.append(QString("%1").arg(cInfo.userName));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  current->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Password"));
  itemData.append(QString("%1").arg(cInfo.password));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  current->addChild(newItem);

  return newItem;
}

QTreeWidgetItem * ConfigurationListForm::appendConfigurationHeader(QTreeWidgetItem *current, const ConfigurationHeader &header)
{
  QStringList itemData;
  CLItemData newCLItemData;
  QVariant d;

  itemData.append(tr("Header")); 
  QTreeWidgetItem *newHeaderItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_HEADER;
  newCLItemData.id = header.getId();
  d.setValue(newCLItemData);
  newHeaderItem->setData(0, Qt::UserRole, d);
  current->addChild(newHeaderItem);

  itemData.clear();
  itemData.append(tr("Id"));
  itemData.append(QString("%1").arg(header.getId()));
  QTreeWidgetItem *newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Name"));
  itemData.append(QString("%1").arg(header.getName()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Author"));
  itemData.append(QString("%1").arg(header.getAuthor()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Version"));
  itemData.append(QString("%1").arg(header.getVersion()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Creation date and time"));
  itemData.append(QString("%1").arg(header.getCreationDateTime().toString("dd.MM.yyyy hh:mm:ss")));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem);

  itemData.clear();
  itemData.append(tr("Last modification date and time"));
  itemData.append(QString("%1").arg(header.getLastModificationDateTime().toString("dd.MM.yyyy hh:mm:ss")));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = -1;
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem);

  itemData.clear();
  itemData.append(tr("User database id"));
  itemData.append(QString("%1").arg(header.getUserDatabaseId()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_USER_DATABASE_ID;
  newCLItemData.id = header.getUserDatabaseId();
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem); 

  itemData.clear();
  itemData.append(tr("User database name"));
  int udbId = header.getUserDatabaseId();
  if ( (udbId >= 1) && (udbId <= userDatabaseList.getDatabases()->size()) )
  {
    ConnectionInfo cInfo = userDatabaseList.getDatabases()->value(udbId - 1);
    itemData.append(cInfo.dbName);    
  }

  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
  newCLItemData.kind = ITEM_KIND_NO_MEANING;
  newCLItemData.id = header.getUserDatabaseId();
  d.setValue(newCLItemData);
  newItem->setData(0, Qt::UserRole, d);
  newHeaderItem->addChild(newItem);

  return newHeaderItem;
}

void ConfigurationListForm::showData()
{
  //������� ������������� ������ ������������
  treeWidget->clear();
  showConfigurationsInfo();
  showUserDatabasesInfo();
}

void ConfigurationListForm::showConfigurationsInfo()
{
  QStringList itemData;    
  itemData.append(QString("%1 (%2)").arg(tr("Configurations")).arg(configurationList.getConfigurations()->size()));
  QTreeWidgetItem *configurations = new QTreeWidgetItem((QTreeWidget*)0, itemData);  
  
  CLItemData newCLItemData;
  newCLItemData.kind = ITEM_KIND_CONFIGURATIONS;
  newCLItemData.id = 0;
  
  QVariant d;
  d.setValue(newCLItemData);
  configurations->setData(0, Qt::UserRole, d);
  treeWidget->insertTopLevelItem(treeWidget->topLevelItemCount(), configurations);
  configurations->setExpanded(true);
  treeWidget->setCurrentItem(configurations); 
 
  //���������� ������������� ������ ������������  
  for ( QList<ConfigurationInfo>::iterator i = configurationList.getConfigurations()->begin(); i != configurationList.getConfigurations()->end(); i++ )
  {    
    ConfigurationInfo configInfo = *i;

    itemData.clear();   
    itemData.append(configInfo.header.getName());
    
    QTreeWidgetItem *newConfiguarationItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
    newCLItemData.kind = ITEM_KIND_CONFIGURATION;
    newCLItemData.id = configInfo.id;
    d.setValue(newCLItemData);
    newConfiguarationItem->setData(0, Qt::UserRole, d);  
    configurations->addChild(newConfiguarationItem);

    itemData.clear();
    itemData.append(tr("Id"));
    itemData.append(QString("%1").arg(configInfo.id));
    QTreeWidgetItem *newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
    newCLItemData.kind = ITEM_KIND_NO_MEANING;
    newCLItemData.id = -1;
    d.setValue(newCLItemData);
    newItem->setData(0, Qt::UserRole, d);
    newConfiguarationItem->addChild(newItem);

    appendConnectionInfo(newConfiguarationItem, configInfo.connectionInfo);
    appendConfigurationHeader(newConfiguarationItem, configInfo.header);  
  }
}

void ConfigurationListForm::showUserDatabasesInfo()
{
  QStringList itemData;
  itemData.append(QString("%1 (%2)").arg(tr("Users databases")).arg(userDatabaseList.getDatabases()->size()));
  QTreeWidgetItem *usersDataBasesItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);  

  CLItemData newCLItemData;
  newCLItemData.kind = ITEM_KIND_USER_DATABASES;
  newCLItemData.id = 0;

  QVariant d;
  d.setValue(newCLItemData);
  usersDataBasesItem->setData(0, Qt::UserRole, d);
  treeWidget->insertTopLevelItem(treeWidget->topLevelItemCount(), usersDataBasesItem);
  //usersDataBasesItem->setExpanded(true);
  //treeWidget->setCurrentItem(root);

  int j = 0;
  for ( QList<ConnectionInfo>::iterator i = userDatabaseList.getDatabases()->begin(); i != userDatabaseList.getDatabases()->end(); i++ )
  {    
    ConnectionInfo cInfo = *i;

    itemData.clear();
    itemData.append((*i).dbName);
    QTreeWidgetItem *newUserDataBaseItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
    newCLItemData.kind = ITEM_KIND_USER_DATABASE;
    j++;
    newCLItemData.id = j;
    d.setValue(newCLItemData);
    newUserDataBaseItem->setData(0, Qt::UserRole, d);
    usersDataBasesItem->addChild(newUserDataBaseItem);

    itemData.clear();
    itemData.append(tr("Id"));
    itemData.append(QString("%1").arg(j));
    QTreeWidgetItem *newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
    newCLItemData.kind = ITEM_KIND_USER_DATABASE;  
    newCLItemData.id = -1;
    d.setValue(newCLItemData);
    newItem->setData(0, Qt::UserRole, d);
    newUserDataBaseItem->addChild(newItem);

    //appendConnectionInfo(newUserDataBaseItem, *i);

    //����� � ����������� ��������� ������������
    itemData.clear();
    itemData.append(QString("%1 (%2)").arg(tr("Linked configurations")).arg(0));   
    QTreeWidgetItem *newLinkedConfigurationsItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
    newCLItemData.kind = ITEM_KIND_LINKED_CONFIGURATIONS;  
    newCLItemData.id = -1;
    d.setValue(newCLItemData);
    newItem->setData(0, Qt::UserRole, d);
    newUserDataBaseItem->addChild(newLinkedConfigurationsItem);

    int linkedConfigCount = 0;
    for ( QList<ConfigurationInfo>::iterator k = configurationList.getConfigurations()->begin(); k != configurationList.getConfigurations()->end(); k++ )
    {
      if ( (*k).header.getUserDatabaseId() == j )
      {
        linkedConfigCount++;

        itemData.clear();
        itemData.append((*k).header.getName());   
        newItem = new QTreeWidgetItem((QTreeWidget*)0, itemData);
        newCLItemData.kind = ITEM_KIND_NO_MEANING;  
        newCLItemData.id = -1;
        d.setValue(newCLItemData);
        newItem->setData(0, Qt::UserRole, d);

        newLinkedConfigurationsItem->addChild(newItem);
        newLinkedConfigurationsItem->setText(0, QString("%1 (%2)").arg(tr("Linked configurations")).arg(linkedConfigCount));
      }
    }   
  }
}

ObjectList * ConfigurationListForm::getCurrentObjectList()
{
  return &currentObjectList;
}

void ConfigurationListForm::currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
  if ( !current ) return; 

  CLItemData itemData = current->data(0, Qt::UserRole).value<CLItemData>(); 

  switch( itemData.kind )
  {
    case ITEM_KIND_CONFIGURATIONS:
    {
      actionNewDatabase->setEnabled(true);
      actionRemoveDatabase->setEnabled(false);
      actionLoadConfiguration->setEnabled(false);
      actionReleaseConfiguration->setEnabled(false);
      actionUploadDatabase->setEnabled(true);
      actionDownloadDatabase->setEnabled(false);
    }
    break;

    case ITEM_KIND_USER_DATABASES:
    {
      actionNewDatabase->setEnabled(true);
      actionRemoveDatabase->setEnabled(false);
      actionLoadConfiguration->setEnabled(false);
      actionReleaseConfiguration->setEnabled(false);
      actionUploadDatabase->setEnabled(false);
      actionDownloadDatabase->setEnabled(false);
    }
    break;

    case ITEM_KIND_CONFIGURATION:
    {
      actionNewDatabase->setEnabled(false);
      actionRemoveDatabase->setEnabled(true);
      actionLoadConfiguration->setEnabled(true);	    
      actionReleaseConfiguration->setEnabled(true);
      actionUploadDatabase->setEnabled(false);
      actionDownloadDatabase->setEnabled(true);

      currentConfigurationId = itemData.id;
    }
    break;

    case ITEM_KIND_USER_DATABASE:
    {
      actionNewDatabase->setEnabled(false);
      actionRemoveDatabase->setEnabled(true);
      actionLoadConfiguration->setEnabled(false);
      actionReleaseConfiguration->setEnabled(false);
      actionUploadDatabase->setEnabled(true);
      actionDownloadDatabase->setEnabled(true);    
    }
    break;

    default:
    {
      actionNewDatabase->setEnabled(false);
      actionLoadConfiguration->setEnabled(false);
	    actionRemoveDatabase->setEnabled(false);
      actionReleaseConfiguration->setEnabled(false);
      actionUploadDatabase->setEnabled(false);
      actionDownloadDatabase->setEnabled(false);
    }
  }
}

void ConfigurationListForm::getConfigurationList()
{  
  getConfigurationListCmd();
}

void ConfigurationListForm::newDatabase()
{
  QTreeWidgetItem *current = treeWidget->currentItem();
  CLItemData itemData = current->data(0, Qt::UserRole).value<CLItemData>(); 
  
  switch ( itemData.kind )
  {
    case ITEM_KIND_CONFIGURATIONS:
    {
      NewConfigurationForm *newConfigForm = new NewConfigurationForm();
      newConfigForm->setUserDatabaseList(userDatabaseList);
  
      if ( newConfigForm->exec() == QDialog::Accepted )
      {    
        ConfigurationInfo configInfo = newConfigForm->getConfigurationInfo();	
        newConfigurationCmd(configInfo);
      }

      delete newConfigForm;
    }
  	break;

    case ITEM_KIND_USER_DATABASES:
    {
      NewUserDatabaseForm *newUserDatabaseForm = new NewUserDatabaseForm(); 

      if ( newUserDatabaseForm->exec() == QDialog::Accepted )
      {    
        ConnectionInfo cInfo = newUserDatabaseForm->getConnectionInfo();	
        newUserDatabaseCmd(cInfo);
      }

      delete newUserDatabaseForm;
    }
    break;

    default:;
  }
}

void ConfigurationListForm::loadConfiguration()
{
  getConfigurationCmd(currentConfigurationId);
}

void ConfigurationListForm::removeDatabase()
{  
  QTreeWidgetItem *current = treeWidget->currentItem();
  CLItemData itemData = current->data(0, Qt::UserRole).value<CLItemData>();  

  switch ( itemData.kind )
  {
    case ITEM_KIND_CONFIGURATION:
    {
      removeConfigurationCmd(itemData.id);
    }
    break;

    case ITEM_KIND_USER_DATABASE:
    {
      removeUserDatabaseCmd(itemData.id);
    }
    break;

    default:;
  }
}

void ConfigurationListForm::releaseConfiguration()
{  
  //std::cout << configurationList.getConfigurations()->size() << std::endl;
  if ( (currentConfigurationId < 0) || ((currentConfigurationId - 1) > configurationList.getConfigurations()->size() - 1) )
  {
    return;
  }

  ConfigurationInfo configInfo = configurationList.getConfigurations()->value(currentConfigurationId - 1);

  //std::cout << userDatabaseList.getDatabases()->size() << std::endl;
  int userDatabaseId = configInfo.header.getUserDatabaseId();
  if ( (userDatabaseId < 0) || ((userDatabaseId - 1) > userDatabaseList.getDatabases()->size() - 1) )
  {
    return;
  }

  ConnectionInfo cInfo = userDatabaseList.getDatabases()->value(userDatabaseId - 1);
  releaseConfigurationCmd(cInfo);
}

void ConfigurationListForm::uploadDatabase()
{
  QTreeWidgetItem *current = treeWidget->currentItem();
  CLItemData itemData = current->data(0, Qt::UserRole).value<CLItemData>();  

  switch ( itemData.kind )
  {
    case ITEM_KIND_CONFIGURATIONS:
    {
      QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"),
              QCoreApplication::applicationDirPath(),
              tr("Configuration object list file (*.olf)"));

      QFile inFile(fileName);
      if ( !inFile.open(QIODevice::ReadOnly) )
      {
        statusBar->showMessage(QString("%1\n%2").arg(tr("File opening error!!!")).arg(fileName));
        return;
      }

      //��������� ����� ����� - ���������� ���� � �������� ����� ������ �����-������� �����, ������� �����  
      QString name;
      int c1 = fileName.lastIndexOf(QChar('/'));
      int c2 = fileName.lastIndexOf(QChar('.'));
      if ( c2 > c1 )
      {
        name = fileName.mid(c1 + 1, c2 - c1 - 1);  
      }  

      NewConfigurationForm *newConfigForm = new NewConfigurationForm();
      newConfigForm->setUserDatabaseList(userDatabaseList);
      newConfigForm->setConfigurationName(name);
      newConfigForm->setDatabaseName(name);

      if ( newConfigForm->exec() == QDialog::Rejected )
      {
        statusBar->showMessage(tr("Action cancelled by user"));
        delete newConfigForm;
        return;
      }

      ConfigurationInfo configInfo = newConfigForm->getConfigurationInfo(); 

      delete newConfigForm;

      newConfigurationCmd(configInfo);
      int cId = configurationList.getConfigurations()->size() + 1;
      getConfigurationCmd(cId);

      QDataStream in(&inFile);
      ObjectList objList;
      in >> objList;
      saveConfigurationCmd(objList, configInfo.header);

      inFile.close();
    }
    break;

    case ITEM_KIND_USER_DATABASE:
    {
      //statusBar->showMessage(tr("Upload algorithm not defined for user database"));
      QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"),
              QCoreApplication::applicationDirPath(),
              tr("User database dump file (*.dump)"));

      QFile inFile(fileName);
      if ( !inFile.open(QIODevice::ReadOnly) )
      {
        statusBar->showMessage(QString("%1\n%2").arg(tr("File opening error!!!")).arg(fileName));
        return;
      }

      restoreUserDatabaseCmd(itemData.id, inFile.readAll());
    }
    break;

    default:;
  }  
}

void ConfigurationListForm::downloadDatabase()
{
  QTreeWidgetItem *current = treeWidget->currentItem();
  CLItemData itemData = current->data(0, Qt::UserRole).value<CLItemData>();

  switch ( itemData.kind )
  {
    case ITEM_KIND_CONFIGURATION:
    {
      statusBar->showMessage(tr("Download configuration"));

      downloadConfigurationFlag = true;
      getConfigurationCmd(currentConfigurationId);
    }
    break;

    case ITEM_KIND_USER_DATABASE:
    {
      statusBar->showMessage(tr("Download user database"));
      dumpUserDatabaseCmd(itemData.id);
    }
    break;

    default:;
  }  
}

void ConfigurationListForm::newConfigurationCmd(const ConfigurationInfo &configInfo)
{  
  statusBar->showMessage(tr("New configuration command processing..."));
  QByteArray ba;
  QDataStream out(&ba, QIODevice::WriteOnly);
  out << configInfo;  
  IdeServerConnectionModule::instance()->newConfiguration(ba);
}

void ConfigurationListForm::getConfigurationListCmd()
{
  statusBar->showMessage(tr("Get configuration list command processing..."));
  IdeServerConnectionModule::instance()->getConfigurationList();
}

void ConfigurationListForm::getConfigurationCmd(const int id)
{
  statusBar->showMessage(tr("Get configuration command processing..."));
  //IdeServerConnectionModule::instance()->getConfiguration(currentConfigurationId);
  IdeServerConnectionModule::instance()->getConfiguration(id);
}

void ConfigurationListForm::saveConfigurationCmd(const ObjectList &objList, const ConfigurationHeader &header)
{
  QByteArray ba;
  QDataStream out(&ba, QIODevice::WriteOnly);
  out << header;
  out << objList;

  IdeServerConnectionModule::instance()->saveConfiguration(ba);
}

void ConfigurationListForm::removeConfigurationCmd(const int id)
{
  statusBar->showMessage(tr("Remove configuration command processing...")); 
  IdeServerConnectionModule::instance()->removeConfiguration(id);
}

void ConfigurationListForm::releaseConfigurationCmd(const ConnectionInfo &cInfo)
{
  statusBar->showMessage(tr("Release configuration command processing..."));
  QByteArray ba;
  QDataStream out(&ba, QIODevice::WriteOnly); 
  out << cInfo;
  IdeServerConnectionModule::instance()->releaseConfiguration(currentConfigurationId, ba);
}

void ConfigurationListForm::getUserDatabaseListCmd()
{
  statusBar->showMessage(tr("Get user database list command processing..."));
  IdeServerConnectionModule::instance()->getUserDatabaseList();
}

void ConfigurationListForm::newUserDatabaseCmd(const ConnectionInfo &cInfo)
{
  statusBar->showMessage(tr("New user database command processing..."));
  QByteArray ba;
  QDataStream out(&ba, QIODevice::WriteOnly);
  out << cInfo;
  IdeServerConnectionModule::instance()->newUserDatabase(ba);
}

void ConfigurationListForm::removeUserDatabaseCmd(const int id)
{
  statusBar->showMessage(tr("Remove user database command processing...")); 
  IdeServerConnectionModule::instance()->removeUserDatabase(id);
}

void ConfigurationListForm::dumpUserDatabaseCmd(const int id)
{
  statusBar->showMessage(tr("Dump user database command processing..."));
  IdeServerConnectionModule::instance()->dumpUserDatabase(id, QByteArray());
}

void ConfigurationListForm::restoreUserDatabaseCmd(const int id, const QByteArray &data)
{
  statusBar->showMessage(tr("Restore user database command processing..."));
  IdeServerConnectionModule::instance()->restoreUserDatabase(id, data);
}

void ConfigurationListForm::processCmdNewConfiguration(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {
    statusBar->showMessage(tr("Creating configuration failed!!!"));
    return;
  }

  statusBar->showMessage(tr("Configuration created successful!!!"));

  //������ ������ ������������ ����� �����������  
  getConfigurationListCmd();
}

void ConfigurationListForm::processCmdConfigurationList(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {
    statusBar->showMessage(tr("Receiving configuration list failed!!!"));  
    return;
  }

  statusBar->showMessage(tr("Configuration list received successful!!!"));  

  QDataStream in(data);

  configurationList.getConfigurations()->clear();
  in >> configurationList;

  //showConfigurationsInfo();
  showData();
}

void ConfigurationListForm::processCmdConfiguration(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {  
    statusBar->showMessage(tr("Loading configuration failed!!!"));
    return;
  }

  statusBar->showMessage(tr("Configuration received successful!!!"));

  //������� �������� ������ ��������
  for ( QList<BaseObject*>::iterator i = currentObjectList.getObjects()->begin(); i != currentObjectList.getObjects()->end(); i++ )
  {
    delete *i;
  }

  currentObjectList.getObjects()->clear();

  //�������� ������ ������ �������� 
  QDataStream in(data);
  in >> currentObjectList;  

  if ( downloadConfigurationFlag )
  {
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save file"),
                                                    QCoreApplication::applicationDirPath(),
                                                    tr("Configuration object list file (*.olf)"));

    QFile outFile(fileName);
    if ( !outFile.open(QIODevice::WriteOnly | QIODevice::Truncate) )
    {
      statusBar->showMessage(QString("%1\n%2").arg(tr("File opening error!!!")).arg(fileName));
      return;
    }  

    QDataStream out(&outFile);
    out << currentObjectList;  

    outFile.close();

    downloadConfigurationFlag = false;
  }

  //std::cout << currentObjectList.toXML().toLocal8Bit().data() << std::endl;

  for ( QList<ConfigurationInfo>::iterator i = configurationList.getConfigurations()->begin(); i != configurationList.getConfigurations()->end(); i++ )
  {
    //����� ���������   
    //if ( (*i).header.getId() == currentConfigurationId )
    if ( (*i).id == currentConfigurationId )
    {      
      (*i).header.setId(currentConfigurationId);
      emit processConfiguration((*i).header);
    }
  }  
}

void ConfigurationListForm::processCmdSaveConfiguration(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {  
    statusBar->showMessage(tr("Saving configuration failed!!!"));
    return;
  }

  statusBar->showMessage(tr("Configuration saved successful!!!"));
}

void ConfigurationListForm::processCmdRemoveConfiguration(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {
    statusBar->showMessage(tr("Removing configuration failed!!!"));    
    return;
  }

  statusBar->showMessage(tr("Configuration removed successful!!!"));

  //������ ������ ������������ ����� ����������� 
  getConfigurationListCmd();
}

void ConfigurationListForm::processCmdReleaseConfiguration(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {
    statusBar->showMessage(tr("Releasing configuration failed!!!"));
    QMessageBox::critical(NULL, tr("Error"), tr("Releasing configuration failed!!!"), QMessageBox::Ok);
    return;
  }

  statusBar->showMessage(tr("Configuration released successful!!!"));
  QMessageBox::information(NULL, tr("Title"),  tr("Configuration released successful!!!"), QMessageBox::Ok);
}

void ConfigurationListForm::processCmdUserDatabaseList(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {
    statusBar->showMessage(tr("Receiving user database list failed!!!"));  
    return;
  }

  statusBar->showMessage(tr("User database list received successful!!!"));  

  QDataStream in(data);

  userDatabaseList.getDatabases()->clear();
  in >> userDatabaseList;

  //showUserDatabasesInfo();
  showData();
}

void ConfigurationListForm::processCmdNewUserDatabase(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {
    statusBar->showMessage(tr("Creating user database failed!!!"));  
    return;
  }

  statusBar->showMessage(tr("User database created successful!!!"));  

  getUserDatabaseListCmd();

  //showUserDatabasesInfo();
  //showData();
}

void ConfigurationListForm::processCmdRemoveUserDatabase(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {
    statusBar->showMessage(tr("Removing user database failed!!!"));    
    return;
  }

  //statusBar->showMessage(tr("User database removed successful!!!"));

  getUserDatabaseListCmd();
}

void ConfigurationListForm::processCmdDumpUserDatabase(const qint32 &result, const QByteArray &data)
{
      if ( result )
      {
        statusBar->showMessage(tr("Dumping database failed!!!"));    
        return;
      }

      //������ ����� � ����
      QString fileName = QFileDialog::getSaveFileName(this,
              tr("Save file"),
              QCoreApplication::applicationDirPath(),
              tr("User database dump file (*.dump)"));

      QFile outFile(fileName);
      if ( !outFile.open(QIODevice::WriteOnly | QIODevice::Truncate) )
      {
        statusBar->showMessage(QString("%1\n%2").arg(tr("File opening error!!!")).arg(fileName));
        return;
      }

      outFile.write(data);
      outFile.close();

      statusBar->showMessage("Database dumping successful!!!", 5000);
}

void ConfigurationListForm::processCmdRestoreUserDatabase(const qint32 &result, const QByteArray &data)
{
      if ( result )
      {
        statusBar->showMessage(tr("Restoring database failed!!!"));    
        return;
      }

      statusBar->showMessage("Database restoring successful!!!", 5000);
}
