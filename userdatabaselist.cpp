#include "userdatabaselist.h"

using namespace ConfiguratorObjects;

UserDatabaseList::UserDatabaseList()
{
}

UserDatabaseList::~UserDatabaseList()
{  
}

const QList<ConnectionInfo> * UserDatabaseList::getDatabases() const
{
  return &databases;
}

QList<ConnectionInfo> * UserDatabaseList::getDatabases()
{
  return &databases;
}

void UserDatabaseList::setDatabases(const QList<ConnectionInfo> &newDatabases)
{
  databases = newDatabases;
}

QString UserDatabaseList::toXML()
{
  QDomDocument xmlDoc;  

  return xmlDoc.toString();
}

bool UserDatabaseList::fromXML(const QString &xml)
{
  QDomDocument xmlDoc;
 
  if ( !xmlDoc.setContent(xml) )
  {
    return false;
  } 
  
  return true;
}

namespace ConfiguratorObjects
{
  QDataStream &operator<<(QDataStream &out, const UserDatabaseList &databases)
  {
    out << databases.getDatabases()->size();
    for ( QList<ConnectionInfo>::const_iterator i = databases.getDatabases()->constBegin(); i != databases.getDatabases()->constEnd(); i++ )
    {
      out << *i;
    }

    return out;
  }

  QDataStream &operator>>(QDataStream &in, UserDatabaseList &databases)
  {
    int size;
    in >> size;

    databases.getDatabases()->clear();
    for ( int i = 0; i < size; i++ )
    {
      ConnectionInfo cInfo;
      in >> cInfo;
      databases.getDatabases()->append(cInfo);
    }

    return in;
  }
}
