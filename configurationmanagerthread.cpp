#include "configurationmanagerthread.h"

#include "userdatabasemanager.h"

#include <QCoreApplication>
#include <QProcess>
#include <QApplication>

#include "../Core/Commands/systemcmd/systemcommands.h"
#include "../Core/Commands/configcmd/configcommands.h"

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;

ConfigurationManagerThread::ConfigurationManagerThread(IdType controllerId, BaseThread* commandMan):BaseThread(controllerId, CONFIGURATION_MANAGER_THREAD, commandMan)
{
  setObjectName("ConfigurationManager");
  Q_INIT_RESOURCE(configurator);
}

ConfigurationManagerThread::~ConfigurationManagerThread()
{
  for ( QMap<int, Configuration*>::iterator i = configurations.begin(); i != configurations.end(); i++ )
  {
    delete *i;
  }
}

void ConfigurationManagerThread::setRegistry(ServerRegistry *reg)
{
  registry = reg;
}

bool ConfigurationManagerThread::command(Command *c)
{
  switch ( c->type() )
  {
		case Command::Config:
		{
			switch (c->type2())
			{
				case ConfigCommand::Configuration:
				{
					ConfigurationCmd::Kind kind = ((ConfigurationCmd*)c)->getKind();
					switch (kind)
					{
						case ConfigurationCmd::NewConfiguration:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;     
							int result = processCommandNewConfiguration(cmd);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());							answerCmd->setKind(ConfigurationCmd::NewConfiguration);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::RemoveConfiguration:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;     
							int result = processCommandRemoveConfiguration(cmd);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::RemoveConfiguration);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::ConfigurationList:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							QByteArray ba;
							int result = processCommandConfigurationList(cmd, ba);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::ConfigurationList);
							answerCmd->setData(ba);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::Configuration:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							QByteArray ba;
							int result = processCommandConfiguration(cmd, ba);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::Configuration);
							answerCmd->setData(ba);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::SaveConfiguration:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							int result = processCommandSaveConfiguration(cmd);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::SaveConfiguration);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::ReleaseConfiguration:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							int result = processCommandReleaseConfiguration(cmd);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::ReleaseConfiguration);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::NewUserDatabase:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							int result = processCommandNewUserDatabase(cmd);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::NewUserDatabase);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::RemoveUserDatabase:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							int result = processCommandRemoveUserDatabase(cmd);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::RemoveUserDatabase);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::UserDatabaseList:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							QByteArray ba;
							int result = processCommandUserDatabaseList(cmd, ba);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setKind(ConfigurationCmd::UserDatabaseList);
							answerCmd->setData(ba);
							answerCmd->setResult(result);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::DumpUserDatabase:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							QByteArray ba;
							int result = processCommandDumpUserDatabase(cmd, ba);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setData(ba);
							answerCmd->setResult(result);
							answerCmd->setKind(ConfigurationCmd::DumpUserDatabase);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::RestoreUserDatabase:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							QByteArray ba;
							int result = processCommandRestoreUserDatabase(cmd);
							ConfigurationCmd *answerCmd = new ConfigurationCmd(cmd->addressee(), cmd->sender());
							answerCmd->setData(ba);
							answerCmd->setResult(result);
							answerCmd->setKind(ConfigurationCmd::RestoreUserDatabase);
							sendCommand(answerCmd);
						}
						break;

						case  ConfigurationCmd::CloseConfiguration:
						{
							ConfigurationCmd *cmd = (ConfigurationCmd*)c;
							processCommandCloseConfiguration(cmd);
						}
						break;

						default:
							break;
					}
				}
				break;

				default:
					break;
			}
		}

		case Command::System:
		{
			switch(c->type2())
			{
				case SystemCommand::CloseSession:
				{
					CloseSessionCmd *cmd = (CloseSessionCmd*)c;      
					processCommandCloseSession(cmd);
				}
				break;

				default:
					break;
			}
		}
		break;

		default:
		  break;
  }
  /*

    case Command::UserDatabase:
    {
      UserDatabaseCmd *cmd = (UserDatabaseCmd*)c;

      QByteArray ba;
      int result = 0;

      switch ( cmd->getKind() )
      {      
        case 1://dump
        {
          result = processCommandDumpUserDatabase(cmd, ba);
        }
        break;

        case 2://restore
        {
          result = processCommandRestoreUserDatabase(cmd);
        }
        break;

        default:;
      }
     
      UserDatabaseCmd *answerCmd = new UserDatabaseCmd(cmd->addressee(), cmd->sender());
      answerCmd->setData(ba);
      answerCmd->setResult(result);
      answerCmd->setKind(cmd->getKind());
      sendCommand(answerCmd);
    }
    break;

    case Command::DocExchange:
    {
	    //DocExchangeCmd *cmd = dynamic_cast<DocExchangeCmd*>(c);//�������� ������ � ��������
      DocExchangeCmd *cmd = (DocExchangeCmd*)c;
    
      //������������ �������� ������� (������ ������� ����������� � ����������)
      DocExchangeCmd *answerCmd = new DocExchangeCmd(c->addressee(), c->sender());      
      answerCmd->setXMLCommand("");
      sendCommand(answerCmd);
    }
    break;    
  }
*/
  return BaseThread::command(c);
}

bool ConfigurationManagerThread::beforeExec()
{
  //��������� ���������� ����������� � ������ ��������� ������
  dbinfo dbInfo = registry->getDBConnectionInfo();
 
  commonDataStorage = QSqlDatabase::addDatabase("QPSQL", dbInfo.dbname); 

  //��������� ���������� �����������
  commonDataStorage.setHostName(dbInfo.host);    
  commonDataStorage.setDatabaseName(dbInfo.dbname);
  commonDataStorage.setUserName(dbInfo.user);
  commonDataStorage.setPassword(dbInfo.pass);  

  if ( !commonDataStorage.open() )
  {
    DPRINT(QString("Common data storage database openning error!!!\ndatabasename: %1\nhost: %2").arg(dbInfo.dbname).arg(dbInfo.host));
    DPRINT("Trying creation new database...");
   
    QSqlDatabase dbTemplate1 = QSqlDatabase::addDatabase("QPSQL", "template1");
    //��������� ���������� �����������
    dbTemplate1.setHostName(dbInfo.host);    
    dbTemplate1.setDatabaseName("template1");
    dbTemplate1.setUserName(dbInfo.user);
    dbTemplate1.setPassword(dbInfo.pass);

    QSqlQuery template1Query(dbTemplate1);
    QString sql;

    if ( !dbTemplate1.open() )
    {
      DPRINT(dbTemplate1.lastError().text());      
      return BaseThread::beforeExec();     
    }   

    //���������� ������� �� �������� �������� ��
    sql = QString("CREATE DATABASE %1 WITH OWNER = %2 ENCODING = 'UTF8' TABLESPACE = pg_default").arg(dbInfo.dbname).arg(dbInfo.user);
    template1Query.prepare(sql);
    if ( !template1Query.exec() )
    {
      DPRINT(template1Query.lastError().text());
      return BaseThread::beforeExec();
    }

    //����������� � ��������� ��
    if ( commonDataStorage.open() )
    {      
      QSqlQuery cdsQuery(commonDataStorage);
      if ( commonDataStorage.transaction() )
      {   
        sql = "CREATE TABLE Configurations ("
                "Id SERIAL PRIMARY KEY,"
                "Host VARCHAR(256),"
                "Port VARCHAR(5),"
                "DataBaseName VARCHAR(256),"
                "UserName VARCHAR(256),"
                "Password VARCHAR(256)"               
              ");"
              "CREATE TABLE UserDatabases ("
                "Id SERIAL PRIMARY KEY,"
                "Host VARCHAR(256),"
                "Port VARCHAR(5),"
                "DatabaseName VARCHAR(256),"
                "UserName VARCHAR(256),"
                "Password VARCHAR(256)"
              ");";

        cdsQuery.prepare(sql);

        if ( !cdsQuery.exec() )
        {            
          DPRINT(cdsQuery.lastError().text());      
          commonDataStorage.rollback();

          sql = QString("DROP DATABASE %1").arg(dbInfo.dbname);
          template1Query.prepare(sql);

          if ( !template1Query.exec() )
          {
            DPRINT(template1Query.lastError().text());
          }

          return false;
        }    
      }

      commonDataStorage.commit();
    }
    else
    {
      DPRINT(QString("Common data storage database openning error!!!\ndatabasename: %1\nhost: %2").arg(dbInfo.dbname).arg(dbInfo.host));
      DPRINT(commonDataStorage.lastError().text());
      return BaseThread::beforeExec();
    }
  
    dbTemplate1.close();
    //QSqlDatabase::removeDatabase("template1");
    DPRINT(QString("Common data storage database created successful!!!\ndatabasename: %1\nhost: %2").arg(dbInfo.dbname).arg(dbInfo.host));
  }

  //QSqlDatabase::removeDatabase("template1");

  QSqlQuery query(commonDataStorage);
  QString sql;

  //��������� ������ ������������
  sql = "SELECT * FROM Configurations";
  query.prepare(sql);

  if ( !query.exec() )
  {
    DPRINT(query.lastError().text());
    return BaseThread::beforeExec();
  }
  
  QList<ConfigurationInfo> configurationInfoList;
  bool qr = query.first();
  while ( qr )
  {
    ConfigurationInfo configurationInfo;  
    configurationInfo.id = query.record().field("Id").value().toInt();
    configurationInfo.connectionInfo.host = query.record().field("Host").value().toString();
    configurationInfo.connectionInfo.port = query.record().field("Port").value().toString();
    configurationInfo.connectionInfo.dbName = query.record().field("DataBaseName").value().toString();
    configurationInfo.connectionInfo.userName = query.record().field("UserName").value().toString();
    configurationInfo.connectionInfo.password = query.record().field("Password").value().toString();   
    configurationInfo.header.setUserDatabaseId(query.record().field("UserDatabaseId").value().toInt());

    Configuration *newConfiguration = new Configuration();
    if ( newConfiguration->connect(configurationInfo.connectionInfo) )
    {
      if ( newConfiguration->loadHeaderFromDatabase() )
      {
        configurationInfo.header = *newConfiguration->getHeader();
        configurationInfoList.append(configurationInfo);
        configurations.insert(configurationInfo.id, newConfiguration);
      }
     
      newConfiguration->disconnect();
    }
        
    qr = query.next();
  }

  configurationList.setConfigurations(configurationInfoList);

  //��������� ������ ���������������� ��
  sql = "SELECT * FROM UserDatabases";
  query.prepare(sql);

  if ( !query.exec() )
  {
    DPRINT(query.lastError().text());
    return BaseThread::beforeExec();
  }

  QList<ConnectionInfo> userDatabaseConnectionInfoList;
  qr = query.first();
  while ( qr )
  {
    ConnectionInfo cInfo;  
    cInfo.host = query.record().field("Host").value().toString();
    cInfo.port = query.record().field("Port").value().toString();
    cInfo.dbName = query.record().field("DataBaseName").value().toString();
    cInfo.userName = query.record().field("UserName").value().toString();
    cInfo.password = query.record().field("Password").value().toString();  

    userDatabaseConnectionInfoList.append(cInfo);

    qr = query.next();
  }

  userDatabaseList.setDatabases(userDatabaseConnectionInfoList);

  /*DataManager *dm = new UserDatabaseManager();
  ((UserDatabaseManager*)dm)->selfTest();*/

  return BaseThread::beforeExec();  
}

bool ConfigurationManagerThread::afterExec()
{
  if ( commonDataStorage.transaction() )
  {
    DBControl::fillIndex(commonDataStorage, "Configurations");
    commonDataStorage.commit();
  }

  commonDataStorage.close();
  QSqlDatabase::removeDatabase(commonDataStorage.databaseName());
  
  return BaseThread::afterExec();
}

int ConfigurationManagerThread::processCommandNewConfiguration(ConfigurationCmd *cmd)
{
  if ( !commonDataStorage.isOpen() )
  {
    return COMMON_DATA_STORAGE_NOT_OPEN;
  }

  QByteArray ba = cmd->getData();
  QDataStream in(&ba, QIODevice::ReadOnly);

  ConfigurationInfo configInfo;
  in >> configInfo;

  configInfo.connectionInfo.host = commonDataStorage.hostName();
  configInfo.connectionInfo.port = QString("%1").arg(commonDataStorage.port());
  configInfo.connectionInfo.userName = commonDataStorage.userName();
  configInfo.connectionInfo.password = commonDataStorage.password();

  Configuration *newConfiguration = new Configuration();
  if ( !newConfiguration->createDatabase(configInfo.connectionInfo) )
  {
    DPRINT(newConfiguration->getLastError());
    return CREATION_DATABASE_FAILED;
  }

  if ( !newConfiguration->connect(configInfo.connectionInfo) )
  {
    //DPRINT("Connection to new database failed");
    DPRINT(newConfiguration->getLastError());
    return CONNECTION_TO_NEW_DATABASE_FAILED;
  }

  newConfiguration->setHeader(configInfo.header);
  if ( !newConfiguration->saveHeaderToDatabase() )
  {
    //DPRINT("Writing header failed");
    DPRINT(newConfiguration->getLastError());
    return CONFIGURATION_SAVE_TO_DATABASE_FAILED;
  }

  configInfo.id = DBControl::getSequenceMaxValue(commonDataStorage, "Configurations") + 1;

  /*configurations.insert(configInfo.id, newConfiguration);
  configurationList.getConfigurations()->append(configInfo);*/

  if ( !commonDataStorage.transaction() )
  {
    DPRINT(commonDataStorage.lastError().text());
    return COMMON_DATA_STORAGE_TRANSACTION_FAILED;
  }
  
  QSqlQuery query(commonDataStorage);  
  QString sql = "INSERT INTO Configurations (Host, Port, DataBaseName, UserName, Password) "
                "VALUES (:Host, :Port, :DataBaseName, :UserName, :Password)";

  query.prepare(sql);
  query.bindValue(":Host", configInfo.connectionInfo.host);
  query.bindValue(":Port", configInfo.connectionInfo.port);
  query.bindValue(":DataBaseName", configInfo.connectionInfo.dbName);
  query.bindValue(":UserName", configInfo.connectionInfo.userName);
  query.bindValue(":Password", configInfo.connectionInfo.password);  

  if ( !query.exec() )
  {
    DPRINT(query.lastError().text());
    commonDataStorage.rollback();

    sql = QString("DROP DATABASE %1").arg(configInfo.connectionInfo.dbName);
    query.prepare(sql);

    if ( !query.exec() )
    {
      return DROPING_DATABASE_FAILED;
    }

    return COMMON_DATA_STORAGE_INSERT_FAILED;
  }  

  //�������� ���������������� ��
  //if ( createUserDatabase(*configInfo.header.getUserDatabaseConnectionInfo()) /*!= NO_ERRORS*/ )
  //{
  //  //������ ����������� ��������
  //  commonDataStorage.rollback();

  //  sql = QString("DROP DATABASE %1").arg(configInfo.connectionInfo.dbName);
  //  query.prepare(sql);

  //  if ( !query.exec() )
  //  {
  //    return DROPING_DATABASE_FAILED;
  //  }
  //}

  commonDataStorage.commit();

  configurations.insert(configInfo.id, newConfiguration);
  configurationList.getConfigurations()->append(configInfo);

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandRemoveConfiguration(ConfigurationCmd *cmd)
{
  //IdType sessionId = cmd->sender();
  int configurationId = cmd->getId();
  /*QByteArray ba = cmd->getData();
  QDataStream in(ba);
  int configurationId;
  in >> configurationId;*/

  for ( QMap<IdType, int>::iterator i = configurationUsingTable.begin(); i != configurationUsingTable.end(); i++ )
  {
	  //������� �������� ������������ ������!!!
	  if ( i.value() == configurationId ) return CONFIGURATION_ACCESS_DENIED;
  }

  Configuration *curConfiguration = NULL;
  
  if ( !configurations.contains(configurationId) )
  {
    return CONFIGURATION_ACCESS_FAILED;
  }

  curConfiguration = configurations.value(configurationId);
  //configurations.remove(configurationId);

  curConfiguration->disconnect();
  delete curConfiguration;

  ConfigurationInfo configInfo;
  bool flag = false;
  for ( QList<ConfigurationInfo>::iterator i = configurationList.getConfigurations()->begin(); i != configurationList.getConfigurations()->end(); i++ )
  {
    if ( (*i).id == configurationId )
    {
      configInfo = *i;
      flag = true;
      configurationList.getConfigurations()->erase(i);
      break;
    }
  }

  if ( !flag )
  {
    return CONFIGURATION_ACCESS_FAILED;
  }

  QSqlQuery query(commonDataStorage);
  QString sql = QString("DROP DATABASE %1").arg(configInfo.connectionInfo.dbName);  
  query.prepare(sql);

  if ( !query.exec() )
  {    
    return DROPING_DATABASE_FAILED;
  }

  if ( !commonDataStorage.transaction() )
  {
    return COMMON_DATA_STORAGE_TRANSACTION_FAILED;
  }
  
  sql = "DELETE FROM Configurations WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", configInfo.id);

  if ( !query.exec() )
  {
    commonDataStorage.rollback();
    return COMMON_DATA_STORAGE_DELETE_FAILED;
  }  

  commonDataStorage.commit();

  configurations.remove(configurationId);

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandConfigurationList(ConfigurationCmd *cmd, QByteArray &ba)
{
  QDataStream out(&ba, QIODevice::WriteOnly); 
  out << configurationList;

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandConfiguration(ConfigurationCmd *cmd, QByteArray &ba)
{
  //��������� ������������ � �������� �������
  Configuration *curConfiguration = NULL;
  IdType sessionId = cmd->sender();
  int configurationId = cmd->getId();

  if ( configurations.contains(configurationId) )
  {
    if ( !configurationUsingTable.isEmpty() )
    {
      for ( QMap<IdType, int>::iterator i = configurationUsingTable.begin(); i != configurationUsingTable.end(); i++ )
      {       
        if ( (i.value() == configurationId) && ( i.key() != sessionId ) )
        {         
          return CONFIGURATION_ACCESS_DENIED;        
        }
      }
    }       

    curConfiguration = configurations.value(configurationId);

    //���������� ������� ������������ ������ � ������������ ��� ������������  
    configurationUsingTable.insert(sessionId, configurationId);
  }

  if ( !curConfiguration )
  {
    return CONFIGURATION_ACCESS_FAILED;    
  }  

  if ( !curConfiguration->connect(curConfiguration->getConnectionInfo()) )
  {
    return CONFIGURATION_CONNECTION_FAILED;
  }

  if ( !curConfiguration->loadObjectsFromDatabase() )
  {
    return CONFIGURATION_LOADIND_OBJECTS_FAILED;
  }

  //������������ �������������� ������
  QDataStream out(&ba, QIODevice::WriteOnly);
  out << *curConfiguration->getObjectList();

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandSaveConfiguration(ConfigurationCmd *cmd)
{
  IdType sessionId = cmd->sender();

  //�������� ����������
  if ( !configurationUsingTable.contains(sessionId) )
  { 
    return CONFIGURATION_ACCESS_DENIED;
  }

  //��������� ��������������� ������������
  Configuration *curConfiguration = NULL;
  int cId = configurationUsingTable.value(sessionId);  

  int id = -1;
  for ( QMap<int, Configuration*>::iterator i = configurations.begin(); i != configurations.end(); i++ )
  {
    if ( i.key() == cId )
    {
      id = i.key();
      break;
    }
  }

  if ( id == -1 )
  {
    return ASSOCIATED_CONFIGURATION_NOT_FOUND;    
  }

  curConfiguration = configurations.value(cId);

  if ( !curConfiguration )
  {
    return ASSOCIATED_CONFIGURATION_NOT_FOUND;
  }
 
  QByteArray ba = cmd->getData();
  QDataStream in(&ba, QIODevice::ReadOnly);

  ConfigurationHeader newHeader;
  in >> newHeader;
  curConfiguration->setHeader(newHeader);

  ObjectList newObjectList;
  in >> newObjectList;
  curConfiguration->setObjectList(newObjectList);
  
  if ( !curConfiguration->saveHeaderToDatabase() )
  {
    DPRINT(curConfiguration->getLastError());
    return CONFIGURATION_SAVE_TO_DATABASE_FAILED;
  }

  if ( !curConfiguration->saveObjectsToDatabase() )
  {
    DPRINT(curConfiguration->getLastError());
    return CONFIGURATION_SAVE_TO_DATABASE_FAILED;
  }

  for ( QList<ConfigurationInfo>::iterator i = configurationList.getConfigurations()->begin(); i != configurationList.getConfigurations()->end(); i++ )
  {
    if ( (*i).id == cId )
    {
      (*i).header = newHeader;
    }
  }  

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandReleaseConfiguration(ConfigurationCmd *cmd)
{ 
  IdType sessionId = cmd->sender();
  int configurationId = cmd->getId(); 

  for ( QMap<IdType, int>::iterator i = configurationUsingTable.begin(); i != configurationUsingTable.end(); i++ )
  {
    //�������� ������������� ������������ ������ �������
    if ( (i.value() == configurationId) && (i.key() != sessionId) ) return CONFIGURATION_ACCESS_DENIED;
  }

  Configuration *curConfiguration = NULL;

  if ( !configurations.contains(configurationId) )
  {
    return CONFIGURATION_ACCESS_FAILED;
  }

  curConfiguration = configurations.value(configurationId);

  ConnectionInfo cInfo;
  QByteArray ba = cmd->getData();
  QDataStream in(&ba, QIODevice::ReadOnly);
  in >> cInfo; 

  if ( !curConfiguration->release(cInfo) )
  {
    DPRINT(curConfiguration->getLastError());
    return CONFIGURATION_RELEASE_FAILED;
  }

  return NO_ERRORS;
}

int ConfigurationManagerThread::createUserDatabase(const ConnectionInfo &cInfo)
{  
  QSqlDatabase dbTemplate1 = QSqlDatabase::addDatabase("QPSQL", "template1");  
  dbTemplate1.setHostName(cInfo.host);    
  dbTemplate1.setDatabaseName("template1");
  dbTemplate1.setUserName(cInfo.userName);
  dbTemplate1.setPassword(cInfo.password);

  if ( !dbTemplate1.open() )
  {
    DPRINT(dbTemplate1.lastError().text());      
    return DATABASE_TEMPLATE1_CONNECTION_FAILED;     
  }

  QSqlQuery template1Query(dbTemplate1);
  QString sql = QString("CREATE DATABASE %1 WITH OWNER = %2 ENCODING = 'UTF8' TABLESPACE = pg_default TEMPLATE template0").arg(cInfo.dbName).arg(cInfo.userName);

  template1Query.prepare(sql);

  if ( !template1Query.exec() )
  {
    DPRINT(template1Query.lastError().text());
    return CREATION_DATABASE_FAILED;
  }  

  QSqlDatabase userDB = QSqlDatabase::addDatabase("QPSQL", cInfo.dbName);
  userDB.setHostName(cInfo.host); 
  userDB.setDatabaseName(cInfo.dbName);
  userDB.setUserName(cInfo.userName);
  userDB.setPassword(cInfo.password);

  if ( !userDB.open() )
  {
    DPRINT(userDB.lastError().text());
    return CONNECTION_TO_NEW_DATABASE_FAILED;
  }

  QSqlQuery userDBQuery(userDB);
  sql = "CREATE TABLE SysInfo ("
          "Id SERIAL PRIMARY KEY,"
          "SystemName VARCHAR(512),"
          "Indexes VARCHAR(2),"
          "ParentId INT,"
          "Kind VARCHAR(32),"
          "DataType VARCHAR(128),"
          "Limits VARCHAR(1024),"
          "LinkedObjectName VARCHAR(1024),"
          "LinkedObjectId INT,"
          "Translation_1 VARCHAR(1024)"
          ");"

        "CREATE INDEX SysInfo_SystemName_Idx ON SysInfo (SystemName);"
        "CREATE INDEX SysInfo_Kind_Idx ON SysInfo (Kind);"
        "CREATE INDEX SysInfo_ParentId_Idx ON SysInfo (ParentId);"
        "CREATE INDEX SysInfo_DataType_Idx ON SysInfo (DataType);"

        "CREATE TABLE UserTables ("
          "Id SERIAL PRIMARY KEY,"
          "SystemName VARCHAR(512),"
          "OwnerId INT,"
          "CreationDateTime TIMESTAMP"
        ");";

  userDBQuery.prepare(sql);

  if ( !userDBQuery.exec() )
  {
    DPRINT(userDBQuery.lastError().text());

    sql = QString("DROP DATABASE %1").arg(cInfo.dbName);
    template1Query.prepare(sql);

    if ( !template1Query.exec() )
    {
      return DROPING_DATABASE_FAILED;
    }

    return USER_DATABASE_CREATION_FAILED;
  }

  userDB.close();
  dbTemplate1.close();
  QSqlDatabase::removeDatabase(cInfo.dbName);

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandNewUserDatabase(ConfigurationCmd *cmd)
{
  QByteArray ba = cmd->getData();
  QDataStream in(ba);

  ConnectionInfo cInfo;
  in >> cInfo;

  cInfo.host = commonDataStorage.hostName();
  cInfo.port = QString("%1").arg(commonDataStorage.port());
  cInfo.userName = commonDataStorage.userName();
  cInfo.password = commonDataStorage.password();

  if ( !commonDataStorage.transaction() )
  {
    return COMMON_DATA_STORAGE_TRANSACTION_FAILED;
  }

  QSqlQuery query(commonDataStorage);

  QString sql = "INSERT INTO UserDatabases (Host, Port, DataBaseName, UserName, Password) "
                "VALUES (:Host, :Port, :DataBaseName, :UserName, :Password)";

  query.prepare(sql);
  query.bindValue(":Host", cInfo.host);
  query.bindValue(":Port", cInfo.port);
  query.bindValue(":DataBaseName", cInfo.dbName);
  query.bindValue(":UserName", cInfo.userName);
  query.bindValue(":Password", cInfo.password);  

  if ( !query.exec() )
  {
    DPRINT(query.lastError().text());
    commonDataStorage.rollback();  

    return COMMON_DATA_STORAGE_INSERT_FAILED;
  }

  if ( createUserDatabase(cInfo) != NO_ERRORS )
  {   
    return USER_DATABASE_CREATION_FAILED;
  }

  commonDataStorage.commit();

  userDatabaseList.getDatabases()->append(cInfo);

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandRemoveUserDatabase(ConfigurationCmd *cmd)
{
  int udbId = cmd->getId();

  if ( !commonDataStorage.transaction() )
  {
    return COMMON_DATA_STORAGE_TRANSACTION_FAILED;
  }

  QSqlQuery query(commonDataStorage);

  QString sql = "SELECT * FROM UserDatabases WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", udbId);

  if ( !query.exec() )
  {
    DPRINT(query.lastError().text());
  }

  bool qr = query.first();
  ConnectionInfo cInfo;
  cInfo.host     = query.record().field("Host").value().toString();
  cInfo.port     = query.record().field("Port").value().toString();  
  cInfo.dbName   = query.record().field("DataBaseName").value().toString();
  cInfo.userName = query.record().field("UserName").value().toString();
  cInfo.password = query.record().field("Password").value().toString();

  sql = "DELETE FROM UserDatabases WHERE Id = :Id";

  query.prepare(sql);
  query.bindValue(":Id", udbId);

  if ( !query.exec() )
  {
    DPRINT(query.lastError().text());
    commonDataStorage.rollback();  

    return COMMON_DATA_STORAGE_DELETE_FAILED;
  }

  //�������� ���������������� �� 
  //QSqlDatabase template1Db = QSqlDatabase::database("template1", false);
  QSqlDatabase template1Db = QSqlDatabase::addDatabase("QPSQL", "template1");
  template1Db.setHostName(cInfo.host);    
  template1Db.setDatabaseName("template1");
  template1Db.setUserName(cInfo.userName);
  template1Db.setPassword(cInfo.password);

  if ( !template1Db.open() )
  {
    DPRINT(template1Db.lastError().text());
    return DATABASE_TEMPLATE1_CONNECTION_FAILED;
  }

  QSqlQuery template1Query(template1Db);
  sql = QString("DROP DATABASE %1").arg(cInfo.dbName);

  template1Query.prepare(sql);

  if ( !template1Query.exec() )
  {
    DPRINT(template1Query.lastError().text());
    return DROPING_DATABASE_FAILED;
  }

  template1Db.close();

  DBControl::fillIndex(commonDataStorage, "UserDatabases");

  commonDataStorage.commit();

  userDatabaseList.getDatabases()->removeAt(udbId - 1);  

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandUserDatabaseList(ConfigurationCmd *cmd, QByteArray &ba)
{
  QDataStream out(&ba, QIODevice::WriteOnly); 
  out << userDatabaseList;

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandDumpUserDatabase(ConfigurationCmd *cmd, QByteArray &ba)
{
  //����� �� ��� ���������� �����
  int udbId = cmd->getId();
  ConnectionInfo cInfo = userDatabaseList.getDatabases()->value(udbId - 1);
  
  /*for ( QList<ConnectionInfo>::iterator i = userDatabaseList.getDatabases()->begin(); i != userDatabaseList.getDatabases()->end(); i++ )
  {
    if ( (*i).id )
    {
    }
  }*/
  //--------------------------------------

  QProcess process;

  QStringList env = QProcess::systemEnvironment();   
  env.append(QString("PGUSER=%1").arg(commonDataStorage.userName()));
  env.append("PGDATABASE=template1");

  process.setEnvironment(env);

  QStringList args; 
  args.append("-Fc");
  args.append(cInfo.dbName);

  process.start("pg_dump", args);

  if ( !process.waitForStarted() )
  {
    qDebug("Error starting pg_dump");
    return DUMPING_DATABASE_FAILED;
  }    

  if ( !process.waitForFinished() )
  {      
    qDebug("Error finishing pg_dump");
    return DUMPING_DATABASE_FAILED;
  }
 
  ba = process.readAll();   

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandRestoreUserDatabase(ConfigurationCmd *cmd)
{
  int udbId = cmd->getId();
  ConnectionInfo cInfo = userDatabaseList.getDatabases()->value(udbId - 1);
  
  QString fname = QString("%1\\%2").arg(QApplication::applicationDirPath()).arg("df.tmp");

  QFile tmp(fname);
  if ( !tmp.open(QIODevice::WriteOnly | QIODevice::Truncate) )
  {
    return -1;//TEMPORARY_FILE_OPEN_FAILED;
  }

  tmp.write(cmd->getData());
  tmp.close();  

  QSqlQuery query(commonDataStorage);
  QString sql = QString("DROP DATABASE %1;CREATE DATABASE %1 WITH TEMPLATE template0;").arg(cInfo.dbName);//��������� 4
  query.prepare(sql);

  if ( !query.exec() )
  {
    qDebug(query.lastError().text().toLocal8Bit().data());
    return RECREATED_DATABASE_FAILED;
  }  

  QProcess process;

  QStringList env = QProcess::systemEnvironment();   
  env.append(QString("PGUSER=%1").arg(commonDataStorage.userName()));
  env.append("PGDATABASE=template1");

  process.setEnvironment(env);  

  QStringList args;
  args.append("-d");
  args.append(cInfo.dbName);
  args.append(fname); 

  process.start("pg_restore", args);

  if ( !process.waitForStarted() )
  {
    qDebug("pg_restore starting failed");
    return RESTORING_DATABASE_FAILED;
  }

  if ( !process.waitForFinished() )
  {      
    qDebug("pg_restore finishing failed");
    return RESTORING_DATABASE_FAILED;
  } 

  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandCloseConfiguration(ConfigurationCmd *cmd)
{
  IdType sessionId = cmd->sender();

  //�������� ����������
  if ( !configurationUsingTable.contains(sessionId) )
  { 
    return CONFIGURATION_ACCESS_DENIED;
  }

  //��������� ��������������� ������������
  Configuration *curConfiguration = NULL;
  int cId = configurationUsingTable.value(sessionId);

  int id = -1;
  for ( QMap<int, Configuration*>::iterator i = configurations.begin(); i != configurations.end(); i++ )
  {
    if ( i.key() == cId )
    {
      id = i.key();
      break;
    }
  }

  if ( id == -1 )
  {
    return ASSOCIATED_CONFIGURATION_NOT_FOUND;    
  }

  curConfiguration = configurations.value(cId);

  if ( !curConfiguration )
  {
    return ASSOCIATED_CONFIGURATION_NOT_FOUND;
  }

  curConfiguration->disconnect();  
  configurationUsingTable.remove(sessionId);
  
  return NO_ERRORS;
}

int ConfigurationManagerThread::processCommandCloseSession(CloseSessionCmd *cmd)
{
  IdType sessionId = cmd->sender();

  //�������� ����������
  if ( !configurationUsingTable.contains(sessionId) )
  { 
    return CONFIGURATION_ACCESS_DENIED;
  }

  //��������� ��������������� ������������
  Configuration *curConfiguration = NULL;
  int cId = configurationUsingTable.value(sessionId);  

  int id = -1;
  for ( QMap<int, Configuration*>::iterator i = configurations.begin(); i != configurations.end(); i++ )
  {
    if ( i.key() == cId )
    {
      id = i.key();
      break;
    }
  }

  if ( id == -1 )
  {
    return ASSOCIATED_CONFIGURATION_NOT_FOUND;    
  }

  curConfiguration = configurations.value(cId);

  if ( !curConfiguration )
  {
    return ASSOCIATED_CONFIGURATION_NOT_FOUND;
  }

  curConfiguration->disconnect();  
  configurationUsingTable.remove(sessionId);

  return NO_ERRORS;
}
