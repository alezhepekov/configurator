/***********************************************************************
* Module:   table.h
* Author:   LGP
* Modified: 16 ����� 2007 �.
* Purpose:  ������������� ������� ������� ��
***********************************************************************/

#ifndef COLUMN_H
#define COLUMN_H

#include "index.h"
#include <QString>

#include "documents/accessory.h"

namespace ConfiguratorObjects
{
  namespace SQL
  {
    class Column
    {
    public:    
      Column(const QString &columnName, const Documents::Accessory &value, const Index &indexValue);
      ~Column();

      QString getName();
      void setName(const QString& newName);

      Documents::Accessory *getAccessory();
      void setAccessory(const Documents::Accessory& newAccessory);

      void setIndex(const Index &newIndex);
      Index *getIndex(); 

    protected:
    private:
      QString name;
      Documents::Accessory accessory;
      Index index;
    };
  }
}

#endif
