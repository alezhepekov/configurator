#include "index.h"

using namespace ConfiguratorObjects::SQL;

Index::Index()
{
  kind = KindNone;
}

Index::Index(enum IndexKind indexKind)
{
  kind = indexKind;
}

Index::~Index()
{
}

enum Index::IndexKind Index::getKind()
{
  return kind;
}

void Index::setKind(enum IndexKind newKind)
{
  kind = newKind;
}

QString Index::getReferenceTableForForeignKey()
{
  return referenceTableForForeignKey;
}

void Index::setReferenceTableForForeignKey(const QString &newReferenceTableForForeignKey)
{
  referenceTableForForeignKey = newReferenceTableForForeignKey;
}

QString Index::getReferenceColumnForForeignKey()
{
  return referenceColumnForForeignKey;
}

void Index::setReferenceColumnForForeignKey(const QString &newReferenceColumnForForeignKey)
{
  referenceColumnForForeignKey = newReferenceColumnForForeignKey;
}
