/***********************************************************************
 * Module:   configurationmanagerthread.h
 * Author:   LGP
 * Modified: 30 ������ 2007 �.
 * Purpose:  ����� ������������� ��������� ������ �������������
 ***********************************************************************/

#ifndef CONFIGURATION_MANAGER_THREAD_H
#define CONFIGURATION_MANAGER_THREAD_H

#include <QString>
#include <QMap>
#include <QList>

#include "../Core/Threads/basethread.h"
#include "../Core/RegistryManager/serverregistry.h"

#include "../Core/defines.h"

#include "configuration.h"
#include "configurationlist.h"
#include "userdatabaselist.h"

class ConfigurationCmd;
class CloseSessionCmd;

namespace ConfiguratorObjects
{
  class ConfigurationManagerThread : public BaseThread
  {
    Q_OBJECT

    //������ ���������� � ��������� ��� ����������� �������������
    ConfigurationList configurationList;

    //������ ���������������� ��
    UserDatabaseList userDatabaseList;

    //QMap<int, ConnectionInfo> userDatabases;

    //������ ��������� ��� ����������� ������������
    QMap<int, Configuration*> configurations;

    //������� ������������ ��������������� ������ � ��������������� ��������������� ������������
    QMap<IdType, int> configurationUsingTable;

    //����� ��������� ������
    QSqlDatabase commonDataStorage;

  public:
    static const int NO_ERRORS                              = 0x0000;  
    static const int CONFIGURATION_ACCESS_FAILED            = 0x0001;
    static const int ASSOCIATED_CONFIGURATION_NOT_FOUND     = 0x0002;
    static const int CONFIGURATION_SAVE_TO_DATABASE_FAILED  = 0x0003;
    static const int CONFIGURATION_ACCESS_DENIED            = 0x0004;
    static const int CONFIGURATION_RELEASE_FAILED           = 0x0005;
    static const int OBJECT_LIST_FORMAT_INVALID             = 0x0006;
    static const int CREATION_DATABASE_FAILED               = 0x0007;
    static const int COMMON_DATA_STORAGE_NOT_OPEN           = 0x0008;
    static const int COMMON_DATA_STORAGE_INSERT_FAILED      = 0x0009;
    static const int COMMON_DATA_STORAGE_DELETE_FAILED      = 0x000A;
    static const int COMMON_DATA_STORAGE_TRANSACTION_FAILED = 0x000B;
    static const int CONNECTION_TO_NEW_DATABASE_FAILED      = 0x000C;
    static const int DROPING_DATABASE_FAILED                = 0x000D;
    static const int USER_DATABASE_CREATION_FAILED          = 0x000E;
    static const int DATABASE_TEMPLATE1_CONNECTION_FAILED   = 0x000F;
    static const int CONFIGURATION_CONNECTION_FAILED        = 0x0010;
    static const int CONFIGURATION_LOADIND_OBJECTS_FAILED   = 0x0011;
    static const int DUMPING_DATABASE_FAILED                = 0x0012;
    static const int RESTORING_DATABASE_FAILED              = 0x0013;
    static const int RECREATED_DATABASE_FAILED              = 0x0014;

    ConfigurationManagerThread(IdType controllerId, BaseThread* commandMan = 0);
    ~ConfigurationManagerThread();

    void setRegistry(ServerRegistry *);

    /*�����  ��������� ���������-������*/
    bool command(Command*);

  protected:
    /* ����� ������������ ���������� �� ������ � run exec(���������� �� run). ����� ���� ������������� � �����������.*/
    virtual bool beforeExec();
    
    /* �����, ���������� �� run ���� ������ �� exec(���������� �� run). ����� ���� ������������� � �����������.*/
    virtual bool afterExec();

    //������ �������
    ServerRegistry *registry;   

    //����� ��������� ��������� ������� NewConfiguration
    int processCommandNewConfiguration(ConfigurationCmd *);

    //����� ��������� ��������� ������� RemoveConfiguration
    int processCommandRemoveConfiguration(ConfigurationCmd *);
    
    //����� ��������� ��������� ������� ConfigurationList
    int processCommandConfigurationList(ConfigurationCmd *, QByteArray &);
    
    //����� ��������� ��������� ������� Configuration
    int processCommandConfiguration(ConfigurationCmd *, QByteArray &);
    
    //����� ��������� ��������� ������� SaveConfiguration
    int processCommandSaveConfiguration(ConfigurationCmd *);
    
    //����� ��������� ��������� ������� ReleaseConfiguration
    int processCommandReleaseConfiguration(ConfigurationCmd *);

    int processCommandCloseConfiguration(ConfigurationCmd *);

    int createUserDatabase(const ConnectionInfo &);

    int processCommandNewUserDatabase(ConfigurationCmd *);

    int processCommandRemoveUserDatabase(ConfigurationCmd *);

    int processCommandUserDatabaseList(ConfigurationCmd *, QByteArray &);

    int processCommandDumpUserDatabase(ConfigurationCmd *, QByteArray &);

    int processCommandRestoreUserDatabase(ConfigurationCmd *);

    //����� ��������� ��������� ��������� ������ ������
    int processCommandCloseSession(CloseSessionCmd *);    
  };
}

#endif
