/***********************************************************************
* Module:   xmlcommand.h
* Author:   LGP
* Modified: 25 ������ 2007 �.
* Purpose:  ����� ������������� xml-�������
***********************************************************************/

#ifndef XML_COMMAND_H
#define XML_COMMAND_H

#include <QList>
#include <QString>
#include <QStringList>
#include <QVariant>
#include <QDateTime>

#include "domxml.h"

class XMLCommand: public Documents::DomXml
{
public:
  enum XMLCommandType
  {
    VoidCommand = 0,
    GetConfigurationList = 1,
    GetConfiguration = 2,
    SetConfiguration = 3,
    ReleaseConfiguration = 4
  };

  enum XMLCommandResultType
  {
    Empty,
    ReturnedValue,
    Error  
  };

  enum XMLValueType
  {
    TVoid,
    TInt,
    TString,
    TBool,
    TDateTime  
  };

  struct XMLError
  {
    int code;
    QString msg;
  };

  struct XMLValue
  {
    enum XMLValueType type;
    QVariant value;
  };

  struct XMLParameter
  {
    QString name;
    XMLValue value;
  };
  
  XMLCommand(const XMLCommandType&);
  XMLCommand(const int id, const XMLCommandType& type);

  int getId();

  XMLCommandType getType();

  XMLCommandResultType getResultType();  

  QString toXML();

  bool fromXML(const QString&);

  void addParameter(const XMLParameter&);

  void removeParameter(const int);

  XMLParameter getParameter(const int);

  int getParametersCount();

  void parametersClear();
  
  void addError(const XMLError&);  

  void removeError(const int);

  XMLError getError(const int);

  int getErrorsCount();

  void errorsClear();

  void addReturnedValue(const XMLValue&);

  void removeReturnedValue(const int);

  XMLValue getReturnedValue(const int);

  int getReturnedValuesCount();

  void returnedValuesClear();

private:
  int id;

  enum XMLCommandType type;

  enum XMLCommandResultType resultType;

  QList<XMLParameter> parameters;

  QList<XMLError> errors;

  QList<XMLValue> returnedValues;

  QDomDocument xmlCommand;
};

#endif
