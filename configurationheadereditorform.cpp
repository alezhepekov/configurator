#include "configurationheadereditorform.h"

using namespace ConfiguratorObjects;

ConfigurationHeaderEditorForm::ConfigurationHeaderEditorForm(QWidget *parent)
:QDialog(parent, Qt::WindowTitleHint|Qt::WindowSystemMenuHint)
{  
  setupUi(this);
}

ConfigurationHeaderEditorForm::~ConfigurationHeaderEditorForm()
{
}

bool ConfigurationHeaderEditorForm::event(QEvent *e)
{
  if ( e->type() == QEvent::LanguageChange )
    retranslateUi(this);

  return QWidget::event(e);
}

ConfigurationHeader ConfigurationHeaderEditorForm::getHeader()
{
  configurationHeader.setName(leName->text());
  configurationHeader.setAuthor(leAuthor->text());
  configurationHeader.setVersion(leVersion->text());

  if ( cbUserDatabase->currentIndex() != -1 )
  {
    configurationHeader.setUserDatabaseId(cbUserDatabase->currentIndex() + 1);
  }

  return configurationHeader;
}

void ConfigurationHeaderEditorForm::setHeader(const ConfigurationHeader &header)
{
  configurationHeader = header;

  leName->setText(header.getName());
  leAuthor->setText(header.getAuthor());
  leVersion->setText(header.getVersion());
  dteCreationDateTime->setDateTime(header.getCreationDateTime());
  dteLastModificationDateTime->setDateTime(header.getLastModificationDateTime());

  if ( !userDatabaseList.getDatabases()->isEmpty() )
  {
    int udbId = header.getUserDatabaseId() - 1;
    if ( (userDatabaseList.getDatabases()->size() > 0) && (udbId <= userDatabaseList.getDatabases()->size() - 1) )
    {
      cbUserDatabase->setCurrentIndex(udbId);
    }
  }
}

void ConfigurationHeaderEditorForm::setUserDatabaseList(const UserDatabaseList &userDatabases)
{
  userDatabaseList = userDatabases;

  for ( QList<ConnectionInfo>::iterator i = userDatabaseList.getDatabases()->begin(); i != userDatabaseList.getDatabases()->end(); i++ )
  {   
    cbUserDatabase->addItem((*i).dbName);
  }

  cbUserDatabase->setCurrentIndex(-1);
}
