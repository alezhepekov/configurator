#include "scriptgenerator.h"

using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;
using namespace ConfiguratorObjects::SQL;

ScriptGenerator::ScriptGenerator()
{
}

ScriptGenerator::~ScriptGenerator()
{  
  for ( QList<Table*>::iterator i = tables.begin(); i != tables.end(); i++ )
  {
    delete *i;
  }

  for ( QMap<QString, QList<Column*> >::iterator i = addingColumns.begin(); i != addingColumns.end(); i++ )
  {
    for ( QList<Column*>::iterator j = i.value().begin(); j != i.value().end(); j++ )
    {
      delete *j;
    }
  } 
}

void ScriptGenerator::addTable(Table *table)
{  
  tables.append(table);
}

void ScriptGenerator::removeTable(const QString &name)
{
  for ( QList<Table*>::iterator i = tables.begin(); i != tables.end(); i++ )
  {
    if ( (*i)->getName().compare(name) == 0 )
    {
      tables.erase(i);
    }
  } 
}

void ScriptGenerator::dropTable(const QString &name)
{
  dropingTables.append(name);  
}

Table * ScriptGenerator::getTable(const QString &name)
{
  for ( QList<Table*>::iterator i = tables.begin(); i != tables.end(); i++ )
  {
    if ( (*i)->getName().compare(name) == 0 )
    {
      return *i;
    }
  }

  return NULL;
}

void ScriptGenerator::addColumn(const QString &tableName, Column *column)
{  
  QList<Column*> columns = addingColumns.value(tableName);
  columns.append(column);
  addingColumns.insert(tableName, columns);
}

void ScriptGenerator::dropColumn(const QString &tableName, const QString &name)
{
  QList<QString> tList = dropingColumns.value(tableName);
  tList.append(name);
  dropingColumns.insert(tableName, tList);  
}

QList<Table*> * ScriptGenerator::getTables()
{
  return &tables;
}

QList<QString> * ScriptGenerator::getDropingTables()
{
  return &dropingTables;
}

QMap<QString, QList<QString> > * ScriptGenerator::getDropingColumns()
{
  return &dropingColumns;
}

QMap<QString, QList<Column*> > * ScriptGenerator::getAddingColumns()
{
  return &addingColumns;
}

QString ScriptGenerator::generateColumn(Column *column)
{
  if ( !column ) return QString("");
  if ( !column->getAccessory()->getType() ) return QString("");  
 
  QString sql;

  QString typeDefinition;
  QString check;
  QString index;

  Accessory *accessory = column->getAccessory();
 
  switch ( accessory->getType()->getTypeKind() )
  {
    case DATA_TYPE_NUMBER:
    {        
      Number *type = dynamic_cast<Number*> (accessory->getType());
      if ( type )
      {
        typeDefinition = QString("Numeric(%1, %2)").arg(type->getLength()).arg(type->getPrecision());
        check = QString("CHECK( \"%1\" >= %2 AND \"%1\" <= %3 )").arg(column->getName()).arg(type->getMin()).arg(type->getMax());

        if ( type->getRequired() )
        {
          typeDefinition.append(" NOT NULL");
        }

        typeDefinition.append(QString(" DEFAULT %1").arg(type->getDefault()));

        if ( column->getIndex()->getKind() == Index::KindPrimaryKey )
        {
          index = " PRIMARY KEY";
        }
        else
        {
          if ( column->getIndex()->getKind() == Index::KindForeignKey )
          {
            index = QString("REFERENCES \"%1\" (\"%2\")").arg(column->getIndex()->getReferenceTableForForeignKey()).arg(column->getIndex()->getReferenceColumnForForeignKey());
          }
        }
      }
    }
    break;

    case DATA_TYPE_INTEGER:
    {        
      Integer *type = dynamic_cast<Integer*> (accessory->getType());
      if ( type )
      {          
        typeDefinition = QString("Int4");
        check = QString("CHECK( \"%1\" >= %2 AND \"%1\" <= %3 )").arg(column->getName()).arg(type->getMin()).arg(type->getMax());

        if ( type->getRequired() )
        {
          typeDefinition.append(" NOT NULL");
        }

        typeDefinition.append(QString(" DEFAULT %1").arg(type->getDefault()));

        if ( column->getIndex()->getKind() == Index::KindPrimaryKey )
        {
          index = "PRIMARY KEY";
        }
        else
        {
          if ( column->getIndex()->getKind() == Index::KindForeignKey )
          {
            index = QString("REFERENCES \"%1\" (\"%2\")").arg(column->getIndex()->getReferenceTableForForeignKey()).arg(column->getIndex()->getReferenceColumnForForeignKey());
          }
        }
      }
    }
    break;

    case DATA_TYPE_FLOAT:
    {        
      Float *type = dynamic_cast<Float*> (accessory->getType());
      if ( type )
      {          
        typeDefinition = QString("Float8");
        check = QString("CHECK( \"%1\" >= %2 AND \"%1\" <= %3 )").arg(column->getName()).arg(type->getMin()).arg(type->getMax());

        if ( type->getRequired() )
        {
          typeDefinition.append(" NOT NULL");
        }

        typeDefinition.append(QString(" DEFAULT %1").arg(type->getDefault()));

        if ( column->getIndex()->getKind() == Index::KindPrimaryKey )
        {
          index = "PRIMARY KEY";
        }
        else
        {
          if ( column->getIndex()->getKind() == Index::KindForeignKey )
          {
            index = QString("REFERENCES \"%1\" (\"%2\")").arg(column->getIndex()->getReferenceTableForForeignKey()).arg(column->getIndex()->getReferenceColumnForForeignKey());
          }
        }
      }
    }
    break;

    case DATA_TYPE_STRING:
    {
      String *type = dynamic_cast<String*> (accessory->getType());
      if ( type )
      {
        typeDefinition = QString("Varchar(%1)").arg(type->getLength());
        //check = QString("CHECK(Length(%1) <= %2").arg(column->getName()).arg(type->getLength());

        if ( type->getRequired() )
        {
          typeDefinition.append(" NOT NULL");
        }

        if ( !type->getDefault().isEmpty() )
        {
          typeDefinition.append(QString(" DEFAULT '%1'").arg(type->getDefault()));
        }        

        if ( column->getIndex()->getKind() == Index::KindPrimaryKey )
        {
          index = "PRIMARY KEY";
        }
        else
        {
          if ( column->getIndex()->getKind() == Index::KindForeignKey )
          {
            index = QString("REFERENCES \"%1\" (\"%2\")").arg(column->getIndex()->getReferenceTableForForeignKey()).arg(column->getIndex()->getReferenceColumnForForeignKey());
          }
        }
      }
    }
    break;

    case DATA_TYPE_DATETIME:
    {
      DateTime *type = dynamic_cast<DateTime*> (accessory->getType());
      if ( type )
      {              
        typeDefinition = QString("TimeStamp");
        //check = QString("CHECK()").arg();

        if ( type->getRequired() )
        {
          typeDefinition.append(" NOT NULL");
        }

        typeDefinition.append(QString(" DEFAULT '%1'").arg(type->getDefault().toString("yyyy.MM.dd hh:mm:ss")));

        if ( column->getIndex()->getKind() == Index::KindPrimaryKey )
        {
          index = "PRIMARY KEY";
        }
        else
        {
          if ( column->getIndex()->getKind() == Index::KindForeignKey )
          {
            index = QString("REFERENCES \"%1\" (\"%2\")").arg(column->getIndex()->getReferenceTableForForeignKey()).arg(column->getIndex()->getReferenceColumnForForeignKey());
          }
        }
      }
    }
    break;

    case DATA_TYPE_BOOLEAN:
    {
      Logical *type = dynamic_cast<Logical*> (accessory->getType());
      if ( type )
      {
        typeDefinition = QString("Bool");
        //check = QString("CHECK()").arg();

        if ( type->getRequired() )
        {
          typeDefinition.append(" NOT NULL");
        }

        QString boolString;
        if ( type->getDefault() )
        {
          boolString = "TRUE";
        }
        else
        {
          boolString = "FALSE";
        }

        typeDefinition.append(QString(" DEFAULT %1").arg(boolString));

        if ( column->getIndex()->getKind() == Index::KindPrimaryKey )
        {
          index = "PRIMARY KEY";
        }
        else
        {
          if ( column->getIndex()->getKind() == Index::KindForeignKey )
          {
            index = QString("REFERENCES \"%1\" (\"%2\")").arg(column->getIndex()->getReferenceTableForForeignKey()).arg(column->getIndex()->getReferenceColumnForForeignKey());
          }
        }
      }
    }
    break;

    case DATA_TYPE_LINK:
    {
      Link *type = dynamic_cast<Link*> (accessory->getType());

      if ( type )
      {
        typeDefinition = QString("Int4");

        if ( column->getIndex()->getKind() == Index::KindPrimaryKey )
        {
          index = "PRIMARY KEY";
        }
        else
        {
          if ( column->getIndex()->getKind() == Index::KindForeignKey )
          {
            QString refTableName = column->getIndex()->getReferenceTableForForeignKey();
            QString refColumnName = column->getIndex()->getReferenceColumnForForeignKey();
            
            if ( !refTableName.isEmpty() && !refColumnName.isEmpty() )
            {
              index = QString("REFERENCES \"%1\" (\"%2\")").arg(refTableName).arg(refColumnName);
            }            
          }
        }
      }      
    }
    break;

    default:;
  }

  sql.append(QString("\"%1\" %2 %3 %4").arg(column->getName()).arg(typeDefinition).arg(check).arg(index));

  return sql;
}

QString ScriptGenerator::generateTable(Table *table)
{
  QString sql;
  sql.append(QString("CREATE TABLE \"%1\" (\n").arg(table->getName()));
  if ( table->getColumns()->isEmpty() )
  {
    sql.append("\"Id\" SERIAL PRIMARY KEY\n");
  }
  else
  {
    sql.append("\"Id\" SERIAL PRIMARY KEY ,\n");
  }  

  QString comma;
  for ( QList<Column*>::iterator i = table->getColumns()->begin(); i != table->getColumns()->end(); i++ )
  {
    Column *column = *i;

    if ( column->getIndex()->getKind() == Index::KindIndex )
    {
      //���������� ������� ��������
      indexes.insert(table->getName(), column->getName());
    }

    QString columnScript = generateColumn(column);

    if ( i != --table->getColumns()->end() )
    {
      columnScript.append(",");
    }

    sql.append(columnScript);
    sql.append("\n");
  }

  sql.append(");\n");

  return sql;
}

void ScriptGenerator::cycleLinkDetected(const QString &msg)
{  
}

void ScriptGenerator::sort(QList<Table*> &tables)
{ 
  //������������ ������
  QStringList sl;
  for ( int i = 0; i < tables.size(); i++ )
  {
    Table *curTable = tables.value(i);

    if ( sl.contains(curTable->getName()) )
    {
      continue;
    }

    int nextPosition = i + 1;
    for ( int j = 0; j < curTable->getColumns()->size(); j++ )
    {
      Column *curColumn = curTable->getColumns()->value(j);
      if ( curColumn->getIndex()->getKind() == Index::KindForeignKey )
      {
        QString refTable = curColumn->getIndex()->getReferenceTableForForeignKey();

        if ( curTable->getName().compare(refTable) == 0 )
        {
          //�����!!!
          continue;
        }
       
        for ( int k = nextPosition/*i + 1*/; k < tables.size(); k++ )
        {
          if ( tables.value(k)->getName().compare(refTable) == 0 )
          {
            if ( sl.contains(tables.value(k)->getName()) )
            {
              //����������� ������!!!
              cycleLinkDetected(QString("Error: Cycle link !!!\nTable %1 column %2 references to table %3").arg(curTable->getName()).arg(curColumn->getName()).arg(refTable));             
              continue;
            }
           
            sl.append(curTable->getName());
            
            //����������� �������           
            tables.swap(k, i);
            nextPosition = k + 1;
            i = -1;
            break;
          }
        }
      }
    }    
  }  
}

QString ScriptGenerator::generate()
{
  QString sql;

  for ( QList<QString>::iterator i = dropingTables.begin(); i != dropingTables.end(); i++ )
  {
    sql.append(QString("DROP TABLE \"%1\" CASCADE;\n").arg(*i));
  }

  for ( QMap<QString, QList<QString> >::iterator i = dropingColumns.begin(); i != dropingColumns.end(); i++ )
  {
    for ( QList<QString>::iterator j = i.value().begin(); j != i.value().end(); j++ )
    {
      sql.append(QString("ALTER TABLE \"%1\" DROP COLUMN \"%2\";\n").arg(i.key()).arg(*j));
    }
  }

  for ( QMap<QString, QList<Column*> >::iterator i = addingColumns.begin(); i != addingColumns.end(); i++ )
  {
    for ( QList<Column*>::iterator j = i.value().begin(); j != i.value().end(); j++ )
    {
      QString columnScript = generateColumn(*j);
      sql.append(QString("ALTER TABLE \"%1\" ADD COLUMN %2;\n").arg(i.key()).arg(columnScript));
    }
  }

  //������������ ������
  sort(tables);  

  for ( QList<Table*>::iterator i = tables.begin(); i != tables.end(); i++ )
  {
    Table *curTable = *i;
    QString tableScript = generateTable(curTable);
    sql.append(tableScript);
  }

  for ( QMap<QString, QString>::iterator i = indexes.begin(); i != indexes.end(); i++ )
  {
    QString index = QString("CREATE INDEX \"%1_%2_index\" ON \"%1\" (\"%2\");\n").arg(i.key()).arg(i.value());
    sql.append(index);
  }

  return sql;
}
