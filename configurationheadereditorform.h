#ifndef CONFIGURATION_HEADER_EDITOR_FORM_H
#define CONFIGURATION_HEADER_EDITOR_FORM_H

#include <QDialog>
#include "generatedfiles/ui_configurationheadereditorform.h"
#include "configurationheader.h"
#include "userdatabaselist.h"

namespace ConfiguratorObjects
{
  class ConfigurationHeaderEditorForm : public QDialog, Ui_ConfigurationHeaderEditorForm
  {    
    Q_OBJECT

    ConfigurationHeader configurationHeader;

    UserDatabaseList userDatabaseList;

  public:
    ConfigurationHeaderEditorForm(QWidget *parent = 0); 
    ~ConfigurationHeaderEditorForm();

    ConfigurationHeader getHeader();  
    void setHeader(const ConfigurationHeader &header);

    void setUserDatabaseList(const UserDatabaseList &userDatabases);

  protected:
    bool event(QEvent *); 
  };
}

#endif
