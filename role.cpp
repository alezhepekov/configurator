#include "role.h"

using namespace ConfiguratorObjects;

Role::Role()
{
  id = -1;
}

Role::Role(const Role &other)
{
  id = other.id;
  externalId = other.externalId;
  name = other.name;
}

Role::~Role()
{
}

int Role::getId() const
{
  return id;
}

void Role::setId(int newId)
{
  id = newId;
}

QString Role::getExternalId() const
{
  return externalId;
}

void Role::setExternalId(const QString &newExternalId)
{
  externalId = newExternalId;
}

QString Role::getName() const
{
  return name;
}

void Role::setName(const QString &newName)
{
  name = newName;
}

namespace ConfiguratorObjects
{
  QDataStream &operator<<(QDataStream &out, const Role &role)
  {
    out << role.getId();
    out << role.getExternalId();
    out << role.getName();

    return out;
  }

  QDataStream &operator>>(QDataStream &in, Role &role)
  {
    int id;
    in >>id;
    role.setId(id);

    QString externalId;
    in >> externalId;
    role.setExternalId(externalId);

    QString name;
    in >> name;
    role.setName(name);

    return in;
  }
}
