/***********************************************************************
* Module:   configuration.h
* Author:   LGP
* Modified: 1 �������� 2006 �.
* Purpose:  ����� ������������� ������������
***********************************************************************/

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "dbcontrol.h"
#include "objectlist.h"
#include "configurationheader.h"
#include "punycode.h"
#include "scriptgenerator.h"

namespace ConfiguratorObjects
{
  class Configuration
  {
    static const int FOREIGN_KEY_INVALID = 0x0001;

    //������ �������� ������������
    ObjectList objectList;

    //������� ������������
    QList<Documents::Script*> scripts;

    //��������� ������������
    ConfigurationHeader header;  

    //������ ���������� ������������ � ��
    DBControl *dbControlObject;  

    //��������� ��������� ������ ��������� � ��
    QString lastError;

  public:
    Configuration();
    ~Configuration();

    //����� ���������� ������ �������� ������������
    ObjectList *getObjectList();

    //����� ������������� ������ �������� ������������
    void setObjectList(const ObjectList &objList);  

    //����� ��������� �������� ��
    bool createDatabase(ConnectionInfo&);

    //����� ��������� ����������� � ��, ������� ����� �������� ���������� �������� ������������
    bool connect(ConnectionInfo&);

    //����� ��������� �������� �������� �����������
    void disconnect();

    void invalidLinkDetected(const Documents::DataType::Link *link, const QString &linkedObjectName);

    bool checkLink(QSqlDatabase &db, QString &linkedObjectName, const Documents::DataType::Link *type);

    bool deleteTablePresent(QSqlDatabase &currentDatabase, const int tableId);

    bool clearUserDatabase(const ConnectionInfo &cInfo);

    //����� ��������� ���������� ������������
    bool release(const ConnectionInfo &cInfo);  

    //����� ��������� �������� ���������� �� ������������ ��
    bool loadObjectsFromDatabase();

    //����� ��������� ���������� ���������� � ������������ ��
    bool saveObjectsToDatabase();

    //����� ��������� �������� ��������� �� ������������ ��
    bool loadHeaderFromDatabase();

    //����� ��������� ���������� ��������� � ������������ ��
    bool saveHeaderToDatabase();

    //����� ���������� ��������� ������������
    ConfigurationHeader *getHeader();

    //����� ������������� ��������� ������������
    void setHeader(ConfigurationHeader &newHeader);

    //����� ���������� ���������� ������� ������������
    QList<Documents::Script*> *getScriptList();

    //����� ������������� ���������� ������� ������������
    void setScriptList(const QList<Documents::Script*> &newScriptList);  

    //����� ��������� ������� �������� ������������
    void clear();

    //����� ���������� ��������� ��������� �������� ���������� ��������� � ��
    QString getLastError() const;

    //����� ���������� ��������� ���������� �����������
    ConnectionInfo getConnectionInfo() const;

  protected:
    //����� ��������� ������������� ������ ���������� � ������������ ���������� ���������� ���������������� ��
    int synchronize(const ConnectionInfo &, ObjectList &);

    int processDocument(Documents::Document *document, QSqlDatabase &currentDatabase, SQL::ScriptGenerator &sg);

    int processDocument(Documents::DTable *document, QSqlDatabase &currentDatabase, SQL::ScriptGenerator &sg);

    //����� ��������� ������������� ���������
    int synchronize(QSqlDatabase &currentDatabase, Documents::BaseDocument *document, SQL::ScriptGenerator &sg);

    //����� ��������� ������������� ����������
    int synchronize(QSqlDatabase &currentDatabase, QList<Documents::Accessory*> &accessories1, QMap<int, QString> &accessories2, int parentId, SQL::ScriptGenerator &sg);

    //����� ��������� ������ ��������� ��� ��������� ����� � ���������������� ��
    int addObject(QSqlDatabase &currentDatabase, const int parentId, const QString &parentKind, const QString &parentName, const QString &systemName, QList<Documents::Accessory*> &accessories, SQL::ScriptGenerator &sg);
  };
}

#endif
