#include "dtimemanager.h"

using namespace ConfiguratorObjects::Documents;

DTimeManager::DTimeManager():
BaseDocument()
{
  objectKind = OBJECT_KIND_DTIME_MANAGER;
  kind = DOC_KIND_TIME_MANAGER; 
}

DTimeManager::DTimeManager(const DTimeManager &other):
BaseDocument(other)
{  
}

DTimeManager::~DTimeManager()
{  
}

DTimeManager & DTimeManager::operator= (const DTimeManager &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);   

  return *this;
}

QString DTimeManager::toXML()
{
  QDomDocument xmlDoc;
  return xmlDoc.toString();
}

void DTimeManager::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
}

bool DTimeManager::loadFromXML(const QString& xml)
{  
  return true;
}

void DTimeManager::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DTimeManager &right)
    {
      out << static_cast<const BaseDocument&>(right);     

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DTimeManager &right)
    {
      in >> static_cast<BaseDocument&>(right);    

      return in;
    }
  }
}
