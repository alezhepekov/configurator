/***********************************************************************
 * Module:   accessory.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ����� ������������� ��������� ���������
 ***********************************************************************/

#ifndef ACCESSORY_H
#define ACCESSORY_H

#include "baseobject.h"
#include "types/tnumber.h"
#include "types/tinteger.h"
#include "types/tfloat.h"
#include "types/tstring.h"
#include "types/tdatetime.h"
#include "types/tlogical.h"
#include "types/tnumerator.h"
#include "types/tlink.h"
#include "types/timage.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int OBJECT_KIND_ACCESSORY = 0x000B;

    class Accessory: public BaseObject
    {      
      //��������� ����� ���������
      int flags;

      //������������� ��������� ����� ���������, ������� ����������� ��������
      int tablePartId;      

      //��� ���������
      DataType::BaseType *type;

    public:
      Accessory();
      Accessory(const Accessory &other);
      ~Accessory();

      Accessory & Accessory::operator= (const Accessory &right);    

      //����� ���������� ��� ���������
      DataType::BaseType *getType() const;   

      //����� ������������� ��� ���������
      void setType(DataType::BaseType *);

      //����� ���������� ��������� ����� ���������
      /*inline */int getFlags() const;

      //����� ������������� ��������� ����� ���������
      /*inline */void setFlags(int);

      //����� ���������� ������������� ��������� �����, ������� ����������� ��������
      /*inline */int getTablePartId() const;

      //����� ������������� �������������� ��������� ��������� �����
      /*inline */void setTablePartId(int);

      //����� ��������� �������������� ����������� ��������� � ������
      static QString limitsToString(Documents::Accessory *accessory);

      //����� ��������� �������������� ���������� ������������� ���� � ��� ���
      static int dataTypeFromString(const QString &type);

      //����� ��������� �������������� ���� ���� ��������� � ��������� �������������
      static QString dataTypeToString(Documents::DataType::BaseType *type);

      friend QDataStream &operator<<(QDataStream &, const Accessory &);
      friend QDataStream &operator>>(QDataStream &, Accessory &);
    };

    QDataStream &operator<<(QDataStream &out, const Accessory &right);
    QDataStream &operator>>(QDataStream &in, Accessory &right);
  }
}

#endif
