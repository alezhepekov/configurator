#include "tablepart.h"

using namespace ConfiguratorObjects::Documents;

TablePart::TablePart():
BaseObject()
{
  objectKind = OBJECT_KIND_TABLE_PART;
}

TablePart::TablePart(const TablePart &other):
BaseObject(other)
{
}

TablePart::~TablePart()
{
}

TablePart & TablePart::operator= (const TablePart &right)
{
  if ( this == &right ) return *this;

  BaseObject::operator=(right);  

  return *this;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const TablePart &right)
    {
      out << static_cast<const BaseObject&>(right);    

      return out;
    }

    QDataStream &operator>>(QDataStream &in, TablePart &right)
    {
      in >> static_cast<BaseObject&>(right);   

      return in;
    }
  }
}
