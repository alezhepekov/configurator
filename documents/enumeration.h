/***********************************************************************
 * Module:   enumeration.h
 * Author:   LGP
 * Modified: 09 ������� 2006 �.
 * Purpose:  ����� ������������� ��������� ���� "Enumeration"
 ***********************************************************************/

#ifndef ENUMERATION_H
#define ENUMERATION_H 1

#include "basedocument.h"
#include "enumitem.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_ENUMERATION = 0x0008;
    const int OBJECT_KIND_ENUMERATION = 0x0009;

    class Enumeration: public BaseDocument
    {
      //������ ��������� ������������
      QList<EnumItem*> items;

    public:
      Enumeration();
      Enumeration(const Enumeration &other);
      ~Enumeration();

      Enumeration & Enumeration::operator= (const Enumeration &right);

      //����� ���������� ������ ��������� ������������
      const QList<EnumItem*>* getItems() const;

      QList<EnumItem*>* getItems();

      //����� ������������� ������ ��������� ������������
      void setItems(const QList<EnumItem*>&);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const Enumeration &);
      friend QDataStream &operator>>(QDataStream &, Enumeration &);
    };

    QDataStream &operator<<(QDataStream &out, const Enumeration &right);
    QDataStream &operator>>(QDataStream &in, Enumeration &right);
  }
}

#endif
