/***********************************************************************
 * Module:   enumitem.h
 * Author:   LGP
 * Modified: 09 ������� 2006 �.
 * Purpose:  ����� ������������� �������� ������������ EnumItem
 ***********************************************************************/

#ifndef ENUM_ITEM_H
#define ENUM_ITEM_H

#include "baseobject.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int OBJECT_KIND_ENUM_ITEM = 0x000A;

    class EnumItem: public BaseObject
    {    
    public:
      EnumItem();
      EnumItem(const EnumItem &other);
      ~EnumItem();

      EnumItem & EnumItem::operator= (const EnumItem &right);

      friend QDataStream &operator<<(QDataStream &, const EnumItem &);
      friend QDataStream &operator>>(QDataStream &, EnumItem &);      
    };

    QDataStream &operator<<(QDataStream &out, const EnumItem &right);
    QDataStream &operator>>(QDataStream &in, EnumItem &right);
  }
}

#endif
