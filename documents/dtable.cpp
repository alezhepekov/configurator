#include "dtable.h"

using namespace ConfiguratorObjects::Documents;

DTable::DTable():
BaseDocument()
{
  objectKind = OBJECT_KIND_DTABLE;
  kind = DOC_KIND_TABLE; 
}

DTable::DTable(const DTable &other):
BaseDocument(other)
{
  for ( QList<Accessory*>::const_iterator i = other.accessories.constBegin(); i != other.accessories.constEnd(); i++ )
  {
    accessories.append(new Accessory(**i));
  }
}

DTable::~DTable()
{
  for ( QList<Accessory*>::iterator i = accessories.begin(); i != accessories.end(); i++ )
  {
    delete *i;
  }
}

DTable & DTable::operator= (const DTable &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->accessories.clear();  
  for ( QList<Accessory*>::const_iterator i = right.accessories.constBegin(); i != right.accessories.constEnd(); i++ )
  {
    this->accessories.append(new Accessory(**i));
  }  

  return *this;
}

const QList<Accessory*>* DTable::getAccessories() const
{
  return &accessories;
}

QList<Accessory*>* DTable::getAccessories()
{
  return &accessories;
}

void DTable::setAccessories(const QList<Accessory*>& newAccessories)
{
  accessories = newAccessories;
}

QString DTable::toXML()
{
  QDomDocument xmlDoc;
  return xmlDoc.toString();
}

void DTable::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{ 
}

bool DTable::loadFromXML(const QString& xml)
{  
  return true;
}

void DTable::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DTable &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.accessories.size();
      for ( QList<Accessory*>::const_iterator i = right.accessories.constBegin(); i != right.accessories.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DTable &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Accessory *newAccessory = new Accessory();
        in >> *newAccessory;
        right.accessories.append(newAccessory);
      }

      return in;
    }
  }
}
