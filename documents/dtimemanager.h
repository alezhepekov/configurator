/***********************************************************************
* Module:   dtaimmanager.h
* Author:   LGP
* Modified: 27 ���� 2007 �.
* Purpose:  ����� ������������� ��������� ���� "DTimeManager"
***********************************************************************/

#ifndef DTIME_MANAGER_H
#define DTIME_MANAGER_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_TIME_MANAGER = 0x0010;
    const int OBJECT_KIND_DTIME_MANAGER = 0x0016;

    class DTimeManager: public BaseDocument
    {
    public:
      DTimeManager();
      DTimeManager(const DTimeManager &other);
      ~DTimeManager();

      DTimeManager & DTimeManager::operator= (const DTimeManager &right);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DTimeManager &);
      friend QDataStream &operator>>(QDataStream &, DTimeManager &);    
    };

    QDataStream &operator<<(QDataStream &out, const DTimeManager &right);
    QDataStream &operator>>(QDataStream &in, DTimeManager &right);
  }
}

#endif
