/***********************************************************************
 * Module:   tablepart.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ����� ������������� ��������� ����� ���������
 ***********************************************************************/

#ifndef TABLE_PART_H
#define TABLE_PART_H

#include "baseobject.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int OBJECT_KIND_TABLE_PART = 0x000C;

    class TablePart: public BaseObject
    {
    public:
      TablePart();
      TablePart(const TablePart &other);
      ~TablePart();

      TablePart & TablePart::operator= (const TablePart &right);    

      friend QDataStream &operator<<(QDataStream &, const TablePart &);
      friend QDataStream &operator>>(QDataStream &, TablePart &);
      
    };

    QDataStream &operator<<(QDataStream &out, const TablePart &right);
    QDataStream &operator>>(QDataStream &in, TablePart &right);
  }
}

#endif
