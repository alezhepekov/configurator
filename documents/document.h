/***********************************************************************
 * Module:   document.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ����� ������������� ��������� "��������"
 ***********************************************************************/

#ifndef DOCUMENT_H
#define DOCUMENT_H 1

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_DOCUMENT    = 0x0002;
    const int OBJECT_KIND_DOCUMENT = 0x0003;

    class Document: public BaseDocument
    {
    protected:
      //������ ���������� ���������
      QList<Accessory*> accessories;

      //������ ��������� ������ ���������
      QList<TablePart*> tableParts;

      //������ ���� ���������
      QList<Form*> forms;

    public:
      Document();
      Document(const Document &other);
      ~Document();

      Document & Document::operator= (const Document &right);

      //����� ���������� ������ ���������
      const QList<Accessory*> *getAccessories() const;

      QList<Accessory*> *getAccessories();

      //����� ������������� ������ ���������
      void setAccessories(const QList<Accessory*>&);

      //����� ���������� ������ ��������� ������
      const QList<TablePart*> *getTableParts() const;

      QList<TablePart*> *getTableParts();

      //����� ������������� ������ ��������� ������
      void setTableParts(const QList<TablePart*>&);

      //����� ���������� ������ ����
      const QList<Form*> *getForms() const;

      QList<Form*> *getForms();

      //������������� ������ ����
      void setForms(const QList<Form*>&);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const Document &);
      friend QDataStream &operator>>(QDataStream &, Document &);
    };

    QDataStream &operator<<(QDataStream &out, const Document &right);
    QDataStream &operator>>(QDataStream &in, Document &right);
  }
}

#endif
