#include "enumitem.h"

using namespace ConfiguratorObjects::Documents;

EnumItem::EnumItem():
BaseObject()
{
  objectKind = OBJECT_KIND_ENUM_ITEM;
}

EnumItem::EnumItem(const EnumItem &other):
BaseObject(other)
{  
}

EnumItem::~EnumItem()
{
}

EnumItem & EnumItem::operator= (const EnumItem &right)
{
  if ( this == &right ) return *this;

  BaseObject::operator=(right); 

  return *this;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const EnumItem &right)
    {
      out << static_cast<const BaseObject&>(right);   

      return out;
    }

    QDataStream &operator>>(QDataStream &in, EnumItem &right)
    {
      in >> static_cast<BaseObject&>(right);
   
      return in;
    }
  }
}
