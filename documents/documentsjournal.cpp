#include "documentsjournal.h"

using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;

DocumentsJournal::DocumentsJournal():
BaseDocument()
{ 
  objectKind = OBJECT_KIND_DOCUMENTS_JOURNAL;
  kind = DOC_KIND_DOCUMENTS_JOURNAL;  
}

DocumentsJournal::DocumentsJournal(const DocumentsJournal &other):
BaseDocument(other)
{
  for ( QMap<int, BaseDocument*>::const_iterator i = other.registeredDocuments.constBegin(); i != other.registeredDocuments.constEnd(); i++ )
  {
    registeredDocuments.insert(i.key(), NULL);
  }

  for ( QMap<int, QList<Accessory*> >::const_iterator i = other.registeredAccessories.constBegin(); i != other.registeredAccessories.constEnd(); i++ )
  {
    QList<Accessory*> accList;

    for ( QList<Accessory*>::const_iterator j = i.value().constBegin(); j != i.value().constEnd(); j++ )
    {
      accList.append(new Accessory(**j));
    }

    registeredAccessories.insert(i.key(), accList);
  }

  for ( QList<Form*>::const_iterator i = other.forms.constBegin(); i != other.forms.constEnd(); i++ )
  {
    forms.append(new Form(**i));
  } 
}

DocumentsJournal::~DocumentsJournal()
{
  QMap<int, BaseDocument*>::iterator i = registeredDocuments.begin();
  while (i != registeredDocuments.end())
  {
    delete i.value();
    ++i;
  }
  
  for ( QMap<int, QList<Accessory*> >::iterator i = registeredAccessories.begin(); i != registeredAccessories.end(); i++ )
  {    
    for ( QList<Accessory*>::iterator j = i.value().begin(); j != i.value().end(); j++ )
    {
      delete *j;      
    } 
  }

  for ( QList<Form*>::iterator j = forms.begin(); j != forms.end(); j++ )
  {
    delete *j;
  }
}

DocumentsJournal & DocumentsJournal::operator= (const DocumentsJournal &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->registeredDocuments.clear();
  for ( QMap<int, BaseDocument*>::const_iterator i = right.registeredDocuments.constBegin(); i != right.registeredDocuments.constEnd(); i++ )
  {
    this->registeredDocuments.insert(i.key(), NULL);
  }

  this->registeredAccessories.clear();
  for ( QMap<int, QList<Accessory*> >::const_iterator i = right.registeredAccessories.constBegin(); i != right.registeredAccessories.constEnd(); i++ )
  {
    QList<Accessory*> accList;
    for ( QList<Accessory*>::const_iterator j = i.value().constBegin(); j != i.value().constEnd(); j++ )
    {
      accList.append(new Accessory(**j));
    }    

    this->registeredAccessories.insert(i.key(), accList);    
  }

  this->forms.clear();
  for ( QList<Form*>::const_iterator i = right.forms.constBegin(); i != right.forms.constEnd(); i++ )
  {
    this->forms.append(new Form(**i));
  } 

  return *this;
}

const QMap<int, BaseDocument*>* DocumentsJournal::getRegisteredDocuments() const
{
  return &registeredDocuments;
}

QMap<int, BaseDocument*>* DocumentsJournal::getRegisteredDocuments()
{
  return &registeredDocuments;
}

void DocumentsJournal::setRegisteredDocuments(const QMap<int, BaseDocument*>& newRegisteredDocuments)
{
  registeredDocuments = newRegisteredDocuments;
}

const QList<Accessory*>* DocumentsJournal::getDocumentAccessories(int docId) const
{  
  return &(registeredAccessories[docId]);
}

QList<Accessory*>* DocumentsJournal::getDocumentAccessories(int docId)
{  
  return &(registeredAccessories[docId]);
}

void DocumentsJournal::setDocumentAccessories(int docId, const QList<Accessory*>& accessories)
{
  registeredAccessories.insert(docId, accessories);
}

void DocumentsJournal::removeDocumentAccessories(int docId)
{
  registeredAccessories.remove(docId);
}

const QMap<int, QList<Accessory*> >* DocumentsJournal::getRegisteredAccessories() const
{
  return &registeredAccessories;
}

QMap<int, QList<Accessory*> >* DocumentsJournal::getRegisteredAccessories()
{
  return &registeredAccessories;
}

void DocumentsJournal::setRegisteredAccessories(const QMap<int, QList<Accessory*> >& newRegisteredAccessories)
{
  registeredAccessories = newRegisteredAccessories;
}

const QList<Form*>* DocumentsJournal::getForms() const
{
  return &forms;
}

QList<Form*>* DocumentsJournal::getForms()
{
  return &forms;
}

void DocumentsJournal::setForms(const QList<Form*>& newForms)
{
  forms = newForms;
}

QString DocumentsJournal::toXML()
{
  QDomDocument xmlDoc;
  QDomElement ne, root;
  QDomText te;

  //����� ������� ������� ���������
  appendToXML(xmlDoc, (BaseDocument*) this);
  
  //��������� ��������� �� �������� �������
  root = xmlDoc.documentElement();  

  ne = xmlDoc.createElement("registered_documents");
  root.appendChild(ne);
 
  int j = 0;
  QMap<int, BaseDocument*>::iterator i = registeredDocuments.begin();
  while (i != registeredDocuments.end())
  {
    //���������� ������� ������� ���������
    appendToXML(xmlDoc, ne, i.value());

    //��������� ����������
    QDomElement ne1 = xmlDoc.createElement("accessories");
   
    //�������� ���������� ��������� ���������� ����������� � �������
    appendToXML(xmlDoc, ne1, &registeredAccessories.value(i.key()));    

    //���������� ����� ���������� � ���� ���������
    ne.childNodes().item(j).appendChild(ne1);
    
    //����������� ����������
    j++;
    ++i;
  } 

  ne = xmlDoc.createElement("forms");
  root.appendChild(ne);

  //���������� ���� ���������
  appendToXML(xmlDoc, ne, &forms);

  return xmlDoc.toString();
}

void DocumentsJournal::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
}

bool DocumentsJournal::loadFromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  if (!xmlDoc.setContent(xml))
  {
    return false;
  }

  loadFromXML(xmlDoc, xmlDoc.documentElement());  

  return true;
}

void DocumentsJournal::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
  takeFromXML(getElement(currentElement, "/document"), this);    

  //�������� ������������������ ����������
  QDomElement ce = getElement(currentElement, "/document/registered_documents/document");

  BaseDocument* document = NULL;
  while (!ce.isNull())
  {
    //document = new BaseDocument();
    ////document->setKind(ce.attribute("kind").toInt());    

    //takeFromXML(ce, document);
    registeredDocuments.insert(document->getId(), NULL);
   
    QList<Accessory*> accessories;
    //takeFromXML(getElement(ce, "/document/accessories"), &accessories);
    registeredAccessories.insert(document->getId(), accessories);
    
    ce = ce.nextSiblingElement("document");
  }

  //�������� ����
  takeFromXML(getElement(currentElement, "/document/forms"), &forms);

  return;
}

QList<Link> * DocumentsJournal::transform(const QMap<int, BaseDocument*>& documents)
{
  QList<Link>* links = new QList<Link>;
  QMap<int, BaseDocument*>::const_iterator i = documents.constBegin();
  while (i != documents.constEnd())
  {
    Link newLink;
    newLink.setLinkedTableName("Objects.Documents");
    newLink.setLinkedTableRecordId(i.key());
    links->append(newLink);

    ++i;
  }

  return links;
}

QMap<int, BaseDocument*>* DocumentsJournal::transform(const QList<Link>& links)
{
  QMap<int, BaseDocument*> *documents = new QMap<int, BaseDocument*>; 
  for (int i = 0; i < links.size(); i++)
  {
    if (links.value(i).getLinkedTableName().compare("Objects.Documents") == 0)
    {
      documents->insert(links.value(i).getLinkedTableRecordId(), NULL);
    }    
  }

  return documents;
}

QList<Link> * DocumentsJournal::transform(const QMap<int, QList<Accessory*> >& accList)
{
  QList<Link> *links = new QList<Link>;
  QMap<int, QList<Accessory*> >::const_iterator i = accList.constBegin();
  while ( i != accList.constEnd() )
  {    
    for ( int j = 0; j < i.value().size(); j++ )
    {
      if ( i.value().value(j)->getFlags() == 1 )
      {
        Link newLink;
        newLink.setFlag(i.key());
        newLink.setLinkedTableName("Objects.Accessories");
        newLink.setLinkedTableRecordId((i.value()).value(j)->getId());

        links->append(newLink);
      }      
    }
    ++i;
  }

  return links;
}

QMap<int, QList<Accessory*> >* DocumentsJournal::transform1(const QList<Link>& links)
{
  QMap<int, QList<Accessory*> >* documentsAccessories = new QMap<int, QList<Accessory*> >;
  for (int i = 0; i < links.size(); i++)
  {
    QList<Accessory*> accList;

    if (links.value(i).getLinkedTableName().compare("Objects.Accessories") == 0)
    {      
      if (!documentsAccessories->contains(links.value(i).getFlag()))
      {
        Accessory *newAccessory = new Accessory();
        newAccessory->setId(links.value(i).getLinkedTableRecordId());
        newAccessory->setFlags(1);
        accList.append(newAccessory);

        //��������� ���� ���������� ������ ���������
        for ( int j = i + 1; j < links.size(); j++ )
        {
          if ( links.value(j).getFlag() == links.value(i).getFlag() )
          {
            newAccessory = new Accessory();
            newAccessory->setId(links.value(j).getLinkedTableRecordId());
            newAccessory->setFlags(1);
            accList.append(newAccessory);          
          }
        }

        documentsAccessories->insert(links.value(i).getFlag(), accList);
        accList.clear();
      }     
    }    
  }

  return documentsAccessories;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DocumentsJournal &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.getRegisteredDocuments()->size();
      for ( QMap<int, BaseDocument*>::iterator i = right.getRegisteredDocuments()->begin(); i != right.getRegisteredDocuments()->end(); i++ )
      {
        out << i.key();   
      }

      out << right.getRegisteredAccessories()->size();
      for ( QMap<int, QList<Accessory*> >::iterator i = right.getRegisteredAccessories()->begin(); i != right.getRegisteredAccessories()->end(); i++ )
      {
        out << i.key();

        out << i.value().size();
        for ( QList<Accessory*>::iterator j = i.value().begin(); j != i.value().end(); j++ )
        {
          out << *(*j);
        }
      } 

      out << right.getForms()->size();
      for ( QList<Form*>::const_iterator i = right.getForms()->constBegin(); i != right.getForms()->constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DocumentsJournal &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;

      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        int id;
        in >> id;
        right.getRegisteredDocuments()->insert(id, NULL);
      }

      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        int id;
        in >> id;

        int iSize;
        in >> iSize;

        QList<Accessory*> accList;
        for ( int j = 0; j < iSize; j++ )
        {
          Accessory *newAcc = new Accessory();
          in >> *newAcc;
          accList.append(newAcc);
        }

        right.getRegisteredAccessories()->insert(id, accList);
      }  

      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Form *newForm = new Form();
        in >> *newForm;
        right.getForms()->append(newForm);
      }

      return in;
    }
  }
}
