#include "dform.h"

using namespace ConfiguratorObjects::Documents;

DForm::DForm():
BaseDocument()
{
  objectKind = OBJECT_KIND_DFORM;
  kind = DOC_KIND_FORM; 
}

DForm::DForm(const DForm &other):
BaseDocument(other)
{
  for ( QList<Form*>::const_iterator i = other.forms.constBegin(); i != other.forms.constEnd(); i++ )
  {
    forms.append(new Form(**i));
  } 
}

DForm::~DForm()
{
  for ( QList<Form*>::iterator i = forms.begin(); i != forms.end(); i++ )
  {
    delete *i;
  }
}

DForm & DForm::operator= (const DForm &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->forms.clear();  
  for ( QList<Form*>::const_iterator i = right.forms.constBegin(); i != right.forms.constEnd(); i++ )
  {
    this->forms.append(new Form(**i));
  }  

  return *this;
}

const QList<Form*>* DForm::getForms() const
{
  return &forms;
}

QList<Form*>* DForm::getForms()
{
  return &forms;
}

void DForm::setForms(const QList<Form*>& newForms)
{
  forms = newForms;
}

QString DForm::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne;

  //����� ������� ������� ���������
  appendToXML(xmlDoc, this);

  //��������� ��������� ��������
  root = xmlDoc.documentElement();

  //����� ���� ���������
  ne = xmlDoc.createElement("forms");
  root.appendChild(ne);
  appendToXML(xmlDoc, ne, &forms);

  return xmlDoc.toString();
}

void DForm::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
  //����� ������� ������� ���������
  appendToXML(xmlDoc, currentElement, this);

  //��������� ��������� ��������
  QDomElement root = currentElement.firstChildElement("document");

  //����� ���� ���������
  QDomElement ne = xmlDoc.createElement("forms");
  root.appendChild(ne);
  appendToXML(xmlDoc, ne, &forms);

  return;
}

bool DForm::loadFromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  if (!xmlDoc.setContent(xml))
  {
    return false;
  }

  loadFromXML(xmlDoc, xmlDoc.documentElement());
  
  return true;
}

void DForm::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{
  //�������� ������� ������� ���������
  takeFromXML(getElement(currentElement, "/document"), this);  

  //�������� ����  
  takeFromXML(getElement(currentElement, "/document/forms"), &forms);

  return;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DForm &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.forms.size();
      for ( QList<Form*>::const_iterator i = right.forms.constBegin(); i != right.forms.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DForm &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Form *newForm = new Form();
        in >> *newForm;
        right.forms.append(newForm);
      }

      return in;
    }
  }
}
