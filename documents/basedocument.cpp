#include "basedocument.h"

using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;

BaseDocument::BaseDocument():
BaseObject(),
kind(DOC_KIND_BASE_DOCUMENT),
status(0),
releaseMark(false),
deleteMark(false),
flags(NO_DOCUMENT_SYSTEM_FLAGS),
script()
{  
  objectKind = OBJECT_KIND_BASE_DOCUMENT;  
}

BaseDocument::BaseDocument(const BaseDocument &other):
BaseObject(other),
kind(other.kind),
status(other.status),
releaseMark(other.releaseMark),
deleteMark(other.deleteMark),
flags(other.flags),
script(other.script)
{
}

BaseDocument::~BaseDocument()
{
}

BaseDocument & BaseDocument::operator= (const BaseDocument &right)
{
  if ( this == &right ) return *this;

  BaseObject::operator=(right);

  this->kind = right.kind;
  this->status = right.status;
  this->releaseMark = right.releaseMark;
  this->deleteMark = right.deleteMark;
  this->flags = right.flags;
  this->script = right.script;

  return *this;
}

int BaseDocument::getKind() const
{
  return kind;
}

int BaseDocument::getStatus() const
{
  return status;
}

void BaseDocument::setStatus(int newStatus)
{
  status = newStatus;
}

bool BaseDocument::getReleaseMark() const
{
  return releaseMark;
}

void BaseDocument::setReleaseMark(bool newReleaseMark)
{
  releaseMark = newReleaseMark;
}

bool BaseDocument::getDeleteMark() const
{
  return deleteMark;
}

void BaseDocument::setDeleteMark(bool newDeleteMark)
{
  deleteMark = newDeleteMark;
}

int BaseDocument::getFlags() const
{
  return flags;
}

void BaseDocument::setFlags(int newFlags)
{
  flags = newFlags;
}

const Script * BaseDocument::getScript() const
{
  return &script;
}

Script * BaseDocument::getScript()
{
  return &script;
}

void BaseDocument::setScript(const Script& newScript)
{
  script = newScript;
}

void BaseDocument::appendToXML(QDomDocument& xmlDoc, QDomElement& currentElement, const QList<Accessory*>* accList)
{
  Accessory *curAcc;
  QDomElement ne, ne1, ce;  
  QDomText te;

  for (int i = 0; i < accList->size(); i++)
  {
    curAcc = accList->value(i);     

    ne = xmlDoc.createElement("accessory");
    ne.setAttribute("id", curAcc->getId());
    ne.setAttribute("flags", curAcc->getFlags());
    ne.setAttribute("tp_id", curAcc->getTablePartId());
    currentElement.appendChild(ne);   

    ce = ne;
    ne = xmlDoc.createElement("translates");
    ce.appendChild(ne);
    appendToXML(xmlDoc, ne, curAcc->getTranslates());    

    ne = xmlDoc.createElement("type");    
    ce.appendChild(ne);
    appendToXML(xmlDoc, ne, curAcc->getType());     
  }  
}

void BaseDocument::appendToXML(QDomDocument& xmlDoc, QDomElement& currentElement, const QMap<int, QString>* translatesList)
{
  QDomElement ne;  
  
  //������� ��������� �����
  QMap<int, QString>::const_iterator i = translatesList->constBegin();
  while (i != translatesList->constEnd())
  {
    //���������� ��������� �� translatesList � xmlDoc
    ne = xmlDoc.createElement("translate");
    ne.setAttribute("id", i.key());
    ne.setAttribute("text", i.value().toLocal8Bit().data());
    currentElement.appendChild(ne);

    ++i;
  }
}

void BaseDocument::appendToXML(QDomDocument& xmlDoc, QDomElement& currentElement, const QList<TablePart*>* tablePartsList)
{
  TablePart *curTablePart;
  QDomElement ne, ne1, ce;  
  QDomText te;
  
  for (int i = 0; i < tablePartsList->size(); i++)
  {
    curTablePart = tablePartsList->value(i);    

    ne = xmlDoc.createElement("tablepart");
    ne.setAttribute("id", curTablePart->getId());
    currentElement.appendChild(ne);  

    ce = ne;
    ne = xmlDoc.createElement("translates");
    ce.appendChild(ne);   
    appendToXML(xmlDoc, ne, curTablePart->getTranslates());
  }
}

void BaseDocument::appendToXML(QDomDocument& xmlDoc, QDomElement& currentElement, const QList<Form*>* formsList)
{
  Form *curForm;
  QDomElement ne, ne1, ce;
  QDomText te;

  for ( int i = 0; i < formsList->size(); i++ )
  {
    curForm = formsList->value(i);
    
    ne = xmlDoc.createElement("form");
    //ne.setAttribute("id", curForm->getId());
    ne.setAttribute("kind", curForm->getKind());
    currentElement.appendChild(ne);

    ne1 = ne;

    ce = ne;
    ne = xmlDoc.createElement("translates");
    ce.appendChild(ne);
    appendToXML(xmlDoc, ne, curForm->getTranslates()); 

    ce = ne1;
    ne = xmlDoc.createElement("body");
    te = xmlDoc.createTextNode(curForm->getBody()->getBody());
    ne.appendChild(te);
    ce.appendChild(ne);

    ne = xmlDoc.createElement("module");
    te = xmlDoc.createTextNode(curForm->getModule()->getBody());
    ne.appendChild(te);
    ce.appendChild(ne);
  }  
}

void BaseDocument::appendToXML(QDomDocument& xmlDoc, QDomElement& currentElement, BaseDocument* document)
{
  QDomElement ne, root;
  QDomText te;

  if (document == NULL)
  {
    return;
  }

  root = xmlDoc.createElement("document");
  currentElement.appendChild(root);  

  root.setAttribute("id", document->getId());
  root.setAttribute("kind", document->getKind());
  root.setAttribute("flags", document->getFlags());

  ne = xmlDoc.createElement("status");
  te = xmlDoc.createTextNode(QString("%1").arg(document->getStatus()));
  ne.appendChild(te);
  root.appendChild(ne);  

  ne = xmlDoc.createElement("release_mark");
  te = xmlDoc.createTextNode(QString("%1").arg(document->getReleaseMark()));
  ne.appendChild(te);
  root.appendChild(ne);

  ne = xmlDoc.createElement("delete_mark");
  te = xmlDoc.createTextNode(QString("%1").arg(document->getDeleteMark()));
  ne.appendChild(te);
  root.appendChild(ne);  

  ne = xmlDoc.createElement("translates");
  
  appendToXML(xmlDoc, ne, document->getTranslates());
  root.appendChild(ne);
}

void BaseDocument::appendToXML(QDomDocument& xmlDoc, BaseDocument* document)
{
  QDomElement ne, root;
  QDomText te;

  if (document == NULL)
  {
    return;
  }

  root = xmlDoc.createElement("document");
  xmlDoc.appendChild(root);

  //appendToXML(xmlDoc, root, document);

  root.setAttribute("id", document->getId());
  root.setAttribute("kind", document->getKind());
  root.setAttribute("flags", document->getFlags());

  ne = xmlDoc.createElement("status");
  te = xmlDoc.createTextNode(QString("%1").arg(document->getStatus()));
  ne.appendChild(te);
  root.appendChild(ne);  

  ne = xmlDoc.createElement("release_mark");
  te = xmlDoc.createTextNode(QString("%1").arg(document->getReleaseMark()));
  ne.appendChild(te);
  root.appendChild(ne);

  ne = xmlDoc.createElement("delete_mark");
  te = xmlDoc.createTextNode(QString("%1").arg(document->getDeleteMark()));
  ne.appendChild(te);
  root.appendChild(ne);  

  ne = xmlDoc.createElement("translates");
  root.appendChild(ne);
  appendToXML(xmlDoc, ne, document->getTranslates());
}

void BaseDocument::appendToXML(QDomDocument& xmlDoc, QDomElement& currentElement, BaseType* type)
{
  QDomElement ne, ce;
  QDomText te;

  if (type == NULL)
  {   
    currentElement.setAttribute("kind", "type not defined");    
    return;
  }
  
  currentElement.setAttribute("kind", type->getTypeKind());
  ce = currentElement;

  switch(type->getTypeKind())
  {
    case DATA_TYPE_NUMBER:
    {
      //�����
      Number* typeNumber = dynamic_cast<Number*> (type);
      if (typeNumber)
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "Number");
        //ne.setAttribute("id", curAcc.getType());
        ce.appendChild(ne);

        ce = ne;
        ne = xmlDoc.createElement("length");
        te = xmlDoc.createTextNode(QString("%1").arg(typeNumber->getLength()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("precision");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeNumber->getPrecision()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("min");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeNumber->getMin()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("max");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeNumber->getMax()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("default");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeNumber->getDefault()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("required");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeNumber->getRequired()));
        ne.appendChild(te);
        ce.appendChild(ne);
      }        
    }
    break;

    case DATA_TYPE_INTEGER:
    {
      //�����
      Integer *typeInteger = dynamic_cast<Integer*> (type);
      if ( typeInteger )
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "Integer");
        //ne.setAttribute("id", curAcc.getType());
        ce.appendChild(ne);

        ce = ne;
        ne = xmlDoc.createElement("min");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeInteger->getMin()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("max");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeInteger->getMax()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("default");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeInteger->getDefault()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("required");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeInteger->getRequired()));
        ne.appendChild(te);
        ce.appendChild(ne);
      }        
    }
    break;

    case DATA_TYPE_FLOAT:
    {
      //������������
      Float *typeFloat = dynamic_cast<Float*> (type);
      if ( typeFloat )
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "Integer");
        //ne.setAttribute("id", curAcc.getType());
        ce.appendChild(ne);

        ce = ne;
        ne = xmlDoc.createElement("min");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeFloat->getMin()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("max");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeFloat->getMax()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("default");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeFloat->getDefault()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("required");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeFloat->getRequired()));
        ne.appendChild(te);
        ce.appendChild(ne);
      }        
    }
    break;

    case DATA_TYPE_STRING:
    {
      //������
      String *typeString = dynamic_cast<String*> (type);
      if ( typeString )
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "String");
        //ne.setAttribute("id", kind);
        ce.appendChild(ne);

        ce = ne;
        ne = xmlDoc.createElement("length");
        te = xmlDoc.createTextNode(QString("%1").arg(typeString->getLength()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("default");      
        te = xmlDoc.createTextNode(typeString->getDefault());
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("required");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeString->getRequired()));
        ne.appendChild(te);
        ce.appendChild(ne);
      }        
    }
    break;

    case DATA_TYPE_DATETIME:
    {
      //���������
      DateTime *typeDateTime = dynamic_cast<DateTime*> (type);
      if (typeDateTime)
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "DateTime");
        //ne.setAttribute("id", kind);
        ce.appendChild(ne);       

        ne = xmlDoc.createElement("default");      
        te = xmlDoc.createTextNode(typeDateTime->getDefault().toString("dd.MM.yyyy hh:mm:ss"));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("required");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeDateTime->getRequired()));
        ne.appendChild(te);
        ce.appendChild(ne);
      }  
    }
    break;

    case DATA_TYPE_BOOLEAN:
    {
      //����������
      Logical *typeLogical = dynamic_cast<Logical*> (type);
      if ( typeLogical )
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "Logical");
        //ne.setAttribute("id", kind);
        ce.appendChild(ne);

        ce = ne;
        ne = xmlDoc.createElement("default");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeLogical->getDefault()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("required");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeLogical->getRequired()));
        ne.appendChild(te);
        ce.appendChild(ne);
      }        
    }
    break;

    case DATA_TYPE_NUMERATOR:
    {
      //���������
      Numerator *typeNumerator = dynamic_cast<Numerator*> (type);
      if (typeNumerator)
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "Numerator");
        //ne.setAttribute("id", kind);
        ce.appendChild(ne);

        ce = ne;
        ne = xmlDoc.createElement("editable");      
        te = xmlDoc.createTextNode(QString("%1").arg(typeNumerator->getEditable()));
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("generator");      
        te = xmlDoc.createTextNode(typeNumerator->getGenerator());
        ne.appendChild(te);
        ce.appendChild(ne);

        ne = xmlDoc.createElement("init");      
        te = xmlDoc.createTextNode(typeNumerator->getInit());
        ne.appendChild(te);
        ce.appendChild(ne); 
      }        
    }
    break;

    case DATA_TYPE_IMAGE:
    {
     
    }
    break;

    case DATA_TYPE_LINK:
    {
      //������
      Link *typeLink = dynamic_cast<Link*> (type);
      if ( typeLink )
      {
        //���������� ���� ��������� �������
        ne = xmlDoc.createElement("def");
        ne.setAttribute("td", "Link");
        ce.appendChild(ne);

		ce = ne;
		ne = xmlDoc.createElement("linkedTableName");
		te = xmlDoc.createTextNode(QString("%1").arg(typeLink->getLinkedTableName()));
		ne.appendChild(te);
		ce.appendChild(ne);

		ne = xmlDoc.createElement("linkedTableRecordId");      
		te = xmlDoc.createTextNode(QString("%1").arg(typeLink->getLinkedTableRecordId()));
		ne.appendChild(te);
		ce.appendChild(ne);

		ne = xmlDoc.createElement("flag");
		te = xmlDoc.createTextNode(QString("%1").arg(typeLink->getFlag()));
		ne.appendChild(te);
		ce.appendChild(ne);
      }            
    }
    break;

    default:
    break;
  }
}

QString BaseDocument::toXML()
{ 
  QDomDocument xmlDoc;
  appendToXML(xmlDoc, this);
  return xmlDoc.toString();
}

void BaseDocument::takeFromXML(const QDomElement& currentElement, QList<Accessory*>* accList)
{  
  //������� ����������
  QDomElement ce = currentElement.firstChild().toElement();  
  while (!ce.isNull())
  {
    Accessory *curAcc = new Accessory();
    curAcc->setId(ce.attribute("id").toInt());
    curAcc->setFlags(ce.attribute("flags").toInt());
    curAcc->setTablePartId(ce.attribute("tp_id").toInt());   
    takeFromXML(getElement(ce, "/accessory/translates"), curAcc->getTranslates());

    QDomElement ce1 = getElement(ce, "/accessory/type");

    BaseType* accType = NULL;
    takeFromXML(getElement(ce, "/accessory/type"), &accType);

    curAcc->setType(accType);    

    accList->append(curAcc);
    ce = ce.nextSibling().toElement();
  }
}

void BaseDocument::takeFromXML(const QDomElement& currentElement, QList<TablePart*>* tablePartsList)
{
  //������� ��������� ������
  QDomElement ce = currentElement.firstChild().toElement();   
  while (!ce.isNull())
  {
    TablePart *curTP = new TablePart();    
    curTP->setId(ce.attribute("id").toInt());    

    //��������� ��������� �� �������� ������� ��������� �����
    QDomElement ce1 = getElement(ce, "/tablepart/translates");
    takeFromXML(ce1, curTP->getTranslates());
    
    tablePartsList->append(curTP);

    ce = ce.nextSibling().toElement();
  }
}

void BaseDocument::takeFromXML(const QDomElement& currentElement, QList<Form*>* formsList)
{
  //������� ����
  QDomElement ce = currentElement.firstChild().toElement();    
  while (!ce.isNull())
  {
    Form *curForm = new Form();    
    
    curForm->setId(ce.attribute("id").toInt());
    curForm->setKind(ce.attribute("kind").toInt());

    QDomElement ce1 = getElement(ce, "/form/translates");
    takeFromXML(ce1, curForm->getTranslates());
   
    ce1 = getElement(ce, "/form/body");
    Script curScript(ce1.firstChild().toText().data());   
    curForm->setBody(curScript);

    ce1 = getElement(ce, "/form/module");
    curScript.setBody(ce1.firstChild().toText().data());
    curForm->setModule(curScript);

    formsList->append(curForm);

    ce = ce.nextSibling().toElement();
  }
}

void BaseDocument::takeFromXML(const QDomElement& currentElement, QMap<int, QString>* translatesList)
{  
  QDomElement ce = currentElement.firstChild().toElement();
  while (!ce.isNull())
  {   
    translatesList->insert(ce.attribute("id", 0).toInt(), ce.attribute("text"));   
    ce = ce.nextSibling().toElement();
  }
}

void BaseDocument::takeFromXML(const QDomElement& currentElement, BaseDocument* document)
{
  if ( !document ) return;
  
  document->setId(currentElement.attribute("id").toInt());
  //document->setKind(currentElement.attribute("kind").toInt());
  document->setFlags(currentElement.attribute("flags").toInt());
  
  QDomElement fe = getElement(currentElement, "/document/status");
  document->setStatus(fe.firstChild().toText().data().toInt());  

  fe = getElement(currentElement, "/document/release_mark");
  document->setReleaseMark((bool)fe.firstChild().toText().data().toInt());

  fe = getElement(currentElement, "/document/delete_mark");
  document->setDeleteMark((bool)fe.firstChild().toText().data().toInt());  

  //�������� ���������
  fe = getElement(currentElement, "/document/translates");
  takeFromXML(fe, document->getTranslates());
}

void BaseDocument::takeFromXML(const QDomDocument& xmlDoc, BaseDocument* document)
{
  return takeFromXML(xmlDoc.documentElement(), document);  
}

void BaseDocument::takeFromXML(const QDomElement& currentElement, BaseType** type)
{  
  switch (currentElement.attribute("kind").toInt())
  {
    case DATA_TYPE_NUMBER:
    {
      Number* accTypeNumber = new Number;        

      accTypeNumber->setLength(getElement(currentElement, "/type/def/length").firstChild().toText().data().toInt());
      accTypeNumber->setPrecision(getElement(currentElement, "/type/def/precision").firstChild().toText().data().toInt());
      accTypeNumber->setMin(getElement(currentElement, "/type/def/min").firstChild().toText().data().toInt());
      accTypeNumber->setMax(getElement(currentElement, "/type/def/max").firstChild().toText().data().toInt());
      accTypeNumber->setDefault(getElement(currentElement, "/type/def/default").firstChild().toText().data().toInt());
     
      QDomElement fe = getElement(currentElement, "/type/def/required");
      accTypeNumber->setRequired((bool)fe.firstChild().toText().data().toInt());

      *type = accTypeNumber;
    }
    break;

    case DATA_TYPE_INTEGER:
    {
      Integer *accTypeInteger = new Integer();
      
      accTypeInteger->setMin(getElement(currentElement, "/type/def/min").firstChild().toText().data().toInt());
      accTypeInteger->setMax(getElement(currentElement, "/type/def/max").firstChild().toText().data().toInt());
      accTypeInteger->setDefault(getElement(currentElement, "/type/def/default").firstChild().toText().data().toInt());

      QDomElement fe = getElement(currentElement, "/type/def/required");
      accTypeInteger->setRequired((bool)fe.firstChild().toText().data().toInt());

      *type = accTypeInteger;
    }
    break;

    case DATA_TYPE_FLOAT:
    {
      Float *accTypeFloat = new Float();

      accTypeFloat->setMin(getElement(currentElement, "/type/def/min").firstChild().toText().data().toDouble());
      accTypeFloat->setMax(getElement(currentElement, "/type/def/max").firstChild().toText().data().toDouble());
      accTypeFloat->setDefault(getElement(currentElement, "/type/def/default").firstChild().toText().data().toDouble());

      QDomElement fe = getElement(currentElement, "/type/def/required");
      accTypeFloat->setRequired((bool)fe.firstChild().toText().data().toInt());

      *type = accTypeFloat;
    }
    break;

    case DATA_TYPE_STRING:
    {
      String* accTypeString = new String;
      accTypeString->setLength(getElement(currentElement, "/type/def/length").firstChild().toText().data().toInt());
      accTypeString->setDefault(getElement(currentElement, "/type/def/default").firstChild().toText().data());
    
      QDomElement fe = getElement(currentElement, "/type/def/required");
      accTypeString->setRequired((bool)fe.firstChild().toText().data().toInt());

      *type = accTypeString;
    }
    break;

    case DATA_TYPE_DATETIME:
    {
      DateTime* accTypeDateTime = new DateTime;
      QString ds = getElement(currentElement, "/type/def/length").firstChild().toText().data();
      accTypeDateTime->setDefault(QDateTime().fromString(ds, "dd.MM.yyyy hh:mm:ss"));

      QDomElement fe = getElement(currentElement, "/type/def/required");
      accTypeDateTime->setRequired((bool)fe.firstChild().toText().data().toInt());

      *type = accTypeDateTime;
    }
    break;

    case DATA_TYPE_BOOLEAN:
    {
      Logical* accTypeLogical = new Logical;
      
      QDomElement fe = getElement(currentElement, "/type/def/default");
      accTypeLogical->setDefault((bool)fe.firstChild().toText().data().toInt());

      fe = getElement(currentElement, "/type/def/required");
      accTypeLogical->setRequired((bool)fe.firstChild().toText().data().toInt());

      *type = accTypeLogical;
    }
    break;

    case DATA_TYPE_NUMERATOR:
    {
      Numerator* accTypeNumerator = new Numerator;

      QDomElement fe = getElement(currentElement, "/type/def/editable");
      accTypeNumerator->setEditable((bool)fe.firstChild().toText().data().toInt());

      accTypeNumerator->setInit(getElement(currentElement, "/type/def/init").firstChild().toText().data());
      accTypeNumerator->setGenerator(getElement(currentElement, "/type/def/generator").firstChild().toText().data());

      *type = accTypeNumerator;      
    }
    break;

    case DATA_TYPE_IMAGE:
    {
      *type = NULL;
    }
    break;    

    case DATA_TYPE_LINK:
    {
      Link* accTypeLink = new Link;
      
	  QDomElement fe = getElement(currentElement, "/type/def/linkedTableName");
	  accTypeLink->setLinkedTableName(fe.firstChild().toText().data());

	  fe = getElement(currentElement, "/type/def/linkedTableRecordId");
      accTypeLink->setLinkedTableRecordId(fe.firstChild().toText().data().toInt());

	  fe = getElement(currentElement, "/type/def/flag");
	  accTypeLink->setFlag(fe.firstChild().toText().data().toInt());

      *type = accTypeLink;
    }
    break;
  }
}

bool BaseDocument::loadFromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  if (!xmlDoc.setContent(xml))
  {
    return false;
  }

  takeFromXML(xmlDoc.firstChildElement("document"), this);

  return true;
}

void BaseDocument::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
}

void BaseDocument::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const BaseDocument &right)
    {
      out << static_cast<const BaseObject&>(right);

      out << right.kind;
      out << right.status;
      out << right.deleteMark;
      out << right.releaseMark;
      out << right.flags;
      out << right.script;   

      return out;
    }

    QDataStream &operator>>(QDataStream &in, BaseDocument &right)
    {
      in >> static_cast<BaseObject&>(right);   

      in >> right.kind;
      in >> right.status;   
      in >> right.deleteMark;     
      in >> right.releaseMark;      
      in >> right.flags;     
      in >> right.script;    

      return in;
    }
  }
}
