/***********************************************************************
 * Module:   form.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ����� ������������� ����� ���������
 ***********************************************************************/

#ifndef FORM_H
#define FORM_H

#include "baseobject.h"
#include "script.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int OBJECT_KIND_FORM = 0x000D;  

    class Form: public BaseObject
    {
      //��� �����
      int kind;

      //��������� ������������� �����
      Script body;

      //����������� ������ �����
      Script module;

    public:
      static const int FORM_KIND_DEFAULT = 0x0000;
      static const int FORM_KIND_START   = 0x0001;

      Form();
      Form(const Form &other);
      ~Form();

      Form & Form::operator= (const Form &right);
     
      //����� ���������� ��� �����
      /*inline*/ int getKind() const;

      //����� ������������� ��� �����
      /*inline*/ void setKind(int newKind);

      //����� ���������� ��������� ������������� �����
      Script *getBody();

      const Script *getBody() const;

      //����� ������������� ��������� ������������� �����
      void setBody(const Script& newBody);

      //����� ���������� ����������� ������ �����
      Script *getModule();

      const Script *getModule() const;

      //����� ������������� ����������� ������ �����
      void setModule(const Script& newModule);   

      friend QDataStream &operator<<(QDataStream &, const Form &);
      friend QDataStream &operator>>(QDataStream &, Form &);
    };

    QDataStream &operator<<(QDataStream &out, const Form &right);
    QDataStream &operator>>(QDataStream &in, Form &right);
  }
}

#endif
