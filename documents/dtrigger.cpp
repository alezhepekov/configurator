#include "dtrigger.h"

using namespace ConfiguratorObjects::Documents;

DTrigger::DTrigger():
BaseDocument()
{
  objectKind = OBJECT_KIND_DTRIGGER;
  kind = DOC_KIND_TRIGGER; 
}

DTrigger::DTrigger(const DTrigger &other):
BaseDocument(other)
{  
}

DTrigger::~DTrigger()
{  
}

DTrigger & DTrigger::operator= (const DTrigger &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);   

  return *this;
}

QString DTrigger::toXML()
{
  QDomDocument xmlDoc;
  return xmlDoc.toString();
}

void DTrigger::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
}

bool DTrigger::loadFromXML(const QString& xml)
{  
  return true;
}

void DTrigger::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DTrigger &right)
    {
      out << static_cast<const BaseDocument&>(right);     

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DTrigger &right)
    {
      in >> static_cast<BaseDocument&>(right);    

      return in;
    }
  }
}
