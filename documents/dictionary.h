/***********************************************************************
 * Module:   dictionary.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ����� ������������� ��������� "����������"
 ***********************************************************************/

#ifndef DICTIONARY_H
#define DICTIONARY_H

#include "document.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_DICTIONARY = 0x0003;
    const int OBJECT_KIND_DICTIONARY = 0x0004;

    class Dictionary: public Document
    {
    public:
      Dictionary();
      Dictionary(const Dictionary &other);
      ~Dictionary();

      Dictionary & Dictionary::operator= (const Dictionary &right);

      friend QDataStream &operator<<(QDataStream &, const Dictionary &);
      friend QDataStream &operator>>(QDataStream &, Dictionary &);
    };

    QDataStream &operator<<(QDataStream &out, const Dictionary &right);
    QDataStream &operator>>(QDataStream &in, Dictionary &right);
  }
}

#endif
