/***********************************************************************
 * Module:   register.h
 * Author:   LGP
 * Modified: 07 ������� 2006 �.
 * Purpose:  ����� ������������� ��������� ���� "Register"
 ***********************************************************************/

#ifndef REGISTER_H
#define REGISTER_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_REGISTER = 0x0006;
    const int OBJECT_KIND_REGISTER = 0x0007;

    class Register: public BaseDocument
    {
      //������ ������ ��������
      QList<Accessory*> facts;

      //������ ����������-���������
      QMap<int, BaseDocument*> dimensions;

    public:
      Register();
      Register(const Register &other);
      ~Register();

      Register & Register::operator= (const Register &right);

      //����� ���������� ������ ���������� ����������-���������
      const QMap<int, BaseDocument*>* getDimensions() const;

      QMap<int, BaseDocument*>* getDimensions();

      //����� ������������� ������ ���������� ����������-���������
      void setDimensions(const QMap<int, BaseDocument*>&);

      //����� ���������� ��������� �� ������ ������ ��������
      const QList<Accessory*>* getFacts() const;

      QList<Accessory*>* getFacts();

      //����� ������������� ������ ������ ��������
      void setFacts(const QList<Accessory*>&);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      //����� ��������� �������������� ������ ������� ���������� � ������ ������
      QList<DataType::Link>* transform(const QMap<int, BaseDocument*>&);

      //����� ��������� �������������� ������ ������ � ������ ������� ����������
      QMap<int, BaseDocument*>* transform(const QList<DataType::Link>&);

      friend QDataStream &operator<<(QDataStream &, const Register &);
      friend QDataStream &operator>>(QDataStream &, Register &);
    };

    QDataStream &operator<<(QDataStream &out, const Register &right);
    QDataStream &operator>>(QDataStream &in, Register &right);
  }
}

#endif
