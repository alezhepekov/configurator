#include "register.h"

using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;

Register::Register():
BaseDocument()
{ 
  objectKind = OBJECT_KIND_REGISTER;
  kind = DOC_KIND_REGISTER;
}

Register::Register(const Register &other):
BaseDocument(other)
{ 
  for ( QMap<int, BaseDocument*>::const_iterator i = other.dimensions.constBegin(); i != other.dimensions.constEnd(); i++ )
  {
    dimensions.insert(i.key(), NULL);
  }

  for ( QList<Accessory*>::const_iterator i = other.facts.constBegin(); i != other.facts.constEnd(); i++ )
  {
    facts.append(new Accessory(**i));
  }
}

Register::~Register()
{
  QMap<int, BaseDocument*>::iterator i = dimensions.begin();
  while ( i != dimensions.end() )
  {
    delete i.value();
    ++i;
  }

  for ( QList<Accessory*>::iterator j = facts.begin(); j != facts.end(); j++ )
  {
    delete *j;
  }  
}

Register & Register::operator= (const Register &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->dimensions.clear();
  for ( QMap<int, BaseDocument*>::const_iterator i = right.dimensions.constBegin(); i != right.dimensions.constEnd(); i++ )
  {
    this->dimensions.insert(i.key(), NULL);
  }

  this->facts.clear();
  for ( QList<Accessory*>::const_iterator i = right.facts.constBegin(); i != right.facts.constEnd(); i++ )
  {
    this->facts.append(new Accessory(**i));
  }

  return *this;
}

const QMap<int, BaseDocument*>* Register::getDimensions() const
{
  return &dimensions;
}

QMap<int, BaseDocument*>* Register::getDimensions()
{
  return &dimensions;
}

void Register::setDimensions(const QMap<int, BaseDocument*>& newDimensions)
{
  dimensions = newDimensions;
}

const QList<Accessory*>* Register::getFacts() const
{
  return &facts;
}

QList<Accessory*>* Register::getFacts()
{
  return &facts;
}

void Register::setFacts(const QList<Accessory*> &newFacts)
{
  facts = newFacts;
}

QString Register::toXML()
{
  QDomDocument xmlDoc;
  QDomElement ne, root;
  QDomText te;

  //����� ������� ������� ���������
  appendToXML(xmlDoc, (BaseDocument*) this);

  //��������� ��������� �� �������� �������
  root = xmlDoc.documentElement();  

  ne = xmlDoc.createElement("dimensions");
  root.appendChild(ne);
 
  QMap<int, BaseDocument*>::iterator i = dimensions.begin();
  while (i != dimensions.end())
  {
    //���������� ������� ������� ���������
    appendToXML(xmlDoc, ne, i.value());
   
    ++i;
  } 

  ne = xmlDoc.createElement("facts");
  root.appendChild(ne);

  //���������� ������ ��������
  appendToXML(xmlDoc, ne, &facts);

  return xmlDoc.toString();
}

bool Register::loadFromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  if (!xmlDoc.setContent(xml))
  {
    return false;
  }

  loadFromXML(xmlDoc, xmlDoc.documentElement());
  
  return true;
}

QList<Link>* Register::transform(const QMap<int, BaseDocument*> &documents)
{
  QList<Link>* links = new QList<Link>;
  QMap<int, BaseDocument*>::const_iterator i = documents.constBegin();
  while (i != documents.constEnd())
  {
    Link newLink;
    newLink.setLinkedTableName("Objects.Documents");
    newLink.setLinkedTableRecordId(i.key());
    links->append(newLink);

    ++i;
  }

  return links;
}

QMap<int, BaseDocument*>* Register::transform(const QList<Link> &links)
{
  QMap<int, BaseDocument*>* documents = new QMap<int, BaseDocument*>;

  for (int i = 0; i < links.size(); i++)
  {
    if (links.value(i).getLinkedTableName().compare("Objects.Documents") == 0)
    {
      documents->insert(links.value(i).getLinkedTableRecordId(), NULL);
    }    
  }

  return documents;
}

void Register::toXML(QDomDocument& xmlDoc, QDomElement &currentElement)
{
}

void Register::loadFromXML(const QDomDocument &xmlDoc, const QDomElement &currentElement)
{ 
  takeFromXML(getElement(currentElement, "/document"), this);    

  //�������� ������������������ ���������
  QDomElement ce = getElement(currentElement, "/document/dimensions/document");

  BaseDocument* document = NULL;
  while (!ce.isNull())
  {
    //document = new BaseDocument();
    //document->setKind(ce.attribute("kind").toInt());

    //takeFromXML(ce, document);
    dimensions.insert(document->getId(), NULL);

    ce = ce.nextSiblingElement("document");
  }

  //�������� ������ ��������
  takeFromXML(getElement(currentElement, "/document/facts"), &facts);

  return;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Register &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.dimensions.size();
      for ( QMap<int, BaseDocument*>::const_iterator i = right.dimensions.constBegin(); i != right.dimensions.constEnd(); i++ )
      {
        out << i.key();      
      }

      out << right.facts.size();
      for ( QList<Accessory*>::const_iterator i = right.facts.constBegin(); i != right.facts.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Register &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;

      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        int id;
        in >> id;
        right.dimensions.insert(id, NULL);
      }

      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Accessory *newAcc = new Accessory();
        in >> *newAcc;
        right.facts.append(newAcc);
      }

      return in;
    }
  }
}
