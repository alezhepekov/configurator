#include "script.h"

using namespace ConfiguratorObjects::Documents;

Script::Script():
BaseObject(),
body(QString())
{
  objectKind = OBJECT_KIND_SCRIPT;
}

Script::Script(const Script &other):
BaseObject(other),
body(other.body)
{
}

Script::Script(const QString &script):
BaseObject(),
body(script)
{
  objectKind = OBJECT_KIND_SCRIPT;
}

Script::~Script()
{
}

Script & Script::operator= (const Script &right)
{
  if ( this == &right ) return *this;

  BaseObject::operator=(right);

  this->body = right.body;
  
  return *this;
}

QString Script::getBody() const
{
  return body;
}

void Script::setBody(const QString &newBody)
{
  body = newBody;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Script &right)
    {
      out << static_cast<const BaseObject&>(right);

      out << right.body;

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Script &right)
    {
      in >> static_cast<BaseObject&>(right);
     
      in >> right.body;     

      return in;
    }
  }
}
