#include "dictionary.h"

using namespace ConfiguratorObjects::Documents;

Dictionary::Dictionary():
Document()
{  
  objectKind = OBJECT_KIND_DICTIONARY;
  kind = DOC_KIND_DICTIONARY; 
}

Dictionary::Dictionary(const Dictionary &other):
Document(other)
{  
}

Dictionary::~Dictionary()
{
}

Dictionary & Dictionary::operator= (const Dictionary &right)
{
  if ( this == &right ) return *this;

  Document::operator=(right);

  return *this;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Dictionary &right)
    {
      out << static_cast<const Document&>(right);

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Dictionary &right)
    {
      in >> static_cast<Document&>(right);   

      return in;
    }
  }
}
