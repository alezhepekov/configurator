/***********************************************************************
 * Module:  Report.h
 * Author:  LGP
 * Modified: 25 ���� 2006 �.
 * Purpose: ����� ������������� ��������� "�����"
 ***********************************************************************/

#ifndef REPORT_H
#define REPORT_H

#include "document.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_REPORT = 0x0004;
    const int OBJECT_KIND_REPORT = 0x0005;

    class Report: public Document
    {
    public:
      Report();
      Report(const Report &other);
      ~Report();

      Report & Report::operator= (const Report &right);

      friend QDataStream &operator<<(QDataStream &, const Report &);
      friend QDataStream &operator>>(QDataStream &, Report &);
    };

    QDataStream &operator<<(QDataStream &out, const Report &right);
    QDataStream &operator>>(QDataStream &in, Report &right);
  }
}

#endif
