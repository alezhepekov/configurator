/***********************************************************************
* Module:   dglobalthread.h
* Author:   LGP
* Modified: 25 ���� 2007 �.
* Purpose:  ����� ������������� ��������� ���� "DGlobalContainer"
***********************************************************************/

#ifndef D_GLOBAL_CONTAINER_H
#define D_GLOBAL_CONTAINER_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_GLOBAL_CONTAINER     = 0x000E;
    const int OBJECT_KIND_DGLOBAL_CONTAINER = 0x0014;

    class DGlobalContainer: public BaseDocument
    {
      QList<Script*> scripts;

    public:
      DGlobalContainer();
      DGlobalContainer(const DGlobalContainer &other);
      ~DGlobalContainer();

      DGlobalContainer & DGlobalContainer::operator= (const DGlobalContainer &right);

      //����� ���������� ��������� �� ������� ���������
      const QList<Script*>* getScripts() const;

      QList<Script*>* getScripts();

      //����� ������������� ������� ���������
      void setScripts(const QList<Script*> &newScripts);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DGlobalContainer &);
      friend QDataStream &operator>>(QDataStream &, DGlobalContainer &);    
    };

    QDataStream &operator<<(QDataStream &out, const DGlobalContainer &right);
    QDataStream &operator>>(QDataStream &in, DGlobalContainer &right);
  }
}

#endif
