#include "report.h"

using namespace ConfiguratorObjects::Documents;

Report::Report():
Document()
{  
  objectKind = OBJECT_KIND_REPORT;
  kind = DOC_KIND_REPORT;
}

Report::Report(const Report &other):
Document(other)
{  
}

Report::~Report()
{
}

Report & Report::operator= (const Report &right)
{
  if ( this == &right ) return *this;

  Document::operator=(right);

  return *this;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Report &right)
    {
      out << static_cast<const Document&>(right);

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Report &right)
    {
      in >> static_cast<Document&>(right);   

      return in;
    }
  }
}
