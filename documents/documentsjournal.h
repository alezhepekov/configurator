/***********************************************************************
 * Module:   documentsjournal.h
 * Author:   LGP
 * Modified: 02 ������� 2006 �.
 * Purpose:  ����� ������������� ��������� ���� "DocumentsJournal"
 ***********************************************************************/

#ifndef DOCUMENTS_JOURNAL_H
#define DOCUMENTS_JOURNAL_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {  
    const int DOC_KIND_DOCUMENTS_JOURNAL    = 0x0005;
    const int OBJECT_KIND_DOCUMENTS_JOURNAL = 0x0006;

    class DocumentsJournal: public BaseDocument
    {
      //������ ������������������ ����������
      QMap<int, BaseDocument*> registeredDocuments;

      //������ ������������������ ����������
      QMap<int, QList<Accessory*> > registeredAccessories;

      //������ ���� ������������� �������
      QList<Form*> forms;

    public:
      DocumentsJournal();
      DocumentsJournal(const DocumentsJournal &other); 
      ~DocumentsJournal();

      DocumentsJournal & DocumentsJournal::operator= (const DocumentsJournal &right);

      //����� ���������� ������ ���������� �� ������������������ ���������
      const QMap<int, BaseDocument*>* getRegisteredDocuments() const;

      QMap<int, BaseDocument*>* getRegisteredDocuments();

      //����� ������������� ������ ���������� �� ������������������ ���������
      void setRegisteredDocuments(const QMap<int, BaseDocument*>&);

      //����� ���������� ��������� �� ������ ��������� � ��������� ���������� ����������
      const QList<Accessory*>* getDocumentAccessories(int) const;

      QList<Accessory*>* getDocumentAccessories(int);

      //����� ������������� ������ ���������� � ��������� ��� � ��������� ��������������� ���������
      void setDocumentAccessories(int, const QList<Accessory*>&);

      //����� ������� ������ ��������� � ��������� ���������� ����������
      void removeDocumentAccessories(int);

      //����� ���������� ��������� ����������
      const QMap<int, QList<Accessory*> >* getRegisteredAccessories() const;

      QMap<int, QList<Accessory*> >* getRegisteredAccessories();

      void setRegisteredAccessories(const QMap<int, QList<Accessory*> >&);

      //����� ���������� ��������� �� ������ ���� ���������
      const QList<Form*>* getForms() const;

      QList<Form*>* getForms();

      //����� ������������� ������ ���� ���������
      void setForms(const QList<Form*>&);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);    

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      //����� ��������� �������������� ������ ���������� � ������ ������
      QList<DataType::Link>* transform(const QMap<int, BaseDocument*>&);

      //����� ��������� �������������� ������ ������ � ������ ���������� 
      QMap<int, BaseDocument*>* transform(const QList<DataType::Link>&);  

      //����� ��������� �������������� ������ ���������� � ������ ������
      QList<DataType::Link>* transform(const QMap<int, QList<Accessory*> >&);

      //����� ��������� �������������� ������ ������ � ������ ���������� 
      QMap<int, QList<Accessory*> >* transform1(const QList<DataType::Link>&); 

      friend QDataStream &operator<<(QDataStream &, const DocumentsJournal &);
      friend QDataStream &operator>>(QDataStream &, DocumentsJournal &);  
    };

    QDataStream &operator<<(QDataStream &out, const DocumentsJournal &right);
    QDataStream &operator>>(QDataStream &in, DocumentsJournal &right);
  }
}

#endif
