/***********************************************************************
 * Module:   constant.h
 * Author:   LGP
 * Modified: 09 ������� 2006 �.
 * Purpose:  ����� ������������� ��������� ���� "Constant"
 ***********************************************************************/

#ifndef CONSTANT_H
#define CONSTANT_H

#include "basedocument.h"
#include "types/basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_CONSTANT    = 0x0007;
    const int OBJECT_KIND_CONSTANT = 0x0008;

    class Constant: public BaseDocument
    {
      //��� ���������
      DataType::BaseType *type;

    public:
      Constant();
      Constant(const Constant &other);
      ~Constant();

      Constant & Constant::operator= (const Constant &right);

      //����� ���������� ��� ���������
      DataType::BaseType* getType() const;

      //����� ������������� ��� ���������
      void setType(DataType::BaseType*);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);
  
      friend QDataStream &operator<<(QDataStream &, const Constant &);
      friend QDataStream &operator>>(QDataStream &, Constant &);
    };

    QDataStream &operator<<(QDataStream &out, const Constant &right);
    QDataStream &operator>>(QDataStream &in, Constant &right);
  }

}

#endif
