#include "dclassmodule.h"

using namespace ConfiguratorObjects::Documents;

DClassModule::DClassModule():
BaseDocument()
{
  objectKind = OBJECT_KIND_DCLASS_MODULE;
  kind = DOC_KIND_CLASS_MODULE; 
}

DClassModule::DClassModule(const DClassModule &other):
BaseDocument(other)
{
  for ( QList<Script*>::const_iterator i = other.scripts.constBegin(); i != other.scripts.constEnd(); i++ )
  {
    scripts.append(new Script(**i));
  } 
}

DClassModule::~DClassModule()
{
  for ( QList<Script*>::iterator i = scripts.begin(); i != scripts.end(); i++ )
  {
    delete *i;
  }
}

DClassModule & DClassModule::operator= (const DClassModule &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->scripts.clear();  
  for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
  {
    this->scripts.append(new Script(**i));
  }  

  return *this;
}

const QList<Script*>* DClassModule::getScripts() const
{
  return &scripts;
}

QList<Script*>* DClassModule::getScripts()
{
  return &scripts;
}

void DClassModule::setScripts(const QList<Script*> &newScripts)
{
  scripts = newScripts;
}

QString DClassModule::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne; 

  return xmlDoc.toString();
}

void DClassModule::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
  
}

bool DClassModule::loadFromXML(const QString& xml)
{  
  return true;
}

void DClassModule::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DClassModule &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.scripts.size();
      for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
      {
        out << **i;
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DClassModule &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Script *newScript = new Script();
        in >> *newScript;
        right.scripts.append(newScript);
      }

      return in;
    }
  }
}
