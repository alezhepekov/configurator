/***********************************************************************
 * Module:   basedocument.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������� ����� ������������� ���������
 ***********************************************************************/

#ifndef BASE_DOCUMENT_H
#define BASE_DOCUMENT_H

#include <QMap>
#include <QList>
#include <QString>
#include <QStringList>
#include <QVariant>
#include <QDomDocument>
#include <QDomElement>
#include <QDomAttr>
#include <iostream>

#include "accessory.h"
#include "tablepart.h"
#include "form.h"
#include "script.h"
#include "../domxml.h"

namespace ConfiguratorObjects
{
  namespace Documents
  { 
    const int DOC_KIND_BASE_DOCUMENT    = 0x0001;
    const int OBJECT_KIND_BASE_DOCUMENT = 0x0002;

    class BaseDocument: public BaseObject, public DomXml
    {
    protected:
      //��� ���������
      int kind;

      //��������� ���������
      int status;

      //������� �� �������  
      bool deleteMark;

      //������� �� ����������
      bool releaseMark;    

      //�������������� ��������� �����
      int flags;

      //������ ���������
      Script script;

    public:
      //��������� �����
      static const int NO_DOCUMENT_SYSTEM_FLAGS = 0;

      BaseDocument();
      BaseDocument(const BaseDocument &other);      
      ~BaseDocument();

      BaseDocument & BaseDocument::operator= (const BaseDocument &right);

      //����� ���������� ��� ���������
      /*inline*/ int getKind() const;    

      //����� ���������� ��������� ���������
      /*inline*/ int getStatus() const;

      //����� ������������� ��������� ���������
      /*inline*/ void setStatus(int);

      //����� ���������� ������ ���������� ���������
      /*inline*/ bool getReleaseMark() const;

      //����� ������������� ������ ���������� ���������
      /*inline*/ void setReleaseMark(bool);

      //����� ���������� ������ �������� ���������
      /*inline*/ bool getDeleteMark() const;

      //����� ������������� ������ �������� ���������
      /*inline*/ void setDeleteMark(bool);

      //����� ���������� ��������� ����� ���������
      /*inline*/ int getFlags() const;

      //����� ������������� ��������� ����� ���������
      /*inline*/ void setFlags(int);   

      //����� ���������� ������ ���������
      const Script *getScript() const;

      Script *getScript();

      //����� ������������� ������ ���������
      void setScript(const Script& newScript);

      //����� ��������� ����������� ���������� ��������� � xml-�������������
      virtual QString toXML() = 0;

      //����� ��������� �������� ���������� ��������� �� xml-�������������
      virtual bool loadFromXML(const QString &) = 0;

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      virtual void toXML(QDomDocument &, QDomElement &) = 0;

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      virtual void loadFromXML(const QDomDocument &, const QDomElement& ) = 0;    

      //����� ��������� � �������� �������� � ���������� ��� � ������������� ��������,
      //�.�. �������� ����� �������� ��������� ���������� �������� xml-��������� ������������� ���������
      void appendToXML(QDomDocument&, QDomElement&, const QList<Accessory*>*);     

      //����� ��������� � �������� ��������� ����� � ���������� ��� � ������������� ��������
      void appendToXML(QDomDocument&, QDomElement&, const QList<TablePart*>*);

      //����� ��������� � �������� ����� � ���������� ��� � ������������� ��������
      void appendToXML(QDomDocument&, QDomElement&, const QList<Form*>*);

      //����� ��������� � �������� ������� � ���������� ��� � ������������� ��������
      void appendToXML(QDomDocument&, QDomElement&, const QMap<int, QString>*);

      //����� ��������� � �������� ������� �������� ���������
      void appendToXML(QDomDocument&, BaseDocument*);

      //����� ��������� � ���� ��������� ������� �������� ���������
      void appendToXML(QDomDocument&, QDomElement&, BaseDocument*);

      //����� ��������� � ���� ��������� �������� ���������
      void appendToXML(QDomDocument&, QDomElement&, DataType::BaseType*);

      //����� ��������� �������� ���������� �� xml-���������
      void takeFromXML(const QDomElement&, QList<Accessory*>*); 

      //����� ��������� �������� ��������� ������ �� xml-���������
      void takeFromXML(const QDomElement&, QList<TablePart*>*);

      //����� ��������� �������� ���� �� xml-���������
      void takeFromXML(const QDomElement&, QList<Form*>*);

      //����� ��������� �������� ���������� �� xml-���������
      void takeFromXML(const QDomElement&, QMap<int, QString>*);

      //����� ��������� �������� ������� ������� ��������� �� xml-����
      void takeFromXML(const QDomElement&, BaseDocument*);

      //����� ��������� �������� ������� ������� ��������� �� xml-���������
      void takeFromXML(const QDomDocument&, BaseDocument*);

      //����� ��������� �������� ���� ��������� �� xml-���� � ���������� ������ ������������� ����
      void takeFromXML(const QDomElement& , DataType::BaseType**);

      friend QDataStream &operator<<(QDataStream &, const BaseDocument &);
      friend QDataStream &operator>>(QDataStream &, BaseDocument &);
    };

    QDataStream &operator<<(QDataStream &out, const BaseDocument &right);
    QDataStream &operator>>(QDataStream &in, BaseDocument &right);
  }
}

#endif
