#include "accessory.h"

using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;

Accessory::Accessory():
BaseObject(),
flags(0),
tablePartId(-1),
type(NULL)
{
  objectKind = OBJECT_KIND_ACCESSORY;
}

Accessory::Accessory(const Accessory &other):
BaseObject(other),
flags(other.flags),
tablePartId(other.tablePartId)
{
  if ( other.type )
  {
    switch ( other.type->getTypeKind() )
    {
      case DATA_TYPE_NUMBER:
      {
        type = new Number(static_cast<const Number&>(*other.type));     
      }
      break;

      case DATA_TYPE_INTEGER:
      {
        type = new Integer(static_cast<const Integer&>(*other.type));     
      }
      break;

      case DATA_TYPE_FLOAT:
      {
        type = new Float(static_cast<const Float&>(*other.type));    
      }
      break;

      case DATA_TYPE_DATETIME:
      {
        type = new DateTime(static_cast<const DateTime&>(*other.type));  
      }
      break;

      case DATA_TYPE_STRING:
      {
        type = new String(static_cast<const String&>(*other.type));    
      }
      break;

      case DATA_TYPE_BOOLEAN:
      {
        type = new Logical(static_cast<const Logical&>(*other.type));    
      }
      break;

      case DATA_TYPE_LINK:
      {
        type = new Link(static_cast<const Link&>(*other.type));    
      }
      break;

      case DATA_TYPE_NUMERATOR:
      {
        type = new Numerator(static_cast<const Numerator&>(*other.type));      
      }
      break;

      case DATA_TYPE_IMAGE:
      {
        type = new Image(static_cast<const Image&>(*other.type));   
      }
      break;

      default:;
    }
  }
}

Accessory::~Accessory()
{
  delete type;
}

Accessory & Accessory::operator= (const Accessory &right)
{
  if ( this == &right ) return *this;

  BaseObject::operator=(right);  
 
  this->flags = right.flags;
  this->tablePartId = right.tablePartId;
  
  if ( right.type ) 
  {    
    switch ( right.type->getTypeKind() )
    {
      case DATA_TYPE_NUMBER:
      {
        this->type = new Number(static_cast<const Number&>(*right.type));     
      }
      break;

      case DATA_TYPE_INTEGER:
      {
        this->type = new Integer(static_cast<const Integer&>(*right.type));     
      }
      break;

      case DATA_TYPE_FLOAT:
      {
        this->type = new Float(static_cast<const Float&>(*right.type));    
      }
      break;

      case DATA_TYPE_DATETIME:
      {
        this->type = new DateTime(static_cast<const DateTime&>(*right.type));  
      }
      break;

      case DATA_TYPE_STRING:
      {
        this->type = new String(static_cast<const String&>(*right.type));    
      }
      break;

      case DATA_TYPE_BOOLEAN:
      {
        this->type = new Logical(static_cast<const Logical&>(*right.type));    
      }
      break;

      case DATA_TYPE_LINK:
      {
        this->type = new Link(static_cast<const Link&>(*right.type));    
      }
      break;

      case DATA_TYPE_NUMERATOR:
      {
        this->type = new Numerator(static_cast<const Numerator&>(*right.type));      
      }
      break;

      case DATA_TYPE_IMAGE:
      {
        this->type = new Image(static_cast<const Image&>(*right.type));   
      }
      break;

      default:;
    }
  }

  return *this;
}

BaseType * Accessory::getType() const
{
   return type;
}

void Accessory::setType(BaseType *newType)
{
   type = newType;
}

int Accessory::getFlags() const
{
  return flags;
}

void Accessory::setFlags(int newFlags)
{
  flags = newFlags;
}

int Accessory::getTablePartId() const
{
  return tablePartId;
}

void Accessory::setTablePartId(int newTablePartId)
{
  tablePartId = newTablePartId;
}

QString Accessory::limitsToString(Accessory *accessory)
{
  if ( !accessory ) return "";
  if ( !accessory->getType() ) return ""; 

  QString limits;
  switch ( accessory->getType()->getTypeKind() )
  {
    case DATA_TYPE_NUMBER:
    {        
      Number *type = dynamic_cast<Number*> (accessory->getType());
      if ( type )
      {
        limits.append(QString("Length=%1;").arg(type->getLength()));
        limits.append(QString("Precision=%1;").arg(type->getPrecision()));
        limits.append(QString("Default=%1;").arg(type->getDefault()));
        limits.append(QString("Required=%1;").arg(type->getRequired()));
        limits.append(QString("Min=%1;").arg(type->getMin()));
        limits.append(QString("Max=%1;").arg(type->getMax()));
      }
    }
    break;

    case DATA_TYPE_INTEGER:
    {        
      Integer *type = dynamic_cast<Integer*> (accessory->getType());
      if ( type )
      {          
        limits.append(QString("Default=%1;").arg(type->getDefault()));
        limits.append(QString("Required=%1;").arg(type->getRequired()));
        limits.append(QString("Min=%1;").arg(type->getMin()));
        limits.append(QString("Max=%1;").arg(type->getMax()));
      }
    }
    break;

    case DATA_TYPE_FLOAT:
    {        
      Float *type = dynamic_cast<Float*> (accessory->getType());
      if ( type )
      {          
        limits.append(QString("Default=%1;").arg(type->getDefault()));
        limits.append(QString("Required=%1;").arg(type->getRequired()));
        limits.append(QString("Min=%1;").arg(type->getMin()));
        limits.append(QString("Max=%1;").arg(type->getMax()));
      }
    }
    break;

    case DATA_TYPE_STRING:
    {
      String *type = dynamic_cast<String*> (accessory->getType());
      if ( type )
      {
        limits.append(QString("Length=%1;").arg(type->getLength()));        
        limits.append(QString("Default=%1;").arg(type->getDefault()));
        limits.append(QString("Required=%1;").arg(type->getRequired()));
      }
    }
    break;

    case DATA_TYPE_DATETIME:
    {
      DateTime *type = dynamic_cast<DateTime*> (accessory->getType());
      if ( type )
      {              
        limits.append(QString("Default=%1;").arg(type->getDefault().toString()));
        limits.append(QString("Required=%1;").arg(type->getRequired()));         
      }
    }
    break;

    case DATA_TYPE_BOOLEAN:
    {
      Logical *type = dynamic_cast<Logical*> (accessory->getType());
      if ( type )
      {                
        limits.append(QString("Default=%1;").arg(type->getDefault()));
        limits.append(QString("Required=%1;").arg(type->getRequired()));
      }
    }
    break;

    case DATA_TYPE_LINK:
    {
      Link *type = dynamic_cast<Link*>(accessory->getType());
      if ( type )
      {
        limits.append(QString("LinkedTableName=%1;").arg(type->getLinkedTableName()));
        limits.append(QString("LinkedTableRecordId=%1;").arg(type->getLinkedTableRecordId()));
        limits.append(QString("Flag=%1;").arg(type->getFlag()));
      }
    }
    break;

    default:;
  }

  return limits;
}

QString Accessory::dataTypeToString(BaseType *type)
{
  QMap<int, QString> typeMatchs;
  typeMatchs.insert(DATA_TYPE_NUMERATOR, "Numerator");
  typeMatchs.insert(DATA_TYPE_NUMBER,    "Number");
  typeMatchs.insert(DATA_TYPE_INTEGER,   "Integer");
  typeMatchs.insert(DATA_TYPE_FLOAT,     "Float");
  typeMatchs.insert(DATA_TYPE_BOOLEAN,   "Boolean");
  typeMatchs.insert(DATA_TYPE_DATETIME,  "DateTime");
  typeMatchs.insert(DATA_TYPE_STRING,    "String");
  typeMatchs.insert(DATA_TYPE_LINK,      "Link");

  return typeMatchs.value(type->getTypeKind());
}

int Accessory::dataTypeFromString(const QString &type)
{
  QMap<QString, int> typeMatchs;
  typeMatchs.insert("Numerator", DATA_TYPE_NUMERATOR);
  typeMatchs.insert("Number",    DATA_TYPE_NUMBER);
  typeMatchs.insert("Integer",   DATA_TYPE_INTEGER);
  typeMatchs.insert("Float",     DATA_TYPE_FLOAT);
  typeMatchs.insert("Boolean",   DATA_TYPE_BOOLEAN);
  typeMatchs.insert("DateTime",  DATA_TYPE_DATETIME);
  typeMatchs.insert("String",    DATA_TYPE_STRING);
  typeMatchs.insert("Link",      DATA_TYPE_LINK);

  return typeMatchs.value(type);
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Accessory &right)
    {
      out << static_cast<const BaseObject&>(right);
     
      out << right.flags;    
      out << right.tablePartId;

      out << right.type->getTypeKind();
      switch( right.type->getTypeKind() )
      {
        case DATA_TYPE_NUMBER:
        {       
          out << static_cast<const Number&>(*right.type);
        }
        break;

        case DATA_TYPE_INTEGER:
        {        
          out << static_cast<const Integer&>(*right.type);
        }
        break;

        case DATA_TYPE_FLOAT:
        {       
          out << static_cast<const Float&>(*right.type);
        }
        break;

        case DATA_TYPE_DATETIME:
        {        
          out << static_cast<const DateTime&>(*right.type);
        }
        break;

        case DATA_TYPE_STRING:
        {       
          out << static_cast<const String&>(*right.type);
        }
        break;

        case DATA_TYPE_BOOLEAN:
        {        
          out << static_cast<const Logical&>(*right.type);
        }
        break;

        case DATA_TYPE_LINK:
        {       
          out << static_cast<const Link&>(*right.type);
        }
        break;

        case DATA_TYPE_NUMERATOR:
        {       
          out << static_cast<const Numerator&>(*right.type);
        }
        break;

        case DATA_TYPE_IMAGE:
        {        
          out << static_cast<const Image&>(*right.type);
        }
        break;

        default:;
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Accessory &right)
    {
      in >> static_cast<BaseObject&>(right);
    
      in >> right.flags;
      in >> right.tablePartId;

      int typeKind;
      in >> typeKind;
      BaseType *newType = NULL;
      switch ( typeKind )
      {
        case DATA_TYPE_NUMBER:
        {
          newType = new Number();
          in >> static_cast<Number&>(*newType);
        }
        break;

        case DATA_TYPE_INTEGER:
        {
          newType = new Integer();
          in >> static_cast<Integer&>(*newType);
        }
        break;

        case DATA_TYPE_FLOAT:
        {
          newType = new Float();
          in >> static_cast<Float&>(*newType);
        }
        break;

        case DATA_TYPE_DATETIME:
        {
          newType = new DateTime();
          in >> static_cast<DateTime&>(*newType);
        }
        break;

        case DATA_TYPE_STRING:
        {
          newType = new String();
          in >> static_cast<String&>(*newType);
        }
        break;

        case DATA_TYPE_BOOLEAN:
        {
          newType = new Logical();
          in >> static_cast<Logical&>(*newType);
        }
        break;

        case DATA_TYPE_LINK:
        {
          newType = new Link();
          in >> static_cast<Link&>(*newType);
        }
        break;

        case DATA_TYPE_NUMERATOR:
        {
          newType = new Numerator();
          in >> static_cast<Numerator&>(*newType);
        }
        break;

        case DATA_TYPE_IMAGE:
        {
          newType = new Image();
          in >> static_cast<Image&>(*newType);
        }
        break;

        default:;
      }

      right.type = newType;

      return in;
    }
  }
}
