#include "dglobalthread.h"

using namespace ConfiguratorObjects::Documents;

DGlobalThread::DGlobalThread():
BaseDocument()
{
  objectKind = OBJECT_KIND_DGLOBAL_THREAD;
  kind = DOC_KIND_GLOBAL_THREAD; 
}

DGlobalThread::DGlobalThread(const DGlobalThread &other):
BaseDocument(other)
{
  for ( QList<Script*>::const_iterator i = other.scripts.constBegin(); i != other.scripts.constEnd(); i++ )
  {
    scripts.append(new Script(**i));
  } 
}

DGlobalThread::~DGlobalThread()
{
  for ( QList<Script*>::iterator i = scripts.begin(); i != scripts.end(); i++ )
  {
    delete *i;
  }
}

DGlobalThread & DGlobalThread::operator= (const DGlobalThread &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->scripts.clear();  
  for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
  {
    this->scripts.append(new Script(**i));
  }  

  return *this;
}

const QList<Script*>* DGlobalThread::getScripts() const
{
  return &scripts;
}

QList<Script*>* DGlobalThread::getScripts()
{
  return &scripts;
}

void DGlobalThread::setScripts(const QList<Script*> &newScripts)
{
  scripts = newScripts;
}

QString DGlobalThread::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne; 

  return xmlDoc.toString();
}

void DGlobalThread::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
  
}

bool DGlobalThread::loadFromXML(const QString& xml)
{  
  return true;
}

void DGlobalThread::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DGlobalThread &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.scripts.size();
      for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DGlobalThread &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Script *newScript = new Script();
        in >> *newScript;
        right.scripts.append(newScript);
      }

      return in;
    }
  }
}
