#include "baseobject.h"

using namespace ConfiguratorObjects::Documents;

BaseObject::BaseObject():
id(-1),
gjoTreeId(-1),
objectKind(OBJECT_KIND_BASE_OBJECT),
systemName(QString())
{
}

BaseObject::BaseObject(const BaseObject &other):
id(other.id),
gjoTreeId(other.gjoTreeId),
objectKind(other.objectKind),
systemName(other.systemName),
translates(other.translates)
{
}

BaseObject::~BaseObject()
{
}

BaseObject & BaseObject::operator= (const BaseObject &right)
{
  if ( this == &right ) return *this;
  
  this->id = right.id;
  this->gjoTreeId = right.gjoTreeId;
  this->objectKind = right.objectKind;
  this->systemName = right.systemName;
  this->translates = right.translates;

  return *this;
}

int BaseObject::getId() const
{
  return id;
}

void BaseObject::setId(int newId)
{
  id = newId;
}

int BaseObject::getGJOTreeId() const
{
  return gjoTreeId;
}

void BaseObject::setGJOTreeId(int newGJOTreeId)
{
  gjoTreeId = newGJOTreeId;
}

const QMap<int, QString> * BaseObject::getTranslates() const
{
  return &translates;
}

QMap<int, QString> * BaseObject::getTranslates()
{
  return &translates;
}

void BaseObject::setTranslates(const QMap<int, QString> &newTranslates)
{
  translates = newTranslates;
}

QString BaseObject::getSystemName() const
{
  return systemName;
}

void BaseObject::setSystemName(const QString &newSystemName)
{
  systemName = newSystemName;
}

int BaseObject::getObjectKind() const
{
  return objectKind;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const BaseObject &right)
    {     
      out << right.id;
      out << right.objectKind;      
      out << right.gjoTreeId;
      out << right.systemName;
      out << right.translates;

      return out;
    }

    QDataStream &operator>>(QDataStream &in, BaseObject &right)
    {    
      in >> right.id;
      in >> right.objectKind;          
      in >> right.gjoTreeId;   
      in >> right.systemName; 
      in >> right.translates;     

      return in;
    }
  }
}
