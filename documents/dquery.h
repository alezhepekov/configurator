/***********************************************************************
* Module:   dquery.h
* Author:   LGP
* Modified: 22 ���� 2007 �.
* Purpose:  ����� ������������� ��������� ���� "DQuery"
***********************************************************************/

#ifndef DQUERY_H
#define DQUERY_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_QUERY     = 0x000B;
    const int OBJECT_KIND_DQUERY = 0x0011;

    class DQuery: public BaseDocument
    {
      QList<Script*> scripts;

    public:
      DQuery();
      DQuery(const DQuery &other);
      ~DQuery();

      DQuery & DQuery::operator= (const DQuery &right);

      //����� ���������� ��������� �� ������� ���������
      const QList<Script*>* getScripts() const;

      QList<Script*>* getScripts();

      //����� ������������� ������� ���������
      void setScripts(const QList<Script*> &newScripts);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DQuery &);
      friend QDataStream &operator>>(QDataStream &, DQuery &);    
    };

    QDataStream &operator<<(QDataStream &out, const DQuery &right);
    QDataStream &operator>>(QDataStream &in, DQuery &right);
  }
}

#endif
