#include "constant.h"

using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;

Constant::Constant():
BaseDocument(),
type(NULL)
{  
  objectKind = OBJECT_KIND_CONSTANT;
  kind = DOC_KIND_CONSTANT;
}

Constant::Constant(const Constant &other):
BaseDocument(other)
{  
  if ( other.type )
  {
    switch( other.type->getTypeKind() )
    {
      case DATA_TYPE_NUMBER:
      {
        type = new Number(static_cast<Number&>(*other.type));   
      }
      break;

      case DATA_TYPE_INTEGER:
      {
        type = new Integer(static_cast<Integer&>(*other.type));       
      }
      break;

      case DATA_TYPE_FLOAT:
      {
        type = new Float(static_cast<Float&>(*other.type));     
      }
      break;

      case DATA_TYPE_DATETIME:
      {
        type = new DateTime(static_cast<DateTime&>(*other.type));        
      }
      break;

      case DATA_TYPE_STRING:
      {
        type = new String(static_cast<String&>(*other.type));      
      }
      break;

      case DATA_TYPE_BOOLEAN:
      {
        type = new Logical(static_cast<Logical&>(*other.type));       
      }
      break;

      case DATA_TYPE_LINK:
      {
        type = new Link(static_cast<Link&>(*other.type));     
      }
      break;

      case DATA_TYPE_NUMERATOR:
      {
        type = new Numerator(static_cast<Numerator&>(*other.type));       
      }
      break;

      case DATA_TYPE_IMAGE:
      {
        type = new Image(static_cast<Image&>(*other.type));       
      }
      break;

      default:;
    }
  }
}

Constant::~Constant()
{
  delete type;
}

Constant & Constant::operator= (const Constant &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  if ( right.type )
  {
    switch( right.type->getTypeKind() )
    {
      case DATA_TYPE_NUMBER:
      {
        this->type = new Number(static_cast<Number&>(*right.type));   
      }
      break;

      case DATA_TYPE_INTEGER:
      {
        this->type = new Integer(static_cast<Integer&>(*right.type));       
      }
      break;

      case DATA_TYPE_FLOAT:
      {
        this->type = new Float(static_cast<Float&>(*right.type));     
      }
      break;

      case DATA_TYPE_DATETIME:
      {
        this->type = new DateTime(static_cast<DateTime&>(*right.type));        
      }
      break;

      case DATA_TYPE_STRING:
      {
        this->type = new String(static_cast<String&>(*right.type));      
      }
      break;

      case DATA_TYPE_BOOLEAN:
      {
        this->type = new Logical(static_cast<Logical&>(*right.type));       
      }
      break;

      case DATA_TYPE_LINK:
      {
        this->type = new Link(static_cast<Link&>(*right.type));     
      }
      break;

      case DATA_TYPE_NUMERATOR:
      {
        this->type = new Numerator(static_cast<Numerator&>(*right.type));       
      }
      break;

      case DATA_TYPE_IMAGE:
      {
        this->type = new Image(static_cast<Image&>(*right.type));       
      }
      break;

      default:;
    }
  }

  return *this;
}

BaseType * Constant::getType() const
{
  return type;
}

void Constant::setType(BaseType* newType)
{
  type = newType; 
}

QString Constant::toXML()
{
  QDomDocument xmlDoc;
  QDomElement ne, root;  

  //����� ������� ������� ���������
  appendToXML(xmlDoc, (BaseDocument*) this);

  //��������� ��������� �� �������� �������
  root = xmlDoc.documentElement();

  ne = xmlDoc.createElement("type");    
  root.appendChild(ne);
  appendToXML(xmlDoc, ne, type);

  return xmlDoc.toString();
}

bool Constant::loadFromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  if (!xmlDoc.setContent(xml))
  {
    return false;
  }

  loadFromXML(xmlDoc, xmlDoc.documentElement());

  return true;
}

void Constant::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
}

void Constant::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
  takeFromXML(getElement(currentElement, "/document"), this);

  //�������� ����
  takeFromXML(getElement(currentElement, "/document/type"), &type);

  return;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Constant &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.type->getTypeKind();
      switch( right.type->getTypeKind() )
      {
        case DATA_TYPE_NUMBER:
        {       
          out << static_cast<const Number&>(*right.type);
        }
        break;

        case DATA_TYPE_INTEGER:
        {        
          out << static_cast<const Integer&>(*right.type);
        }
        break;

        case DATA_TYPE_FLOAT:
        {       
          out << static_cast<const Float&>(*right.type);
        }
        break;

        case DATA_TYPE_DATETIME:
        {        
          out << static_cast<const DateTime&>(*right.type);
        }
        break;

        case DATA_TYPE_STRING:
        {       
          out << static_cast<const String&>(*right.type);
        }
        break;

        case DATA_TYPE_BOOLEAN:
        {        
          out << static_cast<const Logical&>(*right.type);
        }
        break;

        case DATA_TYPE_LINK:
        {       
          out << static_cast<const Link&>(*right.type);
        }
        break;

        case DATA_TYPE_NUMERATOR:
        {       
          out << static_cast<const Numerator&>(*right.type);
        }
        break;

        case DATA_TYPE_IMAGE:
        {        
          out << static_cast<const Image&>(*right.type);
        }
        break;

        default:;
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Constant &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int typeKind;
      in >> typeKind;
      BaseType *newType = NULL;
      switch ( typeKind )
      {
        case DATA_TYPE_NUMBER:
        {
          newType = new Number();
          in >> static_cast<Number&>(*newType);
        }
        break;

        case DATA_TYPE_INTEGER:
        {
          newType = new Integer();
          in >> static_cast<Integer&>(*newType);
        }
        break;

        case DATA_TYPE_FLOAT:
        {
          newType = new Float();
          in >> static_cast<Float&>(*newType);
        }
        break;

        case DATA_TYPE_DATETIME:
        {
          newType = new DateTime();
          in >> static_cast<DateTime&>(*newType);
        }
        break;

        case DATA_TYPE_STRING:
        {
          newType = new String();
          in >> static_cast<String&>(*newType);
        }
        break;

        case DATA_TYPE_BOOLEAN:
        {
          newType = new Logical();
          in >> static_cast<Logical&>(*newType);
        }
        break;

        case DATA_TYPE_LINK:
        {
          newType = new Link();
          in >> static_cast<Link&>(*newType);
        }
        break;

        case DATA_TYPE_NUMERATOR:
        {
          newType = new Numerator();
          in >> static_cast<Numerator&>(*newType);
        }
        break;

        case DATA_TYPE_IMAGE:
        {
          newType = new Image();
          in >> static_cast<Image&>(*newType);
        }
        break;

        default:;
      }

      right.type = newType;

      return in;
    }
  }
}
