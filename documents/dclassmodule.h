/***********************************************************************
* Module:   dclassmodule.h
* Author:   LGP
* Modified: 25 ���� 2007 �.
* Purpose:  ����� ������������� ��������� ���� "DClassModule"
***********************************************************************/

#ifndef D_CLASS_MODULE_H
#define D_CLASS_MODULE_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_CLASS_MODULE     = 0x000C;
    const int OBJECT_KIND_DCLASS_MODULE = 0x0012;

    class DClassModule: public BaseDocument
    {
      QList<Script*> scripts;

    public:
      DClassModule();
      DClassModule(const DClassModule &other);
      ~DClassModule();

      DClassModule & DClassModule::operator= (const DClassModule &right);

      //����� ���������� ��������� �� ������� ���������
      const QList<Script*>* getScripts() const;

      QList<Script*>* getScripts();

      //����� ������������� ������� ���������
      void setScripts(const QList<Script*> &newScripts);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DClassModule &);
      friend QDataStream &operator>>(QDataStream &, DClassModule &);    
    };

    QDataStream &operator<<(QDataStream &out, const DClassModule &right);
    QDataStream &operator>>(QDataStream &in, DClassModule &right);
  }
}

#endif
