#include "dquery.h"

using namespace ConfiguratorObjects::Documents;

DQuery::DQuery():
BaseDocument()
{
  objectKind = OBJECT_KIND_DQUERY;
  kind = DOC_KIND_QUERY; 
}

DQuery::DQuery(const DQuery &other):
BaseDocument(other)
{
  for ( QList<Script*>::const_iterator i = other.scripts.constBegin(); i != other.scripts.constEnd(); i++ )
  {
    scripts.append(new Script(**i));
  } 
}

DQuery::~DQuery()
{
  for ( QList<Script*>::iterator i = scripts.begin(); i != scripts.end(); i++ )
  {
    delete *i;
  }
}

DQuery & DQuery::operator= (const DQuery &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->scripts.clear();  
  for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
  {
    this->scripts.append(new Script(**i));
  }  

  return *this;
}

const QList<Script*>* DQuery::getScripts() const
{
  return &scripts;
}

QList<Script*>* DQuery::getScripts()
{
  return &scripts;
}

void DQuery::setScripts(const QList<Script*> &newScripts)
{
  scripts = newScripts;
}

QString DQuery::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne; 

  return xmlDoc.toString();
}

void DQuery::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
  
}

bool DQuery::loadFromXML(const QString& xml)
{  
  return true;
}

void DQuery::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DQuery &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.scripts.size();
      for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DQuery &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Script *newScript = new Script();
        in >> *newScript;
        right.scripts.append(newScript);
      }

      return in;
    }
  }
}
