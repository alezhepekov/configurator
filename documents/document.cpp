#include "document.h"

using namespace ConfiguratorObjects::Documents;

Document::Document():
BaseDocument()
{  
  objectKind = OBJECT_KIND_DOCUMENT;
  kind = DOC_KIND_DOCUMENT; 
}

Document::Document(const Document &other):
BaseDocument(other)
{
  for ( QList<Accessory*>::const_iterator i = other.accessories.constBegin(); i != other.accessories.constEnd(); i++ )
  {
    accessories.append(new Accessory(**i));
  }

  for ( QList<TablePart*>::const_iterator i = other.tableParts.constBegin(); i != other.tableParts.constEnd(); i++ )
  {
    tableParts.append(new TablePart(**i));
  }

  for ( QList<Form*>::const_iterator i = other.forms.constBegin(); i != other.forms.constEnd(); i++ )
  {
    forms.append(new Form(**i));
  }
}

Document::~Document()
{
  for ( QList<Accessory*>::iterator i = accessories.begin(); i != accessories.end(); i++ )
  {
    delete *i;
  }

  for ( QList<TablePart*>::iterator i = tableParts.begin(); i != tableParts.end(); i++ )
  {
    delete *i;
  }

  for ( QList<Form*>::iterator i = forms.begin(); i != forms.end(); i++ )
  {
    delete *i;
  }
}

Document & Document::operator= (const Document &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->accessories.clear();
  for ( QList<Accessory*>::const_iterator i = right.accessories.constBegin(); i != right.accessories.constEnd(); i++ )
  {
    this->accessories.append(new Accessory(**i));
  }

  this->tableParts.clear();
  for ( QList<TablePart*>::const_iterator i = right.tableParts.constBegin(); i != right.tableParts.constEnd(); i++ )
  {
    this->tableParts.append(new TablePart(**i));
  }

  this->forms.clear();
  for ( QList<Form*>::const_iterator i = right.forms.constBegin(); i != right.forms.constEnd(); i++ )
  {
    this->forms.append(new Form(**i));
  }

  return *this;
}

const QList<Accessory*>* Document::getAccessories() const
{
  return &accessories;
}

QList<Accessory*>* Document::getAccessories()
{
  return &accessories;
}

void Document::setAccessories(const QList<Accessory*>& newAccessories)
{
  accessories = newAccessories;
}

const QList<TablePart*>* Document::getTableParts() const
{
  return &tableParts;
}

QList<TablePart*>* Document::getTableParts()
{
  return &tableParts;
}

void Document::setTableParts(const QList<TablePart*>& newTableParts)
{
  tableParts = newTableParts;
}

const QList<Form*>* Document::getForms() const
{
  return &forms;
}

QList<Form*>* Document::getForms()
{
  return &forms;
}

void Document::setForms(const QList<Form*>& newForms)
{
  forms = newForms;
}

QString Document::toXML()
{
  QDomDocument xmlDoc;
  QDomElement ne, root;
  QDomText te;  

  appendToXML(xmlDoc, this); 

  root = xmlDoc.documentElement();

  ne = xmlDoc.createElement("accessories");
  root.appendChild(ne);
 
  appendToXML(xmlDoc, ne, &accessories); 

  ne = xmlDoc.createElement("tableParts");
  root.appendChild(ne);
  
  appendToXML(xmlDoc, ne, &tableParts);  

  ne = xmlDoc.createElement("forms");
  root.appendChild(ne);
  
  appendToXML(xmlDoc, ne, &forms);
  
  return xmlDoc.toString();
}

bool Document::loadFromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  if (!xmlDoc.setContent(xml))
  {
    return false;
  }

  loadFromXML(xmlDoc, xmlDoc.documentElement());  

  return true;
}

void Document::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
}

void Document::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
  //�������� ������� ������� ���������
  takeFromXML(getElement(currentElement, "/document"), this);  

  //�������� ����������
  takeFromXML(getElement(currentElement, "/document/accessories"), &accessories);

  //�������� ��������� ������ 
  takeFromXML(getElement(currentElement, "/document/tableparts"), &tableParts);

  //�������� ����  
  takeFromXML(getElement(currentElement, "/document/forms"), &forms);

  return;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Document &right)
    {
      out << static_cast<const BaseDocument&>(right);
   
      out << right.accessories.size();
      for ( QList<Accessory*>::const_iterator i = right.accessories.constBegin(); i != right.accessories.constEnd(); i++ )
      {
        out << *(*i);
      }
     
      out << right.tableParts.size();
      for ( QList<TablePart*>::const_iterator i = right.tableParts.constBegin(); i != right.tableParts.constEnd(); i++ )
      {
        out << *(*i);
      }
    
      out << right.forms.size();
      for ( QList<Form*>::const_iterator i = right.forms.constBegin(); i != right.forms.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Document &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;

      for ( int i = 0; i < size; i++ )
      {
        Accessory *newAcc = new Accessory();
        in >> *newAcc;
        right.accessories.append(newAcc);
      }

      in >> size;

      for ( int i = 0; i < size; i++ )
      {
        TablePart *newTp = new TablePart();
        in >> *newTp;
        right.tableParts.append(newTp);
      }

      in >> size;

      for ( int i = 0; i < size; i++ )
      {
        Form *newForm = new Form();
        in >> *newForm;
        right.forms.append(newForm);
      }

      return in;
    }
  }
}
