/***********************************************************************
* Module:   dtrigger.h
* Author:   LGP
* Modified: 27 ���� 2007 �.
* Purpose:  ����� ������������� ��������� ���� "DTrigger"
***********************************************************************/

#ifndef DTRIGGER_H
#define DTRIGGER_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_TRIGGER     = 0x000F;
    const int OBJECT_KIND_DTRIGGER = 0x0015;

    class DTrigger: public BaseDocument
    {
    public:
      DTrigger();
      DTrigger(const DTrigger &other);
      ~DTrigger();

      DTrigger & DTrigger::operator= (const DTrigger &right);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DTrigger &);
      friend QDataStream &operator>>(QDataStream &, DTrigger &);    
    };

    QDataStream &operator<<(QDataStream &out, const DTrigger &right);
    QDataStream &operator>>(QDataStream &in, DTrigger &right);
  }
}

#endif
