#include "enumeration.h"

using namespace ConfiguratorObjects::Documents;

Enumeration::Enumeration():
BaseDocument()
{  
  objectKind = OBJECT_KIND_ENUMERATION;
  kind = DOC_KIND_ENUMERATION;
}

Enumeration::Enumeration(const Enumeration &other):
BaseDocument(other)
{  
  for ( QList<EnumItem*>::const_iterator i = items.constBegin(); i != items.constEnd(); i++ )
  {
    items.append(new EnumItem(**i));
  }
}

Enumeration::~Enumeration()
{
  for ( QList<EnumItem*>::iterator i = items.begin(); i != items.end(); i++ )
  {
    delete *i;
  } 
}

Enumeration & Enumeration::operator= (const Enumeration &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->items.clear();  
  for ( QList<EnumItem*>::const_iterator i = right.items.constBegin(); i != right.items.constEnd(); i++ )
  {
    this->items.append(new EnumItem(**i));
  }  

  return *this;
}

const QList<EnumItem*>* Enumeration::getItems() const
{
  return &items;
}

QList<EnumItem*>* Enumeration::getItems()
{
  return &items;
}

void Enumeration::setItems(const QList<EnumItem*>& newItems)
{
  items = newItems;
}

QString Enumeration::toXML()
{
  QDomDocument xmlDoc;
  QDomElement ne, ne1, ce, root;  

  //����� ������� ������� ���������
  appendToXML(xmlDoc, (BaseDocument*) this);

  //��������� ��������� �� �������� �������
  root = xmlDoc.documentElement();

  //����� ��������� ������������
  ne = xmlDoc.createElement("items");    
  root.appendChild(ne);
  ce = ne;
  for (int i = 0; i < items.size(); i++)
  {
    ne = xmlDoc.createElement("item");
    ne.setAttribute("id", items[i]->getId());
    
    ne1 = xmlDoc.createElement("translates");
    ne.appendChild(ne1);
    //����� ��������� ��������
    appendToXML(xmlDoc, ne1, items[i]->getTranslates());

    ce.appendChild(ne);
  }

  return xmlDoc.toString();
}

bool Enumeration::loadFromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  if (!xmlDoc.setContent(xml))
  {
    return false;
  }

  loadFromXML(xmlDoc, xmlDoc.documentElement());

  return true;
}

void Enumeration::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
}

void Enumeration::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
  takeFromXML(getElement(currentElement, "/document"), this);

  //��������� ��������� ������������
  QDomElement ce = getElement(currentElement, "/document/items/item");
  while (!ce.isNull())
  {
    EnumItem *newEnumItem = new EnumItem();
    newEnumItem->setId(ce.attribute("id").toInt());

    //�������� ��������� ��� �������� ������������
    takeFromXML(getElement(ce, "/item/translates"), newEnumItem->getTranslates());

    //���������� �������� ������������
    items.append(newEnumItem);

    //��������� ���������� ����
    ce = ce.nextSiblingElement();
  }

  return;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Enumeration &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.items.size();
      for ( QList<EnumItem*>::const_iterator i = right.items.constBegin(); i != right.items.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Enumeration &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        EnumItem *newEnumItem = new EnumItem();
        in >> *newEnumItem;
        right.items.append(newEnumItem);
      }

      return in;
    }
  }
}
