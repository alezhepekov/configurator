#include "dglobalcontainer.h"

using namespace ConfiguratorObjects::Documents;

DGlobalContainer::DGlobalContainer():
BaseDocument()
{
  objectKind = OBJECT_KIND_DGLOBAL_CONTAINER;
  kind = DOC_KIND_GLOBAL_CONTAINER; 
}

DGlobalContainer::DGlobalContainer(const DGlobalContainer &other):
BaseDocument(other)
{
  for ( QList<Script*>::const_iterator i = other.scripts.constBegin(); i != other.scripts.constEnd(); i++ )
  {
    scripts.append(new Script(**i));
  } 
}

DGlobalContainer::~DGlobalContainer()
{
  for ( QList<Script*>::iterator i = scripts.begin(); i != scripts.end(); i++ )
  {
    delete *i;
  }
}

DGlobalContainer & DGlobalContainer::operator= (const DGlobalContainer &right)
{
  if ( this == &right ) return *this;

  BaseDocument::operator=(right);

  this->scripts.clear();  
  for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
  {
    this->scripts.append(new Script(**i));
  }  

  return *this;
}

const QList<Script*>* DGlobalContainer::getScripts() const
{
  return &scripts;
}

QList<Script*>* DGlobalContainer::getScripts()
{
  return &scripts;
}

void DGlobalContainer::setScripts(const QList<Script*> &newScripts)
{
  scripts = newScripts;
}

QString DGlobalContainer::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne; 

  return xmlDoc.toString();
}

void DGlobalContainer::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
  
}

bool DGlobalContainer::loadFromXML(const QString& xml)
{  
  return true;
}

void DGlobalContainer::loadFromXML(const QDomDocument& xmlDoc, const QDomElement& currentElement)
{  
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const DGlobalContainer &right)
    {
      out << static_cast<const BaseDocument&>(right);

      out << right.scripts.size();
      for ( QList<Script*>::const_iterator i = right.scripts.constBegin(); i != right.scripts.constEnd(); i++ )
      {
        out << *(*i);
      }

      return out;
    }

    QDataStream &operator>>(QDataStream &in, DGlobalContainer &right)
    {
      in >> static_cast<BaseDocument&>(right);

      int size;
      in >> size;
      for ( int i = 0; i < size; i++ )
      {
        Script *newScript = new Script();
        in >> *newScript;
        right.scripts.append(newScript);
      }

      return in;
    }
  }
}
