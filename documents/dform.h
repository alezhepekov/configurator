/***********************************************************************
* Module:   dform.h
* Author:   LGP
* Modified: 09 ������� 2006 �.
* Purpose:  ����� ������������� ��������� ���� "DForm"
***********************************************************************/

#ifndef DFORM_H
#define DFORM_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_FORM     = 0x0009;
    const int OBJECT_KIND_DFORM = 0x000F;

    class DForm: public BaseDocument
    {
      QList<Form*> forms;

    public:
      DForm();
      DForm(const DForm &other);
      ~DForm();

      DForm & DForm::operator= (const DForm &right);

      //����� ���������� ��������� �� ����� ���������
      const QList<Form*>* getForms() const;

      QList<Form*>* getForms();

      //����� ������������� ����� ���������
      void setForms(const QList<Form*>&);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DForm &);
      friend QDataStream &operator>>(QDataStream &, DForm &);    
    };

    QDataStream &operator<<(QDataStream &out, const DForm &right);
    QDataStream &operator>>(QDataStream &in, DForm &right);
  }
}

#endif
