/***********************************************************************
* Module:   tfloat.h
* Author:   LGP
* Modified: 04 ����� 2007 �.
* Purpose:  ������������� ���� ������ Float
***********************************************************************/

#ifndef FLOAT_H
#define FLOAT_H

#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_FLOAT = 0x0008;

      class Float: public BaseType
      {
        double min;
        double max;
        double defaultValue;
        bool required;

      public:
        Float();
        Float(const Float &other);
        ~Float();

        Float & Float::operator= (const Float &right);

        /*inline*/ double getMin() const;

        /*inline*/ void setMin(double newMin);

        /*inline*/ double getMax() const;

        /*inline*/ void setMax(double newMax);

        /*inline*/ double getDefault() const;

        /*inline*/ void setDefault(double newDefault);

        /*inline*/ bool getRequired() const;

        /*inline*/ void setRequired(bool newRequired);

        friend QDataStream &operator<<(QDataStream &, const Float &);
        friend QDataStream &operator>>(QDataStream &, Float &);
      };

      QDataStream &operator<<(QDataStream &out, const Float &right);
      QDataStream &operator>>(QDataStream &in, Float &right);
    }
  }
}

#endif
