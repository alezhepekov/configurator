/***********************************************************************
 * Module:   tstring.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������������� ���� ������ String
 ***********************************************************************/

#ifndef STRING_H
#define STRING_H 1

#include <QString>
#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_STRING    = 0x0002;

      class String: public BaseType
      {
        int length;
        QString defaultValue;
        bool required;

      public:
        String();
        String(const String &other);
        ~String();

        String & String::operator= (const String &right);

        /*inline*/ int getLength() const;

        /*inline*/ void setLength(int newLength);

        /*inline*/ QString getDefault() const;

        /*inline*/ void setDefault(const QString& newDefault);

        /*inline*/ bool getRequired() const;

        /*inline*/ void setRequired(bool newRequired);

        friend QDataStream &operator<<(QDataStream &, const String &);
        friend QDataStream &operator>>(QDataStream &, String &);
      };

      QDataStream &operator<<(QDataStream &out, const String &right);
      QDataStream &operator>>(QDataStream &in, String &right);
    }
  }
}

#endif
