#include "tnumber.h"

using namespace ConfiguratorObjects::Documents::DataType;

Number::Number():
BaseType(),
length(15),
precision(2),
min(0),
max(0),
defaultValue(0),
required(false)
{
  typeKind = DATA_TYPE_NUMBER;
}

Number::Number(const Number &other):
BaseType(other),
length(other.length),
precision(other.precision),
min(other.min),
max(other.max),
defaultValue(other.defaultValue),
required(other.required)
{
}

Number::~Number()
{ 
}

Number & Number::operator= (const Number &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);
  
  this->id = right.id;
  this->length = right.length;
  this->precision = right.precision;
  this->min = right.min;
  this->max = right.max;
  this->defaultValue = right.defaultValue;
  this->required = right.required;
  
  return *this;
}

int Number::getLength() const
{
   return length;
}

void Number::setLength(int newLength)
{
   length = newLength;
}

int Number::getPrecision() const
{
   return precision;
}

void Number::setPrecision(int newPrecision)
{
   precision = newPrecision;
}

int Number::getMin() const
{
   return min;
}

void Number::setMin(int newMin)
{
   min = newMin;
}

int Number::getMax() const
{
   return max;
}

void Number::setMax(int newMax)
{
   max = newMax;
}

int Number::getDefault() const
{
   return defaultValue;
}

void Number::setDefault(int newDefault)
{
   defaultValue = newDefault;
}

bool Number::getRequired() const
{
   return required;
}

void Number::setRequired(bool newRequired)
{
   required = newRequired;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const Number &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.length;
        out << right.precision;
        out << right.min;
        out << right.max;
        out << right.defaultValue;
        out << right.required;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, Number &right)
      {
        in >> static_cast<BaseType&>(right);
       
        in >> right.length;      
        in >> right.precision;
        in >> right.min;
        in >> right.max;      
        in >> right.defaultValue;
        in >> right.required;      

        return in;
      }
    }
  }
}
