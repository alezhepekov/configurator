/***********************************************************************
* Module:   tlink.h
* Author:   LGP
* Modified: 28 �������� 2006 �.
* Purpose:  ������������� ���� ������ "Link"
***********************************************************************/

#ifndef LINK_H
#define LINK_H

#include <QString>
#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_LINK = 0x0007;

      class Link: public BaseType
      {
        int flag;
        int linkedTableRecordId;
        QString linkedTableName;

      public:
        Link();
        Link(const Link &other);
        ~Link();

        Link & Link::operator= (const Link &right);

        //����� ���������� ��������� ���� �������
        /*inline*/ int getFlag() const;

        //����� ������������� ��������� ���� �������
        /*inline*/ void setFlag(int);

        //����� ���������� ��� ������� �� ������� ��������� ������
        /*inline*/ QString getLinkedTableName() const;

        //����� ������������� ��� ������� �� ������� ��������� ������
        /*inline*/ void setLinkedTableName(const QString &);

        //����� ���������� ������������� ������ � ��������� �������
        /*inline*/ int getLinkedTableRecordId() const;

        //����� ������������� ������������� ������ � ��������� �������
        /*inline*/ void setLinkedTableRecordId(int);  

        friend QDataStream &operator<<(QDataStream &, const Link &);
        friend QDataStream &operator>>(QDataStream &, Link &);
      };

      QDataStream &operator<<(QDataStream &out, const Link &right);
      QDataStream &operator>>(QDataStream &in, Link &right);
    }
  }
}

#endif
