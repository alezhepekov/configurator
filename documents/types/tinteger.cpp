#include "tinteger.h"

using namespace ConfiguratorObjects::Documents::DataType;

Integer::Integer():
BaseType(),
min(0),
max(0),
defaultValue(0),
required(false)
{
  typeKind = DATA_TYPE_INTEGER;
}

Integer::Integer(const Integer &other):
BaseType(other),
min(other.min),
max(other.max),
defaultValue(other.defaultValue),
required(other.required)
{
}

Integer::~Integer()
{
}

Integer & Integer::operator= (const Integer &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);

  this->min = right.min;
  this->max = right.max;
  this->defaultValue = right.defaultValue;
  this->required = right.required;

  return *this;
}

int Integer::getMin() const
{
  return min;
}

void Integer::setMin(int newMin)
{
  min = newMin;
}

int Integer::getMax() const
{
  return max;
}

void Integer::setMax(int newMax)
{
  max = newMax;
}

int Integer::getDefault() const
{
  return defaultValue;
}

void Integer::setDefault(int newDefault)
{
  defaultValue = newDefault;
}

bool Integer::getRequired() const
{
  return required;
}

void Integer::setRequired(bool newRequired)
{
  required = newRequired;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const Integer &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.min;
        out << right.max;
        out << right.defaultValue;
        out << right.required;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, Integer &right)
      {
        in >> static_cast<BaseType&>(right);

        in >> right.min; 
        in >> right.max;
        in >> right.defaultValue;
        in >> right.required;     

        return in;
      }
    }
  }
}
