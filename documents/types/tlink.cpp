#include "tlink.h"

using namespace ConfiguratorObjects::Documents::DataType;

Link::Link():
BaseType(),
flag(0),
linkedTableName(QString()),
linkedTableRecordId(-1)
{  
  typeKind = DATA_TYPE_LINK;  
}

Link::Link(const Link &other):
BaseType(other),
flag(other.flag),
linkedTableName(other.linkedTableName),
linkedTableRecordId(other.linkedTableRecordId)
{
}

Link::~Link()
{   
}

Link & Link::operator= (const Link &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);

  this->flag = right.flag;
  this->linkedTableName = right.linkedTableName;
  this->linkedTableRecordId = right.linkedTableRecordId;

  return *this;
}

int Link::getFlag() const
{
  return flag;
}

void Link::setFlag(int newFlag)
{
  flag = newFlag;
}

QString Link::getLinkedTableName() const
{
  return linkedTableName; 
}

void Link::setLinkedTableName(const QString& newLinkedTableName)
{
  linkedTableName = newLinkedTableName;
}

int Link::getLinkedTableRecordId() const
{
  return linkedTableRecordId;
}

void Link::setLinkedTableRecordId(int newLinkedTableRecordId)
{
  linkedTableRecordId = newLinkedTableRecordId;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const Link &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.flag;
        out << right.linkedTableName;
        out << right.linkedTableRecordId;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, Link &right)
      {
        in >> static_cast<BaseType&>(right);

        in >> right.flag;       
        in >> right.linkedTableName;       
        in >> right.linkedTableRecordId;        

        return in;
      }
    }
  }
}
