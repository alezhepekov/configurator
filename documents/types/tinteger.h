/***********************************************************************
* Module:   tinteger.h
* Author:   LGP
* Modified: 04 ����� 2007 �.
* Purpose:  ������������� ���� ������ Integer
***********************************************************************/

#ifndef INTEGER_H
#define INTEGER_H 1

#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_INTEGER = 0x0009;

      class Integer: public BaseType
      {
        int min;
        int max;
        int defaultValue;
        bool required;

      public:
        Integer();
        Integer(const Integer &other);
        ~Integer();

        Integer & Integer::operator= (const Integer &right);

        /*inline*/ int getMin() const;

        /*inline*/ void setMin(int newMin);

        /*inline*/ int getMax() const;

        /*inline*/ void setMax(int newMax);

        /*inline*/ int getDefault() const;

        /*inline*/ void setDefault(int newDefault);

        /*inline*/ bool getRequired() const;

        /*inline*/ void setRequired(bool newRequired);

        friend QDataStream &operator<<(QDataStream &, const Integer &);
        friend QDataStream &operator>>(QDataStream &, Integer &);
      };

      QDataStream &operator<<(QDataStream &out, const Integer &right);
      QDataStream &operator>>(QDataStream &in, Integer &right);
    }
  }
}

#endif
