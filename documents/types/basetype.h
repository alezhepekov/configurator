/***********************************************************************
 * Module:   basetype.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������� ����� ������������� ���� ������ �������������
 ***********************************************************************/

#ifndef BASE_TYPE_H
#define BASE_TYPE_H

#include <QDataStream>

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      class BaseType
      {
      protected:
        int id;
        int typeKind;

      public:
        BaseType();
        BaseType(const BaseType &other);
        virtual ~BaseType() = 0;

        BaseType & BaseType::operator= (const BaseType &right);

        //����� ���������� ������������� ���� 
        /*inline*/ int getId() const;

        //����� ������������� ������������� ����
        /*inline*/ void setId(const int);

        //����� ���������� ��� ���� 
        /*inline*/ int getTypeKind() const;

        //����� ���������� ��� ���� 
        /*inline*/ void setTypeKind(const int);
        
        friend QDataStream &operator<<(QDataStream &, const BaseType &);
        friend QDataStream &operator>>(QDataStream &, BaseType &);      
      };

      QDataStream &operator<<(QDataStream &out, const BaseType &right);
      QDataStream &operator>>(QDataStream &in, BaseType &right);
    }
  }
}

#endif
