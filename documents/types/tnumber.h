/***********************************************************************
 * Module:   tnumber.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������������� ���� ������ Number
 ***********************************************************************/

#ifndef NUMBER_H
#define NUMBER_H

#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_NUMBER = 0x0001;

      class Number: public BaseType
      {
        int length;
        int precision;
        int min;
        int max;
        int defaultValue;
        bool required;

      public:
        Number();
        Number(const Number &other);
        ~Number();

        Number & Number::operator= (const Number &right);

        /*inline*/ int getLength() const;

        /*inline*/ void setLength(int newLength);

        /*inline*/ int getPrecision() const;

        /*inline*/ void setPrecision(int newPrecision);

        /*inline*/ int getMin() const;

        /*inline*/ void setMin(int newMin);

        /*inline*/ int getMax() const;

        /*inline*/ void setMax(int newMax);

        /*inline*/ int getDefault() const;

        /*inline*/ void setDefault(int newDefault);

        /*inline*/ bool getRequired() const;

        /*inline*/ void setRequired(bool newRequired);

        friend QDataStream &operator<<(QDataStream &, const Number &);
        friend QDataStream &operator>>(QDataStream &, Number &);        
      };

      QDataStream &operator<<(QDataStream &out, const Number &right);
      QDataStream &operator>>(QDataStream &in, Number &right);
    }
  }
}

#endif
