#include "tlogical.h"

using namespace ConfiguratorObjects::Documents::DataType;

Logical::Logical():
BaseType(),
defaultValue(false),
required(false)
{
  typeKind = DATA_TYPE_BOOLEAN;
}

Logical::Logical(const Logical &other):
BaseType(other),
defaultValue(other.defaultValue),
required(other.required)
{  
}

Logical::~Logical()
{   
}

Logical & Logical::operator= (const Logical &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);

  this->defaultValue = right.defaultValue;
  this->required = right.required;

  return *this;
}

bool Logical::getDefault() const
{
  return defaultValue;
}

void Logical::setDefault(bool newDefault)
{
  defaultValue = newDefault;
}

bool Logical::getRequired() const
{
  return required;
}

void Logical::setRequired(bool newRequired)
{
  required = newRequired;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const Logical &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.defaultValue;
        out << right.required;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, Logical &right)
      {
        in >> static_cast<BaseType&>(right);
      
        in >> right.defaultValue;      
        in >> right.required;
      
        return in;
      }
    }
  }
}
