/***********************************************************************
 * Module:   timage.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������������� ���� ������ Image
 ***********************************************************************/

#ifndef IMAGE_H
#define IMAGE_H

#include "basetype.h"
#include <QPixmap>

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_IMAGE = 0x0006;

      class Image: public BaseType
      {
        QPixmap image;

      public:
        Image();
        Image(const Image &other);
        ~Image();

        Image & Image::operator= (const Image &right);

        const QPixmap *getImage() const;
        QPixmap *getImage();

        void setImage(const QPixmap &newImage);
     
        friend QDataStream &operator<<(QDataStream &, const Image &);
        friend QDataStream &operator>>(QDataStream &, Image &);
      };

      QDataStream &operator<<(QDataStream &out, const Image &right);
      QDataStream &operator>>(QDataStream &in, Image &right);
    }
  }
}

#endif
