/***********************************************************************
 * Module:   tdatetime.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������������� ���� ������ DateTime
 ***********************************************************************/

#ifndef DATE_TIME_H
#define DATE_TIME_H 1

#include <QDateTime>
#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_DATETIME  = 0x0003;

      class DateTime: public BaseType
      {
        QDateTime defaultValue;
        bool required;

      public:
        DateTime();
        DateTime(const DateTime &other);
        ~DateTime();

        DateTime & DateTime::operator= (const DateTime &right);

        /*inline*/ QDateTime getDefault() const;

        /*inline*/ void setDefault(const QDateTime &newDefault);

        /*inline*/ bool getRequired() const;

        /*inline*/ void setRequired(bool newRequired);

        friend QDataStream &operator<<(QDataStream &, const DateTime &);
        friend QDataStream &operator>>(QDataStream &, DateTime &);
      };

      QDataStream &operator<<(QDataStream &out, const DateTime &right);
      QDataStream &operator>>(QDataStream &in, DateTime &right);
    }
  }
}

#endif
