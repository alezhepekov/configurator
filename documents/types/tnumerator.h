/***********************************************************************
 * Module:   tnumerator.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������������� ���� ������ Numerator (��������� ����������)
 ***********************************************************************/

#ifndef NUMERATOR_H
#define NUMERATOR_H

#include <QString>
#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_NUMERATOR = 0x0005;

      class Numerator: public BaseType
      {
        bool editable;
        QString initValue;
        QString generator;

      public:
        Numerator();
        Numerator(const Numerator &other);
        ~Numerator();

        Numerator & Numerator::operator= (const Numerator &right);

        /*inline*/ bool getEditable() const;

        /*inline*/ void setEditable(bool newEditable);

        /*inline*/ QString getInit() const;

        /*inline*/ void setInit(const QString &);

        /*inline*/ QString getGenerator() const;

        /*inline*/ void setGenerator(QString newGenerator);

        friend QDataStream &operator<<(QDataStream &, const Numerator &);
        friend QDataStream &operator>>(QDataStream &, Numerator &);
      };

      QDataStream &operator<<(QDataStream &out, const Numerator &right);
      QDataStream &operator>>(QDataStream &in, Numerator &right);
    }
  }
}

#endif
