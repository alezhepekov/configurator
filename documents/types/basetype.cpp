#include "basetype.h"

using namespace ConfiguratorObjects::Documents::DataType;

BaseType::BaseType():
id(-1),
typeKind(0)
{
}

BaseType::BaseType(const BaseType &other):
id(other.id),
typeKind(other.typeKind)
{
}

BaseType::~BaseType()
{
}

BaseType & BaseType::operator= (const BaseType &right)
{
  if ( this == &right ) return *this;
  
  this->id = right.id;
  this->typeKind = right.typeKind;

  return *this;
}

int BaseType::getId() const
{
  return id;
}

void BaseType::setId(const int newId)
{
  id = newId;
}

int BaseType::getTypeKind() const
{
  return typeKind;
}

void BaseType::setTypeKind(const int newTypeKind)
{
  typeKind = newTypeKind;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const BaseType &right)
      {
        out << right.id;
        out << right.typeKind;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, BaseType &right)
      {       
        in >> right.id;       
        in >> right.typeKind;      

        return in;
      }
    }
  }
}
