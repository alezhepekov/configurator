#include "tfloat.h"

using namespace ConfiguratorObjects::Documents::DataType;

Float::Float():
BaseType(),
min(0.0),
max(0.0),
defaultValue(0.0),
required(false)
{
  typeKind = DATA_TYPE_FLOAT;
}

Float::Float(const Float &other):
BaseType(other),
min(other.min),
max(other.max),
defaultValue(other.defaultValue),
required(other.required)
{  
}

Float::~Float()
{
}

Float & Float::operator= (const Float &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);

  this->min = right.min;
  this->max = right.max;
  this->defaultValue = right.defaultValue;
  this->required = right.required;

  return *this;
}

double Float::getMin() const
{
  return min;
}

void Float::setMin(double newMin)
{
  min = newMin;
}

double Float::getMax() const
{
  return max;
}

void Float::setMax(double newMax)
{
  max = newMax;
}

double Float::getDefault() const
{
  return defaultValue;
}

void Float::setDefault(double newDefault)
{
  defaultValue = newDefault;
}

bool Float::getRequired() const
{
  return required;
}

void Float::setRequired(bool newRequired)
{
  required = newRequired;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const Float &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.min;
        out << right.max;
        out << right.defaultValue;
        out << right.required;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, Float &right)
      {
        in >> static_cast<BaseType&>(right);

        in >> right.min;   
        in >> right.max;       
        in >> right.defaultValue;    
        in >> right.required;   

        return in;
      }
    }
  }
}
