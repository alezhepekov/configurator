/***********************************************************************
 * Module:   tlogical.h
 * Author:   LGP
 * Modified: 25 ���� 2006 �.
 * Purpose:  ������������� ���� Logical
 ***********************************************************************/

#ifndef LOGICAL_H
#define LOGICAL_H

#include "basetype.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      const int DATA_TYPE_BOOLEAN   = 0x0004;

      class Logical: public BaseType
      {
        bool defaultValue;
        bool required;

      public:
        Logical();
        Logical(const Logical &other);
        ~Logical();

        Logical & Logical::operator= (const Logical &right);

        /*inline*/ bool getDefault() const;

        /*inline*/ void setDefault(bool newDefault);

        /*inline*/ bool getRequired() const;

        /*inline*/ void setRequired(bool newRequired);

        friend QDataStream &operator<<(QDataStream &, const Logical &);
        friend QDataStream &operator>>(QDataStream &, Logical &);
      };

      QDataStream &operator<<(QDataStream &out, const Logical &right);
      QDataStream &operator>>(QDataStream &in, Logical &right);
    }
  }
}

#endif
