#include "tstring.h"

using namespace ConfiguratorObjects::Documents::DataType;

String::String():
BaseType(),
length(10),
defaultValue(QString()),
required(false)
{
  typeKind = DATA_TYPE_STRING;  
}

String::String(const String &other):
BaseType(other),
length(other.length),
defaultValue(other.defaultValue),
required(other.required)
{ 
}

String::~String()
{   
}

String & String::operator= (const String &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);
  
  this->length = right.length;
  this->defaultValue = right.defaultValue;
  this->required = right.required;

  return *this;
}

int String::getLength() const
{
   return length;
}

void String::setLength(int newLength)
{
   length = newLength;
}

QString String::getDefault() const
{
   return defaultValue;
}

void String::setDefault(const QString& newDefault)
{
   defaultValue = newDefault;
}

bool String::getRequired() const
{
   return required;
}

void String::setRequired(bool newRequired)
{
   required = newRequired;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const String &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.length;     
        out << right.defaultValue;
        out << right.required;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, String &right)
      {
        in >> static_cast<BaseType&>(right);

        in >> right.length;     
        in >> right.defaultValue;    
        in >> right.required;    

        return in;
      }
    }
  }
}
