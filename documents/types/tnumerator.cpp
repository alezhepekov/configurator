#include "tnumerator.h"

using namespace ConfiguratorObjects::Documents::DataType;

Numerator::Numerator():
BaseType(),
editable(false),
initValue(QString("0")),
generator(QString("NEXT"))
{
  typeKind = DATA_TYPE_NUMERATOR;  
}

Numerator::Numerator(const Numerator &other):
BaseType(other),
editable(other.editable),
initValue(other.initValue),
generator(other.generator)
{  
}

Numerator::~Numerator()
{
}

Numerator & Numerator::operator= (const Numerator &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);

  this->editable = right.editable;
  this->initValue = right.initValue;
  this->generator = right.generator;

  return *this;
}

bool Numerator::getEditable() const
{
  return editable;
}

void Numerator::setEditable(bool newEditable)
{
  editable = newEditable;
}

QString Numerator::getInit() const
{
  return initValue;
}

void Numerator::setInit(const QString& newInitValue)
{
  initValue = newInitValue;
}

QString Numerator::getGenerator() const
{
  return generator;
}

void Numerator::setGenerator(QString newGenerator)
{
  generator = newGenerator;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const Numerator &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.editable;
        out << right.initValue;
        out << right.generator;      

        return out;
      }

      QDataStream &operator>>(QDataStream &in, Numerator &right)
      {
        in >> static_cast<BaseType&>(right);    

        in >> right.editable;      
        in >> right.initValue;       
        in >> right.generator;        

        return in;
      }
    }
  }
}
