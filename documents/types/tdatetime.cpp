#include "tdatetime.h"

using namespace ConfiguratorObjects::Documents::DataType;

DateTime::DateTime():
BaseType(),
defaultValue(QDateTime::currentDateTime()),
required(false)
{
  typeKind = DATA_TYPE_DATETIME; 
}

DateTime::DateTime(const DateTime &other):
BaseType(other),
defaultValue(other.defaultValue),
required(other.required)
{
}

DateTime::~DateTime()
{
}

DateTime & DateTime::operator= (const DateTime &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);

  this->defaultValue = right.defaultValue;
  this->required = right.required;

  return *this;
}

QDateTime DateTime::getDefault() const
{
  return defaultValue;
}

void DateTime::setDefault(const QDateTime &newDefault)
{
  defaultValue = newDefault;
}

bool DateTime::getRequired() const
{
  return required;
}

void DateTime::setRequired(bool newRequired)
{
  required = newRequired;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const DateTime &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.defaultValue;
        out << right.required;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, DateTime &right)
      {
        in >> static_cast<BaseType&>(right);  

        in >> right.defaultValue;
        in >> right.required;       

        return in;
      }
    }
  }
}
