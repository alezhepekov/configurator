#include "timage.h"

using namespace ConfiguratorObjects::Documents::DataType;

Image::Image():
BaseType(),
image(QPixmap())
{
  typeKind = DATA_TYPE_IMAGE;
}

Image::Image(const Image &other):
BaseType(other),
image(other.image)
{
}

Image::~Image()
{
}

Image & Image::operator= (const Image &right)
{
  if ( this == &right ) return *this;

  BaseType::operator=(right);

  image = right.image;

  return *this;
}

const QPixmap * Image::getImage() const
{
  return &image;
}

QPixmap * Image::getImage()
{
  return &image;
}

void Image::setImage(const QPixmap &newImage)
{
  image = newImage;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    namespace DataType
    {
      QDataStream &operator<<(QDataStream &out, const Image &right)
      {
        out << static_cast<const BaseType&>(right);

        out << right.image;

        return out;
      }

      QDataStream &operator>>(QDataStream &in, Image &right)
      {
        in >> static_cast<BaseType&>(right);
        
        in >> right.image;        

        return in;
      }
    }
  }
}
