/***********************************************************************
* Module:   dglobalthread.h
* Author:   LGP
* Modified: 25 ���� 2007 �.
* Purpose:  ����� ������������� ��������� ���� "DGlobalThread"
***********************************************************************/

#ifndef D_GLOBAL_THREAD_H
#define D_GLOBAL_THREAD_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_GLOBAL_THREAD     = 0x000D;
    const int OBJECT_KIND_DGLOBAL_THREAD = 0x0013;

    class DGlobalThread: public BaseDocument
    {
      QList<Script*> scripts;

    public:
      DGlobalThread();
      DGlobalThread(const DGlobalThread &other);
      ~DGlobalThread();

      DGlobalThread & DGlobalThread::operator= (const DGlobalThread &right);

      //����� ���������� ��������� �� ������� ���������
      const QList<Script*>* getScripts() const;

      QList<Script*>* getScripts();

      //����� ������������� ������� ���������
      void setScripts(const QList<Script*> &newScripts);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DGlobalThread &);
      friend QDataStream &operator>>(QDataStream &, DGlobalThread &);    
    };

    QDataStream &operator<<(QDataStream &out, const DGlobalThread &right);
    QDataStream &operator>>(QDataStream &in, DGlobalThread &right);
  }
}

#endif
