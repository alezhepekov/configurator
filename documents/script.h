/***********************************************************************
* Module:   script.h
* Author:   LGP
* Modified: 25 ���� 2006 �.
* Purpose:  ����� ������������� �������
***********************************************************************/

#ifndef SCRIPT_H
#define SCRIPT_H

#include "baseobject.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int OBJECT_KIND_SCRIPT = 0x000E;

    class Script: public BaseObject
    {
      QString body;

    public:   
      Script();
      Script(const Script &other);
      Script(const QString &script);
      ~Script();

      Script & Script::operator= (const Script &right);

      /*inline */QString getBody() const;

      /*inline */void setBody(const QString &);

      friend QDataStream &operator<<(QDataStream &, const Script &);
      friend QDataStream &operator>>(QDataStream &, Script &);      
    };

    QDataStream &operator<<(QDataStream &out, const Script &right);
    QDataStream &operator>>(QDataStream &in, Script &right);
  }
}

#endif
