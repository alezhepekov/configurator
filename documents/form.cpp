#include "form.h"

using namespace ConfiguratorObjects::Documents;

Form::Form():
BaseObject(),
kind(FORM_KIND_DEFAULT),
body(Script()),
module(Script())
{
  objectKind = OBJECT_KIND_FORM;  
}

Form::Form(const Form &other):
BaseObject(other),
kind(other.kind),
body(other.body),
module(other.module)
{
}

Form::~Form()
{  
}

Form & Form::operator= (const Form &right)
{
  if ( this == &right ) return *this;

  BaseObject::operator=(right);
  
  kind = right.kind;
  body = right.body;
  module = right.module;

  return *this;
}

int Form::getKind() const
{
  return kind;
}

void Form::setKind(int newKind)
{
  kind = newKind;
}

Script * Form::getBody()
{
  return &body;
}

const Script *Form::getBody() const
{
  return &body;
}

void Form::setBody(const Script& newBody)
{
  body = newBody;
}

Script * Form::getModule()
{
  return &module;
}

const Script * Form::getModule() const
{
  return &module;
}

void Form::setModule(const Script& newModule)
{
  module = newModule;
}

namespace ConfiguratorObjects
{
  namespace Documents
  {
    QDataStream &operator<<(QDataStream &out, const Form &right)
    {
      out << static_cast<const BaseObject&>(right);
      
      out << right.kind;
      out << right.body;
      out << right.module;

      return out;
    }

    QDataStream &operator>>(QDataStream &in, Form &right)
    {
      in >> static_cast<BaseObject&>(right);
   
      in >> right.kind;   
      in >> right.body;     
      in >> right.module;     

      return in;
    }
  }
}
