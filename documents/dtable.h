/***********************************************************************
* Module:   dtable.h
* Author:   LGP
* Modified: 22 ���� 2007 �.
* Purpose:  ����� ������������� ��������� ���� "DTable"
***********************************************************************/

#ifndef DTABLE_H
#define DTABLE_H

#include "basedocument.h"

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int DOC_KIND_TABLE     = 0x000A;
    const int OBJECT_KIND_DTABLE = 0x0010;

    class DTable: public BaseDocument
    {
      QList<Accessory*> accessories;

    public:
      DTable();
      DTable(const DTable &other);
      ~DTable();

      DTable & DTable::operator= (const DTable &right);

      //����� ���������� ��������� �� ������ ���������� ���������
      const QList<Accessory*>* getAccessories() const;

      QList<Accessory*>* getAccessories();

      //����� ������������� ��������� ���������
      void setAccessories(const QList<Accessory*>&);

      //����� ���������� ���c����� ������������� xml-������������� ���������
      QString toXML();

      //����� ��������� �������� ������ �� xml-���������
      bool loadFromXML(const QString&);

      //����� ��������� ������������ xml-������������� ��������� � ���������� ��� � ���������� ���� xml-������
      void toXML(QDomDocument&, QDomElement&);

      //����� ��������� �������� ��������� �� xml-������������� � ���������� ���� xml-������
      void loadFromXML(const QDomDocument&, const QDomElement&);

      friend QDataStream &operator<<(QDataStream &, const DTable &);
      friend QDataStream &operator>>(QDataStream &, DTable &);    
    };

    QDataStream &operator<<(QDataStream &out, const DTable &right);
    QDataStream &operator>>(QDataStream &in, DTable &right);
  }
}

#endif
