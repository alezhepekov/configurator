/***********************************************************************
* Module:   baseobject.h
* Author:   LGP
* Modified: 30 ����� 2007 �.
* Purpose:  ������� ����� ������� ������������
***********************************************************************/

#ifndef BASE_OBJECT_H
#define BASE_OBJECT_H

#include <QString>
#include <QMap>
#include <QDataStream>

namespace ConfiguratorObjects
{
  namespace Documents
  {
    const int OBJECT_KIND_BASE_OBJECT = 0x0001;

    class BaseObject
    {
    protected:
      //�������������
      int id;

      //������������� ������� � ������ ���
      int gjoTreeId;

      //��� �������
      int objectKind;    

      //���������� ���
      QString systemName;    

      //��������
      QMap<int, QString> translates;

    public:
      BaseObject();
      BaseObject(const BaseObject &other);     
      virtual ~BaseObject();

      BaseObject & BaseObject::operator= (const BaseObject &right);

      //����� ���������� ������������� �������
      /*inline */int getId() const;

      //����� ������������� ������������� �������
      /*inline */void setId(int);

      //����� ���������� ������������� ������� � ������ ���
      /*inline */int getGJOTreeId() const;

      //����� ������������� ������������� ������� � ������ ���
      /*inline */void setGJOTreeId(int);

      //����� ���������� ��� �������
      /*inline */int getObjectKind() const;  

      //����� ���������� ��������� ��� �������
      /*inline */QString getSystemName() const;

      //����� ������������� ��������� ��� �������
      /*inline */void setSystemName(const QString &);

      //����� ���������� �������� �������
      const QMap<int, QString> *getTranslates() const;

      QMap<int, QString> *getTranslates();

      //����� ������������� �������� �������
      void setTranslates(const QMap<int, QString> &);

      friend QDataStream &operator<<(QDataStream &, const BaseObject &);
      friend QDataStream &operator>>(QDataStream &, BaseObject &);
    };

    QDataStream &operator<<(QDataStream &out, const BaseObject &right);
    QDataStream &operator>>(QDataStream &in, BaseObject &right);
  }
}

#endif
