#ifndef CONFIGURATION_LIST_FORM_H
#define CONFIGURATION_LIST_FORM_H

#include <QDialog>
#include <QToolBar>
#include <QTreeWidget>
#include <QStatusBar>
#include <QVBoxLayout>
#include "generatedfiles/ui_configurationlistform.h"
#include "configurationlist.h"
#include "userdatabaselist.h"
#include "objectlist.h"

namespace ConfiguratorObjects
{
  struct CLItemData
  {
    int id;
    int kind;
  };  
  
  class ConfigurationListForm : public QDialog, Ui_ConfigurationListForm
  {
    Q_OBJECT

    //���� ����� ������
    static const int ITEM_KIND_NO_MEANING            = 0;
    static const int ITEM_KIND_CONFIGURATION         = 1;
    static const int ITEM_KIND_HEADER                = 2;
    static const int ITEM_KIND_CONFIGURATIONS        = 3;
    static const int ITEM_KIND_USER_DATABASES        = 4;
    static const int ITEM_KIND_USER_DATABASE         = 5;
    static const int ITEM_KIND_CONNECTION_INFO       = 6;
    static const int ITEM_KIND_USER_DATABASE_ID      = 7;
    static const int ITEM_KIND_LINKED_CONFIGURATIONS = 8;

    bool downloadConfigurationFlag; 

    ConfigurationList configurationList;

    UserDatabaseList userDatabaseList;

    //������������� ������� ������������
    int currentConfigurationId;    

    ObjectList currentObjectList;    

    QToolBar    *toolBar;
    QTreeWidget *treeWidget;
    QStatusBar  *statusBar;
    QVBoxLayout *layout;

    QAction *actionGetConfigurationList;
    QAction *actionNewDatabase;
    QAction *actionLoadConfiguration;
    QAction *actionRemoveDatabase;
    QAction *actionReleaseConfiguration;
    QAction *actionUploadDatabase;
    QAction *actionDownloadDatabase;  

  public:
    ConfigurationListForm(QWidget *parent = 0);
    ~ConfigurationListForm();

    ConfigurationList *getConfigurations();
    void setConfigurations(const ConfigurationList &newList);

    UserDatabaseList *getUserDatabases();
    void setUserDatabases(const UserDatabaseList &newList);

    QTreeWidgetItem *appendConnectionInfo(QTreeWidgetItem *current, const ConnectionInfo &cInfo);
    QTreeWidgetItem *appendConfigurationHeader(QTreeWidgetItem *current, const ConfigurationHeader &header);

    void showData();

    void showConfigurationsInfo();

    void showUserDatabasesInfo();

    ObjectList *getCurrentObjectList();

signals:  
    void processConfiguration(const ConfigurationHeader &); 

  protected:
    bool event(QEvent *);

  private slots:
    void currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *);

    void getConfigurationList();
    void newDatabase();
    void loadConfiguration();
    void removeDatabase();

  public slots:
    void releaseConfiguration();

  private slots:
    void uploadDatabase();
    void downloadDatabase();  

  public:
    void newConfigurationCmd(const ConfigurationInfo &configInfo);
    void getConfigurationListCmd();
    void getConfigurationCmd(const int id);
    void saveConfigurationCmd(const ObjectList &objList, const ConfigurationHeader &header);
    void removeConfigurationCmd(const int id);
    void releaseConfigurationCmd(const ConnectionInfo &cInfo);    

    void getUserDatabaseListCmd();
    void newUserDatabaseCmd(const ConnectionInfo &cInfo);
    void removeUserDatabaseCmd(const int id);
    void dumpUserDatabaseCmd(const int id);
    void restoreUserDatabaseCmd(const int id, const QByteArray &data);

  protected slots:
    void processCmdNewConfiguration(const qint32 &result, const QByteArray &data);
    void processCmdConfigurationList(const qint32 &result, const QByteArray &data);
    void processCmdConfiguration(const qint32 &result, const QByteArray &data);
    void processCmdSaveConfiguration(const qint32 &result, const QByteArray &data);
    void processCmdRemoveConfiguration(const qint32 &result, const QByteArray &data);
    void processCmdReleaseConfiguration(const qint32 &result, const QByteArray &data);

    void processCmdUserDatabaseList(const qint32 &result, const QByteArray &data);
    void processCmdNewUserDatabase(const qint32 &result, const QByteArray &data);
    void processCmdRemoveUserDatabase(const qint32 &result, const QByteArray &data);
    void processCmdDumpUserDatabase(const qint32 &result, const QByteArray &data);
	  void processCmdRestoreUserDatabase(const qint32 &result, const QByteArray &data);
  };
}

Q_DECLARE_METATYPE(ConfiguratorObjects::CLItemData);

#endif
