/***********************************************************************
* Module:   privilege.h
* Author:   LGP
* Modified: 06 апреля 2007 г.
* Purpose:  Представление привилегии конфигурации
***********************************************************************/

#ifndef PRIVILEGE_H
#define PRIVILEGE_H

#include <QString>
#include <QDataStream>

namespace ConfiguratorObjects
{  
  class Privilege
  {
    public:
      Privilege();
      Privilege(const Privilege &);
      ~Privilege();

      /*inline*/ int getId() const;
      /*inline*/ void setId(int newId);

      /*inline*/ QString getExternalId() const;
      /*inline*/ void setExternalId(const QString &newExternalId);

      /*inline*/ QString getName() const;
      /*inline*/ void setName(const QString &newName);

    protected:
    private:
      int id;

      QString externalId;

      QString name;
  };

  QDataStream &operator<<(QDataStream &, const Privilege &);
  QDataStream &operator>>(QDataStream &, Privilege &);
}

#endif
