#ifndef _ABSTRACT_CONFIGURATOR_H
#define _ABSTRACT_CONFIGURATOR_H

#include <QObject>

#include "../GJOTree/src/iddef.h"

class AbstractDocument;
class AbstractComponent;

class QMenu;
class QToolBar;
class QWidget;

class AbstractConfigurator : public QObject
{
	Q_OBJECT

public:
	AbstractConfigurator(QWidget *parent) : _parent(parent) { }	// �������� - ��������� �� MainWindow
	virtual ~AbstractConfigurator() { }
	QWidget *parent() const { return _parent; }

	virtual QMenu *menu() const = 0;		// ���� �������������
	virtual QWidget *workingArea() const = 0;		// ������� ������������ �������������
	virtual QWidget *dockWindowContents() const = 0;	// ���������� ���������� ���� �������������
  virtual QToolBar *toolBar() const = 0;	

  virtual void reloadDocument(int id)  = 0;
	
signals:
	void openDocument(GJO::Id id);
	void documentDeleted(GJO::Id id);
  void documentContentsChanged(GJO::Id id);
  void refreshXMLView(const QList<GJO::Id> &idList);
  void parseXMLView(const QList<GJO::Id> &idList); 
	
	void opened();		// ������������ ���� �������. ������ ���������.
	void closed();		// ��������

private:
	QWidget *_parent;
};

#endif
