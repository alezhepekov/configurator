#include "configuration.h"

#include <QApplication>
#include <QTextStream>

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;
using namespace SQL;

Configuration::Configuration()
{ 
  dbControlObject = new DBControl(); 
}

Configuration::~Configuration()
{
  delete dbControlObject;
}

bool Configuration::createDatabase(ConnectionInfo& connectionInfo)
{
  return dbControlObject->createDatabase(connectionInfo);
}

bool Configuration::connect(ConnectionInfo& connectionInfo)
{
  return dbControlObject->connect(connectionInfo);
}

void Configuration::disconnect()
{
  if ( dbControlObject->getDatabase()->isOpen() )
  {  
    dbControlObject->getDatabase()->close();
  }
}

void Configuration::invalidLinkDetected(const Link *link, const QString &linkedObjectName)
{
  QByteArray ba;
  QDataStream out(&ba, QIODevice::WriteOnly);

  out << linkedObjectName;
  out << *link;

  lastError = QString("Error: Invalid link !!!\n{LinkedObjectName=%1; LinkedTableName=%2; LinkedTableRecordId=%3}")
              .arg(linkedObjectName)
              .arg(link->getLinkedTableName())
              .arg(link->getLinkedTableRecordId());

  std::cout << QString("void Configuration::invalidLinkDetected(const Link *link, const QString &linkedObjectName)\n%1").arg(lastError).toLocal8Bit().data() << std::endl; 
}

bool Configuration::checkLink(QSqlDatabase &db, QString &linkedObjectName, const Link *type)
{
  QSqlQuery query(*dbControlObject->getDatabase());

  QString name = type->getLinkedTableName();
  if ( name.compare("Documents") )
  {
    invalidLinkDetected(type, linkedObjectName);
    return false;
  }
 
  if ( type->getLinkedTableRecordId() == -1 )
  {
    invalidLinkDetected(type, linkedObjectName);
    return false;
  }

  //������ ����� ������� �� ������
  QString sql = "SELECT Language1 FROM Objects.Translates WHERE Id = "
                "(SELECT TranslatesId FROM Objects.Documents WHERE Id = :Id)";
  query.prepare(sql);
  query.bindValue(":Id", type->getLinkedTableRecordId());      

  if ( !query.exec() )
  {
    invalidLinkDetected(type, linkedObjectName);
    lastError = query.lastError().text();    
    return false;
  }      

  if ( !query.first() )
  {
    invalidLinkDetected(type, linkedObjectName);
    return false;
  }

  PunyCode pc;
  linkedObjectName = pc.encode(query.record().field("Language1").value().toString());      

  if ( linkedObjectName.isEmpty() )
  {
    invalidLinkDetected(type, linkedObjectName);
    return false;
  }

  //�������� ������� ������
  QSqlQuery udbQuery(db);
  sql = "SELECT Id FROM SysInfo WHERE SystemName = :SystemName AND Kind = 'Document'";
  udbQuery.prepare(sql);
  udbQuery.bindValue(":SystemName", linkedObjectName);

  if ( !udbQuery.exec() )
  {
    invalidLinkDetected(type, linkedObjectName);
    lastError = udbQuery.lastError().text();
    return false;
  }

  bool bf1 = false;
  if ( !udbQuery.first() )
  {
    bf1 = true;
  }

  sql = "SELECT Id FROM Objects.Documents WHERE Id = :Id AND ReleaseMark = TRUE";
  query.prepare(sql);
  query.bindValue(":Id", type->getLinkedTableRecordId());

  if ( !query.exec() )
  {
    invalidLinkDetected(type, linkedObjectName);
    lastError = query.lastError().text();
    return false;
  }

  bool bf2 = false;
  if ( !query.first() )
  {
    bf2 = true;
  }

  if ( bf1 && bf2 )
  {
    invalidLinkDetected(type, linkedObjectName);
    return false;
  }

  return true;
}

bool Configuration::deleteTablePresent(QSqlDatabase &currentDatabase, const int tableId)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT SystemName FROM SysInfo WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue("Id", tableId);  

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  if ( !query.first() )
  {
    return false;
  }

  QString tableName = query.record().field("SystemName").value().toString();

  sql = QString("DROP TABLE \"%1\" CASCADE").arg(tableName);
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  //�������� ������ ������� SysInfo ��� ���������
  sql = "DELETE FROM SysInfo WHERE Id = :ParentId OR Id IN (SELECT Id WHERE Kind = 'Accessory' AND ParentId = :ParentId)";
  query.prepare(sql);
  query.bindValue("ParentId", tableId);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  return true;
}

bool Configuration::clearUserDatabase(const ConnectionInfo &cInfo)
{
  QSqlDatabase currentDatabase = QSqlDatabase::addDatabase("QPSQL", cInfo.dbName);

  //��������� ���������� �����������
  currentDatabase.setHostName(cInfo.host);
  currentDatabase.setDatabaseName(cInfo.dbName);
  currentDatabase.setUserName(cInfo.userName);
  currentDatabase.setPassword(cInfo.password);  

  if ( !currentDatabase.open() )
  {
    //������ �����������
    QSqlDatabase::removeDatabase(cInfo.dbName);
    lastError = currentDatabase.lastError().text();
    return false;
  }

  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT Id FROM SysInfo WHERE Kind = 'Document'";
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  //������ ����������
  if ( !currentDatabase.transaction() )
  {
    currentDatabase.close();
    QSqlDatabase::removeDatabase(cInfo.dbName);

    return false;
  }

  QSqlQuery subQuery(currentDatabase);

  bool qr = query.first();
  while ( qr )
  {
    int docId = query.record().field("Id").value().toInt();

    sql = "SELECT Id FROM SysInfo WHERE Kind = 'TablePart' AND ParentId = :ParentId";
    subQuery.prepare(sql);
    subQuery.bindValue("ParentId", docId);

    if ( !subQuery.exec() )
    {
      lastError = subQuery.lastError().text();
      return false;
    }

    bool sqr = subQuery.first();
    while ( sqr )
    {
      int tpId = subQuery.record().field("Id").value().toInt();
      deleteTablePresent(currentDatabase, tpId);

      sqr = subQuery.next();
    }

    deleteTablePresent(currentDatabase, docId); 

    qr = query.next();
  }

  //������� ������������������
  sql = QString("ALTER SEQUENCE %1_id_seq RESTART WITH 1").arg("SysInfo");
  query.prepare(sql);

  if ( !query.exec() )
  {    
    lastError = query.lastError().text();
    return false;
  }  

  //���������� ���������� � ���������� ��
  if ( !currentDatabase.commit() )
  {
    lastError = currentDatabase.lastError().text();
    currentDatabase.rollback();
    currentDatabase.close();
    QSqlDatabase::removeDatabase(cInfo.dbName);
    return false;
  }
  
  currentDatabase.close();
  QSqlDatabase::removeDatabase(cInfo.dbName);

  return true;
}

bool Configuration::release(const ConnectionInfo &cInfo)
{  
  if ( !dbControlObject->getDatabase()->isOpen() )
  {
    dbControlObject->connect(dbControlObject->getConnectionInfo());
  }

  //��������� ������ ���������� �� ��
  QSqlQuery query(*(dbControlObject->getDatabase()));
  QString sql;

  sql = "SELECT Id FROM Objects.Documents WHERE ReleaseMark = 'TRUE'";
  query.prepare(sql);
  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return false;
  }

  ObjectList objectList;

  //��������� ������ �������
  BaseDocument *curDocument = NULL;
  bool qr = query.first();
  while ( qr )
  {
    curDocument = dbControlObject->getDocument(query.record().field("Id").value().toInt());   
    objectList.getObjects()->append(curDocument);
    qr = query.next();
  }

  if ( !objectList.getObjects()->isEmpty() )
  {
    if ( synchronize(cInfo, objectList) != NO_ERRORS )
    {
      return false;
    }
  }
  else
  {
    clearUserDatabase(cInfo);
  }

  dbControlObject->disconnect();

  return true;
}

int Configuration::synchronize(const ConnectionInfo &connectionInfo, ObjectList &objList)
{
  QSqlDatabase currentDatabase = QSqlDatabase::addDatabase("QPSQL", connectionInfo.dbName);

  //��������� ���������� �����������
  currentDatabase.setHostName(connectionInfo.host);
  currentDatabase.setDatabaseName(connectionInfo.dbName);
  currentDatabase.setUserName(connectionInfo.userName);
  currentDatabase.setPassword(connectionInfo.password);  

  if ( !currentDatabase.open() )
  {
    //������ �����������
    QSqlDatabase::removeDatabase(connectionInfo.dbName);
    lastError = currentDatabase.lastError().text();
    return -1;
  }

  QSqlQuery query(currentDatabase);
  QString sql;

  sql = "SELECT Id, SystemName FROM SysInfo WHERE Kind = 'Document'";
  query.prepare(sql);
  
  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  QMap<QString, int> documents;
  bool qr = query.first();
  while ( qr )
  {
    documents.insert(query.record().field("SystemName").value().toString(), query.record().field("Id").value().toInt());
    qr = query.next();
  }

  PunyCode pc; 

  ScriptGenerator scriptGenerator;  

  //������ ����������
  if ( !currentDatabase.transaction() )
  {
    currentDatabase.close();
    QSqlDatabase::removeDatabase(connectionInfo.dbName);

    return -1;
  }

  for ( QList<BaseObject*>::iterator i = objList.getObjects()->begin(); i != objList.getObjects()->end(); i++ )
  {    
    switch ( (*i)->getObjectKind() )
    {
      case OBJECT_KIND_DOCUMENT:
      case OBJECT_KIND_DICTIONARY:
      case OBJECT_KIND_REPORT:
      case OBJECT_KIND_DTABLE:     
      {
        QString systemName = pc.encode((*i)->getTranslates()->value(1));       
        if ( documents.contains(systemName) )
        {        
          documents.remove(systemName);
        }        

        //���� �������� ���������� �� �������������� � ���� ������, �� ��������� ���� �� ������� ��������� 
        if ( synchronize(currentDatabase, dynamic_cast<BaseDocument*>(*i), scriptGenerator) )
        {
          //��������� ������������� ���������
          return -2;
        }
      }
      break;
      
      default:;
    }    
  }

  for ( QMap<QString, int>::iterator i = documents.begin(); i != documents.end(); i++ )
  {
    scriptGenerator.dropTable(i.key());

    //������ � �������� ��������� ������ ���������
    sql = QString("SELECT SystemName FROM SysInfo WHERE Kind = 'TablePart' AND ParentId = "
                  "(SELECT Id From SysInfo WHERE SystemName = '%1')").arg(i.key());

    query.prepare(sql);

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }

    bool qr = query.first();
    while (qr)
    {
      scriptGenerator.dropTable(query.record().field("SystemName").value().toString());
      qr = query.next();
    }

    //�������� ������ �� ������� SysInfo
    sql = QString("DELETE FROM SysInfo WHERE SystemName = '%1' OR "
                  "ParentId = (SELECT Id From SysInfo WHERE SystemName = '%1')").arg(i.key());

    query.prepare(sql);

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }
  }
  
  //����������� ���������� � ������� � ������� SysInfo
  sql = "SELECT Id, LinkedObjectName FROM SysInfo WHERE DataType = 'Link' AND Kind = 'Accessory'";
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
  }

  QSqlQuery subQuery(currentDatabase);
  qr = query.first();
  while ( qr )
  {
    sql = "SELECT Id FROM SysInfo WHERE SystemName = :Name AND Kind = 'Document'";
    subQuery.prepare(sql);
    subQuery.bindValue(":Name", query.record().field("LinkedObjectName").value().toString());

    if ( !subQuery.exec() )
    {
      lastError = query.lastError().text();
    }

    int linkedObjectId = -1;
    if ( subQuery.first() )
    {
      //������ �� ������� ��������� ������ ������ � ������� SysInfo
      linkedObjectId = subQuery.record().field("Id").value().toInt();
    }   

    sql = "UPDATE SysInfo SET LinkedObjectId = :LinkedObjectId WHERE Id = :Id";
    subQuery.prepare(sql);
    subQuery.bindValue(":LinkedObjectId", linkedObjectId);
    subQuery.bindValue(":Id", query.record().field("Id").value().toInt());    

    if ( !subQuery.exec() )
    {
      lastError = query.lastError().text();
    }

    qr = query.next();
  }  

  QString sqlScript = scriptGenerator.generate();

  query.prepare(sqlScript);

  if ( !query.exec() )
  {    
    lastError = query.lastError().text();

    //���� �������
    QString fname = QString("%1\\%2").arg(QApplication::applicationDirPath()).arg("userdatabasemodify.sql");

    QFile outFile(fname);
    if ( outFile.open(QIODevice::WriteOnly | QIODevice::Truncate) )
    {
      QTextStream out(&outFile);      
      out << sqlScript;
    }

    currentDatabase.rollback();
    currentDatabase.close();
    QSqlDatabase::removeDatabase(connectionInfo.dbName);

    return -1;
  }

  //���������� ������ � ��
  currentDatabase.commit();
  currentDatabase.close();  
  QSqlDatabase::removeDatabase(connectionInfo.dbName);

  return 0;
}

int Configuration::processDocument(Document *document, QSqlDatabase &currentDatabase, ScriptGenerator &sg)
{  
  QSqlQuery query(currentDatabase);
  QString sql;
  
  PunyCode pc;

  //������������� ���������� �����
  QString systemName = pc.encode(document->getTranslates()->value(1));

  //������������ ������ ���������� �����
  QList<Accessory*> accessories;
  for ( QList<Accessory*>::iterator j = document->getAccessories()->begin(); j != document->getAccessories()->end(); j++ )
  {
    if ( (*j)->getTablePartId() == -1 ) accessories.append(*j);    
  }

  sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(systemName);
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  bool newObject = false;

  if ( !query.first() )
  {
    //�������� � ����� ��������� ������ �� ������      
    Table *newTable = new Table(systemName);
    sg.addTable(newTable);
    newObject = true;
  }

  int parentId = -1;
  //������������ ������ ���������� ����� ��������� �� ���������� � ������� ��������� ����������
  QMap<int, QString> dAccessories;
  //������������ ������ ��������� ������
  QMap<int, QString> dTableParts;

  if ( !newObject )
  {
    parentId = query.record().field("Id").value().toInt();

    //���������� ���������� � ���������� ����������
    sql = "SELECT Id, SystemName, Kind FROM SysInfo WHERE "
      "ParentId = :ParentId AND (Kind = 'Accessory' OR Kind = 'TablePart')";
    query.prepare(sql);
    query.bindValue(":ParentId", parentId);

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }

    bool qr = query.first();
    while ( qr )
    {
      if ( query.record().field("Kind").value().toString().compare("Accessory") == 0 )
      {
        dAccessories.insert(query.record().field("Id").value().toInt(), query.record().field("SystemName").value().toString());
      }
      else
      {
        if ( query.record().field("Kind").value().toString().compare("TablePart") == 0 )
        {
          dTableParts.insert(query.record().field("Id").value().toInt(), query.record().field("SystemName").value().toString());
        }
      }

      qr = query.next();
    }      

    synchronize(currentDatabase, accessories, dAccessories, parentId, sg);

    for ( QMap<int, QString>::iterator i = dAccessories.begin(); i != dAccessories.end(); i++ )
    {
      //��������� ����������, ������� ����� �� �������� ����������� ��� ���������     
      sg.dropColumn(systemName, i.value());
      sql = "DELETE FROM SysInfo WHERE Id = :Id";
      query.prepare(sql);
      query.bindValue(":Id", i.key());

      if ( !query.exec() )
      {
        lastError = query.lastError().text();
        return -1;
      }
    }
  }
  else
  {
    //���������� ������ � ���������
    int r = addObject(currentDatabase, 0, "Document", document->getTranslates()->value(1), systemName, accessories, sg);
    if (  r  )
    {        
      if ( r == FOREIGN_KEY_INVALID )
      {
        lastError = "Invalid link detected !!!\nReleasing stopped!";         
      }

      return -1;
    }      

    sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(systemName);
    query.prepare(sql);

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }

    query.first();
    parentId = query.record().field("Id").value().toInt();
  }

  //��������� ��������� ������ ���������
  for ( QList<TablePart*>::iterator i = document->getTableParts()->begin(); i != document->getTableParts()->end(); i++ )
  {
    QString tpSystemName = pc.encode((*i)->getTranslates()->value(1));
    //���������� ����� ��������� ����� ���������      
    tpSystemName.insert(0, systemName);

    //������������ ������ ���������� ��������� �����
    accessories.clear();
    for ( QList<Accessory*>::iterator j = document->getAccessories()->begin(); j != document->getAccessories()->end(); j++ )
    {
      if ( (*j)->getTablePartId() == (*i)->getId() ) accessories.append(*j);    
    }

    sql = QString("SELECT * FROM SysInfo WHERE SystemName = '%1' AND Kind = 'TablePart' AND ParentId = :ParentId").arg(tpSystemName);
    query.prepare(sql);
    query.bindValue(":ParentId", parentId);

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }

    bool newTPObject = false;
    if ( !query.first() )
    {
      //��������� ����� � ����� ��������� ������ �� �������       
      Table *newTable = new Table(tpSystemName);

      //������� ������������� ���������-��������� ������
      Accessory *newAccessory = new Accessory();
      Integer *newType = new Integer();
      newType->setMin(1);
      newType->setMax(INT_MAX);
      newType->setDefault(1);
      newType->setRequired(true);
      newAccessory->setType(newType);
      Index index(Index::KindForeignKey);
      index.setReferenceTableForForeignKey(systemName);
      index.setReferenceColumnForForeignKey("Id");
      Column *newColumn = new Column("OwnerId", static_cast<Accessory&>(*newAccessory), index);

      newColumn->setIndex(index);
      newTable->addColumn(newColumn);

      sg.addTable(newTable);

      newTPObject = true;
    }
    else
    {
      dTableParts.remove(query.record().field("Id").value().toInt());
    }

    int tpId = -1;
    if ( !newTPObject )
    {
      tpId = query.record().field("Id").value().toInt();      

      //���������� ���������� � ����������
      sql = "SELECT Id, SystemName FROM SysInfo WHERE "
            "ParentId = :ParentId AND Kind = 'Accessory'";
      query.prepare(sql);
      query.bindValue(":ParentId", tpId);

      if ( !query.exec() )
      {
        lastError = query.lastError().text();
        return -1;
      }

      QMap<int, QString> tpAccessories;
      bool qr = query.first();
      while ( qr )
      {
        tpAccessories.insert(query.record().field("Id").value().toInt(), query.record().field("SystemName").value().toString());
        qr = query.next();
      }

      synchronize(currentDatabase, accessories, tpAccessories, tpId, sg);        

      for ( QMap<int, QString>::iterator j = tpAccessories.begin(); j != tpAccessories.end(); j++ )
      {
        //��������� ����������, ������� ����� �� �������� ����������� ��� ��������� ����� ���������         
        sg.dropColumn(tpSystemName, j.value());

        sql = "DELETE FROM SysInfo WHERE Id = :Id";
        query.prepare(sql);
        query.bindValue(":Id", j.key());

        if ( !query.exec() )
        {
          lastError = query.lastError().text();
          return -1;
        }
      }
    }
    else
    {
      //���������� ������ � ��������� �����
      addObject(currentDatabase, parentId, "TablePart", (*i)->getTranslates()->value(1), tpSystemName, accessories, sg);        
    }
  }

  for ( QMap<int, QString>::iterator i = dTableParts.begin(); i != dTableParts.end(); i++ )
  {
    //��������� ����������, ������� ����� �� �������� ����������� ��� ���������      
    sg.dropTable(i.value());

    //�������� ������ � SysInfo
    sql = "DELETE FROM SysInfo WHERE Id = :Id OR ParentId = :ParentId";
    query.prepare(sql);
    query.bindValue(":Id", i.key());
    query.bindValue(":ParentId", i.key());

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }
  }

  return 0;
}

int Configuration::processDocument(DTable *document, QSqlDatabase &currentDatabase, ScriptGenerator &sg)
{
  QSqlQuery query(currentDatabase);
  QString sql;

  PunyCode pc;

  //������������� ���������� �����
  QString systemName = pc.encode(document->getTranslates()->value(1));  

  sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = 'Document'").arg(systemName);
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  bool newObject = false;

  if ( !query.first() )
  {
    //�������� � ����� ��������� ������ �� ������      
    Table *newTable = new Table(systemName);
    sg.addTable(newTable);
    newObject = true;
  }

  int parentId = -1;
  //������������ ������ ���������� ����� ��������� �� ���������� � ������� ��������� ����������
  QMap<int, QString> dAccessories; 

  if ( !newObject )
  {
    parentId = query.record().field("Id").value().toInt();

    //���������� ���������� � ���������� ����������
    sql = "SELECT Id, SystemName FROM SysInfo WHERE "
          "ParentId = :ParentId AND Kind = 'Accessory'";
    query.prepare(sql);
    query.bindValue(":ParentId", parentId);

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }

    bool qr = query.first();
    while ( qr )
    {      
      dAccessories.insert(query.record().field("Id").value().toInt(), query.record().field("SystemName").value().toString());
     
      qr = query.next();
    }      

    synchronize(currentDatabase, *(document->getAccessories()), dAccessories, parentId, sg);

    for ( QMap<int, QString>::iterator i = dAccessories.begin(); i != dAccessories.end(); i++ )
    {
      //��������� ����������, ������� ����� �� �������� ����������� ��� ���������     
      sg.dropColumn(systemName, i.value());
      sql = "DELETE FROM SysInfo WHERE Id = :Id";
      query.prepare(sql);
      query.bindValue(":Id", i.key());

      if ( !query.exec() )
      {
        lastError = query.lastError().text();
        return -1;
      }
    }
  }
  else
  {
    //���������� ������ � ���������
    int r = addObject(currentDatabase, 0, "Document", document->getTranslates()->value(1), systemName, *(document->getAccessories()), sg);
    if (  r  )
    {        
      if ( r == FOREIGN_KEY_INVALID )
      {
        lastError = "Invalid link detected !!!\nReleasing stopped!";         
      }

      return -1;
    }
  }

  return 0;
}

int Configuration::synchronize(QSqlDatabase &currentDatabase, BaseDocument *document, ScriptGenerator &sg)
{
  switch ( document->getKind() )
  {
    case DOC_KIND_DOCUMENT:
    case DOC_KIND_DICTIONARY:
    case DOC_KIND_REPORT:
    {      
      return processDocument(dynamic_cast<Document*>(document), currentDatabase, sg);
    }
  	break;

    case DOC_KIND_TABLE:
    {
      return processDocument(dynamic_cast<DTable*>(document), currentDatabase, sg);
    }
    break;

    default:;
  }  

  return 0;
}

int Configuration::synchronize(QSqlDatabase &currentDatabase, QList<Accessory*> &accessories1, QMap<int, QString> &accessories2, int parentId, ScriptGenerator &sg)
{
  QSqlQuery query(currentDatabase);

  QString sql = "SELECT SystemName FROM SysInfo WHERE Id = :Id";
  query.prepare(sql);
  query.bindValue(":Id", parentId);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  if ( !query.first() )
  {
    return -2;//������ ��� �������� ���
  }

  QString parentSystemName = query.record().field("SystemName").value().toString();

  PunyCode pc;
  int accessoryId = -1;

  //��������� ����������
  for ( QList<Accessory*>::iterator i = accessories1.begin(); i != accessories1.end(); i++ )
  {    
    Accessory *accessory = *i;
    QString name = pc.encode(accessory->getTranslates()->value(1));

    sql = QString("SELECT Id, DataType, Limits FROM SysInfo WHERE ParentId = :ParentId "                  
                  "AND SystemName = '%1' AND Kind = 'Accessory'").arg(name);
    query.prepare(sql);
    query.bindValue(":ParentId", parentId);   

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }

    bool newAccessory = false;
    if ( !query.first() )
    {
      //���������� ����� �������
      Index index(Index::KindNone);      
      sql = QString("INSERT INTO SysInfo(SystemName, ParentId, Kind, DataType, Limits, Translation_1)"
                    "VALUES ('%1', :ParentId, 'Accessory', :DataType, :Limits, :Tr1)").arg(name);

      if ( accessory->getType()->getTypeKind() == DATA_TYPE_LINK )
      {
        Link *type = dynamic_cast<Link*>(accessory->getType());

        QString linkedObjectName;
        if ( checkLink(currentDatabase, linkedObjectName, type) )
        {        
          //return FOREIGN_KEY_INVALID;
          if ( !linkedObjectName.isEmpty() )
          {
            sql = QString("INSERT INTO SysInfo(SystemName, ParentId, Kind, DataType, Limits, LinkedObjectName, LinkedObjectId, Translation_1) "
                          "VALUES ('%1', :ParentId, 'Accessory', :DataType, :Limits, '%2', 0, :Tr1)").arg(name).arg(linkedObjectName);

            index.setKind(Index::KindForeignKey);
            index.setReferenceTableForForeignKey(linkedObjectName);
            index.setReferenceColumnForForeignKey("Id");
          }     
        }   
      }

      //���������� ������ � ������� SysInfo
      QString limits = Accessory::limitsToString(accessory);
      QString dataType = Accessory::dataTypeToString(accessory->getType());
      
      query.prepare(sql);
      query.bindValue(":ParentId", parentId);
      query.bindValue(":DataType", dataType);
      query.bindValue(":Limits", limits);
      query.bindValue(":Tr1", accessory->getTranslates()->value(1));

      if ( !query.exec() )
      {
        lastError = query.lastError().text();
        return -1;
      }

      Column *newColumn = new Column(name, static_cast<Accessory&>(*accessory), index);
      sg.addColumn(parentSystemName, newColumn);

      newAccessory = true;
    }
    else
    {
      accessoryId = query.record().field("Id").value().toInt();
      accessories2.remove(accessoryId);
    }

    //�������� ���� � �����������
    //���� ������������ ��������, �� ������� �� �������� ��������� �� ���������� ����� � ���������� ������
    if ( !newAccessory )
    {
      int typeKind = Accessory::dataTypeFromString(query.record().field("DataType").value().toString());
      if ( accessory->getType()->getTypeKind() != typeKind )
      {
        //���� �� ���������      
        sg.dropColumn(parentSystemName, name);

        sql = "UPDATE SysInfo SET DataType = :DataType, Limits = :Limits WHERE Id = :Id";
        
        Index index(Index::KindNone);
        //�������� ������������ ������
        if ( accessory->getType()->getTypeKind() == DATA_TYPE_LINK )
        {          
          Link *type = dynamic_cast<Link*>(accessory->getType());

          QString linkedObjectName;
          if ( checkLink(currentDatabase, linkedObjectName, type) )
          {        
            //return FOREIGN_KEY_INVALID;
            if ( !linkedObjectName.isEmpty() )
            {
              sql = QString("UPDATE SysInfo SET DataType = :DataType, Limits = :Limits, LinkedObjectName = '%1', LinkedObjectId = %2 WHERE Id = :Id").arg(linkedObjectName).arg(-1);

              index.setKind(Index::KindForeignKey);
              index.setReferenceTableForForeignKey(linkedObjectName);
              index.setReferenceColumnForForeignKey("Id");
            }     
          }          
        }

        //���������� ���������� � ���������        
        query.prepare(sql);
        query.bindValue(":DataType", Accessory::dataTypeToString(accessory->getType()));
        query.bindValue(":Limits", Accessory::limitsToString(accessory));
        query.bindValue(":Id", accessoryId);

        if ( !query.exec() )
        {
          lastError = query.lastError().text();
          return -1;
        }

        Column *newColumn = new Column(name, static_cast<Accessory&>(*accessory), index);
        sg.addColumn(parentSystemName, newColumn);
      }
      else
      {
        //������������ ������ �����������
        QString limits = Accessory::limitsToString(accessory);     

        if ( limits.compare(query.record().field("Limits").value().toString()) )
        {
          //����������� �� ���������     
          sg.dropColumn(parentSystemName, name);

          sql = "UPDATE SysInfo SET Limits = :Limits WHERE Id = :Id";

          Index index(Index::KindNone);        
          if ( accessory->getType()->getTypeKind() == DATA_TYPE_LINK )
          {
            Link *type = dynamic_cast<Link*>(accessory->getType());

            QString linkedObjectName;
            if ( checkLink(currentDatabase, linkedObjectName, type) )
            {        
              //return FOREIGN_KEY_INVALID;
              if ( !linkedObjectName.isEmpty() )
              {
                sql = QString("UPDATE SysInfo SET Limits = :Limits, LinkedObjectName = '%1', LinkedObjectId = %2 WHERE Id = :Id").arg(linkedObjectName).arg(-1);

                index.setKind(Index::KindForeignKey);
                index.setReferenceTableForForeignKey(linkedObjectName);
                index.setReferenceColumnForForeignKey("Id");
              }     
            }
          }          

          //���������� ���������� � ���������          
          query.prepare(sql);        
          query.bindValue(":Limits", limits);      
          query.bindValue(":Id", accessoryId);

          if ( !query.exec() )
          {
            lastError = query.lastError().text();
            return -1;
          }

          Column *newColumn = new Column(name, static_cast<Accessory&>(*accessory), index);      
          sg.addColumn(parentSystemName, newColumn);
        }
      }
    }   
  }

  return 0;
}

int Configuration::addObject(QSqlDatabase &currentDatabase, const int parentId, const QString &parentKind, const QString &parentName, const QString &systemName, QList<Accessory*> &accessories, ScriptGenerator &sg)
{
  //���������� ������ � ���������
  QString sql = QString("INSERT INTO SysInfo(SystemName, ParentId, Kind, Translation_1)"
                        "VALUES ('%1', :ParentId, :ParentKind, :Tr_1)").arg(systemName);
  QSqlQuery query(currentDatabase);
  query.prepare(sql);
  query.bindValue(":ParentId", parentId);
  query.bindValue(":ParentKind", parentKind);
  query.bindValue(":Tr_1", parentName);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  sql = QString("SELECT Id FROM SysInfo WHERE SystemName = '%1' AND Kind = :ParentKind").arg(systemName);
  query.prepare(sql);
  query.bindValue(":ParentKind", parentKind);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    return -1;
  }

  query.first();
  int parentObjectId = query.record().field("Id").value().toInt();

  PunyCode pc;

  for ( QList<Accessory*>::iterator i = accessories.begin(); i != accessories.end(); i++ )
  {
    Accessory *accessory = *i;
    QString name = pc.encode(accessory->getTranslates()->value(1));
    
    Index index(Index::KindNone);
  
    sql = QString("INSERT INTO SysInfo(SystemName, ParentId, Kind, DataType, Limits, Translation_1) "
                  "VALUES ('%1', :ParentId, 'Accessory', :DataType, :Limits, :Tr1)").arg(name);

    if ( accessory->getType()->getTypeKind() == DATA_TYPE_LINK )
    {
      Link *type = dynamic_cast<Link*>(accessory->getType());

      
      QString linkedObjectName;
      if ( checkLink(currentDatabase, linkedObjectName, type) )
      {      
        if ( !linkedObjectName.isEmpty() )
        {
          sql = QString("INSERT INTO SysInfo(SystemName, ParentId, Kind, DataType, Limits, LinkedObjectName, LinkedObjectId, Translation_1) "
                        "VALUES ('%1', :ParentId, 'Accessory', :DataType, :Limits, '%2', 0, :Tr1)").arg(name).arg(linkedObjectName);

          index.setKind(Index::KindForeignKey);
          index.setReferenceTableForForeignKey(linkedObjectName);
          index.setReferenceColumnForForeignKey("Id");
        }     
      }   
    }
    
    QString limits = Accessory::limitsToString(accessory);
    QString dataType = Accessory::dataTypeToString(accessory->getType());

    query.prepare(sql);
    query.bindValue(":ParentId", parentObjectId);
    query.bindValue(":DataType", dataType);
    query.bindValue(":Limits", limits);
    query.bindValue(":Tr1", accessory->getTranslates()->value(1));

    if ( !query.exec() )
    {
      lastError = query.lastError().text();
      return -1;
    }

    //���������� �������
    Column *newColumn = new Column(name, static_cast<Accessory&>(*accessory), index);
    sg.getTable(systemName)->addColumn(newColumn);
  }

  return 0;
}

void Configuration::clear()
{  
  dbControlObject->clear();
  for ( QList<Script*>::iterator i = scripts.begin(); i != scripts.end(); i++ )
  {
    delete *i;
  }
  scripts.clear();

  for ( QList<BaseObject*>::iterator i = objectList.getObjects()->begin(); i != objectList.getObjects()->end(); i++ )
  {
    delete *i;
  }
  objectList.getObjects()->clear();
}

QString Configuration::getLastError() const
{
  return QString("Configuration last error: %1\nDBControl last error: %2").arg(lastError).arg(dbControlObject->getLastError());
}

ConnectionInfo Configuration::getConnectionInfo() const
{
  return dbControlObject->getConnectionInfo();
}

QList<Script*> * Configuration::getScriptList()
{  
  return &scripts;
}

void Configuration::setScriptList(const QList<Script*> &newScriptList)
{  
  scripts = newScriptList;
}

ConfigurationHeader * Configuration::getHeader()
{
  return &header;  
}

void Configuration::setHeader(ConfigurationHeader &newHeader)
{
  header = newHeader;  
}

ObjectList * Configuration::getObjectList()
{
  return &objectList;
}

void Configuration::setObjectList(const ObjectList &objList)
{
  objectList = objList;
}

bool Configuration::loadObjectsFromDatabase()
{
  if ( !dbControlObject->getDatabase()->isOpen() )
  {
    return false;
  }
  
  //��������� ������ �������� ������� ������������ 
  QSqlQuery query(*dbControlObject->getDatabase());
  QString sql;

  sql = "SELECT Id FROM Objects.Documents";
  query.prepare(sql);
  if ( !query.exec() )
  {
    lastError =  query.lastError().text();
    return false;
  }

  ObjectList newObjectList;

  //��������� ������ �������
  BaseDocument *curDocument = NULL;
  bool qr = query.first();    
  while ( qr )
  {
    curDocument = dbControlObject->getDocument(query.record().field("Id").value().toInt());   

    if ( curDocument )
    {
      newObjectList.getObjects()->append(curDocument);
    }
    
    qr = query.next();
  }  

  //���������� � ������ �������� ���������� �������� ������������
  QList<Script*> scriptList = dbControlObject->getConfigurationScripts();
  for ( QList<Script*>::iterator i = scriptList.begin(); i != scriptList.end(); i++ )
  {    
    newObjectList.getObjects()->append(*i);
  }

  objectList = newObjectList;
  return true;
}

bool Configuration::saveObjectsToDatabase()
{
  if ( !dbControlObject->getDatabase()->isOpen() )
  {
    return false;
  }

  if ( !dbControlObject->getDatabase()->transaction() )
  {
    return false;
  }
 
  //������� ���������
  if ( !dbControlObject->clear() )
  {
    return false;
  } 

  //���������� ���������� �� ����������
  QList<Script*> newScriptList;
  for ( int i = 0; i < objectList.getObjects()->size(); i++ )
  {
    switch ( objectList.getObjects()->value(i)->getObjectKind() )
    {
      case OBJECT_KIND_DOCUMENT:
      case OBJECT_KIND_DICTIONARY:
      case OBJECT_KIND_REPORT:
      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      case OBJECT_KIND_REGISTER:
      case OBJECT_KIND_ENUMERATION:
      case OBJECT_KIND_CONSTANT:
      case OBJECT_KIND_DFORM:
      case OBJECT_KIND_DTABLE:
      case OBJECT_KIND_DQUERY:
      case OBJECT_KIND_DCLASS_MODULE:
      case OBJECT_KIND_DGLOBAL_THREAD:
      case OBJECT_KIND_DGLOBAL_CONTAINER:
      case OBJECT_KIND_DTRIGGER:
      case OBJECT_KIND_DTIME_MANAGER:
      {
        BaseDocument *curDocument = dynamic_cast<BaseDocument*>(objectList.getObjects()->value(i));       

        if ( curDocument )
        {
          if ( !dbControlObject->setDocument(curDocument) )
          {
            dbControlObject->getDatabase()->rollback();
            return false;
          }
        }
      }
      break;

      case OBJECT_KIND_SCRIPT:
      {
        Script *curScript = dynamic_cast<Script*>(objectList.getObjects()->value(i));       
        newScriptList.append(curScript);
      }
      break;

      default:;
    }
  }

  //������ ��������
  if ( !dbControlObject->setConfigurationScripts(newScriptList) )
  {
    dbControlObject->getDatabase()->rollback();
    return false;
  }

  dbControlObject->getDatabase()->commit();
  return true;
}

bool Configuration::loadHeaderFromDatabase()
{
  if ( !dbControlObject->getDatabase()->isOpen() )
  {
    return false;
  }
  
  header = dbControlObject->getHeader(); 

  return true;
}

bool Configuration::saveHeaderToDatabase()
{
  if ( !dbControlObject->getDatabase()->isOpen() )
  {
    return false;
  }

  if ( !dbControlObject->getDatabase()->transaction() )
  {
    return false;
  }

  //������ ���������
  if ( !dbControlObject->setHeader(header) )
  {
    dbControlObject->getDatabase()->rollback();
    return false;
  } 

  dbControlObject->getDatabase()->commit();
  return true;
}
