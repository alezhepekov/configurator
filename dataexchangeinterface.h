/***********************************************************************
* Module:   dataexchangeinterface.h
* Author:   LGP
* Modified: 25 ���� 2007 �.
* Purpose:  ��������� ������ � ������� ����������
***********************************************************************/

#ifndef DATA_EXCHANGE_INTERFACE_H
#define DATA_EXCHANGE_INTERFACE_H

#include <QString>
#include <QPair>
#include <QVariant>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QDataStream>
#include <QStringList>

namespace ConfiguratorObjects
{
  typedef QMap<int, QList<QPair<QString, QVariant> > > DataBlock;

  class DataExchangeInterface
  {
  protected:
    QString lastError;
   
    //����� ������� ������ ������� � ����� ������
    void outData(QSqlQuery &query, QDataStream &out)
    {
      bool qr = query.first();

      out << query.record().count();
      for ( int i = 0; i < query.record().count(); i++ )
      {     
        out << query.record().field(i).name();
      }

      out << query.size();
      while ( qr )
      {
        for ( int i = 0; i < query.record().count(); i++ )
        {     
          out << query.record().field(i).value();
        }    

        qr = query.next();
      }     
    }

    //����� ��������� ������ �� ������ ������
    void inData(QDataStream &in, QMap<int, QList<QPair<QString, QVariant> > > &values)
    {
      int recordCount;
      in >> recordCount;

      QStringList fieldNames;
      for ( int j = 0; j < recordCount; j++ )
      {
        QString fieldName;    
        in >> fieldName;

        fieldNames.append(fieldName);
      }

      int sise;
      in >> sise;
      for ( int i = 0; i < sise; i++ )
      {
        QList<QPair<QString, QVariant> > l;

        for ( int j = 0; j < recordCount; j++ )
        {      
          QPair<QString, QVariant> p;

          p.first = fieldNames.value(j);
          in >> p.second;

          l.append(p);
        }

        values.insert(i, l);
      }
    }

  public:
    //����� ��������� sql-������ � �������� ��
    //sql  - ����� sql-�������
    //data - ����� ������ ������
    virtual bool exec(const QString &sql, QDataStream &data) = 0;

    //����� ���������� ��������� ��������� ��������� �������� ��� ���������� ��������
    QString getLastError() {return lastError;};
  };
}

#endif
