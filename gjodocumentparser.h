/***********************************************************************
* Module:   gjodocumentparser.h
* Author:   LGP
* Modified: 26 ������ 2007 �.
* Purpose:  ����� ���������� ������� ����������� � ������� ���
***********************************************************************/

#ifndef GJO_DOCUMENT_PARSER
#define GJO_DOCUMENT_PARSER

#define LinkDataTypeId 32000

#include "../GJOTree/src/gjotree.h"

#include "documents/document.h"
#include "documents/dictionary.h"
#include "documents/report.h"
#include "documents/documentsjournal.h"
#include "documents/register.h"
#include "documents/enumeration.h"
#include "documents/constant.h"
#include "documents/dform.h"
#include "documents/dtable.h"
#include "documents/dquery.h"
#include "documents/dclassmodule.h"
#include "documents/dglobalthread.h"
#include "documents/dglobalcontainer.h"
#include "documents/dtrigger.h"
#include "documents/dtimemanager.h"

namespace ConfiguratorObjects
{
  class GJODocumentParser
  {
  public:
    GJODocumentParser(GJO::Tree *);
    ~GJODocumentParser();

    //����� ��������� ������ ���������
    int parse(Documents::BaseDocument *document);

    //����� ��������� ������ ��������� � ������ ���  
    int setDocument(Documents::BaseDocument *document);

    //����� ��������� �������� �� gjoTree
    Documents::BaseDocument *getDocument(int id);

    //����� ������� �� ������ ��� �������� � �������� ���������������
    void removeDocument(int gjoId);

    //����� ��������� � ������ ��� ����� ���������
    void appendForm(const int ownerId, Documents::Form *form);

    //����� ��������� � ������ ��� �������� ���������
    GJO::Property *appendProperty(const int ownerId, Documents::Accessory *accessory);

    //����� ��������� � ������ ��� ��������� ����� ���������
    GJO::TablePart *appendTablePart(const int ownerId, Documents::TablePart *tablePart);

    //����� ��������� � ������ ��� ������
    GJO::Trait *appendLink(const int ownerId, const int linkedObjectId);

    GJO::Script *appendScript(const int ownerId, Documents::Script *script);

    GJO::Module *appendGlobalModule(Documents::Script *script);

    void removeGlobalModule(int gjoId);

    //����� ��������� ����� ��������������� ������ ��� ��� ���������
    void clearDocumentGJOTreeId(Documents::BaseDocument *document);

  protected:
    //����� ������� �������� �� ������ �������� ������ ���
    Documents::Accessory *createAccessory(GJO::Property *gjoAcc);

    //����� ������� ����� ��������� �� ������ ������� ������ ���
    Documents::Form *createForm(GJO::Object *gjoForm);

    //������ �������� ��������� ������ ���������� �� ������ ���
    void loadDocumentContent(int gjoId, Documents::Document *document);
    void loadDocumentContent(int gjoId, Documents::DocumentsJournal *document);
    void loadDocumentContent(int gjoId, Documents::Register *document);
    void loadDocumentContent(int gjoId, Documents::Enumeration *document);
    void loadDocumentContent(int gjoId, Documents::Constant *document);
    void loadDocumentContent(int gjoId, Documents::DForm *document);
    void loadDocumentContent(int gjoId, Documents::DTable *document);
    void loadDocumentContent(int gjoId, Documents::DQuery *document);
    void loadDocumentContent(int gjoId, Documents::DClassModule *document);
    void loadDocumentContent(int gjoId, Documents::DGlobalThread *document);
    void loadDocumentContent(int gjoId, Documents::DGlobalContainer *document);
    void loadDocumentContent(int gjoId, Documents::DTrigger *document);
    void loadDocumentContent(int gjoId, Documents::DTimeManager *document);

    //������ ���������� �������� ������ ��������� � ������ ���
    void scanDocument(Documents::Document *document);  
    void scanDocument(Documents::DocumentsJournal *document);  
    void scanDocument(Documents::Register *document);
    void scanDocument(Documents::Enumeration *document);
    void scanDocument(Documents::Constant *document);
    void scanDocument(Documents::DForm *document);
    void scanDocument(Documents::DTable *document);
    void scanDocument(Documents::DQuery *document);
    void scanDocument(Documents::DClassModule *document);
    void scanDocument(Documents::DGlobalThread *document);
    int scanDocument(Documents::DGlobalContainer *document);
    void scanDocument(Documents::DTrigger *document);
    void scanDocument(Documents::DTimeManager *document);    

  private:
    GJO::Tree *gjoTree;
  };
}

#endif
