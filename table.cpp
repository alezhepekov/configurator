#include "table.h"

using namespace ConfiguratorObjects::SQL;

Table::Table(const QString &tableName)
{
  name = tableName;
}

Table::~Table()
{
  for ( QList<Column*>::iterator i = columns.begin(); i != columns.end(); i++ ) delete *i;  
}

QString Table::getName()
{
  return name;
}

void Table::setName(const QString& newName)
{
  name = newName;
}

void Table::addColumn(Column *column)
{  
  columns.append(column);
}

void Table::removeColumn(const QString &name)
{
  for ( QList<Column*>::iterator i = columns.begin(); i != columns.end(); i++ )
  {
    if ( (*i)->getName().compare(name) == 0 )
    {
      columns.erase(i);
    }    
  }      
}

QList<Column*> * Table::getColumns()
{
  return &columns;
}
