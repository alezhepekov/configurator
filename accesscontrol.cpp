#include "accesscontrol.h"

AccessControl::AccessControl()
{  
  //��������� ������� �������� 1x1 �������
  accessMatrix = new Matrix(1, 1);
}

AccessControl::~AccessControl()
{
  //accessMatrix->clear();
  delete accessMatrix;
  accessSubjects.clear();
  accessObjects.clear();
}

AccessControl & AccessControl::operator= (const AccessControl &op)
{
  //this->
  
  return *this;
}

int AccessControl::addSubject(quint64 subjectId, int permission)
{
  try
  {  
    accessSubjects.append(subjectId);
    int rn = accessMatrix->addRow();

    //���������� ������� ���� �������
    for (int i = 0; i < accessMatrix->getColCount(); i++)
    {
      accessMatrix->setItem(rn, i, permission);
    }
    
    //������� ������� ������������ ��������
    return rn;
  }
  catch (...)
  {
    return -1;
  }
}

bool AccessControl::deleteSubject(int index)
{
  try
  {
    if (index < 0 || index >= accessSubjects.size())
      return false;

    accessSubjects.removeAt(index);
    accessMatrix->deleteRow(index);
    
    return true;
  }
  catch (...)
  {
    return false;
  }
}

int AccessControl::addObject(int objectId)
{
  try
  {
    accessObjects.append(objectId);
    int cn = accessMatrix->addColumn();
    for (int i = 0; i < accessMatrix->getRowCount(); i++)
    {
      accessMatrix->setItem(i, cn, 0);
    }

    //������� ������� ������������ �������
    return cn;
  }
  catch (...)
  {
    return -1;
  }
}

bool AccessControl::deleteObject(int index)
{
  try
  {
    if (index < 0 || index >= accessObjects.size())
      return false;

    accessObjects.removeAt(index);
    accessMatrix->deleteColumn(index);
    
    return true;
  }
  catch (...)
  {
    return false;
  }
}

quint64 AccessControl::getSubject(int index)
{
  return accessSubjects.value(index);
}

int AccessControl::getObject(int index)
{
  return accessObjects.value(index);
}
QList<quint64>* AccessControl::getSubjectList()
{
  return &accessSubjects;
}

QList<int>* AccessControl::getObjectList()
{
  return &accessObjects;
}
