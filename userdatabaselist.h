/***********************************************************************
* Module:   userdatabaselist.h
* Author:   LGP
* Modified: 07 ���� 2007 �.
* Purpose:  ����� ������������� ������ ���������������� ��
***********************************************************************/

#ifndef USER_DATABASE_LIST_H
#define USER_DATABASE_LIST_H

#include "domxml.h"
#include "configurationheader.h"

namespace ConfiguratorObjects
{
  class UserDatabaseList: public Documents::DomXml
  {
  public:
    UserDatabaseList();
    ~UserDatabaseList();

    const QList<ConnectionInfo> *getDatabases() const;
    QList<ConnectionInfo> *getDatabases();

    void setDatabases(const QList<ConnectionInfo> &);

    //����� ��������� ������������ xml-�������������
    QString toXML();

    //����� ��������� �������� ������ �� xml-�������������
    bool fromXML(const QString&);  

  protected:
  private: 
    QList<ConnectionInfo> databases;
  };

  QDataStream &operator<<(QDataStream &, const UserDatabaseList &);
  QDataStream &operator>>(QDataStream &, UserDatabaseList &);
}

#endif
