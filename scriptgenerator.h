/***********************************************************************
* Module:   scriptgenerator.h
* Author:   LGP
* Modified: 15 ����� 2007 �.
* Purpose:  ��������� sql-�������
***********************************************************************/

#ifndef SCRIPT_GENERATOR_H
#define SCRIPT_GENERATOR_H

#include "table.h"

#include <QMap>
#include <QList>
#include <QString>

namespace ConfiguratorObjects
{
  namespace SQL
  {
    class ScriptGenerator
    {
      QList<Table*> tables;

      QList<QString> dropingTables;

      QMap<QString, QList<QString> > dropingColumns;

      QMap<QString, QList<Column*> > addingColumns;

      QMap<QString, QString> indexes;

    public:
      ScriptGenerator();
      ~ScriptGenerator();

      void addTable(Table *table);

      void removeTable(const QString &name);

      void dropTable(const QString &name);

      void addColumn(const QString &tableName, Column *column);

      void dropColumn(const QString &tableName, const QString &name);

      Table *getTable(const QString &name);

      void cycleLinkDetected(const QString &msg);

      void sort(QList<Table*> &tables);

      QString generate();

      QString generateTable(Table *table);

      QString generateColumn(Column *column);

      QList<Table*> *getTables();

      QList<QString> *getDropingTables();

      QMap<QString, QList<QString> > *getDropingColumns();

      QMap<QString, QList<Column*> > *getAddingColumns();      
    };
  }
}

#endif
