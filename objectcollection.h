/***********************************************************************
* Module:   objectcollection.h
* Author:   LGP
* Modified: 
* Purpose:  ��������� �������� 
***********************************************************************/

#ifndef OBJECT_COLLECTION_H
#define OBJECT_COLLECTION_H

#include "utils/glglobal.h"

#include GL_BEFORE_QT_HEADERS
#include <QString>
#include <QVariant>
#include <QMetaObject>
#include <QMetaProperty>
#include <QStringList>
#include GL_AFTER_QT_HEADERS


#include <map>

class QVariant;
class QObject;

#ifndef PROPERTYOBJECT_H
#define PROPERTYOBJECT_H
class PropertyObjectImpl{
public:
	virtual const char* propertyClass(const QString& propertyName) const = 0;
	virtual bool setProperty(const QString& name, const QVariant& value) = 0;
	virtual QVariant property(const QString& name) const = 0;
	virtual QMetaProperty metaProperty(const QString& name) const = 0;
	virtual QStringList propertyList() const = 0;
	virtual PropertyObjectImpl* clone() const = 0;
	virtual ~PropertyObjectImpl(){}
};

template<typename T>
class PropertyObjectSimpleImpl:public PropertyObjectImpl{
public:
	PropertyObjectSimpleImpl(T object):object_(object){}

	virtual bool setProperty(const QString& name, const QVariant& value){
		return object_->setProperty(name, value);
	}
	virtual QVariant property(const QString& name) const{
		return object_->property(name);
	}
	virtual const char* propertyClass(const QString&) const{	return 0;	}

	virtual QMetaProperty metaProperty(const QString&) const{return QMetaProperty();}
	virtual QStringList propertyList() const {return QStringList(); }	

	virtual PropertyObjectImpl* clone() const{
		return new PropertyObjectSimpleImpl<T>(object_);
	}
private:
	T object_;
};

//��������, ������ ���� �������������� ��� QObject
template<>
inline QMetaProperty PropertyObjectSimpleImpl<QObject*>::metaProperty(const QString& name) const{		
	const QMetaObject* metaObj = object_->metaObject();
	return metaObj->property(metaObj->indexOfProperty(name));		
}
template<> inline const char* 
PropertyObjectSimpleImpl<QObject*>::propertyClass(const QString& propertyName) const
{	
	const QMetaObject* meta = object_->metaObject();
	const int propertyIndex = meta->indexOfProperty(propertyName);
	if (propertyIndex != -1)
	{
		while (propertyIndex < meta->propertyOffset())
			meta = meta->superClass();
	}		
	return meta->className();
}
template<>
inline QStringList PropertyObjectSimpleImpl<QObject*>::propertyList() const{		
	QStringList result;
	const QMetaObject* metaObject = object_->metaObject();
	for(int i = 0; i < metaObject->propertyCount(); i++)
	{
		result.append(metaObject->property(i).name());
	}
	return result;
}

class PropertyObject{
public:
	template <typename T> PropertyObject(T object):impl_(new PropertyObjectSimpleImpl<T>(object)){}
	PropertyObject(const PropertyObject& other);
	PropertyObject& operator=(const PropertyObject& other);

	const char* className(const QString& propertyName) const;

	bool setProperty(const QString& name, const QVariant& value);
	QVariant property(const QString& name) const;
	QMetaProperty metaProperty(const QString& name) const;
	QStringList propertyList() const;
	
private:
	void swap(PropertyObject& other);
	std::auto_ptr<PropertyObjectImpl> impl_;
};
#endif

class ObjectCollection
{
public:
	/**
	* ��������� ������ ���������.	
	*/
	ObjectCollection();
	~ObjectCollection();

	/**
	 * ��������� � ��������� ��� ������� name ������ object.  ���� ������ � ����� ������ ��� ����������, ��
	 * ��������� ���������� �� �����.
	 */
	void add(const QString& name, QObject* object);
	void add(const QString& name, const PropertyObject& object);
	/**
	 * ��������� propertiesList ����������� ������� ���� ������� �������� � ��������� 
	 * (��������� ����� ������� � ����� ������ propertiesList)
	 */
	void fillPropertiesList(QStringList& propertiesList) const;
	/**
	 * ���������� ��������� �� ������� � �������� ������(��� ���� � ������ ����������
	 * �������� � ����� ������).
	 */
	PropertyObject* object(const QString& name);
	const PropertyObject* object(const QString& name) const;
	/**
	 * ��������������� �������� �������� �� ����������� ����� (���������� QObject)
	 */
	QVariant property(const QString& name) const;
	QMetaProperty metaProperty(const QString& name) const;
	/**
	 * ������� ������� � �������� ������ �� ���������. 
	 */
	void remove(const QString& name);
	/**
	 * ����������� �������� �� ��������� ����� (���������� QObject)
	 */
	bool setProperty(const QString& name, const QVariant& value);

	/**
	 * ���������� ��� ������, � ������� �������� ����� ��������� ��������
	 * propertyName
	 */
	const char* className(const QString& propertyName) const;
private:	
	/**
	*��������� � ������ ���������� ������� �������� ��������� �������
	*/
	void addObjectProperties(const PropertyObject& object, const QString& objectName, QStringList& list) const;
	
	typedef std::map<QString, PropertyObject> ObjectMap;
	typedef ObjectMap::const_iterator ObjectConstIterator;
	typedef ObjectMap::iterator ObjectIterator;
	ObjectMap objectCollection_;

};
#endif


