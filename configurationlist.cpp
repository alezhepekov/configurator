#include "configurationlist.h"

using namespace ConfiguratorObjects;

ConfigurationList::ConfigurationList()
{
}

ConfigurationList::~ConfigurationList()
{  
}

const QList<ConfigurationInfo> * ConfigurationList::getConfigurations() const
{
  return &configurations;
}

QList<ConfigurationInfo> * ConfigurationList::getConfigurations()
{
  return &configurations;
}

void ConfigurationList::setConfigurations(const QList<ConfigurationInfo> &newConfigurations)
{
  configurations = newConfigurations;
}

QString ConfigurationList::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne;

  //�������� ��������� ��������
  xmlDoc.appendChild(xmlDoc.createElement("configurations"));

  //��������� ��������� ��������
  root = xmlDoc.documentElement(); 

  //��������� ������ ��������
  for ( QList<ConfigurationInfo>::iterator i = configurations.begin(); i != configurations.end(); i++ )
  {    
    ConfigurationInfo configInfo = *i;
    //�������� �������� ������������� ������������
    ne = xmlDoc.createElement("configuration");
    root.appendChild(ne);

    ne.setAttribute("id", configInfo.id);    

    QDomElement ce = ne;

    //���������� ���������� � �����������        
    ne = xmlDoc.createElement("connectioninfo");
    ce.appendChild(ne);

    QDomElement sce = ne;

    ne = xmlDoc.createElement("host");
    QDomText te = xmlDoc.createTextNode(configInfo.connectionInfo.host);
    ne.appendChild(te);
    sce.appendChild(ne);

    ne = xmlDoc.createElement("port");
    te = xmlDoc.createTextNode(configInfo.connectionInfo.port);
    ne.appendChild(te);
    sce.appendChild(ne);

    ne = xmlDoc.createElement("databasename");
    te = xmlDoc.createTextNode(configInfo.connectionInfo.dbName);
    ne.appendChild(te);
    sce.appendChild(ne);

    ne = xmlDoc.createElement("username");
    te = xmlDoc.createTextNode(configInfo.connectionInfo.userName);
    ne.appendChild(te);
    sce.appendChild(ne);

    ne = xmlDoc.createElement("password");
    te = xmlDoc.createTextNode(configInfo.connectionInfo.password);
    ne.appendChild(te);
    sce.appendChild(ne);

    //���������� ��������� ������������   
    configInfo.header.toXML(xmlDoc, ce);   
  }

  return xmlDoc.toString();
}

bool ConfigurationList::fromXML(const QString &xml)
{
  QDomDocument xmlDoc;
  //�������� xml-���������
  if ( !xmlDoc.setContent(xml) )
  {
    return false;
  }

  QDomElement ce = getElement(xmlDoc, "/configurations").firstChild().toElement();  
  while ( !ce.isNull() )
  {
    ConfigurationInfo curConfigurationInfo;

    curConfigurationInfo.id = ce.attribute("id").toInt();
  
    curConfigurationInfo.connectionInfo.host     = getElement(ce, "/configuration/connectioninfo/host").firstChild().toText().data();
    curConfigurationInfo.connectionInfo.port     = getElement(ce, "/configuration/connectioninfo/port").firstChild().toText().data();
    curConfigurationInfo.connectionInfo.dbName   = getElement(ce, "/configuration/connectioninfo/databasename").firstChild().toText().data();
    curConfigurationInfo.connectionInfo.userName = getElement(ce, "/configuration/connectioninfo/username").firstChild().toText().data();
    curConfigurationInfo.connectionInfo.password = getElement(ce, "/configuration/connectioninfo/password").firstChild().toText().data();
   
    curConfigurationInfo.header.fromXML(xmlDoc, getElement(ce, "/configuration/header"));  

    //���������� ������������� ������� � ������
    configurations.append(curConfigurationInfo);   

    //������� � ���������� �������
    ce = ce.nextSibling().toElement();
  }
  
  return true;
}

namespace ConfiguratorObjects
{
  QDataStream &operator<<(QDataStream &out, const ConfigurationList &configurations)
  {
    out << configurations.getConfigurations()->size();
    for ( QList<ConfigurationInfo>::const_iterator i = configurations.getConfigurations()->constBegin(); i != configurations.getConfigurations()->constEnd(); i++ )
    {
      out << *i;
    }

    return out;
  }

  QDataStream &operator>>(QDataStream &in, ConfigurationList &configurations)
  {
    int size;
    in >> size;

    for ( int i = 0; i < size; i++ )
    {
      ConfigurationInfo configInfo;
      in >> configInfo;
      configurations.getConfigurations()->append(configInfo);
    }

    return in;
  }
}
