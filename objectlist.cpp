#include "objectlist.h"

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;

ObjectList::ObjectList()
{
}

ObjectList::ObjectList(const ObjectList &other)
{
  for ( QList<BaseObject*>::const_iterator i = other.objects.constBegin(); i != other.objects.constEnd(); i++ )
  {
    BaseObject *newObject = NULL;
    switch ( (*i)->getObjectKind() )
    {
      case OBJECT_KIND_SCRIPT:
      {        
        newObject = new Script(static_cast<const Script&>(**i));        
      }
      break;   

      case OBJECT_KIND_DOCUMENT:
      {
        newObject = new Document(static_cast<const Document&>(**i));       
      }
      break;

      case OBJECT_KIND_DICTIONARY:
      {
        newObject = new Dictionary(static_cast<const Dictionary&>(**i));       
      }
      break;

      case OBJECT_KIND_REPORT:
      {
        newObject = new Report(static_cast<const Report&>(**i));      
      }
      break;

      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      {
        newObject = new DocumentsJournal(static_cast<const DocumentsJournal&>(**i));       
      }
      break;

      case OBJECT_KIND_REGISTER:
      {
        newObject = new Register(static_cast<const Register&>(**i));      
      }
      break;

      case OBJECT_KIND_ENUMERATION:
      {
        newObject = new Enumeration(static_cast<const Enumeration&>(**i));      
      }
      break;

      case OBJECT_KIND_CONSTANT:
      {
        newObject = new Constant(static_cast<const Constant&>(**i));       
      }
      break;

      case OBJECT_KIND_DFORM:
      {
        newObject = new DForm(static_cast<const DForm&>(**i));       
      }
      break;

      case OBJECT_KIND_DTABLE:
      {
        newObject = new DTable(static_cast<const DTable&>(**i));
      }
      break;

      case OBJECT_KIND_DQUERY:
      {
        newObject = new DQuery(static_cast<const DQuery&>(**i));
      }
      break;

      case OBJECT_KIND_DCLASS_MODULE:
      {
        newObject = new DClassModule(static_cast<const DClassModule&>(**i));
      }
      break;

      case OBJECT_KIND_DGLOBAL_THREAD:
      {
        newObject = new DGlobalThread(static_cast<const DGlobalThread&>(**i));
      }
      break;

      case OBJECT_KIND_DGLOBAL_CONTAINER:
      {
        newObject = new DGlobalContainer(static_cast<const DGlobalContainer&>(**i));
      }
      break;

      case OBJECT_KIND_DTRIGGER:
      {
        newObject = new DTrigger(static_cast<const DTrigger&>(**i));
      }
      break;

      case OBJECT_KIND_DTIME_MANAGER:
      {
        newObject = new DTimeManager(static_cast<const DTimeManager&>(**i));
      }
      break;

      default:;
    }

    objects.append(newObject);
  } 
}

ObjectList::~ObjectList()
{
  for ( QList<BaseObject*>::iterator i = objects.begin(); i != objects.end(); i++ )
  {
    delete *i;
  }
}

ObjectList & ObjectList::operator= (const ObjectList &right)
{  
  if ( this == &right ) return *this;

  this->objects.clear();
  for ( QList<BaseObject*>::const_iterator i = right.objects.constBegin(); i != right.objects.constEnd(); i++ )
  {
    BaseObject *newObject = NULL;
    switch ( (*i)->getObjectKind() )
    {
      case OBJECT_KIND_SCRIPT:
      {        
        newObject = new Script(static_cast<const Script&>(**i));        
      }
      break;   

      case OBJECT_KIND_DOCUMENT:
      {
        newObject = new Document(static_cast<const Document&>(**i));       
      }
      break;

      case OBJECT_KIND_DICTIONARY:
      {
        newObject = new Dictionary(static_cast<const Dictionary&>(**i));       
      }
      break;

      case OBJECT_KIND_REPORT:
      {
        newObject = new Report(static_cast<const Report&>(**i));      
      }
      break;

      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      {
        newObject = new DocumentsJournal(static_cast<const DocumentsJournal&>(**i));       
      }
      break;

      case OBJECT_KIND_REGISTER:
      {
        newObject = new Register(static_cast<const Register&>(**i));      
      }
      break;

      case OBJECT_KIND_ENUMERATION:
      {
        newObject = new Enumeration(static_cast<const Enumeration&>(**i));      
      }
      break;

      case OBJECT_KIND_CONSTANT:
      {
        newObject = new Constant(static_cast<const Constant&>(**i));       
      }
      break;

      case OBJECT_KIND_DFORM:
      {
        newObject = new DForm(static_cast<const DForm&>(**i));       
      }
      break;

      case OBJECT_KIND_DTABLE:
      {
        newObject = new DTable(static_cast<const DTable&>(**i));
      }
      break;

      case OBJECT_KIND_DQUERY:
      {
        newObject = new DQuery(static_cast<const DQuery&>(**i));
      }
      break;

      case OBJECT_KIND_DCLASS_MODULE:
      {
        newObject = new DClassModule(static_cast<const DClassModule&>(**i));
      }
      break;

      case OBJECT_KIND_DGLOBAL_THREAD:
      {
        newObject = new DGlobalThread(static_cast<const DGlobalThread&>(**i));
      }
      break;

      case OBJECT_KIND_DGLOBAL_CONTAINER:
      {
        newObject = new DGlobalContainer(static_cast<const DGlobalContainer&>(**i));
      }
      break;

      case OBJECT_KIND_DTRIGGER:
      {
        newObject = new DTrigger(static_cast<const DTrigger&>(**i));
      }
      break;

      case OBJECT_KIND_DTIME_MANAGER:
      {
        newObject = new DTimeManager(static_cast<const DTimeManager&>(**i));
      }
      break;

      default:;
    }

    this->objects.append(newObject);
  }

  return *this;
}

const QList<BaseObject*>* ObjectList::getObjects() const
{
  return &objects;
}

QList<BaseObject*>* ObjectList::getObjects()
{
  return &objects;
}

void ObjectList::setObjects(const QList<BaseObject*>& newObjects)
{
  objects = newObjects;
}

QString ObjectList::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne;

  //�������� ��������� ��������
  xmlDoc.appendChild(xmlDoc.createElement("configuration_objects"));

  //��������� ��������� ��������
  root = xmlDoc.documentElement(); 

  //��������� ������ ��������
  for ( QList<BaseObject*>::iterator i = objects.begin(); i != objects.end(); i++ )
  {    
    //�������� �������� ������������� �������
    ne = xmlDoc.createElement("object");    
    
    root.appendChild(ne);

    QDomDocument curXmlDoc;
    QString strXmlDoc;
    switch( (*i)->getObjectKind() )
    {
      case OBJECT_KIND_SCRIPT:
      {       
        ne.setAttribute("type", OBJECT_KIND_SCRIPT);      

        Script *newScript = dynamic_cast<Script*>(*i);
        if ( newScript )
        {
          ne.setAttribute("id", newScript->getId());

          QDomElement ce = ne;
          
          //���������� ���������        
          ne = xmlDoc.createElement("translates");
          ce.appendChild(ne);
          appendToXML(xmlDoc, ne, newScript->getTranslates());

          //���������� ������������� �������
          ne = xmlDoc.createElement("body");
          QDomText te = xmlDoc.createTextNode(newScript->getBody());
          ne.appendChild(te);
          ce.appendChild(ne);
          
          ne = ce;
        }        
      }
      break;      

      case OBJECT_KIND_DOCUMENT:
      {
        Document *newDocument = dynamic_cast<Document*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_DOCUMENT);
        }
      }
      break;

      case OBJECT_KIND_DICTIONARY:
      {
        Dictionary *newDocument = dynamic_cast<Dictionary*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_DICTIONARY);
        }
      }
      break;

      case OBJECT_KIND_REPORT:
      {
        Report *newDocument = dynamic_cast<Report*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_REPORT);
        }      
      }
      break;

      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      {
        DocumentsJournal *newDocument = dynamic_cast<DocumentsJournal*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_DOCUMENTS_JOURNAL);
        }      
      }
      break;

      case OBJECT_KIND_REGISTER:
      {
        Register *newDocument = dynamic_cast<Register*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_REGISTER);
        }
      }
      break;

      case OBJECT_KIND_ENUMERATION:
      {
        Enumeration *newDocument = dynamic_cast<Enumeration*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_ENUMERATION);
        }
      }
      break;

      case OBJECT_KIND_CONSTANT:
      {
        Constant *newDocument = dynamic_cast<Constant*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_CONSTANT);
        }    
      }
      break;

      case OBJECT_KIND_DFORM:
      {
        DForm *newDocument = dynamic_cast<DForm*>(*i);
        if ( newDocument )
        {
          strXmlDoc = newDocument->toXML();
          ne.setAttribute("type", OBJECT_KIND_DFORM);
        }       
        //((DForm*)objects[i])->toXML(xmlDoc, ne);
      }
      break;

      default:;
    }

    curXmlDoc.setContent(strXmlDoc);
    ne.appendChild(curXmlDoc.documentElement());
  }

  return xmlDoc.toString();
}

bool ObjectList::fromXML(const QString& xml)
{
  QDomDocument xmlDoc;
  //�������� xml-���������
  if ( !xmlDoc.setContent(xml) )
  {
    return false;
  }

  //��������� ��������
  BaseObject *curObject = NULL;
  QDomElement ce = getElement(xmlDoc, "/configuration_objects").firstChild().toElement();
  while ( !ce.isNull() )
  {
    switch ( ce.attribute("type").toInt() )
    {
      case OBJECT_KIND_SCRIPT:
      {        
        curObject = new Script(getElement(ce, "/object/body").firstChild().toText().data());
        Script *curScript = dynamic_cast<Script*>(curObject);
        //int id = ce.attribute("id").toInt();
        curScript->setId(ce.attribute("id").toInt());
        takeFromXML(getElement(ce, "/object/translates"), curScript->getTranslates());
      }
      break;      

      case OBJECT_KIND_DOCUMENT:
      {
        curObject = new Document();
        ((Document*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));
      }
      break;

      case OBJECT_KIND_DICTIONARY:
      {
        curObject = new Dictionary();
        ((Dictionary*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));       
      }
      break;

      case OBJECT_KIND_REPORT:
      {
        curObject = new Report();
        ((Report*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));         
      }
      break;

      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      {
        curObject = new DocumentsJournal();
        ((DocumentsJournal*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));     
      }
      break;

      case OBJECT_KIND_REGISTER:
      {
        curObject = new Register();
        ((Register*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));      
      }
      break;

      case OBJECT_KIND_ENUMERATION:
      {
        curObject = new Enumeration();
        ((Enumeration*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));       
      }
      break;

      case OBJECT_KIND_CONSTANT:
      {
        curObject = new Constant();
        ((Constant*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));
      }
      break;

      case OBJECT_KIND_DFORM:
      {
        curObject = new DForm();
        ((DForm*)curObject)->loadFromXML(xmlDoc, getElement(ce, "/object/document"));   
      }
      break;

      default:;
    }
    
    //���������� ������������� ������� � ������
    objects.append(curObject);   

    //������� � ���������� �������
    ce = ce.nextSibling().toElement();
  }

  return true;
}

void ObjectList::appendToXML(QDomDocument& xmlDoc, QDomElement& currentElement, const QMap<int, QString>* translatesList)
{
  QDomElement ne;  

  //������� ��������� �����  
  QMap<int, QString>::const_iterator i = translatesList->constBegin();
  while (i != translatesList->constEnd())
  {
    //���������� ��������� �� translatesList � xmlDoc
    ne = xmlDoc.createElement("translate");
    ne.setAttribute("id", i.key());
    ne.setAttribute("text", i.value().toLocal8Bit().data());
    currentElement.appendChild(ne);

    ++i;
  }
}

void ObjectList::takeFromXML(const QDomElement& currentElement, QMap<int, QString>* translatesList)
{  
  QDomElement ce = currentElement.firstChild().toElement();
  while (!ce.isNull())
  {   
    translatesList->insert(ce.attribute("id", 0).toInt(), ce.attribute("text"));   
    ce = ce.nextSibling().toElement();
  }
}

namespace ConfiguratorObjects
{
  QDataStream &operator<<(QDataStream &out, const ObjectList &right)
  {
    out << right.objects.size();
    for ( QList<BaseObject*>::const_iterator i = right.objects.constBegin(); i != right.objects.constEnd(); i++ )
    {
      out << (*i)->getObjectKind();
      switch( (*i)->getObjectKind() )
      {
        case OBJECT_KIND_SCRIPT:
        {         
          out << static_cast<const Script&>(**i);
        }
        break;

        case OBJECT_KIND_DOCUMENT:
        {
          out << static_cast<const Document&>(**i);
        }
        break;

        case OBJECT_KIND_DICTIONARY:
        {
          out << static_cast<const Dictionary&>(**i);
        }
        break;

        case OBJECT_KIND_REPORT:
        {
           out << static_cast<const Report&>(**i);
        }
        break;

        case OBJECT_KIND_DOCUMENTS_JOURNAL:
        {
          out << static_cast<const DocumentsJournal&>(**i);
        }
        break;

        case OBJECT_KIND_REGISTER:
        {
          out << static_cast<const Register&>(**i);
        }
        break;

        case OBJECT_KIND_ENUMERATION:
        {
          out << static_cast<const Enumeration&>(**i);
        }
        break;

        case OBJECT_KIND_CONSTANT:
        {
          out << static_cast<const Constant&>(**i);
        }
        break;

        case OBJECT_KIND_DFORM:
        {
          out << static_cast<const DForm&>(**i);
        }
        break;

        case OBJECT_KIND_DTABLE:
        {
          out << static_cast<const DTable&>(**i);        
        }
        break;

        case OBJECT_KIND_DQUERY:
        {
          out << static_cast<const DQuery&>(**i);         
        }
        break;

        case OBJECT_KIND_DCLASS_MODULE:
        {
          out << static_cast<const DClassModule&>(**i);
        }
        break;

        case OBJECT_KIND_DGLOBAL_THREAD:
        {
          out << static_cast<const DGlobalThread&>(**i);
        }
        break;

        case OBJECT_KIND_DGLOBAL_CONTAINER:
        {
          out << static_cast<const DGlobalContainer&>(**i);
        }
        break;

        case OBJECT_KIND_DTRIGGER:
        {
          out << static_cast<const DTrigger&>(**i);
        }
        break;

        case OBJECT_KIND_DTIME_MANAGER:
        {
          out << static_cast<const DTimeManager&>(**i);
        }
        break;

        default:;
      }
    }

    return out;
  }

  QDataStream &operator>>(QDataStream &in, ObjectList &right)
  {
    int size;
    in >> size;
    for ( int i = 0; i < size; i++ )
    {
      int objectKind;
      in >> objectKind;

      BaseObject *curObject = NULL;
      switch ( objectKind )
      {
        case OBJECT_KIND_SCRIPT:
        {        
          curObject = new Script();
          in >> *((Script*)curObject);//static_cast<const Script&>(*curObject);
        }
        break;      

        case OBJECT_KIND_DOCUMENT:
        {
          curObject = new Document();
          in >> *((Document*)curObject);
        }
        break;

        case OBJECT_KIND_DICTIONARY:
        {
          curObject = new Dictionary();
          in >> *((Dictionary*)curObject);
        }
        break;

        case OBJECT_KIND_REPORT:
        {
          curObject = new Report();
          in >> *((Report*)curObject);
        }
        break;

        case OBJECT_KIND_DOCUMENTS_JOURNAL:
        {
          curObject = new DocumentsJournal();
          in >> *((DocumentsJournal*)curObject);
        }
        break;

        case OBJECT_KIND_REGISTER:
        {
          curObject = new Register();
          in >> *((Register*)curObject);
        }
        break;

        case OBJECT_KIND_ENUMERATION:
        {
          curObject = new Enumeration();
          in >> *((Enumeration*)curObject);
        }
        break;

        case OBJECT_KIND_CONSTANT:
        {
          curObject = new Constant();
          in >> *((Constant*)curObject);
        }
        break;

        case OBJECT_KIND_DFORM:
        {
          curObject = new DForm();
          in >> *((DForm*)curObject);
        }
        break;

        case OBJECT_KIND_DTABLE:
        {
          curObject = new DTable();
          in >> *((DTable*)curObject);
        }
        break;

        case OBJECT_KIND_DQUERY:
        {
          curObject = new DQuery();
          in >> *((DQuery*)curObject);
        }
        break;

        case OBJECT_KIND_DCLASS_MODULE:
        {          
          curObject = new DClassModule();
          in >> *((DClassModule*)curObject);
        }
        break;

        case OBJECT_KIND_DGLOBAL_THREAD:
        {           
          curObject = new DGlobalThread();
          in >> *((DGlobalThread*)curObject);
        }
        break;

        case OBJECT_KIND_DGLOBAL_CONTAINER:
        {            
          curObject = new DGlobalContainer();
          in >> *((DGlobalContainer*)curObject);
        }
        break;

        case OBJECT_KIND_DTRIGGER:
        {
          curObject = new DTrigger();
          in >> *((DTrigger*)curObject);
        }
        break;

        case OBJECT_KIND_DTIME_MANAGER:
        {
          curObject = new DTimeManager();
          in >> *((DTimeManager*)curObject);
        }
        break;

        default:;
      }

      right.objects.append(curObject);
    }  

    return in;
  }
}
