#include "configurationheader.h"

namespace ConfiguratorObjects
{
  QDataStream &operator<<(QDataStream &out, const ConfigurationInfo &configInfo)
  {
    out << configInfo.connectionInfo;
    out << configInfo.id;
    out << configInfo.header;

    return out;
  }

  QDataStream &operator>>(QDataStream &in, ConfigurationInfo &configInfo)
  {
    in >> configInfo.connectionInfo;
    in >> configInfo.id;    
    in >> configInfo.header;    

    return in;
  } 

  QDataStream &operator<<(QDataStream &out, const ConfigurationHeader &header)
  {
    out << header.id;    
    out << header.name;
    out << header.author;
    out << header.version;
    out << header.creationDateTime;
    out << header.lastModificationDateTime;
    out << header.lastModificationUserName;
    out << header.userDatabaseId;

    out << header.roles.size();
    for ( QList<Role*>::const_iterator i = header.roles.constBegin(); i != header.roles.constEnd(); i++ )
    {
      out << *(*i);
    }

    out << header.privileges.size();
    for ( QList<Privilege*>::const_iterator i = header.privileges.constBegin(); i != header.privileges.constEnd(); i++ )
    {
      out << *(*i);
    }

    return out;
  }

  QDataStream &operator>>(QDataStream &in, ConfigurationHeader &header)
  {
    in >> header.id;   
    in >> header.name;   
    in >> header.author;   
    in >> header.version;   
    in >> header.creationDateTime;
    in >> header.lastModificationDateTime;
    in >> header.lastModificationUserName;
    in >> header.userDatabaseId;   

    int size;
    in >> size;
    for ( int i = 0; i < size; i++ )
    {
      Role *newRole = new Role();
      in >> *newRole;
      header.roles.append(newRole);
    }

    in >> size;
    for ( int i = 0; i < size; i++ )
    {
      Privilege *newPrivilege = new Privilege();
      in >> *newPrivilege;
      header.privileges.append(newPrivilege);
    }

    return in;
  }
}

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;

ConfigurationHeader::ConfigurationHeader() :
id(-1)
{  
}

ConfigurationHeader::ConfigurationHeader(const ConfigurationHeader &other) :
id(other.id),
name(other.name),
author(other.author),
version(other.version),
creationDateTime(other.creationDateTime),
lastModificationDateTime(other.lastModificationDateTime),
lastModificationUserName(other.lastModificationUserName),
userDatabaseId(other.userDatabaseId)
{  
  for ( QList<Role*>::const_iterator i = other.roles.constBegin(); i != other.roles.constEnd(); i++ )
  {
    roles.append(new Role(**i));
  }
  
  for ( QList<Privilege*>::const_iterator i = other.privileges.constBegin(); i != other.privileges.constEnd(); i++ )
  {
    privileges.append(new Privilege(**i));
  }
}

ConfigurationHeader::~ConfigurationHeader()
{
  for ( QList<Role*>::iterator i = roles.begin(); i != roles.end(); i++ )
  {
    delete *i;
  }

  for ( QList<Privilege*>::iterator i = privileges.begin(); i != privileges.end(); i++ )
  {
    delete *i;
  }
}

ConfigurationHeader & ConfigurationHeader::operator= (const ConfigurationHeader &right)
{
  //�������� �� ����������������
  if ( this == &right ) return *this;  

  this->id = right.id; 
  this->name = right.name;
  this->author = right.author;
  this->version = right.version;
  this->creationDateTime = right.creationDateTime;
  this->lastModificationDateTime = right.lastModificationDateTime;
  this->lastModificationUserName = right.lastModificationUserName;
  this->userDatabaseId = right.userDatabaseId;

  this->roles.clear();
  for ( QList<Role*>::const_iterator i = right.roles.constBegin(); i != right.roles.constEnd(); i++ )
  {
    this->roles.append(new Role(**i));
  }

  this->privileges.clear();
  for ( QList<Privilege*>::const_iterator i = right.privileges.constBegin(); i != right.privileges.constEnd(); i++ )
  {
    this->privileges.append(new Privilege(**i));
  }

  return *this;
}

int ConfigurationHeader::getId() const
{
  return id;
}

void ConfigurationHeader::setId(int newId)
{
  id = newId;
}

QString ConfigurationHeader::getName() const
{
  return name;
}

void ConfigurationHeader::setName(const QString &newName)
{
  name = newName;
}

QString ConfigurationHeader::getAuthor() const
{
  return author;
}

void ConfigurationHeader::setAuthor(const QString &newAuthor)
{
  author = newAuthor;
}

QString ConfigurationHeader::getVersion() const
{
  return version;
}

void ConfigurationHeader::setVersion(const QString &newVersion)
{
  version = newVersion;
}

QDateTime ConfigurationHeader::getCreationDateTime() const
{
  return creationDateTime;
}

void ConfigurationHeader::setCreationDateTime(const QDateTime &newCreationTime)
{
  creationDateTime = newCreationTime;
}

QDateTime ConfigurationHeader::getLastModificationDateTime() const
{
  return lastModificationDateTime;
}

void ConfigurationHeader::setLastModificationDateTime(const QDateTime &newLastModificationTime)
{
  lastModificationDateTime = newLastModificationTime;
}

QString ConfigurationHeader::getLastModificationUserName() const
{
  return lastModificationUserName;
}

void ConfigurationHeader::setLastModificationUserName(const QString &userName)
{
  lastModificationUserName = userName;
}

int ConfigurationHeader::getUserDatabaseId() const
{
  return userDatabaseId;
}

void ConfigurationHeader::setUserDatabaseId(const int newUserDatabaseId)
{
  userDatabaseId = newUserDatabaseId;
}

const QList<Privilege*> * ConfigurationHeader::getPrivileges() const
{
  return &privileges;
}

QList<Privilege*> * ConfigurationHeader::getPrivileges()
{
  return &privileges;
}

void ConfigurationHeader::setPriveleges(const QList<Privilege*> &newPrivileges)
{
  privileges = newPrivileges;
}

const QList<Role*> * ConfigurationHeader::getRoles() const
{
  return &roles;
}

QList<Role*> * ConfigurationHeader::getRoles()
{
  return &roles;
}

void ConfigurationHeader::setRoles(const QList<Role*> &newRoles)
{
  roles = newRoles;
}

QString ConfigurationHeader::toXML()
{
  QDomDocument xmlDoc;
  QDomElement root, ne;

  //�������� ��������� ��������
  xmlDoc.appendChild(xmlDoc.createElement("header"));

  //��������� ��������� ��������
  root = xmlDoc.documentElement();

  appendContentToXML(xmlDoc, root);  

  return xmlDoc.toString();
}

void ConfigurationHeader::appendContentToXML(QDomDocument &xmlDoc, QDomElement &currentElement)
{
  currentElement.setAttribute("id", id);

  QDomElement ne = xmlDoc.createElement("name");
  QDomText te = xmlDoc.createTextNode(name);
  ne.appendChild(te); 
  currentElement.appendChild(ne);

  ne = xmlDoc.createElement("author");
  te = xmlDoc.createTextNode(author);
  ne.appendChild(te); 
  currentElement.appendChild(ne);

  ne = xmlDoc.createElement("version");
  te = xmlDoc.createTextNode(version);
  ne.appendChild(te); 
  currentElement.appendChild(ne);

  ne = xmlDoc.createElement("creationdatetime"); 
  te = xmlDoc.createTextNode(creationDateTime.toLocalTime().toString("dd.MM.yyyy hh:mm:ss"));
  ne.appendChild(te); 
  currentElement.appendChild(ne);

  ne = xmlDoc.createElement("roles");
  currentElement.appendChild(ne);
  QDomElement ce = ne;

  for ( QList<Role*>::iterator i = roles.begin(); i != roles.end(); i++ )
  {
    //�������� �������� ������������� �������
    ne = xmlDoc.createElement("role");
    ne.setAttribute("id", (*i)->getId());

    QDomElement sce = ne;

    ne = xmlDoc.createElement("externalid");
    te = xmlDoc.createTextNode((*i)->getExternalId());
    ne.appendChild(te);
    sce.appendChild(ne);

    ne = xmlDoc.createElement("name");
    te = xmlDoc.createTextNode((*i)->getName());
    ne.appendChild(te);
    sce.appendChild(ne);

    ce.appendChild(sce);
  }

  ne = xmlDoc.createElement("privileges");
  currentElement.appendChild(ne);
  ce = ne; 

  for ( QList<Privilege*>::iterator i = privileges.begin(); i != privileges.end(); i++ )
  {
    //�������� �������� ������������� �������
    ne = xmlDoc.createElement("privilege");
    ne.setAttribute("id", (*i)->getId());

    QDomElement sce = ne;

    ne = xmlDoc.createElement("externalid");
    te = xmlDoc.createTextNode((*i)->getExternalId());
    ne.appendChild(te);
    sce.appendChild(ne);

    ne = xmlDoc.createElement("name");
    te = xmlDoc.createTextNode((*i)->getName());
    ne.appendChild(te);
    sce.appendChild(ne);

    ce.appendChild(sce);
  }
}

void ConfigurationHeader::toXML(QDomDocument& xmlDoc, QDomElement& currentElement)
{
  //�������� ��������� ��������
  QDomElement ne = xmlDoc.createElement("header");
  currentElement.appendChild(ne);

  appendContentToXML(xmlDoc, ne);
}

bool ConfigurationHeader::fromXML(const QString &xml)
{
  QDomDocument xmlDoc;
  //�������� xml-���������
  if ( !xmlDoc.setContent(xml) )
  {
    return false;
  }

  fromXML(xmlDoc, xmlDoc.documentElement());  

  return true;
}

void ConfigurationHeader::fromXML(const QDomDocument &xmlDoc, const QDomElement &currentElement)
{  
  id = currentElement.attribute("id").toInt();
  name =       getElement(currentElement, "/header/name").firstChild().toText().data();
  author =     getElement(currentElement, "/header/author").firstChild().toText().data();
  version =    getElement(currentElement, "/header/version").firstChild().toText().data();
  creationDateTime = QDateTime::fromString(getElement(currentElement, "/header/creationdatetime").firstChild().toText().data(), "dd.MM.yyyy hh:mm:ss");

  Role *newRole = NULL;
  QDomElement ce = getElement(currentElement, "/header/roles").firstChild().toElement();
  while ( !ce.isNull() )
  {
    newRole = new Role();
    newRole->setId(ce.attribute("id").toInt());

    newRole->setExternalId(getElement(ce, "/role/externalid").firstChild().toText().data());
    newRole->setName(getElement(ce, "/role/name").firstChild().toText().data());

    //���������� ������������� ������� � ������
    roles.append(newRole);

    //������� � ���������� �������
    ce = ce.nextSibling().toElement();
  }

  Privilege *newPrivilege = NULL;
  ce = getElement(currentElement, "/header/privileges").firstChild().toElement();
  while ( !ce.isNull() )
  {
    newPrivilege = new Privilege();
    newPrivilege->setId(ce.attribute("id").toInt());

    newPrivilege->setExternalId(getElement(ce, "/privilege/externalid").firstChild().toText().data());
    newPrivilege->setName(getElement(ce, "/privilege/name").firstChild().toText().data());

    //���������� ������������� ������� � ������
    privileges.append(newPrivilege);

    //������� � ���������� �������
    ce = ce.nextSibling().toElement();
  }
}
