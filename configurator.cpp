#include "configurator.h"
#include "../ConfiguratorComponent/ConfigSettingsForm/configsettingsform.h"
#include "../core/IdeServerConnectionModule/ideserverconnectionmodule.h"

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;

Configurator::Configurator(QWidget *parent):
AbstractConfigurator(parent)
{
  //������������� ������
  gjoTree = GJO::Tree::instance();

  /*
  //----------------------------------------------------------------------------------------
  GJODocumentParser gjoParser(gjoTree);
  DTable *dt = new DTable();
  Accessory *newAcc = new Accessory();
  newAcc->getTranslates()->insert(1, "Acc1");
  Number *newNumberType = new Number();
  newAcc->setType(newNumberType);
  dt->getAccessories()->append(newAcc);

  newAcc = new Accessory();
  newAcc->getTranslates()->insert(1, "Acc2");
  Integer *newNumberInteger = new Integer();
  newAcc->setType(newNumberInteger);
  dt->getAccessories()->append(newAcc);

  int gjoId = gjoParser.setDocument(dt);
  delete dt;

  DTable *dt1 = dynamic_cast<DTable *>(gjoParser.getDocument(gjoId));
  if (dt1)
  {
    std::cout << dt1->getAccessories()->size() << std::endl;

    for(QList<Accessory*>::iterator i = dt1->getAccessories()->begin(); i != dt1->getAccessories()->end(); i++ )
    {
      std::cout << (*i)->getTranslates()->value(1).toLocal8Bit().data() << std::endl;
    }

    delete dt1;
  }*/

  /*
  Document *document = new Document();
  document->getTranslates()->insert(1, "L1");
  document->getTranslates()->insert(2, "L2");
  document->getScript()->setBody("<!DOCTYPE GJOScriptXML>\n"
    "<ScriptData>\n"
    "<Script>test script</Script>\n"
    "<Breakpoints lines="" />\n"
    "<ExecutableLines lines="" />\n"
    "</ScriptData>");
  int gjoId = gjoParser.setDocument(document);
  Document *document1 = dynamic_cast<Document*>(gjoParser.getDocument(gjoId));
  qDebug(document->getScript()->getBody().toLocal8Bit().data());
  qDebug(document1->getScript()->getBody().toLocal8Bit().data());
  std::cout << document->getGJOTreeId() << std::endl;
  std::cout << document1->getGJOTreeId() <<std::endl;
  delete document;
  delete document1;*/
 /* std::cout << document->getId() << std::endl;
  std::cout << document1->getId() <<std::endl;
  std::cout << document->getTranslates()->value(1).toLocal8Bit().data() << std::endl;
  std::cout << document->getTranslates()->value(2).toLocal8Bit().data() << std::endl;

  std::cout << document1->getTranslates()->value(1).toLocal8Bit().data() << std::endl;
  std::cout << document1->getTranslates()->value(2).toLocal8Bit().data() << std::endl;*/
  
  /*DGlobalContainer *globalContainer = new DGlobalContainer();
  globalContainer->getTranslates()->insert(1, "L1");
  globalContainer->getTranslates()->insert(2, "L2");
  globalContainer->getScript()->setBody("<!DOCTYPE GJOScriptXML>\n"
                                        "<ScriptData>\n"
                                        "<Script>test script</Script>\n"
                                        "<Breakpoints lines="" />\n"
                                        "<ExecutableLines lines="" />\n"
                                        "</ScriptData>");
  gjoId = gjoParser.setDocument(globalContainer);
  DGlobalContainer *globalContainer1 = dynamic_cast<DGlobalContainer*>(gjoParser.getDocument(gjoId));
  qDebug(globalContainer->getScript()->getBody().toLocal8Bit().data());
  qDebug(globalContainer1->getScript()->getBody().toLocal8Bit().data());
  std::cout << globalContainer->getId() << std::endl;
  std::cout << globalContainer1->getId() <<std::endl;
  std::cout << globalContainer->getTranslates()->value(1).toLocal8Bit().data() << std::endl;
  std::cout << globalContainer->getTranslates()->value(2).toLocal8Bit().data() << std::endl;

  std::cout << globalContainer1->getTranslates()->value(1).toLocal8Bit().data() << std::endl;
  std::cout << globalContainer1->getTranslates()->value(2).toLocal8Bit().data() << std::endl;*/
  //----------------------------------------------------------------------------------------

  //������� ������� ����������
  mainWidget = new QWidget(parent);

  actionConfigurationManager = new QAction(mainWidget);
  actionConfigurationManager->setText(tr("Configuration manager"));
  actionConfigurationManager->setStatusTip(tr("Configuration manager"));
  actionConfigurationManager->setShortcut(tr("Ctrl+M"));
  actionConfigurationManager->setIcon(QIcon(QString::fromUtf8(":/Configurator/configurationmanager.png")));
  actionConfigurationManager->setEnabled(false);

  actionSaveConfiguration = new QAction(mainWidget); 
  actionSaveConfiguration->setText(tr("Save configuration"));
  actionSaveConfiguration->setStatusTip(tr("Save the current configuration"));
  actionSaveConfiguration->setShortcut(tr("Ctrl+Alt+S"));
  actionSaveConfiguration->setIcon(QIcon(QString::fromUtf8(":/Configurator/saveconfiguration.png")));
  actionSaveConfiguration->setEnabled(false);
  
  actionReleaseConfiguration = new QAction(mainWidget); 
  actionReleaseConfiguration->setText(tr("Release configuration"));
  actionReleaseConfiguration->setStatusTip(tr("Release the current configuration"));
  //actionReleaseConfiguration->setShortcut(tr("Ctrl+Alt+S"));
  actionReleaseConfiguration->setIcon(QIcon(QString::fromUtf8(":/Configurator/releaseconfiguration.png")));
  actionReleaseConfiguration->setEnabled(false);

  actionCloseConfiguration = new QAction(mainWidget); 
  actionCloseConfiguration->setText(tr("Close configuration"));
  actionCloseConfiguration->setStatusTip(tr("Close the current configuration"));
  //actionCloseConfiguration->setShortcut(tr("Ctrl+Alt+S"));
  //actionCloseConfiguration->setIcon(QIcon(QString::fromUtf8(":/Configurator/saveconfiguration.png")));
  actionCloseConfiguration->setEnabled(false);

  actionParameters = new QAction(mainWidget);
  actionParameters->setText(tr("Settings"));
  actionParameters->setStatusTip(tr("Settings of current configuration"));
  //actionParameters->setShortcut("Ctrl+P");
  actionParameters->setIcon(QIcon(QString::fromUtf8(":/Configurator/settings.png")));
  actionParameters->setEnabled(false);

  actionAddObject = new QAction(mainWidget);
  actionAddObject->setText(tr("Add configuration object"));
  actionAddObject->setStatusTip(tr("Add configuration object"));
  actionAddObject->setShortcut(tr("Ctrl+Insert"));
  actionAddObject->setIcon(QIcon(QString::fromUtf8(":/Configurator/additem.png")));
  actionAddObject->setEnabled(false);

  actionRemoveObject = new QAction(mainWidget);
  actionRemoveObject->setText(tr("Remove configuration object"));
  actionRemoveObject->setStatusTip(tr("Remove configuration object"));
  actionRemoveObject->setShortcut(tr("Ctrl+Delete"));
  actionRemoveObject->setIcon(QIcon(QString::fromUtf8(":/Configurator/removeitem.png")));
  actionRemoveObject->setEnabled(false);

  treeWidget = new QTreeWidget(mainWidget);
  treeWidget->setColumnCount(1);
  treeWidget->headerItem()->setHidden(true);

  _toolBar = new QToolBar(mainWidget);
  _toolBar->setFixedHeight(34);
  _toolBar->setOrientation(Qt::Horizontal);  
  _toolBar->setIconSize(QSize(20, 20));
  _toolBar->addAction(actionConfigurationManager);
  _toolBar->addAction(actionSaveConfiguration);
  _toolBar->addAction(actionReleaseConfiguration);
  _toolBar->addSeparator();
  _toolBar->addAction(actionAddObject);
  _toolBar->addAction(actionRemoveObject);
  _toolBar->addSeparator();
  _toolBar->addAction(actionParameters);
  
  _menu = new QMenu(tr("Configurator"), parent);
  _menu->addAction(actionConfigurationManager);
  _menu->addAction(actionSaveConfiguration);
  _menu->addAction(actionCloseConfiguration);
  _menu->addSeparator();
  _menu->addAction(actionAddObject);
  _menu->addAction(actionRemoveObject);
  _menu->addSeparator();
  _menu->addAction(actionParameters);

  statusBar = new QStatusBar();
  
  //���������� ��������� ����������
  layout = new QVBoxLayout();
  layout->setMargin(2);
  layout->setSpacing(0);
  layout->addWidget(_toolBar);
  layout->addWidget(treeWidget);
  layout->addWidget(statusBar);
  mainWidget->setLayout(layout);
 
  connect(actionConfigurationManager, SIGNAL(triggered()), this, SLOT(configurationManager()));
  connect(actionSaveConfiguration, SIGNAL(triggered()), this, SLOT(saveConfiguration()));
  connect(actionReleaseConfiguration, SIGNAL(triggered()), this, SLOT(releaseConfiguration()));
  connect(actionCloseConfiguration, SIGNAL(triggered()), this, SLOT(closeConfiguration()));
  connect(actionAddObject, SIGNAL(triggered()), this, SLOT(newObject()));
  connect(actionRemoveObject, SIGNAL(triggered()), this, SLOT(removeObject()));
  connect(treeWidget , SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)));  
  //connect(treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)), this, SLOT(editItemName(QTreeWidgetItem *, int)));
  connect(treeWidget, SIGNAL(itemActivated(QTreeWidgetItem *, int)), this, SLOT(itemActivated(QTreeWidgetItem *, int)));
  connect(actionParameters, SIGNAL(triggered()), this, SLOT(setConfigParameters()));  
  
  connect(IdeServerConnectionModule::instance(), SIGNAL(configurationSaved(const qint32 &, const QByteArray &)), this, SLOT(processCmdSaveConfiguration(const qint32 &, const QByteArray &)));
  connect(IdeServerConnectionModule::instance(), SIGNAL(stateChanged()), this, SLOT(processConnectionStateChange()));

  configurationListForm = new ConfigurationListForm(mainWidget);  
  connect(configurationListForm, SIGNAL(processConfiguration(const ConfigurationHeader &)), this, SLOT(openConfiguration(const ConfigurationHeader &)));
}

Configurator::~Configurator()
{
  delete actionConfigurationManager;
  delete actionSaveConfiguration;
  delete actionReleaseConfiguration;
  delete actionCloseConfiguration;
  delete actionParameters;
  delete actionAddObject;
  delete actionRemoveObject;

  delete _toolBar;
  delete _menu;
  delete treeWidget;
  delete statusBar;
  delete layout;

  delete mainWidget;
  delete configurationListForm;
}

void Configurator::reloadDocument(int id)
{  
  if ( !treeWidget->topLevelItemCount() ) return;
  
  QTreeWidgetItem *root = treeWidget->topLevelItem(0);
  QTreeWidgetItem *foundItem = NULL;
  int foundItemPosition = -1;
  for ( int i = 0; (i < root->childCount()) && (foundItem == NULL); i++ )
  {
    QTreeWidgetItem *current = root->child(i);
    ItemData itemData = current->data(0, Qt::UserRole).value<ItemData>();

    switch( itemData.kind )
    {
      case ITEM_KIND_DOCUMENTS:
      case ITEM_KIND_DICTIONARIES:
      case ITEM_KIND_REPORTS:
      case ITEM_KIND_DOCUMENTS_JOURNALS:
      case ITEM_KIND_REGISTERS:
      case ITEM_KIND_ENUMERATIONS:
      case ITEM_KIND_CONSTANTS:
      case ITEM_KIND_DFORMS:
      case ITEM_KIND_DTABLES:
      case ITEM_KIND_DQUERIES:
      case ITEM_KIND_DCLASSMODULES:
      case ITEM_KIND_DGLOBAL_THREADS:
      case ITEM_KIND_DGLOBAL_CONTAINERS:
      {
        for ( int j = 0; (j < current->childCount()) && (foundItem == NULL); j++ )
        {
          QTreeWidgetItem *subCurrent = current->child(j);
          ItemData subItemData = subCurrent->data(0, Qt::UserRole).value<ItemData>();

          switch( subItemData.kind )
          {
            case ITEM_KIND_DOCUMENT:
            case ITEM_KIND_DICTIONARY:
            case ITEM_KIND_REPORT:
            case ITEM_KIND_DOCUMENTS_JOURNAL:
            case ITEM_KIND_REGISTER:
            case ITEM_KIND_ENUMERATION:
            case ITEM_KIND_CONSTANT:
            case ITEM_KIND_DFORM:
            case ITEM_KIND_DTABLE:
            case ITEM_KIND_DQUERY:
            case ITEM_KIND_DCLASSMODULE:
            case ITEM_KIND_DGLOBAL_THREAD:
            case ITEM_KIND_DGLOBAL_CONTAINER:
            {
              if ( subItemData.gjoTreeId == id )
              {
                //����� ��������
                foundItem = subCurrent;
                foundItemPosition = j;
              }
            }
            break;

            default:;
          }          
        }
      }
      break;

      default:;
    }    
  }

  GJODocumentParser *gjoParser = new GJODocumentParser(gjoTree);
  BaseDocument *document = gjoParser->getDocument(id);  
  delete gjoParser;

  if ( !document || !foundItem ) return;
  
  ItemData itemData = foundItem->data(0, Qt::UserRole).value<ItemData>();
  for ( int i = 0; i < currentObjectList.getObjects()->size(); i++ )
  {
    if ( currentObjectList.getObjects()->value(i)->getId() == itemData.id )
    {      
      //���������� ��������� � ������ �������� ������������
      currentObjectList.getObjects()->replace(i, document);
      break;
    }
  }

  QTreeWidgetItem *parent = foundItem->parent();

  //�������� ���� ������������ ������������� ���������  
  delete parent->takeChild(foundItemPosition);

  //���������� ���� ���������
  switch( document->getKind() )
  {
    case DOC_KIND_DOCUMENT:
    {
      Document *newDocument = dynamic_cast<Document*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDocument(parent, newDocument, false));        
      }      
    }
    break;

    case DOC_KIND_DICTIONARY:
    {
      Dictionary *newDocument = dynamic_cast<Dictionary*>(document);
      if ( newDocument )
      {        
        parent->insertChild(foundItemPosition, appendDocument(parent, newDocument, false));        
      }      
    }
    break;

    case DOC_KIND_REPORT:
    {
      Report *newDocument = dynamic_cast<Report*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDocument(parent, newDocument, false));        
      }
    }
    break;

    case DOC_KIND_DOCUMENTS_JOURNAL:
    {
      DocumentsJournal *newDocument = dynamic_cast<DocumentsJournal*>(document);
      if ( newDocument )
      {        
        parent->insertChild(foundItemPosition, appendDocumentsJournal(parent, newDocument, false));        
      }
    }
    break;

    case DOC_KIND_REGISTER:
    {
      Register *newDocument = dynamic_cast<Register*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendRegister(parent, newDocument, false));        
      }
    }
    break;

    case DOC_KIND_FORM:
    {
      DForm *newDocument = dynamic_cast<DForm*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDForm(parent, newDocument, false));        
      }
    }
    break;

    case DOC_KIND_ENUMERATION:
    {
      Enumeration *newDocument = dynamic_cast<Enumeration*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendEnumeration(parent, newDocument, false));        
      }
    }
    break;

    case DOC_KIND_CONSTANT:
    {
      Constant *newDocument = dynamic_cast<Constant*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendConstant(parent, newDocument, false));        
      }
    }
    break;

    case DOC_KIND_TABLE:
    {
      DTable *newDocument = dynamic_cast<DTable*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDTable(parent, newDocument, false));
      }
    }
    break;

    case DOC_KIND_QUERY:
    {
      DQuery *newDocument = dynamic_cast<DQuery*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDQuery(parent, newDocument, false));
      }
    }
    break;

    case DOC_KIND_CLASS_MODULE:
    {
      DClassModule *newDocument = dynamic_cast<DClassModule*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDClassModule(parent, newDocument, false));
      }
    }
    break;

    case DOC_KIND_GLOBAL_THREAD:
    {
      DGlobalThread *newDocument = dynamic_cast<DGlobalThread*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDGlobalThread(parent, newDocument, false));
      }
    }
    break;

    case DOC_KIND_GLOBAL_CONTAINER:
    {
      DGlobalContainer *newDocument = dynamic_cast<DGlobalContainer*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDGlobalContainer(parent, newDocument, false));
      }
    }
    break;

    case DOC_KIND_TRIGGER:
    {
      DTrigger *newDocument = dynamic_cast<DTrigger*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDTrigger(parent, newDocument, false));
      }
    }
    break;

    case DOC_KIND_TIME_MANAGER:
    {
      DTimeManager *newDocument = dynamic_cast<DTimeManager*>(document);
      if ( newDocument )
      {       
        parent->insertChild(foundItemPosition, appendDTimeManager(parent, newDocument, false));
      }
    }
    break;

    default:;
  }
}

QMenu * Configurator::menu() const
{
  return _menu;
}

QToolBar * Configurator::toolBar() const
{
  return _toolBar;
}

QWidget * Configurator::workingArea() const
{
  return NULL;
}

QWidget * Configurator::dockWindowContents() const
{  
  return mainWidget;
}

BaseDocument * Configurator::findDocument(int id)
{
  for ( int i = 0; i < currentObjectList.getObjects()->size(); i++ )
  {
    if ( (currentObjectList.getObjects()->value(i)->getObjectKind() != OBJECT_KIND_SCRIPT) && (currentObjectList.getObjects()->value(i)->getId() == id) )
    {
      return dynamic_cast<BaseDocument*>(currentObjectList.getObjects()->value(i)); 
    }
  }

  return NULL;
}

QTreeWidgetItem * Configurator::appendObjectList(QTreeWidgetItem *currentItem, const ObjectList &objList)
{
  //��������� ������ �������� ������� ������������
  QTreeWidgetItem *item = NULL;
  GJODocumentParser *gjoDocumentParser = new GJODocumentParser(gjoTree);
  for ( int i = 0; i < objList.getObjects()->size(); i++ )
  {
    switch ( objList.getObjects()->value(i)->getObjectKind() )
    {
      case OBJECT_KIND_DOCUMENT:
      case OBJECT_KIND_DICTIONARY:
      case OBJECT_KIND_REPORT:
      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      case OBJECT_KIND_REGISTER:
      case OBJECT_KIND_ENUMERATION:
      case OBJECT_KIND_CONSTANT:
      case OBJECT_KIND_DFORM:
      case OBJECT_KIND_DTABLE:
      case OBJECT_KIND_DQUERY:
      case OBJECT_KIND_DCLASS_MODULE:
      case OBJECT_KIND_DGLOBAL_THREAD:
      case OBJECT_KIND_DGLOBAL_CONTAINER:
      case OBJECT_KIND_DTRIGGER:
      case OBJECT_KIND_DTIME_MANAGER:
      {
        BaseDocument *document = dynamic_cast<BaseDocument*>(objList.getObjects()->value(i));

        //���������� ��������� � ������ ���
        gjoDocumentParser->setDocument(document);

        switch( document->getKind() )
        {
          case DOC_KIND_DOCUMENT:
          {
            Document *newDocument = dynamic_cast<Document*> (document);
            if ( newDocument )
            {          
              item = currentItem->child(2);             
              appendDocument(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Documents")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_DICTIONARY:
          {
            Dictionary *newDocument = dynamic_cast<Dictionary*>(document);
            if ( newDocument )
            {
              item = currentItem->child(3);             
              appendDocument(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Dictionaries")).arg(item->childCount()));
            }      
          }
          break;

          case DOC_KIND_REPORT:
          {
            Report *newDocument = dynamic_cast<Report*>(document);
            if ( newDocument )
            {
              item = currentItem->child(4);            
              appendDocument(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Reports")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_DOCUMENTS_JOURNAL:
          {
            DocumentsJournal *newDocument = dynamic_cast<DocumentsJournal*>(document);
            if ( newDocument )
            {
              item = currentItem->child(5);            
              appendDocumentsJournal(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Documents journals")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_REGISTER:
          {
            Register *newDocument = dynamic_cast<Register*>(document);
            if ( newDocument )
            {
              item = currentItem->child(6);            
              appendRegister(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Registers")).arg(item->childCount()));
            }
          }
          break;          

          case DOC_KIND_ENUMERATION:
          {
            Enumeration *newDocument = dynamic_cast<Enumeration*>(document);
            if ( newDocument )
            {
              item = currentItem->child(7);              
              appendEnumeration(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Enumerations")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_CONSTANT:
          {
            Constant *newDocument = dynamic_cast<Constant*>(document);
            if ( newDocument )
            {
              item = currentItem->child(8);             
              appendConstant(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Constants")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_FORM:
          {
            DForm *newDocument = dynamic_cast<DForm*>(document);
            if ( newDocument )
            {
              item = currentItem->child(9);           
              appendDForm(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("DocumentForms")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_TABLE:
          {
            DTable *newDocument = dynamic_cast<DTable*>(document);
            if ( newDocument )
            {
              item = currentItem->child(10);           
              appendDTable(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("DocumentTables")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_QUERY:
          {
            DQuery *newDocument = dynamic_cast<DQuery*>(document);
            if ( newDocument )
            {
              item = currentItem->child(11);           
              appendDQuery(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("DocumentQueries")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_CLASS_MODULE:
          {
            DClassModule *newDocument = dynamic_cast<DClassModule*>(document);
            if ( newDocument )
            {
              item = currentItem->child(12);           
              appendDClassModule(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("ClassModules")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_GLOBAL_THREAD:
          {
            DGlobalThread *newDocument = dynamic_cast<DGlobalThread*>(document);
            if ( newDocument )
            {
              item = currentItem->child(13);           
              appendDGlobalThread(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("GlobalThreads")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_GLOBAL_CONTAINER:
          {
            DGlobalContainer *newDocument = dynamic_cast<DGlobalContainer*>(document);
            if ( newDocument )
            {
              item = currentItem->child(14);           
              appendDGlobalContainer(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("GlobalContainers")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_TRIGGER:
          {
            DTrigger *newDocument = dynamic_cast<DTrigger*>(document);
            if ( newDocument )
            {
              item = currentItem->child(15);           
              appendDTrigger(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("Triggers")).arg(item->childCount()));
            }
          }
          break;

          case DOC_KIND_TIME_MANAGER:
          {
            DTimeManager *newDocument = dynamic_cast<DTimeManager*>(document);
            if ( newDocument )
            {
              item = currentItem->child(16);           
              appendDTimeManager(item, newDocument);
              item->setText(0, QString("%1 (%2)").arg(tr("TimeManagers")).arg(item->childCount()));
            }
          }
          break;

          default:;
        }
      }
      break;

      case OBJECT_KIND_SCRIPT:
      {
        Script *curScript = dynamic_cast<Script*>(objList.getObjects()->value(i));

        gjoDocumentParser->appendGlobalModule(curScript);

        //���������� �������� ������������ ������������� ����������� �������
        item = currentItem->child(1);
        appendGlobalScript(item, curScript);
        item->setText(0, QString("%1 (%2)").arg(tr("GlobalModules")).arg(item->childCount()));
      }
      break;

      default:;
    }
  }

  parseGJOTreeData();

  return currentItem;
}

QTreeWidgetItem * Configurator::appendConfiguration(const ObjectList &objects, const ConfigurationHeader &header)
{
  //�������� ��������� ������������� �������� ������������
  QFont serifFont("Times", 10, QFont::Bold);

  QTreeWidgetItem *root = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("%1 - %2").arg(tr("Configuration")).arg(header.getName())));
  root->setFont(0, serifFont);
  root->setExpanded(true);

  QVariant v;
  ItemData itemData;
  itemData.id = -1;
  itemData.kind = ITEM_KIND_CONFIGURATION;
  v.setValue(itemData);
  root->setData(0, Qt::UserRole, v);
 
  QTreeWidgetItem *headerItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  headerItem->setText(0, tr("Header"));
  itemData.id = header.getId();
  itemData.kind = ITEM_KIND_HEADER; 
  v.setValue(itemData);
  headerItem->setData(0, Qt::UserRole, v);

  //appendConfigurationHeader(headerItem, header);

  QTreeWidgetItem *scriptsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  scriptsItem->setText(0, QString("%1 (%2)").arg(tr("GlobalModules")).arg(scriptsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_GLOBAL_SCRIPTS;
  v.setValue(itemData);
  scriptsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *documentsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  documentsItem->setText(0, QString("%1 (%2)").arg(tr("Documents")).arg(documentsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DOCUMENTS;
  v.setValue(itemData);
  documentsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dictionariesItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dictionariesItem->setText(0, QString("%1 (%2)").arg(tr("Dictionaries")).arg(dictionariesItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DICTIONARIES;
  v.setValue(itemData);
  dictionariesItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *reportsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  reportsItem->setText(0, QString("%1 (%2)").arg(tr("Reports")).arg(reportsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_REPORTS;
  v.setValue(itemData);
  reportsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *documentsJournalsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  documentsJournalsItem->setText(0, QString("%1 (%2)").arg(tr("Documents journals")).arg(documentsJournalsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DOCUMENTS_JOURNALS;
  v.setValue(itemData);
  documentsJournalsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *registersItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  registersItem->setText(0, QString("%1 (%2)").arg(tr("Registers")).arg(registersItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_REGISTERS;
  v.setValue(itemData);
  registersItem->setData(0, Qt::UserRole, v);  

  QTreeWidgetItem *enumerationsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  enumerationsItem->setText(0, QString("%1 (%2)").arg(tr("Enumerations")).arg(enumerationsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_ENUMERATIONS;
  v.setValue(itemData);
  enumerationsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *constantsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  constantsItem->setText(0, QString("%1 (%2)").arg(tr("Constants")).arg(constantsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_CONSTANTS;
  v.setValue(itemData);
  constantsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dformsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dformsItem->setText(0, QString("%1 (%2)").arg(tr("DocumentForms")).arg(dformsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DFORMS;
  v.setValue(itemData);
  dformsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dtablesItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dtablesItem->setText(0, QString("%1 (%2)").arg(tr("DocumentTables")).arg(dtablesItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DTABLES;
  v.setValue(itemData);
  dtablesItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dqueriesItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dqueriesItem->setText(0, QString("%1 (%2)").arg(tr("DocumentQueries")).arg(dqueriesItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DQUERIES;
  v.setValue(itemData);
  dqueriesItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dclassmodulesItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dclassmodulesItem->setText(0, QString("%1 (%2)").arg(tr("ClassModules")).arg(dclassmodulesItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DCLASSMODULES;
  v.setValue(itemData);
  dclassmodulesItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dglobalthreadsItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dglobalthreadsItem->setText(0, QString("%1 (%2)").arg(tr("GlobalThreads")).arg(dglobalthreadsItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DGLOBAL_THREADS;
  v.setValue(itemData);
  dglobalthreadsItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dglobalcontainersItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dglobalcontainersItem->setText(0, QString("%1 (%2)").arg(tr("GlobalContainers")).arg(dglobalcontainersItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DGLOBAL_CONTAINERS;
  v.setValue(itemData);
  dglobalcontainersItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dtriggersItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dtriggersItem->setText(0, QString("%1 (%2)").arg(tr("Triggers")).arg(dtriggersItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DTRIGGERS;
  v.setValue(itemData);
  dtriggersItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *dtimemanagersItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList());
  dtimemanagersItem->setText(0, QString("%1 (%2)").arg(tr("TimeManagers")).arg(dtimemanagersItem->childCount()));
  itemData.id = -1;
  itemData.gjoTreeId = -1;
  itemData.kind = ITEM_KIND_DTIME_MANAGERS;
  v.setValue(itemData);
  dtimemanagersItem->setData(0, Qt::UserRole, v);

  //������������ ������ ������������� �������� ������������
  root->addChild(headerItem);                       //childIndex = 0
  root->addChild(scriptsItem);                      //childIndex = 1
  root->addChild(documentsItem);                    //childIndex = 2
  root->addChild(dictionariesItem);                 //childIndex = 3
  root->addChild(reportsItem);                      //childIndex = 4
  root->addChild(documentsJournalsItem);            //childIndex = 5
  root->addChild(registersItem);                    //childIndex = 6
  root->addChild(enumerationsItem);                 //childIndex = 7
  root->addChild(constantsItem);                    //childIndex = 8
  root->addChild(dformsItem);                       //childIndex = 9
  root->addChild(dtablesItem);                      //childIndex = 10
  root->addChild(dqueriesItem);                     //childIndex = 11
  root->addChild(dclassmodulesItem);                //childIndex = 12
  root->addChild(dglobalthreadsItem);               //childIndex = 13
  root->addChild(dglobalcontainersItem);            //childIndex = 14
  root->addChild(dtriggersItem);                    //childIndex = 15
  root->addChild(dtimemanagersItem);                //childIndex = 16

  appendObjectList(root, objects);

  return root;
}

QTreeWidgetItem * Configurator::appendDocument(QTreeWidgetItem *currentItem, Document *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;
  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DOCUMENT;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);
  //newDocumentItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(tr("Script")));

  itemData.id = -1;
  itemData.gjoTreeId = document->getScript()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newDocumentItem->addChild(newScriptItem);

  QTreeWidgetItem *tablePartsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(tr("Table parts")));
  itemData.id = -1;
  itemData.kind = ITEM_KIND_TABLE_PARTS;
  v.setValue(itemData);
  tablePartsItem->setData(0, Qt::UserRole, v);

  //���������� ��������� ������ ���������
  QMap<int, QTreeWidgetItem*> tablePartList;
  for (int j = 0; j < document->getTableParts()->size(); j++)
  {    
    TablePart *curTablePart = document->getTableParts()->value(j);   
    tablePartList.insert(curTablePart->getId(),
      appendTablePart(tablePartsItem, curTablePart));         
  }  

  //���������� ����������
  QTreeWidgetItem *accessoriesItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  itemData.id = -1;
  itemData.kind = ITEM_KIND_ACCESSORIES;
  v.setValue(itemData);
  accessoriesItem->setData(0, Qt::UserRole, v);

  //���������� ���� ����������
  newDocumentItem->addChild(accessoriesItem);
  accessoriesItem->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(accessoriesItem->childCount()));

  for (int j = 0; j < document->getAccessories()->size(); j++)
  {
    Accessory *curAccessory = document->getAccessories()->value(j);
    if ( !(curAccessory->getTablePartId() == -1) )
    {
      QTreeWidgetItem *curTP = tablePartList.value(curAccessory->getTablePartId());
      if ( curTP )
      {        
        appendAccessory(curTP->child(0), curAccessory);
        curTP->child(0)->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(curTP->child(0)->childCount()));
      }
    }
    else
    {      
      appendAccessory(accessoriesItem, curAccessory);
      accessoriesItem->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(accessoriesItem->childCount()));
    }                    
  }

  //���������� ���� ��������� ������
  newDocumentItem->addChild(tablePartsItem);
  tablePartsItem->setText(0, QString("%1 (%2)").arg(tr("Table parts")).arg(tablePartsItem->childCount()));

  QTreeWidgetItem *formsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(tr("Forms")));
  itemData.id = -1;
  itemData.kind = ITEM_KIND_FORMS;
  v.setValue(itemData);
  formsItem->setData(0, Qt::UserRole, v);
  
  //���������� ����
  for (int j = 0; j < document->getForms()->size(); j++)
  {
    appendForm(formsItem, document->getForms()->value(j));
  }  
  
  newDocumentItem->addChild(formsItem);
  formsItem->setText(0, QString("%1 (%2)").arg(tr("Forms")).arg(formsItem->childCount()));
  
  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendRegister(QTreeWidgetItem *currentItem, Register *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newRegisterItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId).toLocal8Bit().data())); 

  QVariant v;
  ItemData itemData;
  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_REGISTER;
  v.setValue(itemData);
  newRegisterItem->setData(0, Qt::UserRole, v);
  //newRegisterItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  //���������� ���������
  QTreeWidgetItem *dimensionsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  itemData.id = -1;
  itemData.kind = ITEM_KIND_DIMENSIONS;
  v.setValue(itemData);
  dimensionsItem->setData(0, Qt::UserRole, v);
  newRegisterItem->addChild(dimensionsItem);  

  int index = 1;
  QMap<int, BaseDocument*>::iterator i = document->getDimensions()->begin();
  while (i != document->getDimensions()->end())
  {
    appendLink(dimensionsItem, index);    

    index++;
    i++;
  }
  dimensionsItem->setText(0, QString("%1 (%2)").arg(tr("Dimensions")).arg(dimensionsItem->childCount()));

  //���������� ������
  QTreeWidgetItem *factsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  itemData.id = -1;
  itemData.kind = ITEM_KIND_FACTS;
  v.setValue(itemData);
  factsItem->setData(0, Qt::UserRole, v);
  newRegisterItem->addChild(factsItem);  

  QTreeWidgetItem *newFactItem = NULL;
  ItemData factItemData;  
  factItemData.kind = ITEM_KIND_FACT;
  QVariant d;
  for (int j = 0; j < document->getFacts()->size(); j++)
  {   
    newFactItem = appendAccessory(factsItem, document->getFacts()->value(j));
    Accessory *curFact = document->getFacts()->value(j);
    factItemData.id = curFact->getId();
    factItemData.gjoTreeId = curFact->getGJOTreeId();
    d.setValue(factItemData);
    newFactItem->setData(0, Qt::UserRole, d);
  }
  factsItem->setText(0, QString("%1 (%2)").arg(tr("Facts")).arg(factsItem->childCount()));
  
  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newRegisterItem);
  }

  return newRegisterItem;
}

QTreeWidgetItem * Configurator::appendDocumentsJournal(QTreeWidgetItem *currentItem, DocumentsJournal *document, bool appendToItemFlag)
{  
  QTreeWidgetItem *newDocumentsJournalItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    document->getTranslates()->value(languageId).toLocal8Bit().data()));

  QVariant v;
  ItemData itemData;
  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DOCUMENTS_JOURNAL;
  v.setValue(itemData);
  newDocumentsJournalItem->setData(0, Qt::UserRole, v);
  //newDocumentsJournalItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);
  
  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentsJournalItem);
  } 

  return newDocumentsJournalItem;
}

QTreeWidgetItem * Configurator::appendEnumeration(QTreeWidgetItem *currentItem, Enumeration *document, bool appendToItemFlag)
{  
  QTreeWidgetItem *newEnumerationItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    document->getTranslates()->value(languageId).toLocal8Bit().data()));

  QVariant v;
  ItemData itemData;
  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_ENUMERATION;
  v.setValue(itemData);
  newEnumerationItem->setData(0, Qt::UserRole, v);
  //newEnumerationItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newEnumerationItem);
  } 

  return newEnumerationItem;
}

QTreeWidgetItem * Configurator::appendConstant(QTreeWidgetItem *currentItem, Constant *document, bool appendToItemFlag)
{  
  QTreeWidgetItem *newConstantItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    document->getTranslates()->value(languageId).toLocal8Bit().data()));

  QVariant v;
  ItemData itemData;
  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_CONSTANT;
  v.setValue(itemData);
  newConstantItem->setData(0, Qt::UserRole, v);
  //newConstantItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);
  
  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newConstantItem);
  }

  return newConstantItem;
}

QTreeWidgetItem * Configurator::appendDForm(QTreeWidgetItem *currentItem, DForm *document, bool appendToItemFlag)
{  
  QTreeWidgetItem *newDFormItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DFORM;
  v.setValue(itemData);
  newDFormItem->setData(0, Qt::UserRole, v);
  //newDFormItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  /*QTreeWidgetItem *formsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(tr("Forms")));
  itemData.id = -1;
  itemData.kind = ITEM_KIND_FORMS;
  v.setValue(itemData);
  formsItem->setData(0, Qt::UserRole, v);*/

  //���������� ����
  /*for ( int j = 0; j < document->getForms()->size(); j++ )
  {
    appendForm(formsItem, document->getForms()->value(j));    
  }*/

  //newDFormItem->addChild(formsItem);
  //formsItem->setText(0, QString("%1 (%2)").arg(tr("Forms")).arg(formsItem->childCount()));

  appendForm(newDFormItem, document->getForms()->value(0));

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDFormItem);
  }

  return newDFormItem;
}

QTreeWidgetItem * Configurator::appendDTable(QTreeWidgetItem *currentItem, DTable *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DTABLE;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);

  //���������� ����������
  QTreeWidgetItem *accessoriesItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  itemData.id = -1;
  itemData.kind = ITEM_KIND_ACCESSORIES;
  v.setValue(itemData);
  accessoriesItem->setData(0, Qt::UserRole, v);

  //���������� ���� ����������
  newDocumentItem->addChild(accessoriesItem);
  accessoriesItem->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(accessoriesItem->childCount()));

  for ( int j = 0; j < document->getAccessories()->size(); j++ )
  {
    Accessory *curAccessory = document->getAccessories()->value(j);
    appendAccessory(accessoriesItem, curAccessory);
    accessoriesItem->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(accessoriesItem->childCount()));            
  }

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendDQuery(QTreeWidgetItem *currentItem, DQuery *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DQUERY;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(tr("Script")));

  itemData.id = document->getScript()->getId();
  itemData.gjoTreeId = document->getScript()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newDocumentItem->addChild(newScriptItem);  

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendDClassModule(QTreeWidgetItem *currentItem, Documents::DClassModule *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DCLASSMODULE;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(tr("Script")));

  itemData.id = document->getScript()->getId();
  itemData.gjoTreeId = document->getScript()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newDocumentItem->addChild(newScriptItem);

  //���������� ��������
  /*QTreeWidgetItem *scriptsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  itemData.id = -1;
  itemData.kind = ITEM_KIND_SCRIPTS;
  v.setValue(itemData);
  scriptsItem->setData(0, Qt::UserRole, v);

  //���������� ���� ����������
  newDocumentItem->addChild(scriptsItem);
  scriptsItem->setText(0, QString("%1 (%2)").arg(tr("Scripts")).arg(scriptsItem->childCount()));

  for ( int j = 0; j < document->getScripts()->size(); j++ )
  {
    Script *curScript = document->getScripts()->value(j);
    appendScript(scriptsItem, curScript);
    scriptsItem->setText(0, QString("%1 (%2)").arg(tr("Scripts")).arg(scriptsItem->childCount()));            
  }*/

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendDGlobalThread(QTreeWidgetItem *currentItem, Documents::DGlobalThread *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DGLOBAL_THREAD;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(tr("Script")));

  itemData.id = document->getScript()->getId();
  itemData.gjoTreeId = document->getScript()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newDocumentItem->addChild(newScriptItem);

  //���������� ��������
  /*QTreeWidgetItem *scriptsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  itemData.id = -1;
  itemData.kind = ITEM_KIND_SCRIPTS;
  v.setValue(itemData);
  scriptsItem->setData(0, Qt::UserRole, v);

  //���������� ���� ����������
  newDocumentItem->addChild(scriptsItem);
  scriptsItem->setText(0, QString("%1 (%2)").arg(tr("Scripts")).arg(scriptsItem->childCount()));

  for ( int j = 0; j < document->getScripts()->size(); j++ )
  {
    Script *curScript = document->getScripts()->value(j);
    appendScript(scriptsItem, curScript);
    scriptsItem->setText(0, QString("%1 (%2)").arg(tr("Scripts")).arg(scriptsItem->childCount()));            
  }*/

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendDGlobalContainer(QTreeWidgetItem *currentItem, Documents::DGlobalContainer *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DGLOBAL_CONTAINER;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(tr("Script")));

  itemData.id = document->getScript()->getId();
  itemData.gjoTreeId = document->getScript()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newDocumentItem->addChild(newScriptItem);

  //���������� ��������
  /*QTreeWidgetItem *scriptsItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList());
  itemData.id = -1;
  itemData.kind = ITEM_KIND_SCRIPTS;
  v.setValue(itemData);
  scriptsItem->setData(0, Qt::UserRole, v);

  newDocumentItem->addChild(scriptsItem);
  scriptsItem->setText(0, QString("%1 (%2)").arg(tr("Scripts")).arg(scriptsItem->childCount()));

  for ( int j = 0; j < document->getScripts()->size(); j++ )
  {
    Script *curScript = document->getScripts()->value(j);
    appendScript(scriptsItem, curScript);
    scriptsItem->setText(0, QString("%1 (%2)").arg(tr("Scripts")).arg(scriptsItem->childCount()));            
  }*/

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendDTrigger(QTreeWidgetItem *currentItem, Documents::DTrigger *document, bool appendToItemFlag)
{
  std::cout << document->getTranslates()->value(languageId).toLocal8Bit().data() << std::endl;
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DTRIGGER;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(tr("Script")));

  itemData.id = document->getScript()->getId();
  itemData.gjoTreeId = document->getScript()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newDocumentItem->addChild(newScriptItem);

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendDTimeManager(QTreeWidgetItem *currentItem, Documents::DTimeManager *document, bool appendToItemFlag)
{
  QTreeWidgetItem *newDocumentItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(document->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = document->getId();
  itemData.gjoTreeId = document->getGJOTreeId();
  itemData.kind = ITEM_KIND_DTIME_MANAGER;
  v.setValue(itemData);
  newDocumentItem->setData(0, Qt::UserRole, v);

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(tr("Script")));

  itemData.id = document->getScript()->getId();
  itemData.gjoTreeId = document->getScript()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newDocumentItem->addChild(newScriptItem);

  if ( appendToItemFlag && (currentItem != NULL) )
  {
    currentItem->addChild(newDocumentItem);
  }

  return newDocumentItem;
}

QTreeWidgetItem * Configurator::appendAccessory(QTreeWidgetItem *currentItem, Accessory *accessory)
{
  QTreeWidgetItem *newItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    (accessory->getTranslates())->value(languageId).toLocal8Bit().data()));

  QVariant v;
  ItemData itemData;
  itemData.id = accessory->getId();
  itemData.gjoTreeId = accessory->getGJOTreeId();
  itemData.kind = ITEM_KIND_ACCESSORY;
  v.setValue(itemData);
  newItem->setData(0, Qt::UserRole, v);
  //newItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  currentItem->addChild(newItem);
  return newItem;
}

QTreeWidgetItem * Configurator::appendTablePart(QTreeWidgetItem *currentItem, TablePart *tablePart)
{
  QTreeWidgetItem* newItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    (tablePart->getTranslates())->value(languageId).toLocal8Bit().data()));

  QVariant v;
  ItemData itemData;
  itemData.id = tablePart->getId();
  itemData.gjoTreeId = tablePart->getGJOTreeId();
  itemData.kind = ITEM_KIND_TABLE_PART;
  v.setValue(itemData);
  newItem->setData(0, Qt::UserRole, v);
  //newItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  QTreeWidgetItem *accessories = new QTreeWidgetItem((QTreeWidget*)0,  QStringList());
  itemData.id = -1;  
  itemData.kind = ITEM_KIND_ACCESSORIES;
  v.setValue(itemData);
  accessories->setData(0, Qt::UserRole, v);
  accessories->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(accessories->childCount()));
  newItem->addChild(accessories);

  currentItem->addChild(newItem);
  return newItem;
}

QTreeWidgetItem * Configurator::appendForm(QTreeWidgetItem *currentItem, Form *form)
{
  QTreeWidgetItem *newFormItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    (form->getTranslates())->value(languageId).toLocal8Bit().data()));

  QVariant v;
  ItemData itemData;

  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(tr("Script")));
  itemData.id = -1;
  itemData.gjoTreeId = form->getModule()->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  newFormItem->addChild(newScriptItem);

  itemData.id = form->getId();
  itemData.gjoTreeId = form->getGJOTreeId();
  itemData.kind = ITEM_KIND_FORM;
  v.setValue(itemData);
  newFormItem->setData(0, Qt::UserRole, v);
  //newFormItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  currentItem->addChild(newFormItem);
  return newFormItem;
}

QTreeWidgetItem * Configurator::appendLink(QTreeWidgetItem *currentItem, int index)
{
  //���������� �������� ������������ �������������
  QTreeWidgetItem *newLinkItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
    QString("%1%2").arg(tr("Dimension")).arg(index)));

  //newLinkItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  //��������� ���������� ����
  ItemData newLinkData;
  newLinkData.id = index;
  newLinkData.gjoTreeId = -1;
  newLinkData.kind = ITEM_KIND_DIMENSION;//��� �������, ��� ������ ����������� ������ ��� ��������
  QVariant d;
  d.setValue(newLinkData);
  newLinkItem->setData(0, Qt::UserRole, d);

  if ( currentItem )
  {
    currentItem->addChild(newLinkItem);
  }

  return newLinkItem;
}

QTreeWidgetItem * Configurator::appendScript(QTreeWidgetItem *currentItem, Script *script)
{
  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
      script->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = script->getId();
  itemData.gjoTreeId = script->getGJOTreeId();
  itemData.kind = ITEM_KIND_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  //newScriptItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  currentItem->addChild(newScriptItem);
  return newScriptItem;
}

QTreeWidgetItem * Configurator::appendGlobalScript(QTreeWidgetItem *currentItem, Script *script)
{
  QTreeWidgetItem *newScriptItem = new QTreeWidgetItem((QTreeWidget*)0,
    QStringList(
      script->getTranslates()->value(languageId)));

  QVariant v;
  ItemData itemData;

  itemData.id = script->getId();
  itemData.gjoTreeId = script->getGJOTreeId();
  itemData.kind = ITEM_KIND_GLOBAL_SCRIPT;
  v.setValue(itemData);
  newScriptItem->setData(0, Qt::UserRole, v);
  //newScriptItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

  currentItem->addChild(newScriptItem);
  return newScriptItem;
}

QTreeWidgetItem * Configurator::appendConnectionInfo(QTreeWidgetItem *currentItem, const ConnectionInfo &cInfo)
{
  QStringList itemStringData;
  ItemData itemData;
  QVariant d;

  itemStringData.append(tr("Host"));
  itemStringData.append(QString("%1").arg(cInfo.host));
  QTreeWidgetItem *newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Port"));
  itemStringData.append(QString("%1").arg(cInfo.port));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Database name"));
  itemStringData.append(QString("%1").arg(cInfo.dbName));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("User"));
  itemStringData.append(QString("%1").arg(cInfo.userName));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Password"));
  itemStringData.append(QString("%1").arg(cInfo.password));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  return newItem;
}

QTreeWidgetItem * Configurator::appendConfigurationHeader(QTreeWidgetItem *currentItem, const ConfigurationHeader &header)
{
  QStringList itemStringData;
  ItemData itemData;
  QVariant d; 

  itemStringData.clear();
  itemStringData.append(tr("Id"));
  itemStringData.append(QString("%1").arg(header.getId()));
  QTreeWidgetItem *newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Name"));
  itemStringData.append(QString("%1").arg(header.getName()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Author"));
  itemStringData.append(QString("%1").arg(header.getAuthor()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Version"));
  itemStringData.append(QString("%1").arg(header.getVersion()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Creation date and time"));
  itemStringData.append(QString("%1").arg(header.getCreationDateTime().toString("dd.MM.yyyy hh:mm:ss")));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("Last modification date and time"));
  itemStringData.append(QString("%1").arg(header.getLastModificationDateTime().toString("dd.MM.yyyy hh:mm:ss")));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);
  
  itemStringData.clear();
  itemStringData.append(tr("Last modification user name"));
  itemStringData.append(QString("%1").arg(header.getLastModificationUserName()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_NO_MEANING;
  itemData.id = -1;
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem);

  itemStringData.clear();
  itemStringData.append(tr("User database id"));
  itemStringData.append(QString("%1").arg(header.getUserDatabaseId()));
  newItem = new QTreeWidgetItem((QTreeWidget*)0, itemStringData);
  itemData.kind = ITEM_KIND_USER_DATABASE_ID;
  itemData.id = header.getUserDatabaseId();
  d.setValue(itemData);
  newItem->setData(0, Qt::UserRole, d);
  currentItem->addChild(newItem); 

  return currentItem;
}

void Configurator::configurationManager()
{
  configurationListForm->getConfigurationListCmd();
  configurationListForm->getUserDatabaseListCmd();
  configurationListForm->exec();
}

void Configurator::openConfiguration(const ConfigurationHeader &header)
{
  //������� ������������� ������� ������������
  clearConfiguration();

  //�������� ������ ����� ������������  
  currentObjectList = *configurationListForm->getCurrentObjectList();

  currentHeader = header;

  QTreeWidgetItem *configurationItem = appendConfiguration(currentObjectList, currentHeader);
  treeWidget->insertTopLevelItem(0, configurationItem);
  treeWidget->topLevelItem(0)->setExpanded(true);

  actionSaveConfiguration->setEnabled(true);
  actionReleaseConfiguration->setEnabled(true);
  actionCloseConfiguration->setEnabled(true);
  actionParameters->setEnabled(true);

  emit opened();
}

void Configurator::saveConfiguration()
{
  //���������� ������ � ������ ��� ��� ���������� �������� � xml-�������������
  refreshGJOTreeData();  

  //���������� ���������� �� ����������
  QList<Script*> scriptList;
  for ( int i = 0; i < currentObjectList.getObjects()->size(); i++ )
  {
    switch ( currentObjectList.getObjects()->value(i)->getObjectKind() )
    {
      case OBJECT_KIND_DOCUMENT:
      case OBJECT_KIND_DICTIONARY:
      case OBJECT_KIND_REPORT:
      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      case OBJECT_KIND_REGISTER:
      case OBJECT_KIND_ENUMERATION:
      case OBJECT_KIND_CONSTANT:
      case OBJECT_KIND_DFORM:
      case OBJECT_KIND_DTABLE:
      case OBJECT_KIND_DQUERY:
      case OBJECT_KIND_DCLASS_MODULE:
      case OBJECT_KIND_DGLOBAL_THREAD:
      case OBJECT_KIND_DGLOBAL_CONTAINER:
      case OBJECT_KIND_DTRIGGER:
      case OBJECT_KIND_DTIME_MANAGER:
      {
        BaseDocument *curDocument = dynamic_cast<BaseDocument*>(currentObjectList.getObjects()->value(i));       

        //������ ������� ���������        
        /*std::cout << QString("void Configurator::saveConfiguration()\ndocument->getGJOTreeId()=\n"
          "").arg(curDocument->getGJOTreeId()).toLocal8Bit().data() << std::endl;*/

        GJO::Script *gjoScript = gjoTree->scriptByParentId(curDocument->getGJOTreeId());
        if ( gjoScript )
        {
          std::cout << "Error: Script not found into gjo tree !!!" << std::endl;
          curDocument->getScript()->setBody(gjoTree->scriptByParentId(curDocument->getGJOTreeId())->toXML());
        }        

        //������ ������� � ������������� ��� ���� ���������
        switch ( curDocument->getKind() )
        {
          case DOC_KIND_DOCUMENT:
          case DOC_KIND_DICTIONARY:
          case DOC_KIND_REPORT:
          {
            Document *document = dynamic_cast<Document*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {            
                (*i)->getBody()->setBody(gjoTree->xmlView((*i)->getGJOTreeId()));            
                (*i)->getModule()->setBody(gjoTree->scriptByParentId((*i)->getGJOTreeId())->toXML());
              }             
            }
          }
          break;

          case DOC_KIND_DOCUMENTS_JOURNAL:
          {
            DocumentsJournal *document = dynamic_cast<DocumentsJournal*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {            
                (*i)->getBody()->setBody(gjoTree->xmlView((*i)->getGJOTreeId()));            
                (*i)->getModule()->setBody(gjoTree->scriptByParentId((*i)->getGJOTreeId())->toXML());          
              }
            }
          }
          break;

          case DOC_KIND_FORM:
          {
            DForm *document = dynamic_cast<DForm*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {            
                (*i)->getBody()->setBody(gjoTree->xmlView((*i)->getGJOTreeId()));            
                (*i)->getModule()->setBody(gjoTree->scriptByParentId((*i)->getGJOTreeId())->toXML()); 
              }
            }
          }
          break;

          /*case DOC_KIND_TABLE:
          {
          }
          break;

          case DOC_KIND_QUERY:
          {
          }
          break;

          case DOC_KIND_CLASS_MODULE:
          {
          }
          break;

          case DOC_KIND_GLOBAL_THREAD:
          {
          }
          break;

          case DOC_KIND_GLOBAL_CONTAINER:
          {
          }
          break;

          case DOC_KIND_TRIGGER:
          {
          }
          break;

          case DOC_KIND_TIME_MANAGER:
          {
          }
          break;*/

          default:;
        }
      }
      break;

      case OBJECT_KIND_SCRIPT:
      {
        Script *curScript = dynamic_cast<Script*>(currentObjectList.getObjects()->value(i));
        curScript->setBody(gjoTree->scriptById(curScript->getGJOTreeId())->toXML());
        scriptList.append(curScript);
      }
      break;

      default:;
    }
  }

  //��������� ���� � ������� ��������� �����������
  currentHeader.setLastModificationDateTime(QDateTime::currentDateTime());

  //��������� ������������ ��������� �����������
  currentHeader.setLastModificationUserName(QString());

  QByteArray ba;
  QDataStream out(&ba, QIODevice::WriteOnly);

  out << currentHeader;
  out << currentObjectList;

  IdeServerConnectionModule::instance()->saveConfiguration(ba);

  configurationListForm->getConfigurationListCmd();
}

void Configurator::releaseConfiguration()
{
  configurationListForm->releaseConfiguration();
}

void Configurator::closeConfiguration()
{ 
  actionSaveConfiguration->setEnabled(false);
  actionReleaseConfiguration->setEnabled(false);
  actionCloseConfiguration->setEnabled(false);
  actionParameters->setEnabled(false);
  actionAddObject->setEnabled(false);
  actionRemoveObject->setEnabled(false);

  IdeServerConnectionModule::instance()->closeConfiguration(currentHeader.getId());

  clearConfiguration();

  emit closed();
}

void Configurator::clearConfiguration()
{  
  GJODocumentParser *gjoParser = new GJODocumentParser(gjoTree);
  for ( QList<BaseObject*>::iterator i = currentObjectList.getObjects()->begin(); i != currentObjectList.getObjects()->end(); i++ ) 
  {
    switch ( (*i)->getObjectKind() )
    {
      case OBJECT_KIND_DOCUMENT:
      case OBJECT_KIND_DICTIONARY:
      case OBJECT_KIND_REPORT:
      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      case OBJECT_KIND_REGISTER:
      case OBJECT_KIND_ENUMERATION:
      case OBJECT_KIND_CONSTANT:
      case OBJECT_KIND_DFORM:
      case OBJECT_KIND_DTABLE:
      case OBJECT_KIND_DQUERY:
      case OBJECT_KIND_DTRIGGER:
      case OBJECT_KIND_DTIME_MANAGER:
      {
        /*BaseDocument *curDocument = dynamic_cast<BaseDocument*>(currentObjectList.getObjects()->value(i));
        if ( curDocument )
        {*/
        gjoParser->removeDocument((*i)->getGJOTreeId());
        //}       
      }
      break;
      
      case OBJECT_KIND_SCRIPT:
      {
        //���������� ������� ���������� ������(�������) !!!
        gjoParser->removeGlobalModule((*i)->getGJOTreeId());
      }
      break;

      default:;
    }

    delete *i;
  }
 
  delete gjoParser;

  currentObjectList.getObjects()->clear();

  treeWidget->clear(); 
}

void Configurator::processCmdSaveConfiguration(const qint32 &result, const QByteArray &data)
{
  if ( result )
  {        
    statusBar->showMessage(tr("Saving configuration failed"));
    //QMessageBox::critical(NULL, tr("Error"),  tr("Saving configuration failed"), QMessageBox::Ok);
    return;
  }

  statusBar->showMessage(tr("Configuration saved successful"));
}

void Configurator::setConfigParameters()
{ 
  TConfigSettingsForm *form  = new TConfigSettingsForm(parent());
  form->setWindowModality(Qt::WindowModal);
  form->fillFormsTree(currentObjectList.getObjects());
  form->show();
}

void Configurator::processConnectionStateChange()
{
  if ( IdeServerConnectionModule::instance()->state() == IdeServerConnectionModule::Connected )
  {
     actionConfigurationManager->setEnabled(true);
  }
  else
  {
    if ( IdeServerConnectionModule::instance()->state() == IdeServerConnectionModule::ConnectionClosed )
    {
      actionConfigurationManager->setEnabled(false);

      closeConfiguration();
    }
  }
}

void Configurator::newObject()
{
  if ( !treeWidget->currentItem() )
  {
    return;
  }

  QTreeWidgetItem *currentItem = treeWidget->currentItem();  
  ItemData itemData = currentItem->data(0, Qt::UserRole).value<ItemData>();  

  BaseDocument *newDocument = NULL;
  GJODocumentParser *gjoParser = new GJODocumentParser(gjoTree);

  switch( itemData.kind )
  {
    case ITEM_KIND_DOCUMENTS:
    {      
      newDocument = new Document();
      //��������� �������������� ��������������      
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("Document")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� �������� ������������ ������������� ���������
      appendDocument(currentItem, (Document *)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Documents")).arg(currentItem->childCount()));            
    }
    break;

    case ITEM_KIND_DICTIONARIES:
    {     
      newDocument = new Dictionary();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("Dictionary")).arg(currentItem->childCount() + 1));     

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDocument(currentItem, (Document *)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Dictionaries")).arg(currentItem->childCount()));      
    }
    break;

    case ITEM_KIND_REPORTS:
    {     
      newDocument = new Report();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("Report")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDocument(currentItem, (Document *)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Reports")).arg(currentItem->childCount()));      
    }
    break;

    case ITEM_KIND_DOCUMENTS_JOURNALS:
    {     
      newDocument = new DocumentsJournal();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId, QString("%1%2").arg(tr("DocumentsJournal")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������     
      appendDocumentsJournal(currentItem, (DocumentsJournal *)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Documents journals")).arg(currentItem->childCount()));    
    }
    break;

    case ITEM_KIND_REGISTERS:
    {
      newDocument = new Register();
      //��������� �������������� ��������������      
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("Register")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendRegister(currentItem, (Register *)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Registers")).arg(currentItem->childCount()));     
    }
    break;

    case ITEM_KIND_ENUMERATIONS:
    {     
      newDocument = new Enumeration();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("Enumeration")).arg(currentItem->childCount() + 1));     

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendEnumeration(currentItem, (Enumeration *)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Enumerations")).arg(currentItem->childCount()));    
    }
    break;

    case ITEM_KIND_CONSTANTS:
    {      
      newDocument = new Constant();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("Constant")).arg(currentItem->childCount() + 1));

      Integer *newInteger = new Integer();
      ((Constant*)newDocument)->setType(newInteger);

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendConstant(currentItem, (Constant *)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Constants")).arg(currentItem->childCount()));     
    }
    break;

    case ITEM_KIND_DFORMS:
    {      
      newDocument = new DForm();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("DocumentForm")).arg(currentItem->childCount() + 1));

      Form *newForm = new Form();
      newForm->getTranslates()->insert(languageId,
        QString("%1").arg(tr("Form")));

      ((DForm*)newDocument)->getForms()->append(newForm);

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDForm(currentItem, (DForm*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("DocumentForms")).arg(currentItem->childCount()));    
    }
    break;

    case ITEM_KIND_DTABLES:
    {
      newDocument = new DTable();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("DocumentTable")).arg(currentItem->childCount() + 1));
     
      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDTable(currentItem, (DTable*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("DocumentTables")).arg(currentItem->childCount()));  
    }
    break;

    case ITEM_KIND_DQUERIES:
    {
      newDocument = new DQuery();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("DocumentQuery")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDQuery(currentItem, (DQuery*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("DocumentQueries")).arg(currentItem->childCount()));
    }
    break;

    case ITEM_KIND_DCLASSMODULES:
    {
      newDocument = new DClassModule();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("ClassModule")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDClassModule(currentItem, (DClassModule*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("ClassModules")).arg(currentItem->childCount()));
    }
    break;

    case ITEM_KIND_DGLOBAL_THREADS:
    {
      newDocument = new DGlobalThread();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("GlobalThread")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDGlobalThread(currentItem, (DGlobalThread*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("GlobalThreads")).arg(currentItem->childCount()));
    }
    break;

    case ITEM_KIND_DGLOBAL_CONTAINERS:
    {
      newDocument = new DGlobalContainer();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("GlobalContainer")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDGlobalContainer(currentItem, (DGlobalContainer*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("GlobalContainers")).arg(currentItem->childCount()));
    }
    break;

    case ITEM_KIND_DTRIGGERS:
    {
      newDocument = new DTrigger();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("Trigger")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDTrigger(currentItem, (DTrigger*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("Triggers")).arg(currentItem->childCount()));
    }
    break;

    case ITEM_KIND_DTIME_MANAGERS:
    {
      newDocument = new DTimeManager();
      newDocument->setId(currentObjectList.getObjects()->size() + 1);
      newDocument->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("TimeManager")).arg(currentItem->childCount() + 1));

      gjoParser->setDocument(newDocument);
      currentObjectList.getObjects()->append(newDocument);

      //���������� ��������� � ������ ��������
      appendDTimeManager(currentItem, (DTimeManager*)newDocument);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("TimeManagers")).arg(currentItem->childCount()));
    }
    break;

    case ITEM_KIND_ACCESSORIES:
    {
      //����������� �������������� ������ ����������
      QTreeWidgetItem *parent = currentItem->parent();
      //int index = parent->indexOfChild(currentItem);      

      Accessory *newAcc = new Accessory();
      newAcc->setId(currentItem->childCount() + 1);
      newAcc->getTranslates()->insert(languageId, QString("%1%2").arg(tr("Accessory")).arg(newAcc->getId()));

      //����������� ���� �� ���������
      Integer *newType = new Integer();
      newAcc->setType(newType);
 
      ItemData ownerItemData = parent->data(0, Qt::UserRole).value<ItemData>();     
      //������ �������������� ������
      if ( ownerItemData.kind == ITEM_KIND_TABLE_PART ) //�������� ����������� ��������� ����� ���������
      {       
        //��������� �������������� ��������� �����
        newAcc->setTablePartId(ownerItemData.id);
        //��������� �������������� ���������-���������
        ownerItemData = parent->parent()->parent()->data(0, Qt::UserRole).value<ItemData>();              
      }
      else
      {
        if ( ownerItemData.kind == ITEM_KIND_DOCUMENT )
        {
          //�������� ����������� ����� ���������
        }
      }

      //--------------------------------------------------------------------------------------------------------
      BaseDocument *document = findDocument(ownerItemData.id);
      if ( document )
      {
        int ownerId = document->getGJOTreeId();

        switch ( document->getKind() )
        {
          case DOC_KIND_DOCUMENT:
          {
            if ( newAcc->getTablePartId() > 0 )
            {
              //��������� �������������� ��������� ��������� � ������ ���
              ownerId = ((Document*)document)->getTableParts()->value(newAcc->getTablePartId() - 1)->getGJOTreeId();
            }

            ((Document*)document)->getAccessories()->append(newAcc);
          }
        	break;

          case DOC_KIND_TABLE:
          {            
            ((DTable*)document)->getAccessories()->append(newAcc);
          }

          default:;
        }        

        gjoParser->appendProperty(ownerId, newAcc);

        //���������� �������� ������������ ������������� ���������
        QTreeWidgetItem *newAccessoryItem = appendAccessory(currentItem, newAcc);
        //newAccessoryItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);
        currentItem->addChild(newAccessoryItem);
        currentItem->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(currentItem->childCount()));        

        emit documentContentsChanged(document->getGJOTreeId());
      }
      //--------------------------------------------------------------------------------------------------------

      //Document *document = dynamic_cast<Document*> (findDocument(ownerItemData.id));
      //if ( document )
      //{
      //  //Accessory *newAcc = new Accessory();
      //  //newAcc->setId(currentItem->childCount() + 1);
      //  //newAcc->getTranslates()->insert(languageId, QString("%1%2").arg(tr("Accessory")).arg(newAcc->getId()));

      //  ////����������� ���� �� ���������
      //  //Integer *newType = new Integer();
      //  //newAcc->setType(newType);

      //  //���������� ��������� � ������ ���
      //  int ownerId = document->getGJOTreeId();
      //  if ( newAcc->getTablePartId() > 0 )
      //  {
      //    //��������� �������������� ��������� ��������� � ������ ���
      //    ownerId = document->getTableParts()->value(newAcc->getTablePartId() - 1)->getGJOTreeId();
      //  }
      //  
      //  gjoParser->appendProperty(ownerId, newAcc);

      //  //���������� �������� ������������ ������������� ���������
      //  QTreeWidgetItem *newAccessoryItem = appendAccessory(currentItem, newAcc);
      //  //newAccessoryItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);
      //  currentItem->addChild(newAccessoryItem);
      //  currentItem->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(currentItem->childCount()));

      //  //���������� ��������� � ��������
      //  document->getAccessories()->append(newAcc);
      // 
      //  emit documentContentsChanged(document->getGJOTreeId());
      //}
    }
    break;   

    case ITEM_KIND_TABLE_PARTS:
    {
      //����������� �������������� ��������� �����
      QTreeWidgetItem *parent = currentItem->parent();
      //int index = parent->indexOfChild(currentItem);

      ItemData parentItemData = parent->data(0, Qt::UserRole).value<ItemData>();
      TablePart *newTP = new TablePart();      
      newTP->setId(currentItem->childCount() + 1);
      newTP->getTranslates()->insert(languageId, QString("%1%2").arg(tr("TablePart")).arg(newTP->getId()));
      if ( parentItemData.kind == ITEM_KIND_DOCUMENT )
      {
        //���������� ��������� � ��������� �������������� ��������� �����        
        Document *document = dynamic_cast<Document*> (findDocument(parentItemData.id));
        if ( document )
        {
          //���������� �� � ������ ���         
          gjoParser->appendTablePart(document->getGJOTreeId(), newTP);

          //���������� �������� ������������ ������������� ��
          QTreeWidgetItem *newTablePartItem = appendTablePart(currentItem, newTP);
          //newTablePartItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);
          currentItem->addChild(newTablePartItem);
          currentItem->setText(0, QString("%1 (%2)").arg(tr("Table parts")).arg(currentItem->childCount()));

          //���������� �� � ��������
          document->getTableParts()->append(newTP);

          emit documentContentsChanged(document->getGJOTreeId());
        }
      }
    }
    break;    

    case ITEM_KIND_FORMS:
    {
      //����������� �������������� ������ ����������
      QTreeWidgetItem *parent = currentItem->parent();
      //int index = parent->indexOfChild(currentItem);

      Form *newForm = new Form();
      newForm->setId(currentItem->childCount() + 1);
      newForm->getTranslates()->insert(languageId, QString("%1%2").arg(tr("Form")).arg(newForm->getId()));

      ItemData ownerItemData = parent->data(0, Qt::UserRole).value<ItemData>();
      BaseDocument *curDocument = dynamic_cast<BaseDocument*>(findDocument(ownerItemData.id));
      if ( curDocument )
      {
        switch( curDocument->getKind() )
        {
          case DOC_KIND_DOCUMENT:
          {
            Document *document = dynamic_cast<Document*>(curDocument);
            if ( !document ) return;
           
            //���������� ����� � ������ ���        
            gjoParser->appendForm(document->getGJOTreeId(), newForm);

            //���������� �������� ������������ ������������� �����           
            appendForm(currentItem, newForm);
        
            currentItem->setText(0, QString("%1 (%2)").arg(tr("Forms")).arg(currentItem->childCount()));

            //���������� ����� � ��������
            document->getForms()->append(newForm);

            emit documentContentsChanged(document->getGJOTreeId());
          }
          break;

          case DOC_KIND_FORM:
          {
            //����������� ������ ���� ���������-����� �� �����������
            delete newForm;
          }
          break;

          default:;
        }        
      }
      else
      {
        delete newForm;
      }
    }
    break;  

    case ITEM_KIND_DIMENSIONS:
    {
      //����������� �������������� ������ ����������
      QTreeWidgetItem *parent = currentItem->parent();
      //int index = parent->indexOfChild(currentItem);
      ItemData ownerItemData = parent->data(0, Qt::UserRole).value<ItemData>();

      Register *document = dynamic_cast<Register*> (findDocument(ownerItemData.id));
      if ( document )
      {        
        int dimensionsSize = document->getDimensions()->size() + 1;       
        //���������� ��������� � �������
        document->getDimensions()->insert(dimensionsSize, NULL);
        appendLink(currentItem, dimensionsSize);
        gjoParser->appendLink(document->getGJOTreeId(), dimensionsSize);
        
        currentItem->setText(0, QString("%1 (%2)").arg(tr("Dimensions")).arg(currentItem->childCount()));

        emit documentContentsChanged(document->getGJOTreeId());
      }      
    }
    break;  

    case ITEM_KIND_FACTS:
    {
      //����������� �������������� ������ ����������
      QTreeWidgetItem *parent = currentItem->parent();
      //int index = parent->indexOfChild(currentItem); 
      ItemData ownerItemData = parent->data(0, Qt::UserRole).value<ItemData>();

      Register *document = dynamic_cast<Register*> (findDocument(ownerItemData.id));
      if ( document )
      {
        Accessory *newAcc = new Accessory();
        newAcc->setId(currentItem->childCount() + 1);
        newAcc->getTranslates()->insert(languageId, QString("%1%2").arg(tr("Fact")).arg(newAcc->getId()));

        //���������� ��������� � ������ ���      
        gjoParser->appendProperty(document->getGJOTreeId(), newAcc);
        
        //�������� �������� ������������ ������������� �����
        QTreeWidgetItem *newFactItem = appendAccessory(currentItem, newAcc);
        //newFactItem->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled);

        //��������� ���������� ����
        ItemData factItemData;  
        factItemData.kind = ITEM_KIND_FACT;
        factItemData.id = newAcc->getId();
        QVariant d;
        d.setValue(factItemData);
        newFactItem->setData(0, Qt::UserRole, d);

        //���������� ������������ ��������
        currentItem->addChild(newFactItem);
        currentItem->setText(0, QString("%1 (%2)").arg(tr("Facts")).arg(currentItem->childCount()));

        //���������� ����� � �������
        document->getFacts()->append(newAcc);

        emit documentContentsChanged(document->getGJOTreeId());
      }      
    }
    break;

    case ITEM_KIND_GLOBAL_SCRIPTS:
    {
      //���������� ������� � ������ ��������
      Script *newScript = new Script();
      newScript->setId(currentObjectList.getObjects()->size() + 1);
      newScript->getTranslates()->insert(languageId,
        QString("%1%2").arg(tr("GlobalModule")).arg(currentItem->childCount() + 1));

      gjoParser->appendGlobalModule(newScript);

      currentObjectList.getObjects()->append(newScript);

      //���������� �������� ������������ ������������� ���������
      appendGlobalScript(currentItem, newScript);
      currentItem->setText(0, QString("%1 (%2)").arg(tr("GlobalModules")).arg(currentItem->childCount()));

      //GJO::Module *gjoModule = gjoTree->module("����������������");

      /*if ( gjoModule )
      {
        //���������� ������� � ������ ���
        int gjoId = gjoTree->makeScriptId();
        newScript->setGJOTreeId(gjoId);

        GJO::Script *gjoScript = new GJO::Script(gjoId, gjoModule->id(), "", "");
        gjoTree->appendScript(gjoScript);

        currentObjectList.getObjects()->append(newScript);

        //���������� �������� ������������ ������������� ���������
        appendScript(currentItem, newScript);
        currentItem->setText(0, QString("%1 (%2)").arg(tr("Scripts")).arg(currentItem->childCount()));
      }*/
    }
    break;

    default:;
  } 

  delete gjoParser;
}

void Configurator::removeObject()
{  
  if ( !treeWidget->currentItem() )
  {
    return;
  }

  QTreeWidgetItem *currentItem = treeWidget->currentItem();
  ItemData itemData = currentItem->data(0, Qt::UserRole).value<ItemData>();   

  GJODocumentParser *gjoParser = new GJODocumentParser(gjoTree);

  switch( itemData.kind )
  {
    case ITEM_KIND_DOCUMENT:
    case ITEM_KIND_DICTIONARY:
    case ITEM_KIND_REPORT:
    case ITEM_KIND_DOCUMENTS_JOURNAL:
    case ITEM_KIND_REGISTER:
    case ITEM_KIND_ENUMERATION:
    case ITEM_KIND_CONSTANT:
    case ITEM_KIND_DFORM:
    case ITEM_KIND_DTABLE:
    case ITEM_KIND_DQUERY:
    case ITEM_KIND_DCLASSMODULE:
    case ITEM_KIND_DGLOBAL_THREAD:
    case ITEM_KIND_DGLOBAL_CONTAINER:
    case ITEM_KIND_DTRIGGER:
    case ITEM_KIND_DTIME_MANAGER:
    {
      QTreeWidgetItem *parent = currentItem->parent();
      int index = parent->indexOfChild(currentItem);

      BaseDocument *document = NULL;
      for (int i = 0; i < currentObjectList.getObjects()->size(); i++)
      {        
        switch ( currentObjectList.getObjects()->value(i)->getObjectKind() )
        {
          case OBJECT_KIND_DOCUMENT:
          case OBJECT_KIND_DICTIONARY:
          case OBJECT_KIND_REPORT:
          case OBJECT_KIND_DOCUMENTS_JOURNAL:
          case OBJECT_KIND_REGISTER:
          case OBJECT_KIND_ENUMERATION:
          case OBJECT_KIND_CONSTANT:
          case OBJECT_KIND_DFORM:
          case OBJECT_KIND_DTABLE:
          case OBJECT_KIND_DQUERY:
          case OBJECT_KIND_DCLASS_MODULE:
          case OBJECT_KIND_DGLOBAL_THREAD:
          case OBJECT_KIND_DGLOBAL_CONTAINER:
          case OBJECT_KIND_DTRIGGER:
          case OBJECT_KIND_DTIME_MANAGER:
          {
            document = dynamic_cast<BaseDocument*>(currentObjectList.getObjects()->value(i));        
            if ( document->getId() == itemData.id )
            {
              //�������� ���������          
              gjoParser->removeDocument(document->getGJOTreeId());

              //�������� ������� � ������������ ������
              delete document;
              currentObjectList.getObjects()->removeAt(i);

              //�������� ������������ �������� ���������������� ������� ������������
              delete parent->takeChild(index);

              //���������� �������� ��������
              QString str = parent->text(0);
              str.truncate(str.indexOf(QChar('(')) - 1);          
              parent->setText(0, QString("%1 (%2)").arg(str).arg(parent->childCount()));

              break;
            }
          }
          break;

          default:;
        }
      }
    }
    break;    

    case ITEM_KIND_ACCESSORY:
    {
      QTreeWidgetItem *parent = currentItem->parent();
      int index = parent->indexOfChild(currentItem);

      QTreeWidgetItem *tablePartsItem = NULL;
      QTreeWidgetItem *accessoriesItem = NULL;

      ItemData ownerDataItem = parent->parent()->data(0, Qt::UserRole).value<ItemData>();
      if ( ownerDataItem.kind == ITEM_KIND_TABLE_PART )
      {
        //�������� ��������� ����� ���������
        ownerDataItem = parent->parent()->parent()->parent()->data(0, Qt::UserRole).value<ItemData>();

        //��������� ��������� �� ��������� ���������
        accessoriesItem = parent->parent()->parent()->parent()->child(1);

        //��������� ��������� �� ��������� �����
        tablePartsItem = parent->parent()->parent();        
      }
      else
      {
        if ( ownerDataItem.kind == ITEM_KIND_DOCUMENT )
        {
          //�������� ����� ���������

          //��������� ��������� �� ��������� ���������
          accessoriesItem = parent->parent()->child(1);

          //��������� ��������� �� ��������� �����
          tablePartsItem = parent->parent()->child(2);          
        }
      }

      Document *document = dynamic_cast<Document*> (findDocument(ownerDataItem.id));

      if ( document )
      {        
        //����� ������� ��������
        ownerDataItem = parent->parent()->data(0, Qt::UserRole).value<ItemData>();
        for ( int i = 0; i < document->getAccessories()->size(); i++ )
        {
          Accessory *acc = document->getAccessories()->value(i);
          if ( acc->getId() == itemData.id )
          {            
            int tpId = acc->getTablePartId();
            if ( (ownerDataItem.kind == ITEM_KIND_TABLE_PART) && (tpId > 0) )
            {              
              //�������� ������� ��������� �����
              if ( tpId == ownerDataItem.id )
              {
                //�������� ���������������� ��������� ������������ ��������
                delete parent->takeChild(index);
                parent->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(parent->childCount()));

                //�������� ��������� ��������� ����� �� ������ ���
                gjoTree->removeProperty(acc->getGJOTreeId());                  

                //�������� ��������� ��������� ����� ���������
                document->getAccessories()->removeAt(i);

                emit documentContentsChanged(document->getGJOTreeId());

                break;                
              }
            }
            else
            {
              if ( (ownerDataItem.kind == ITEM_KIND_DOCUMENT) && (tpId == -1) )
              {                               
                delete parent->takeChild(index);
                parent->setText(0, QString("%1 (%2)").arg(tr("Accessories")).arg(parent->childCount()));
                gjoTree->removeProperty(acc->getGJOTreeId());
                document->getAccessories()->removeAt(i);

                emit documentContentsChanged(document->getGJOTreeId());

                break;
              }              
            }            
          }
        }        

        //��������������� ��������
        ItemData newItemData;
        newItemData.kind = ITEM_KIND_ACCESSORY;
        QVariant v;
        int tpAccItemIndex = 0;
        int accItemIndex = 0;
        int curTpId = -1;        
        for ( int i = 0; i < document->getAccessories()->size(); i++ )
        {          
          int accTpId = document->getAccessories()->value(i)->getTablePartId();          
          if ( accTpId > 0 )
          {          
            //����������� �������������� ��������� ��������� �����
            if ( accTpId != curTpId )
            {
              curTpId = accTpId;
              tpAccItemIndex = 0;
            }

            //��������� ������� �������
            tpAccItemIndex++;

            //��������� � ������ ���������� ��������� ����� ���������
            if (tablePartsItem)
            {
              newItemData = tablePartsItem->child(curTpId - 1)->child(0)->child(tpAccItemIndex - 1)->data(0, Qt::UserRole).value<ItemData>();
              newItemData.id = tpAccItemIndex;
              v.setValue(newItemData);             
              tablePartsItem->child(curTpId - 1)->child(0)->child(tpAccItemIndex - 1)->setData(0, Qt::UserRole, v);
              document->getAccessories()->value(i)->setId(tpAccItemIndex);
            }
          }
          else
          {
            //����������� ������� �������          
            accItemIndex++;

            newItemData = accessoriesItem->child(accItemIndex - 1)->data(0, Qt::UserRole).value<ItemData>();
            newItemData.id = accItemIndex;
            v.setValue(newItemData);
            accessoriesItem->child(accItemIndex - 1)->setData(0, Qt::UserRole, v);

            //��������� ��������������
            document->getAccessories()->value(i)->setId(accItemIndex);
          }
        } 
      }   
    }
    break;   

    case ITEM_KIND_TABLE_PART:
    {
      QTreeWidgetItem *parent = currentItem->parent();
      int index = parent->indexOfChild(currentItem);

      ItemData ownerDataItem = parent->parent()->data(0, Qt::UserRole).value<ItemData>();
      Document *document = dynamic_cast<Document*> (findDocument(ownerDataItem.id));
      if ( document )
      {
        ItemData newItemData;
        newItemData.kind = ITEM_KIND_TABLE_PART;
        QVariant v; 
        //��������������� �������� � ����� ������� ��������
        for (int i = 0; i < document->getTableParts()->size(); i++)
        {
          TablePart *tp = document->getTableParts()->value(i);          
          if ( tp->getId() == itemData.id )
          {            
            //�������� ���������������� ������� ������������ ��������
            delete parent->takeChild(index);
            parent->setText(0, QString("%1 (%2)").arg(tr("Table parts")).arg(parent->childCount()));

            //�������� �� �� ������ ���
            gjoTree->removeObject(tp->getGJOTreeId());

            //�������� �� ���������
            document->getTableParts()->removeAt(i);

            i--;

            //�������� ���������� ��������������� ��������� �����
            for (int j = 0; j < document->getAccessories()->size(); j++)
            {              
              if ( document->getAccessories()->value(j)->getTablePartId() == tp->getId() )
              {
                document->getAccessories()->removeAt(j);
                j--;
              }
              else
              {
                if ( document->getAccessories()->value(j)->getTablePartId() > tp->getId() )
                {
                  //��������� �������������� �������������� ��������� ��������� �����
                  document->getAccessories()->value(j)->setTablePartId(document->getAccessories()->value(j)->getTablePartId() - 1);
                }
              }
            }

            emit documentContentsChanged(document->getGJOTreeId());
          }
          else
          {
            document->getTableParts()->value(i)->setId(i + 1);

            newItemData = parent->child(i)->data(0, Qt::UserRole).value<ItemData>();
            newItemData.id = i + 1;          
            v.setValue(newItemData);
            parent->child(i)->setData(0, Qt::UserRole, v);
          }
        }        
      }
    }
    break;    

    case ITEM_KIND_FORM:
    {
      QTreeWidgetItem *parent = currentItem->parent();
      int index = parent->indexOfChild(currentItem);

      ItemData ownerDataItem = parent->parent()->data(0, Qt::UserRole).value<ItemData>();
      BaseDocument *curDocument = dynamic_cast<BaseDocument*>(findDocument(ownerDataItem.id));

      if ( curDocument )
      {
        switch( curDocument->getKind() )
        {
          case DOC_KIND_DOCUMENT:
          {
            Document *document = dynamic_cast<Document*>(curDocument);
            if ( !document ) return;
          
            //��������������� �������� � ����� ������� ��������
            ItemData newItemData;
            newItemData.kind = ITEM_KIND_FORM;
            QVariant v;        
            for ( int i = 0; i < document->getForms()->size(); i++ )
            {
              Form *form = document->getForms()->value(i);
              if ( form->getId() == itemData.id )
              {            
                //�������� ���������������� ������� ������������ ��������
                delete parent->takeChild(index);
                parent->setText(0, QString("%1 (%2)").arg(tr("Forms")).arg(parent->childCount()));

                //�������� ����� �� ������ ���
                gjoTree->removeObject(form->getGJOTreeId());

                //�������� ����� ���������
                document->getForms()->removeAt(i);

                i--;

                emit documentContentsChanged(document->getGJOTreeId());
              }
              else
              {
                document->getForms()->value(i)->setId(i + 1);

                newItemData = parent->child(i)->data(0, Qt::UserRole).value<ItemData>();
                newItemData.id = i + 1;
                v.setValue(newItemData);
                parent->child(i)->setData(0, Qt::UserRole, v);
              }          
            }
          }
          break;

          case DOC_KIND_FORM:
          {
            //����������� ������ ���� ���������-����� �� �����������
          }
          break;

          default:;
        }        
      }     
    }
    break;   

    case ITEM_KIND_DIMENSION:
    {
      QTreeWidgetItem *parent = currentItem->parent();
      int index = parent->indexOfChild(currentItem);

      ItemData ownerDataItem = parent->parent()->data(0, Qt::UserRole).value<ItemData>();
      Register *document = dynamic_cast<Register*> (findDocument(ownerDataItem.id));      

      if ( document )
      {
        //����� ������� ��������
        QMap<int, BaseDocument*>::iterator i = document->getDimensions()->begin();
        while ( i != document->getDimensions()->end() )
        {
          if ( i.key() == itemData.id )
          {
            //�������� �������� �� ������ ���
            ItemData currentItemData = currentItem->data(0, Qt::UserRole).value<ItemData>();
            gjoTree->removeProperty(currentItemData.gjoTreeId);

            //�������� �������� ������������ ������������� ���������
            delete parent->takeChild(index);
            parent->setText(0, QString("%1 (%2)").arg(tr("Dimensions")).arg(parent->childCount()));

            //�������� ��������� �� ��������
            document->getDimensions()->erase(i);

            break;
          }

          i++;
        }
      }
    }
    break;  

    case ITEM_KIND_FACT:
    {
      QTreeWidgetItem *parent = currentItem->parent();
      int index = parent->indexOfChild(currentItem);

      ItemData ownerDataItem = parent->parent()->data(0, Qt::UserRole).value<ItemData>();
      Register *document = dynamic_cast<Register*> (findDocument(ownerDataItem.id));

      if ( document )
      {
        //��������������� �������� � ����� ������� ��������
        ItemData newItemData;
        newItemData.kind = ITEM_KIND_FACT;
        QVariant v;       
        for ( int i = 0; i < document->getFacts()->size(); i++ )
        {
          Accessory *fact = document->getFacts()->value(i);
          if ( fact->getId() == itemData.id )
          {            
            //�������� ���������������� ������� ������������ ��������
            delete parent->takeChild(index);
            parent->setText(0, QString("%1 (%2)").arg(tr("Facts")).arg(parent->childCount()));

            //�������� �������� �� ������ ���
            gjoTree->removeProperty(fact->getGJOTreeId());

            //�������� ����� �� ��������
            document->getFacts()->removeAt(i);

            //��������� ������� ��������
            i--;            
          }
          else
          {
            document->getFacts()->value(i)->setId(i + 1);

            newItemData = parent->child(i)->data(0, Qt::UserRole).value<ItemData>();
            newItemData.id = i + 1;          
            v.setValue(newItemData);
            parent->child(i)->setData(0, Qt::UserRole, v);
          }          
        }
      }     
    }
    break;

    case ITEM_KIND_GLOBAL_SCRIPT:
    {
      QTreeWidgetItem *parent = currentItem->parent();
      int index = parent->indexOfChild(currentItem);

      ItemData dataItem = currentItem->data(0, Qt::UserRole).value<ItemData>();

      //����� ������� � ������ ��������    
      for ( QList<BaseObject*>::iterator i = currentObjectList.getObjects()->begin(); i != currentObjectList.getObjects()->end(); i++ )
      {
        if ( ((*i)->getObjectKind() == OBJECT_KIND_SCRIPT) && (dataItem.id == (*i)->getId()) )
        {          
          gjoParser->removeGlobalModule((*i)->getGJOTreeId());

          //�������� ���������������� ������� ������������ ��������
          delete parent->takeChild(index);
          parent->setText(0, QString("%1 (%2)").arg(tr("GlobalModules")).arg(parent->childCount()));

          delete *i;

          i = currentObjectList.getObjects()->erase(i);
          if ( i == currentObjectList.getObjects()->end() )
          {
            break;
          }
          i--;
        }
      }
    }
    break;

    default:;
  }

  delete gjoParser;
}

void Configurator::itemActivated(QTreeWidgetItem *item, int column)
{
  //��������� �������� ����� ��� ������� ������� Enter
  if ( !item ) return;

  ItemData itemData = item->data(0, Qt::UserRole).value<ItemData>();
  std::cout << QString("Configurator debug info: id=%1 gjoid=%2").arg(itemData.id).arg(itemData.gjoTreeId).toLocal8Bit().data() << std::endl; 
  
  switch( itemData.kind )
  {
    case ITEM_KIND_DOCUMENT:
    case ITEM_KIND_DICTIONARY:
    case ITEM_KIND_REPORT:
    case ITEM_KIND_DOCUMENTS_JOURNAL:
    case ITEM_KIND_REGISTER:
    case ITEM_KIND_ENUMERATION:
    case ITEM_KIND_CONSTANT:    
    case ITEM_KIND_FORM:
    case ITEM_KIND_SCRIPT:
    case ITEM_KIND_GLOBAL_SCRIPT:
    case ITEM_KIND_DFORM:
    case ITEM_KIND_DTABLE:
    case ITEM_KIND_DQUERY:
    case ITEM_KIND_DCLASSMODULE:
    case ITEM_KIND_DGLOBAL_THREAD:
    case ITEM_KIND_DGLOBAL_CONTAINER:   
    case ITEM_KIND_DTRIGGER:
    case ITEM_KIND_DTIME_MANAGER:
    {      
      emit openDocument(itemData.gjoTreeId);
    }
    break;

    case ITEM_KIND_HEADER:
    {
      ConfigurationHeaderEditorForm *headerEditor = new ConfigurationHeaderEditorForm();      
      headerEditor->setUserDatabaseList(*configurationListForm->getUserDatabases());
      headerEditor->setHeader(currentHeader);
      if ( headerEditor->exec() == QDialog::Accepted )
      {
        currentHeader = headerEditor->getHeader();

        configurationListForm->getConfigurationListCmd();
      }
      delete headerEditor;
    }
    break;

    default:;
  }
}

void Configurator::currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
  if ( !current ) return; 
  
  ItemData itemData = current->data(0, Qt::UserRole).value<ItemData>(); 

  switch( itemData.kind )
  {
    case ITEM_KIND_CONFIGURATION:
    case ITEM_KIND_SCRIPT:
    {
      actionAddObject->setEnabled(false);
      actionRemoveObject->setEnabled(false);
    }
    break;

    case ITEM_KIND_DOCUMENT:
    case ITEM_KIND_DICTIONARY:
    case ITEM_KIND_REPORT:
    case ITEM_KIND_DOCUMENTS_JOURNAL:
    case ITEM_KIND_REGISTER:
    case ITEM_KIND_ENUMERATION:
    case ITEM_KIND_CONSTANT:
    case ITEM_KIND_DFORM:
    case ITEM_KIND_DTABLE:
    case ITEM_KIND_DQUERY:
    case ITEM_KIND_DCLASSMODULE:
    case ITEM_KIND_DGLOBAL_THREAD:
    case ITEM_KIND_DGLOBAL_CONTAINER:
    case ITEM_KIND_DTRIGGER:
    case ITEM_KIND_DTIME_MANAGER:
    case ITEM_KIND_GLOBAL_SCRIPT:
    case ITEM_KIND_ACCESSORY:
    case ITEM_KIND_TABLE_PART:
    case ITEM_KIND_FORM:
    case ITEM_KIND_DIMENSION:
    case ITEM_KIND_FACT:
    {
      actionAddObject->setEnabled(false);
      actionRemoveObject->setEnabled(true);
    }
    break;

    case ITEM_KIND_DOCUMENTS:
    case ITEM_KIND_DICTIONARIES:
    case ITEM_KIND_REPORTS:
    case ITEM_KIND_DOCUMENTS_JOURNALS:
    case ITEM_KIND_REGISTERS:
    case ITEM_KIND_ENUMERATIONS:
    case ITEM_KIND_CONSTANTS:
    case ITEM_KIND_DFORMS:
    case ITEM_KIND_DTABLES:
    case ITEM_KIND_DQUERIES:
    case ITEM_KIND_DCLASSMODULES:
    case ITEM_KIND_DGLOBAL_THREADS:
    case ITEM_KIND_DGLOBAL_CONTAINERS:   
    case ITEM_KIND_DTRIGGERS:
    case ITEM_KIND_DTIME_MANAGERS:
    case ITEM_KIND_GLOBAL_SCRIPTS:
    case ITEM_KIND_ACCESSORIES:
    case ITEM_KIND_TABLE_PARTS:
    case ITEM_KIND_FORMS:
    case ITEM_KIND_DIMENSIONS:
    case ITEM_KIND_FACTS:
    {
      actionAddObject->setEnabled(true);
      actionRemoveObject->setEnabled(false);
    }
    break;    

    default:;
  } 
}

//void Configurator::editItemName(QTreeWidgetItem *item, int column)
//{  
//  if ( !item ) return;
//
//  if ( item->flags() & Qt::ItemIsEditable )
//  {
//    treeWidget->editItem(item, column);
//  }  
//}

void Configurator::refreshGJOTreeData()
{  
  QList<GJO::Id> formIdList;
  QList<GJO::Id> scriptIdList;

  for ( int i = 0; i < currentObjectList.getObjects()->size(); i++ )
  {
    switch ( currentObjectList.getObjects()->value(i)->getObjectKind() )
    {
      case OBJECT_KIND_DOCUMENT:
      case OBJECT_KIND_DICTIONARY:
      case OBJECT_KIND_REPORT:
      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      //case OBJECT_KIND_REGISTER:
      //case OBJECT_KIND_ENUMERATION:
      //case OBJECT_KIND_CONSTANT:
      case OBJECT_KIND_DFORM:
      {
        BaseDocument *curDocument = dynamic_cast<BaseDocument*>(currentObjectList.getObjects()->value(i));        

        //������ ������� ���������    
        curDocument->getScript()->setBody(gjoTree->scriptByParentId(curDocument->getGJOTreeId())->script());

        //������ ������� � ������������� ��� ���� ���������
        switch ( curDocument->getKind() )
        {
          case DOC_KIND_DOCUMENT:
          case DOC_KIND_DICTIONARY:
          case DOC_KIND_REPORT:
          {
            Document *document = dynamic_cast<Document*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {          
                formIdList.append((*i)->getGJOTreeId());                
                scriptIdList.append((*i)->getModule()->getGJOTreeId());
              }
            }
          }
          break;

          case DOC_KIND_DOCUMENTS_JOURNAL:
          {
            DocumentsJournal *document = dynamic_cast<DocumentsJournal*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {            
                formIdList.append((*i)->getGJOTreeId());
                scriptIdList.append((*i)->getModule()->getGJOTreeId());
              }
            }
          }
          break;

          case DOC_KIND_FORM:
          {
            DForm *document = dynamic_cast<DForm*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {           
                formIdList.append((*i)->getGJOTreeId());
                scriptIdList.append((*i)->getModule()->getGJOTreeId());
              }
            }
          }
          break;

        default:;
        }
      }
      break;

      case OBJECT_KIND_SCRIPT:
      {
        scriptIdList.append(currentObjectList.getObjects()->value(i)->getGJOTreeId());
      }
      break;

      default:;
    }
  }

  //������ ���������� ������������� ����
  emit refreshXMLView(formIdList);

  //������ ���������� ��������
  emit refreshXMLView(scriptIdList);
}

void Configurator::parseGJOTreeData()
{  
  QList<GJO::Id> formIdList;
  QList<GJO::Id> scriptIdList;

  for ( int i = 0; i < currentObjectList.getObjects()->size(); i++ )
  {
    switch ( currentObjectList.getObjects()->value(i)->getObjectKind() )
    {
      case OBJECT_KIND_DOCUMENT:
      case OBJECT_KIND_DICTIONARY:
      case OBJECT_KIND_REPORT:
      case OBJECT_KIND_DOCUMENTS_JOURNAL:
      //case OBJECT_KIND_REGISTER:
      //case OBJECT_KIND_ENUMERATION:
      //case OBJECT_KIND_CONSTANT:
      case OBJECT_KIND_DFORM:
      {
        BaseDocument *curDocument = dynamic_cast<BaseDocument*>(currentObjectList.getObjects()->value(i));        

        //������ ������� ���������    
        curDocument->getScript()->setBody(gjoTree->scriptByParentId(curDocument->getGJOTreeId())->script());

        //������ ������� � ������������� ��� ���� ���������
        switch ( curDocument->getKind() )
        {
          case DOC_KIND_DOCUMENT:
          case DOC_KIND_DICTIONARY:
          case DOC_KIND_REPORT:
          {
            Document *document = dynamic_cast<Document*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {          
                formIdList.append((*i)->getGJOTreeId());                
                scriptIdList.append((*i)->getModule()->getGJOTreeId());
              }
            }
          }
          break;

          case DOC_KIND_DOCUMENTS_JOURNAL:
          {
            DocumentsJournal *document = dynamic_cast<DocumentsJournal*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {            
                formIdList.append((*i)->getGJOTreeId());
                scriptIdList.append((*i)->getModule()->getGJOTreeId());
              }
            }
          }
          break;

          case DOC_KIND_FORM:
          {
            DForm *document = dynamic_cast<DForm*>(curDocument);
            if ( document )
            {
              for ( QList<Form*>::iterator i = document->getForms()->begin(); i != document->getForms()->end(); i++ )
              {
                formIdList.append((*i)->getGJOTreeId());
                scriptIdList.append((*i)->getModule()->getGJOTreeId());
              }
            }
          }
          break;

          default:;
        }
      }
      break;

      case OBJECT_KIND_SCRIPT:
      {
        scriptIdList.append(currentObjectList.getObjects()->value(i)->getGJOTreeId());
      }
      break;

      default:;
    }
  }

  //������ ������� ������������� ����
  emit parseXMLView(formIdList);

  //������ ������� ��������
  //emit parseXMLView(scriptIdList);
}
