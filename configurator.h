/***********************************************************************
* Module:   configurator.h
* Author:   LGP
* Modified: 31 ������ 2007 �.
* Purpose:  ����� ������������� ������������� � IDE 
***********************************************************************/

#ifndef CONFIGURATOR_H
#define CONFIGURATOR_H

#include <QTreeWidget>
#include <QToolBar>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QHBoxLayout>

#include "abstractconfigurator.h"
#include "configuration.h"
#include "configurationlist.h"
#include "configurationlistform.h"
//#include "newconfigurationform.h"
#include "configurationheadereditorform.h"
#include "gjodocumentparser.h"

#define languageId 1

namespace ConfiguratorObjects
{
  struct ItemData
  {
    //������������� ������������� ���������� ��������
    int id;

    //������������� �������� � ������ ���
    int gjoTreeId;

    //��� �������� ������������
    int kind;
  };

  class Configurator: public AbstractConfigurator
  {
    Q_OBJECT

    //���� ����� ������
    static const int ITEM_KIND_CONFIGURATION      = 0;
    static const int ITEM_KIND_DOCUMENTS          = 1;
    static const int ITEM_KIND_DOCUMENT           = 2;
    static const int ITEM_KIND_DICTIONARIES       = 3;
    static const int ITEM_KIND_DICTIONARY         = 4;
    static const int ITEM_KIND_REPORTS            = 5;
    static const int ITEM_KIND_REPORT             = 6;
    static const int ITEM_KIND_DOCUMENTS_JOURNALS = 7;
    static const int ITEM_KIND_DOCUMENTS_JOURNAL  = 8;
    static const int ITEM_KIND_REGISTERS          = 9;
    static const int ITEM_KIND_REGISTER           = 10;
    static const int ITEM_KIND_DFORMS             = 11;
    static const int ITEM_KIND_DFORM              = 12;
    static const int ITEM_KIND_ENUMERATIONS       = 13;
    static const int ITEM_KIND_ENUMERATION        = 14;
    static const int ITEM_KIND_CONSTANTS          = 15;
    static const int ITEM_KIND_CONSTANT           = 16;
    static const int ITEM_KIND_ACCESSORIES        = 17;
    static const int ITEM_KIND_ACCESSORY          = 18;
    static const int ITEM_KIND_TABLE_PARTS        = 19;
    static const int ITEM_KIND_TABLE_PART         = 20;
    static const int ITEM_KIND_FORMS              = 21;
    static const int ITEM_KIND_FORM               = 22;
    static const int ITEM_KIND_DIMENSIONS         = 23;
    static const int ITEM_KIND_DIMENSION          = 24;
    static const int ITEM_KIND_FACTS              = 25;
    static const int ITEM_KIND_FACT               = 26;
    static const int ITEM_KIND_GLOBAL_SCRIPTS     = 27;    
    static const int ITEM_KIND_GLOBAL_SCRIPT      = 28;
    static const int ITEM_KIND_SCRIPT             = 29;
    static const int ITEM_KIND_DTABLES            = 30;
    static const int ITEM_KIND_DTABLE             = 31;
    static const int ITEM_KIND_DQUERIES           = 32;
    static const int ITEM_KIND_DQUERY             = 33;
    static const int ITEM_KIND_DCLASSMODULES      = 34;
    static const int ITEM_KIND_DCLASSMODULE       = 35;
    static const int ITEM_KIND_DGLOBAL_THREADS    = 36;
    static const int ITEM_KIND_DGLOBAL_THREAD     = 37;
    static const int ITEM_KIND_DGLOBAL_CONTAINERS = 38;
    static const int ITEM_KIND_DGLOBAL_CONTAINER  = 39;
    static const int ITEM_KIND_DTRIGGERS          = 40;
    static const int ITEM_KIND_DTRIGGER           = 41;
    static const int ITEM_KIND_DTIME_MANAGERS     = 42;
    static const int ITEM_KIND_DTIME_MANAGER      = 43;

    static const int ITEM_KIND_NO_MEANING         = 0xFFFF;
    static const int ITEM_KIND_USER_DATABASE_ID   = 44;
    static const int ITEM_KIND_HEADER             = 45;

    //dockWindowContents
    QWidget *mainWidget;
    QTreeWidget *treeWidget;
    QStatusBar *statusBar;
    QAction *actionConfigurationManager;
    QAction *actionSaveConfiguration;
    QAction *actionReleaseConfiguration;
    QAction *actionCloseConfiguration;
    QAction *actionParameters;
    QAction *actionAddObject;
    QAction *actionRemoveObject;
    QToolBar *_toolBar;
    QVBoxLayout *layout;

    //menu
    QMenu *_menu;

    //������� ������ ��������� ������������
    ConfigurationList configurationList;

    //���� ��������� ������������
    ConfigurationListForm *configurationListForm;

    //����� �������� ������������
    ObjectList currentObjectList;

    ConfigurationHeader currentHeader;

    //������� ������ ���
    GJO::Tree *gjoTree;

  public:
    Configurator(QWidget *);
    ~Configurator();    

    QMenu *menu() const;
    QWidget *workingArea() const;
    QWidget *dockWindowContents() const;
    QToolBar *toolBar() const;

  public slots:
    void reloadDocument(int id);

    void configurationManager(); 
    void openConfiguration(const ConfigurationHeader &header);
    void saveConfiguration();
    void releaseConfiguration();
    void closeConfiguration();
    void clearConfiguration();  

  private slots:
    void newObject();
    void removeObject();
    void currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *);
    void itemActivated(QTreeWidgetItem *, int);
    //void editItemName(QTreeWidgetItem *, int);  
    void setConfigParameters();

    void processConnectionStateChange();
    //void test(); 

    //void processCmdConfiguration(const qint32 &result, const QByteArray &data);
    void processCmdSaveConfiguration(const qint32 &result, const QByteArray &data);
    //void processCmdReleaseConfiguration(const qint32 &result, const QByteArray &data);

  private:
    //������ ���������� �������� � treeWidget
    QTreeWidgetItem *appendConfiguration(const ObjectList &objects, const ConfigurationHeader &header);
    QTreeWidgetItem *appendObjectList(QTreeWidgetItem *, const ObjectList &objList);
    QTreeWidgetItem *appendDocument(QTreeWidgetItem *, Documents::Document *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendRegister(QTreeWidgetItem *, Documents::Register *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDocumentsJournal(QTreeWidgetItem *, Documents::DocumentsJournal *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendEnumeration(QTreeWidgetItem *, Documents::Enumeration *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendConstant(QTreeWidgetItem *, Documents::Constant *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDForm(QTreeWidgetItem *, Documents::DForm *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDTable(QTreeWidgetItem *, Documents::DTable *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDQuery(QTreeWidgetItem *, Documents::DQuery *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDClassModule(QTreeWidgetItem *, Documents::DClassModule *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDGlobalThread(QTreeWidgetItem *, Documents::DGlobalThread *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDGlobalContainer(QTreeWidgetItem *, Documents::DGlobalContainer *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDTrigger(QTreeWidgetItem *, Documents::DTrigger *, bool appendToItemFlag = true);
    QTreeWidgetItem *appendDTimeManager(QTreeWidgetItem *, Documents::DTimeManager *, bool appendToItemFlag = true);

    QTreeWidgetItem *appendAccessory(QTreeWidgetItem *, Documents::Accessory *);
    QTreeWidgetItem *appendTablePart(QTreeWidgetItem *, Documents::TablePart *);
    QTreeWidgetItem *appendForm(QTreeWidgetItem *, Documents::Form *);    
    QTreeWidgetItem *appendScript(QTreeWidgetItem *, Documents::Script *);
    QTreeWidgetItem *appendGlobalScript(QTreeWidgetItem *, Documents::Script *);
    QTreeWidgetItem *appendLink(QTreeWidgetItem *, int index);

    QTreeWidgetItem *appendConnectionInfo(QTreeWidgetItem *, const ConnectionInfo &);
    QTreeWidgetItem *appendConfigurationHeader(QTreeWidgetItem *, const ConfigurationHeader &);

    //����� ��������� ����� ��������� � ��������� ��������������� � ������ ������� ����������
    //� ������ ������ ���������� ��������� �� ��������
    Documents::BaseDocument *findDocument(int);

    //������ ��������� ���������� � ������������� ������ � ������ ���
    void refreshGJOTreeData();
    void parseGJOTreeData();
  };
}

Q_DECLARE_METATYPE(ConfiguratorObjects::ItemData);

#endif
