#include "gjodocumentparser.h"
#include "punycode.h"
#include "../GJOTree/src/entities/trait.h"

using namespace ConfiguratorObjects;
using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::Documents::DataType;


GJODocumentParser::GJODocumentParser(GJO::Tree *tree)
{
  gjoTree = tree; 
}

GJODocumentParser::~GJODocumentParser()
{
}

int GJODocumentParser::parse(BaseDocument *document)
{
  if ( !document ) return -1;
 
  //�������� ������� � ��������� �������������� gjo
  if (document->getGJOTreeId() == -1)
  {
    //������ document->getKind() � ������������ ���c������ ������������� ���� ���������
    QMap<int, QString> documentTypes;
    QString moduleName;
    if ( gjoTree->languageId() == 1 )
    {
      documentTypes.insert(DOC_KIND_DOCUMENT,          "��������");
      documentTypes.insert(DOC_KIND_DICTIONARY,        "����������");
      documentTypes.insert(DOC_KIND_REPORT,            "�����");
      documentTypes.insert(DOC_KIND_DOCUMENTS_JOURNAL, "����������������");
      documentTypes.insert(DOC_KIND_REGISTER,          "�������");
      documentTypes.insert(DOC_KIND_ENUMERATION,       "������������");
      documentTypes.insert(DOC_KIND_CONSTANT,          "���������");
      documentTypes.insert(DOC_KIND_FORM,              "�������������");
      documentTypes.insert(DOC_KIND_TABLE,             "���������������");
      documentTypes.insert(DOC_KIND_QUERY,             "��������������");
      documentTypes.insert(DOC_KIND_CLASS_MODULE,      "�������������");
      documentTypes.insert(DOC_KIND_GLOBAL_THREAD,     "���������������");
      documentTypes.insert(DOC_KIND_GLOBAL_CONTAINER,  "���������������");

      moduleName = "���������";
    }
    else
    {
      if ( gjoTree->languageId() == 2 )
      {
        documentTypes.insert(DOC_KIND_DOCUMENT,          "Document");
        documentTypes.insert(DOC_KIND_DICTIONARY,        "Dictionary");
        documentTypes.insert(DOC_KIND_REPORT,            "Report");
        documentTypes.insert(DOC_KIND_DOCUMENTS_JOURNAL, "DocumentsJournal");
        documentTypes.insert(DOC_KIND_REGISTER,          "Register");
        documentTypes.insert(DOC_KIND_ENUMERATION,       "Enumeration");
        documentTypes.insert(DOC_KIND_CONSTANT,          "Constant");
        documentTypes.insert(DOC_KIND_FORM,              "DocumentForm");
        documentTypes.insert(DOC_KIND_TABLE,             "DocumentTable");
        documentTypes.insert(DOC_KIND_QUERY,             "DocumentQuery");
        documentTypes.insert(DOC_KIND_CLASS_MODULE,      "ClassModuleType");
        documentTypes.insert(DOC_KIND_GLOBAL_THREAD,     "GlobalThread");
        documentTypes.insert(DOC_KIND_GLOBAL_CONTAINER,  "GlobalContainer");

        moduleName = "Documents";
      }
    }
     
    int docTypeId = 0;
    QString docTypeName = documentTypes.value(document->getKind());
    if ( !docTypeName.isEmpty() )
    {
      GJO::Type *t = gjoTree->type(docTypeName);
      docTypeId = t->id();
    }

    PunyCode pc;
    QString systemName = pc.encode(document->getTranslates()->value(1));
    document->setSystemName(systemName);

    int gjoObjectId = 0;
    int gjoObjectTranslationId = gjoTree->makeTranslationId();
    if ( document->getKind() == DOC_KIND_GLOBAL_THREAD )
    {
      gjoObjectId = gjoTree->makeModuleId();
     
      GJO::Module *newModule = new GJO::Module(
        gjoObjectId,
        document->getTranslates()->value(gjoTree->languageId()),
        gjoObjectTranslationId,
        GlobalThreadTypeId);

      gjoTree->appendModule(newModule);
    }
    else
    {
      if ( document->getKind() == DOC_KIND_GLOBAL_CONTAINER )
      {
        gjoObjectId = gjoTree->makeModuleId();

        GJO::Module *newModule = new GJO::Module(
          gjoObjectId,
          document->getTranslates()->value(gjoTree->languageId()),
          gjoObjectTranslationId,
          GlobalContainerTypeId);

        gjoTree->appendModule(newModule);
      }
      else
      {
        gjoObjectId = gjoTree->makeObjectId();      

        GJO::Document *gjoDocument = new GJO::Document(gjoObjectId,
          document->getTranslates()->value(gjoTree->languageId()).toLocal8Bit().data(),
          gjoObjectTranslationId,
          gjoTree->module(moduleName)->id(),
          docTypeId,
          document->getSystemName());

        gjoTree->appendObject(gjoDocument);
      }      
    }

    document->setGJOTreeId(gjoObjectId);

    int gjoTreeId = gjoTree->makeScriptId();
    document->getScript()->setGJOTreeId(gjoTreeId);

    GJO::Script *gjoScript = new GJO::Script(gjoTreeId,
      gjoObjectId);

    gjoScript->fromXML(document->getScript()->getBody());

    gjoTree->appendScript(gjoScript);

    //���������� � ������ ����������� ������� ���������
    //Id
    QVariant data;
    data.setValue(document->getId());
    GJO::Trait *gjoTrait = new GJO::Trait(0,
      "Id",
      gjoTree->makeTranslationId(),
      gjoObjectId,
      0);

    gjoTrait->setValue(data);
    gjoTree->appendTrait(gjoTrait);

    //Kind
    data.setValue(document->getKind());
    gjoTrait = new GJO::Trait(0,
      "Kind",
      gjoTree->makeTranslationId(),
      gjoObjectId,
      0);

    gjoTrait->setValue(data);
    gjoTree->appendTrait(gjoTrait);

    //Status
    data.setValue(document->getStatus());
    gjoTrait = new GJO::Trait(0,
      "Status",
      gjoTree->makeTranslationId(),
      gjoObjectId,
      0);

    gjoTrait->setValue(data);
    gjoTree->appendTrait(gjoTrait);

    //DeleteMark
    data.setValue(document->getDeleteMark());
    gjoTrait = new GJO::Trait(0,
      "DeleteMark",
      gjoTree->makeTranslationId(),
      gjoObjectId,
      0);

    gjoTrait->setValue(data);
    gjoTree->appendTrait(gjoTrait);

    //ReleaseMark
    data.setValue(document->getReleaseMark());
    gjoTrait = new GJO::Trait(0,
      "ReleaseMark",
      gjoTree->makeTranslationId(),
      gjoObjectId,
      0);

    gjoTrait->setValue(data);
    gjoTree->appendTrait(gjoTrait);

    //Flags
    data.setValue(document->getFlags());
    gjoTrait = new GJO::Trait(0,
      "Flags",
      gjoTree->makeTranslationId(),
      gjoObjectId,
      0);

    gjoTrait->setValue(data);
    gjoTree->appendTrait(gjoTrait);    

    //���������� ��������� ���������
    QMap<int, QString>::iterator i = document->getTranslates()->begin();
    while ( i != document->getTranslates()->end() )
    {
      //���� ������� ��� ��������� �� ����������, �� �� ����������� � ������
      if ( i.key() != gjoTree->languageId() )
      {
        gjoTree->appendTranslation(gjoObjectTranslationId, i.key(), i.value().toLocal8Bit().data());
      }

      i++;
    }   
  }

  //����� ��������� �������� ��������� � ����� ����������� ���������, ������� ��� �� ���� ���������������� � GJOTree
  //��������� ������ �������� �������� ��������� gjoTreeId = -1
  switch(document->getKind())
  {
    case DOC_KIND_DOCUMENT:
    case DOC_KIND_DICTIONARY:
    case DOC_KIND_REPORT:
    {
      scanDocument((Document *)document);
    }
    break;   

    case DOC_KIND_DOCUMENTS_JOURNAL:
    {
      scanDocument(static_cast<DocumentsJournal*>(document));
    }
    break;

    case DOC_KIND_REGISTER:
    {
      scanDocument(static_cast<Register*>(document));      
    }
    break;

    case DOC_KIND_ENUMERATION:
    {
      scanDocument(static_cast<Enumeration*>(document));      
    }
    break;

    case DOC_KIND_CONSTANT:
    {
      scanDocument(static_cast<Constant*>(document));      
    }
    break;

    case DOC_KIND_FORM:
    {
      scanDocument(static_cast<DForm*>(document));      
    }
    break;

    case DOC_KIND_TABLE:
    {
      scanDocument(static_cast<DTable*>(document));      
    }
    break;

    case DOC_KIND_QUERY:
    {
      scanDocument(static_cast<DQuery*>(document));      
    }
    break;

    case DOC_KIND_CLASS_MODULE:
    {
      scanDocument(static_cast<DClassModule*>(document));      
    }
    break;

    case DOC_KIND_TRIGGER:
    {
      scanDocument(static_cast<DTrigger*>(document));      
    }
    break;

    case DOC_KIND_TIME_MANAGER:
    {
      scanDocument(static_cast<DTimeManager*>(document));      
    }
    break;

    default:;
  }

  return 0;
}

int GJODocumentParser::setDocument(BaseDocument *document)
{
  if ( !document ) return 0;  

  //���� ���� ��������� ���������� ���������
  if ( document->getGJOTreeId() != -1 )
  {
    gjoTree->removeObject(document->getGJOTreeId());
    clearDocumentGJOTreeId(document);      
  }
  
  //������ ���������
  parse(document);

  //������� �������������� ��������� � ������ ���
  return document->getGJOTreeId(); 
}

BaseDocument * GJODocumentParser::getDocument(int id)
{
  GJO::Entity *entity = gjoTree->entityById(id);

  if ( !entity )
  {
    return NULL;
  }

  if ( !(entity->kind() == GJO::ModuleKind || entity->kind() == GJO::DocumentKind) )
  {
    return NULL;
  }

  BaseDocument *newDocument = NULL;  
  if ( entity->kind() == GJO::ModuleKind )
  {
    GJO::Module *module = gjoTree->moduleById(entity->id());
    if ( !module )
    {
      return NULL;
    }

    if ( module->typeId() == GlobalContainerTypeId )
    {
      newDocument = new DGlobalContainer();
     
      newDocument->getTranslates()->insert(1, gjoTree->translation(module->translationId(), 1));
      newDocument->getTranslates()->insert(2, gjoTree->translation(module->translationId(), 2));
    }
    else
    {
      if ( module->typeId() == GlobalThreadTypeId )
      {
        newDocument = new DGlobalThread();

        newDocument->getTranslates()->insert(1, gjoTree->translation(module->translationId(), 1));
        newDocument->getTranslates()->insert(2, gjoTree->translation(module->translationId(), 2));
      }
      else
      {
        return NULL;
      }
    }
  }

  if ( entity->kind() == GJO::DocumentKind )
  {
    GJO::Object *obj = gjoTree->object(id);
    if ( !obj )
    {
      return NULL;
    }

    GJO::Trait *gjoTrait = gjoTree->traitByParentIdAndName(obj->id(), "Kind");
    if ( !gjoTrait ) return NULL;
    QVariant data = gjoTrait->value();
   
    switch( data.toInt() )
    {
      case DOC_KIND_DOCUMENT:
      {      
        newDocument = new Document();
        Document *curDocument = static_cast<Document*>(newDocument);
        loadDocumentContent(id, curDocument);      
      }
      break;

      case DOC_KIND_DICTIONARY:
      {
        newDocument = new Dictionary();
        Document *curDocument = static_cast<Document*>(newDocument);
        loadDocumentContent(id, curDocument);
      }
      break;

      case DOC_KIND_REPORT:
      {
        newDocument = new Report();
        Document *curDocument = static_cast<Document*>(newDocument);
        loadDocumentContent(id, curDocument);
      }
      break;

      case DOC_KIND_DOCUMENTS_JOURNAL:
      {
        newDocument = new DocumentsJournal();
        DocumentsJournal *curDocument = static_cast<DocumentsJournal*>(newDocument);
        loadDocumentContent(id, curDocument);
      }
      break;

      case DOC_KIND_REGISTER:
      {
        newDocument = new Register();
        Register *curDocument = static_cast<Register*>(newDocument);
        loadDocumentContent(id, curDocument);
      }
      break;

      case DOC_KIND_ENUMERATION:
      {
        newDocument = new Enumeration();
        Enumeration *curDocument = static_cast<Enumeration*>(newDocument);
        loadDocumentContent(id, curDocument);
      }
      break;

      case DOC_KIND_CONSTANT:
      {
        newDocument = new Constant();
        Constant *curDocument = static_cast<Constant*>(newDocument);
        loadDocumentContent(id, curDocument);
      }
      break;

      case DOC_KIND_FORM:
      {
        newDocument = new DForm();
        DForm *curDocument = static_cast<DForm*>(newDocument);
        loadDocumentContent(id, curDocument);
      }
      break;

      case DOC_KIND_TABLE:
      {
        newDocument = new DTable();
        loadDocumentContent(id, static_cast<DTable*>(newDocument));
      }
      break;

      case DOC_KIND_QUERY:
      {
        newDocument = new DQuery();
        loadDocumentContent(id, static_cast<DQuery*>(newDocument));
      }
      break;

      case DOC_KIND_TRIGGER:
      {
        newDocument = new DTrigger();
        loadDocumentContent(id, static_cast<DTrigger*>(newDocument));
      }
      break;

      case DOC_KIND_TIME_MANAGER:
      {
        newDocument = new DTimeManager();
        loadDocumentContent(id, static_cast<DTimeManager*>(newDocument));
      }
      break;  

      default:;
    }

    if ( newDocument )
    {      
      //�������� ��������� 
      newDocument->getTranslates()->insert(1, gjoTree->translation(obj->translationId(), 1));
      newDocument->getTranslates()->insert(2, gjoTree->translation(obj->translationId(), 2));
    }    
  }  

  if ( newDocument )
  {
    //�������� ������� ������� ���������
    QVariant data = gjoTree->traitByParentIdAndName(entity->id(), "Id")->value();  
    newDocument->setId(data.toInt());

    newDocument->setGJOTreeId(entity->id());

    data = gjoTree->traitByParentIdAndName(entity->id(), "Status")->value();  
    newDocument->setStatus(data.toInt());

    data = gjoTree->traitByParentIdAndName(entity->id(), "DeleteMark")->value();   
    newDocument->setDeleteMark(data.toBool());

    data = gjoTree->traitByParentIdAndName(entity->id(), "ReleaseMark")->value();  
    newDocument->setReleaseMark(data.toBool());

    data = gjoTree->traitByParentIdAndName(entity->id(), "Flags")->value();   
    newDocument->setFlags(data.toInt());

    //�������� �������
    GJO::Script *gjoScript = gjoTree->scriptByParentId(entity->id());
    if ( gjoScript )
    {
      Script newScript(gjoScript->toXML());      
      newScript.setGJOTreeId(gjoScript->id());
      newDocument->setScript(newScript);
    }   
  }  

  return newDocument;
}

void GJODocumentParser::removeDocument(int id)
{  
  gjoTree->removeObject(id);
}

void GJODocumentParser::clearDocumentGJOTreeId(BaseDocument *document)
{
  if ( !document ) return;

  document->setGJOTreeId(-1);
  document->getScript()->setGJOTreeId(-1);

  switch ( document->getKind() )
  {
    case DOC_KIND_DOCUMENT:
    case DOC_KIND_DICTIONARY:
    case DOC_KIND_REPORT:
    {
      Document *doc = dynamic_cast<Document *> (document); 
      if ( doc )
      {
        for ( QList<Accessory*>::iterator i = doc->getAccessories()->begin(); i != doc->getAccessories()->end(); i++ )
        {          
          (*i)->setGJOTreeId(-1);         
        }

        for ( QList<TablePart*>::iterator i = doc->getTableParts()->begin(); i != doc->getTableParts()->end(); i++ )
        {
          (*i)->setGJOTreeId(-1);
        }

        for ( QList<Form*>::iterator i = doc->getForms()->begin(); i != doc->getForms()->end(); i++ )
        {
          (*i)->setGJOTreeId(-1);
          (*i)->getBody()->setGJOTreeId(-1);
          (*i)->getModule()->setGJOTreeId(-1);
        }
      }
    }
    break;

    case DOC_KIND_DOCUMENTS_JOURNAL:
    {      
      DocumentsJournal *doc = dynamic_cast<DocumentsJournal *> (document);
      if ( doc )
      {
        //�������� ����
        for ( QList<Form*>::iterator i = doc->getForms()->begin(); i != doc->getForms()->end(); i++ )
        {
          (*i)->setGJOTreeId(-1);         
        }
      }
    }
    break;

    case DOC_KIND_FORM:
    {      
      DForm *doc = dynamic_cast<DForm *> (document);
      if ( doc )
      {
        for ( QList<Form*>::iterator i = doc->getForms()->begin(); i != doc->getForms()->end(); i++ )
        {
          (*i)->setGJOTreeId(-1);         
        }
      }      
    }
    break;

    case DOC_KIND_TABLE:
    {
      DTable *doc = dynamic_cast<DTable*> (document); 
      if ( doc )
      {
        for ( QList<Accessory*>::iterator i = doc->getAccessories()->begin(); i != doc->getAccessories()->end(); i++ )
        {          
          (*i)->setGJOTreeId(-1);         
        }
      }
    }
    break;

    default:;
  }
}

void GJODocumentParser::appendForm(const int ownerId, Form *form)
{
  int gjoTreeId = gjoTree->makeObjectId();

  //������ �������������� gjoTree
  form->setGJOTreeId(gjoTreeId);

  QString formTypeName;
  if (gjoTree->languageId() == 1)
  {
    formTypeName = "�����";
  }
  else
  {
    if (gjoTree->languageId() == 2)
    {
      formTypeName = "Form";
    }
  } 

  GJO::Form *gjoForm = new GJO::Form(gjoTreeId,
    form->getTranslates()->value(gjoTree->languageId()).toLocal8Bit().data(),
    gjoTree->makeTranslationId(),
    ownerId,
    gjoTree->type(formTypeName)->id(),
    form->getSystemName());

  gjoTree->appendObject(gjoForm);

  //���������� � ������ ����������� ������ �����
  //Id
  QVariant data;
  data.setValue(form->getId());
  GJO::Trait *gjoTrait = new GJO::Trait(0,
    "Id",
    gjoTree->makeTranslationId(),
    gjoForm->id(),
    0);

  gjoTrait->setValue(data);
  gjoTree->appendTrait(gjoTrait);

  //Kind
  data.setValue(form->getKind());
  gjoTrait = new GJO::Trait(0,
    "Kind",
    gjoTree->makeTranslationId(),
    gjoForm->id(),
    0);

  gjoTrait->setValue(data);
  gjoTree->appendTrait(gjoTrait);
  
  gjoTreeId = gjoTree->makeScriptId(); 
  form->getModule()->setGJOTreeId(gjoTreeId);

  GJO::Script *gjoScript = new GJO::Script(gjoTreeId,
    gjoForm->id(), form->getModule()->getBody()); 
  gjoTree->appendScript(gjoScript);

  //���������� ����� � � xml-�������������
  gjoTree->setXMLView(gjoForm->id(), form->getBody()->getBody().toLocal8Bit().data());

  //���������� ��������� �����
  QMap<int, QString>::iterator j = form->getTranslates()->begin();
  while (j != form->getTranslates()->end())
  {
    //���� ������� ��� ��������� �� ����������, �� �� ����������� � ������
    if (j.key() != gjoTree->languageId())
    {             
      gjoTree->appendTranslation(gjoForm->translationId(), j.key(), j.value().toLocal8Bit().data());
    }

    j++;
  }

  //������ �������� ��������� �����
  if ( form->getKind() == Form::FORM_KIND_START )
  {    
    gjoTree->header().startFormId = gjoForm->id();
  }  
}

GJO::Property * GJODocumentParser::appendProperty(const int ownerId, Accessory *accessory)
{
  //������ �������������� ��� ������ ��������
  int gjoTreeId = gjoTree->makePropertyId(ownerId);

  //������ �������������� gjoTree
  accessory->setGJOTreeId(gjoTreeId);

  int typeId = 0; 
  
  //����������� ������������ ��������� ����� ������
  QMap<int, int> accessoryTypesMatch;
  accessoryTypesMatch.insert(DATA_TYPE_NUMBER,   NumberDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_INTEGER,  IntegerDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_FLOAT,    RealDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_STRING,   StringDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_DATETIME, DateTimeDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_BOOLEAN,  BooleanDataTypeId);  
  accessoryTypesMatch.insert(DATA_TYPE_LINK,     LinkDataTypeId);

  QString limits;
  if ( accessory->getType() )
  {
    typeId = accessoryTypesMatch.value(accessory->getType()->getTypeKind());
    
    //������������ ������ �����������
    switch (accessory->getType()->getTypeKind())
    {
      case DATA_TYPE_NUMBER:
      {        
        Number *type = dynamic_cast<Number*> (accessory->getType());
        if ( type )
        {
          limits.append(QString("Length=%1;").arg(type->getLength()));
          limits.append(QString("Precision=%1;").arg(type->getPrecision()));
          limits.append(QString("Default=%1;").arg(type->getDefault()));
          limits.append(QString("Required=%1;").arg(type->getRequired()));
          limits.append(QString("Min=%1;").arg(type->getMin()));
          limits.append(QString("Max=%1;").arg(type->getMax()));
        }
      }
      break;

      case DATA_TYPE_INTEGER:
      {        
        Integer *type = dynamic_cast<Integer*> (accessory->getType());
        if ( type )
        {          
          limits.append(QString("Default=%1;").arg(type->getDefault()));
          limits.append(QString("Required=%1;").arg(type->getRequired()));
          limits.append(QString("Min=%1;").arg(type->getMin()));
          limits.append(QString("Max=%1;").arg(type->getMax()));
        }
      }
      break;

      case DATA_TYPE_FLOAT:
      {        
        Float *type = dynamic_cast<Float*> (accessory->getType());
        if ( type )
        {          
          limits.append(QString("Default=%1;").arg(type->getDefault()));
          limits.append(QString("Required=%1;").arg(type->getRequired()));
          limits.append(QString("Min=%1;").arg(type->getMin()));
          limits.append(QString("Max=%1;").arg(type->getMax()));
        }
      }
      break;

      case DATA_TYPE_STRING:
      {
        String *type = dynamic_cast<String*> (accessory->getType());
        if ( type )
        {
          limits.append(QString("Length=%1;").arg(type->getLength()));        
          limits.append(QString("Default=%1;").arg(type->getDefault()));
          limits.append(QString("Required=%1;").arg(type->getRequired()));
        }
      }
      break;

      case DATA_TYPE_DATETIME:
      {
        DateTime *type = dynamic_cast<DateTime*> (accessory->getType());
        if ( type )
        {              
          limits.append(QString("Default=%1;").arg(type->getDefault().toString("dd.MM.yyyy hh:mm:ss")));
          limits.append(QString("Required=%1;").arg(type->getRequired()));         
        }
      }
      break;

      case DATA_TYPE_BOOLEAN:
      {
        Logical *type = dynamic_cast<Logical*> (accessory->getType());
        if ( type )
        {                
          limits.append(QString("Default=%1;").arg(type->getDefault()));
          limits.append(QString("Required=%1;").arg(type->getRequired()));
        }
      }
      break;

      case DATA_TYPE_LINK:
	    {
	      Link *type = dynamic_cast<Link*> (accessory->getType());
		    if ( type )
		    {
		      limits.append(QString("LinkedTableName=%1;").arg(type->getLinkedTableName()));
		      limits.append(QString("LinkedTableRecordId=%1;").arg(type->getLinkedTableRecordId()));
		      limits.append(QString("Flag=%1;").arg(type->getFlag()));
		    }
	    }
	    break;

      default:;
    }    
  }
  
  GJO::Accessory *gjoAcc = new GJO::Accessory(gjoTreeId,
    accessory->getTranslates()->value(gjoTree->languageId()).toLocal8Bit().data(),
    gjoTree->makeTranslationId(),
    ownerId,
    typeId,
    accessory->getSystemName());

  gjoTree->appendProperty(gjoAcc);

  /*
  std::cout << QString("GJO::Property * GJODocumentParser::appendProperty(const int ownerId, Accessory *accessory)\n"
                       "gjoId=%1\ngjoParentId=%2\nSystemName=%3").arg(gjoAcc->id()).arg(gjoAcc->parentId()).arg(gjoAcc->databaseName()).toLocal8Bit().data() << std::endl;
  */
  
  //������� ������������� ���������
  //Id
  QVariant data;
  data.setValue(accessory->getId());
  GJO::Trait *gjoTrait = new GJO::Trait(0,
    "Id",
    gjoTree->makeTranslationId(),
    gjoAcc->id(),
    0);

  gjoTrait->setValue(data);
  gjoTree->appendTrait(gjoTrait);

  //Flags
  data.setValue(accessory->getFlags());
  gjoTrait = new GJO::Trait(0,
    "Flags",
    gjoTree->makeTranslationId(),
    gjoAcc->id(),
    0);

  gjoTrait->setValue(data);
  gjoTree->appendTrait(gjoTrait);  

  //���������� ��������� ���������
  QMap<int, QString>::iterator j = accessory->getTranslates()->begin();
  while (j != accessory->getTranslates()->end())
  {
    //���� ������� ��� ��������� �� ����������, �� �� ����������� � ������
    if ( j.key() != gjoTree->languageId() )
    {
      gjoTree->appendTranslation(gjoAcc->translationId(), j.key(), j.value().toLocal8Bit().data());
    }

    j++;
  }

  //��������� ������ �����������
  gjoTree->setPropertyLimits(gjoAcc->id(), limits);

  return gjoAcc;
}

GJO::TablePart * GJODocumentParser::appendTablePart(const int ownerId, TablePart *tablePart)
{
  int gjoTreeId = gjoTree->makeObjectId();

  //������ �������������� gjoTree
  tablePart->setGJOTreeId(gjoTreeId);
  
  GJO::TablePart *gjoTP= new GJO::TablePart(gjoTreeId,
    tablePart->getTranslates()->value(gjoTree->languageId()),
    gjoTree->makeTranslationId(),
    ownerId,
    0,
    tablePart->getSystemName());

  gjoTree->appendObject(gjoTP);

  //���������� � ������ ����������� ������ ��������� �����
  //Id
  QVariant data;
  data.setValue(tablePart->getId());
  GJO::Trait *gjoTrait = new GJO::Trait(0,
    "Id",
    gjoTree->makeTranslationId(),
    gjoTP->id(),
    0);

  gjoTrait->setValue(data);
  gjoTree->appendTrait(gjoTrait); 

  //���������� ��������� ��������� �����
  QMap<int, QString>::iterator j = tablePart->getTranslates()->begin();
  while (j != tablePart->getTranslates()->end())
  {
    //���� ������� ��� ��������� �� ����������, �� �� ����������� � ������
    if ( j.key() != gjoTree->languageId() )
    {             
      gjoTree->appendTranslation(gjoTP->translationId(), j.key(), j.value().toLocal8Bit().data());
    }

    j++;
  }

  return gjoTP;
}

GJO::Trait * GJODocumentParser::appendLink(const int ownerId, const int linkedObjectId)
{ 
  GJO::Link *newGJOLink = new GJO::Link(0,
    QString("%1%2").arg("Link").arg(gjoTree->traits(ownerId).size() + 1),
    gjoTree->makeTranslationId(),
    ownerId,
    0);

  gjoTree->appendTrait(newGJOLink);

  QVariant data;
  data.setValue(linkedObjectId);
  GJO::Trait *gjoTrait = new GJO::Trait(0,
    "Id",
    gjoTree->makeTranslationId(),
    newGJOLink->id(),
    0);

  gjoTrait->setValue(data);
  gjoTree->appendTrait(gjoTrait);

  return newGJOLink;
}

GJO::Script * GJODocumentParser::appendScript(const int ownerId, Script *script)
{    
  int gjoId = gjoTree->makeScriptId();
  script->setGJOTreeId(gjoId);

  //GJO::Script *gjoScript = new GJO::Script(gjoId, ownerId, script->getBody());
  GJO::Script *gjoScript = new GJO::Script(gjoId, ownerId);
  gjoScript->fromXML(script->getBody());
  gjoTree->appendScript(gjoScript);

  return gjoScript;
}

GJO::Module * GJODocumentParser::appendGlobalModule(Documents::Script *script)
{
  if ( !script ) return NULL;
  int gjoId = gjoTree->makeModuleId();

  GJO::Module *newGlobalModule = new GJO::Module(
    gjoId,
    script->getTranslates()->value(gjoTree->languageId()),
    gjoTree->makeTranslationId(),
    GlobalModuleTypeId);

  appendScript(gjoId, script);  

  //���������� ��������� ��������� �����
  QMap<int, QString>::iterator j = script->getTranslates()->begin();
  while ( j != script->getTranslates()->end() )
  {
    //���� ������� ��� ������� �� ����������, �� �� ����������� � ������
    if ( j.key() != gjoTree->languageId() )
    {             
      gjoTree->appendTranslation(newGlobalModule->translationId(), j.key(), j.value());
    }

    j++;
  }

  gjoTree->appendModule(newGlobalModule);

  return newGlobalModule;
}

void GJODocumentParser::removeGlobalModule(int gjoId)
{  
  gjoTree->removeModuleById(gjoTree->scriptById(gjoId)->parentId());
}

Accessory * GJODocumentParser::createAccessory(GJO::Property *gjoAcc)
{
  if ( !gjoAcc ) return NULL;
  
  Accessory *newAcc = new Accessory();

  newAcc->setGJOTreeId(gjoAcc->id());
  newAcc->setSystemName(((GJO::Accessory*)gjoAcc)->databaseName());

  //std::cout << QString("Accessory * GJODocumentParser::createAccessory(GJO::Property *gjoAcc)\ngjoId=%1\ngjoParentId=%2\nsystemName=%3").arg(gjoAcc->id()).arg(gjoAcc->parentId()).arg(((GJO::Accessory*)gjoAcc)->databaseName()).toLocal8Bit().data() << std::endl;
 
  GJO::Trait *gjoTrait = gjoTree->traitByParentIdAndName(gjoAcc->id(), "Id");
  if ( !gjoTrait )
  {
    delete newAcc;
    return NULL;
  }

  QVariant data = gjoTrait->value();//gjoTree->traitByParentIdAndName(gjoAcc->id(), "Id")->value();
  //QVariant data = gjoTree->traitValue(gjoTree->trait(gjoAcc->id(), "Id")->id());
  newAcc->setId(data.toInt());

  gjoTrait = gjoTree->traitByParentIdAndName(gjoAcc->id(), "Flags");
  if ( !gjoTrait )
  {
    delete newAcc;
    return NULL;
  }

  //data = gjoTree->traitByParentIdAndName(gjoAcc->id(), "Flags")->value();
  //data = gjoTree->traitValue(gjoTree->trait(gjoAcc->id(), "Flags")->id());
  data = gjoTrait->value();
  newAcc->setFlags(data.toInt());
  
  newAcc->getTranslates()->insert(1, gjoTree->translation(gjoAcc->translationId(), 1));
  newAcc->getTranslates()->insert(2, gjoTree->translation(gjoAcc->translationId(), 2));
  
  //��������� ������ �����������
  QString limits = gjoTree->propertyLimits(gjoAcc->id());
  QStringList constraints = limits.split(";", QString::SkipEmptyParts);

  //������ ������ � ��������� �����������
  switch( gjoAcc->typeId() )
  {
    case NumberDataTypeId:
    {
      Number *newType = new Number();

      for ( QStringList::iterator i = constraints.begin(); i != constraints.end(); i++ )
      {
        QString curItem = (*i);
        QString value = curItem.right(curItem.length() - curItem.indexOf(QChar('=')) - 1);
        if ( curItem.startsWith("Length") )
        {          
          int length = value.toInt();
          newType->setLength(length);
        }
        else
        {
          if ( curItem.startsWith("Precision") )
          {            
            int precision = value.toInt();           
            newType->setPrecision(precision);
          }
          else
          {
            if ( curItem.startsWith("Default") )
            {
              int defaultValue = value.toInt();
              newType->setDefault(defaultValue);
            }
            else
            {
              if ( curItem.startsWith("Required") )
              {
                int required = value.toInt();
                newType->setRequired((bool)required);
              }
              else
              {
                if ( curItem.startsWith("Min") )
                {
                  int min = value.toInt();
                  newType->setMin(min);
                }
                else
                {
                  if ( curItem.startsWith("Max") )
                  {
                    int max = value.toInt();
                    newType->setMax(max);
                  }
                }
              }
            }
          }
        }
      }

      newAcc->setType(newType);
    }
    break;

    case IntegerDataTypeId:
    {
      Integer *newType = new Integer();

      for ( QStringList::iterator i = constraints.begin(); i != constraints.end(); i++ )
      {
        QString curItem = (*i);
        QString value = curItem.right(curItem.length() - curItem.indexOf(QChar('=')) - 1);
        
        if ( curItem.startsWith("Default") )
        {
          int defaultValue = value.toInt();
          newType->setDefault(defaultValue);
        }
        else
        {
          if ( curItem.startsWith("Required") )
          {
            int required = value.toInt();
            newType->setRequired((bool)required);
          }
          else
          {
            if ( curItem.startsWith("Min") )
            {
              int min = value.toInt();
              newType->setMin(min);
            }
            else
            {
              if ( curItem.startsWith("Max") )
              {
                int max = value.toInt();
                newType->setMax(max);
              }
            }
          }
        }
      }

      newAcc->setType(newType);
    }
    break;

    case RealDataTypeId:
    {
      Float *newType = new Float();

      for ( QStringList::iterator i = constraints.begin(); i != constraints.end(); i++ )
      {
        QString curItem = (*i);
        QString value = curItem.right(curItem.length() - curItem.indexOf(QChar('=')) - 1);

        if ( curItem.startsWith("Default") )
        {
          double defaultValue = value.toDouble();
          newType->setDefault(defaultValue);
        }
        else
        {
          if ( curItem.startsWith("Required") )
          {
            int required = value.toInt();
            newType->setRequired((bool)required);
          }
          else
          {
            if ( curItem.startsWith("Min") )
            {
              double min = value.toDouble();
              newType->setMin(min);
            }
            else
            {
              if ( curItem.startsWith("Max") )
              {
                double max = value.toDouble();
                newType->setMax(max);
              }
            }
          }
        }
      }

      newAcc->setType(newType);
    }
    break;

    case StringDataTypeId:
    {
      String *newType = new String();

      for ( QStringList::iterator i = constraints.begin(); i != constraints.end(); i++ )
      {
        QString curItem = (*i);
        QString value = curItem.right(curItem.length() - curItem.indexOf(QChar('=')) - 1);
        
        if ( curItem.startsWith("Length") )
        {          
          int length = value.toInt();
          newType->setLength(length);
        }
        else
        {
          if ( curItem.startsWith("Default") )
          {
            QString defaultValue = value;
            newType->setDefault(defaultValue);
          }
          else
          {
            if ( curItem.startsWith("Required") )
            {
              int required = value.toInt();
              newType->setRequired((bool)required);
            }
          }
        }
      }
      
      newAcc->setType(newType);
    }
    break;

    case DateTimeDataTypeId:
    {      
      DateTime *newType = new DateTime();
      
      for ( QStringList::iterator i = constraints.begin(); i != constraints.end(); i++ )
      {
        QString curItem = (*i);
        QString value = curItem.right(curItem.length() - curItem.indexOf(QChar('=')) - 1);

        if ( curItem.startsWith("Default") )
        {
          QDateTime defaultValue = QDateTime::fromString(value, "dd.MM.yyyy hh:mm:ss");
          newType->setDefault(defaultValue);
        }
        else
        {
          if ( curItem.startsWith("Required") )
          {
            int required = value.toInt();
            newType->setRequired((bool)required);
          }
        }
      }
     
      newAcc->setType(newType);
    }
    break;

    case BooleanDataTypeId:
    {
      Logical *newType = new Logical();

      for ( QStringList::iterator i = constraints.begin(); i != constraints.end(); i++ )
      {
        QString curItem = (*i);
        QString value = curItem.right(curItem.length() - curItem.indexOf(QChar('=')) - 1);

        if ( curItem.startsWith("Default") )
        {
          int defaultValue = value.toInt();        
          newType->setDefault((bool)defaultValue);
        }
        else
        {
          if ( curItem.startsWith("Required") )
          {
            int required = value.toInt();
            newType->setRequired((bool)required);
          }
        }
      }
   
      newAcc->setType(newType);
    }
    break;

    case LinkDataTypeId:
	{
	  Link *newType = new Link();

	  for ( QStringList::iterator i = constraints.begin(); i != constraints.end(); i++ )
	  {
		QString curItem = (*i);
		QString value = curItem.right(curItem.length() - curItem.indexOf(QChar('=')) - 1);

		if ( curItem.startsWith("LinkedTableName") )
		{
			QString linkedTableName = value;        
			newType->setLinkedTableName(linkedTableName);
		}
		else if ( curItem.startsWith("LinkedTableRecordId") )
		{
			int linkedTableRecordId = value.toInt();
			newType->setLinkedTableRecordId(linkedTableRecordId);
		}
		else if ( curItem.startsWith("Flag") )
		{
			int flag = value.toInt();
			newType->setFlag(flag);
		}
	  }

	  newAcc->setType(newType);
	}
	break;
    default:;
  }

  return newAcc;
}

Form * GJODocumentParser::createForm(GJO::Object *gjoForm)
{
  if ( !gjoForm ) return NULL; 

  Form *newForm = new Form();

  newForm->setGJOTreeId(gjoForm->id());
  newForm->setSystemName(((GJO::Form*)gjoForm)->databaseName());

  QVariant data = gjoTree->trait(gjoForm->id(), "Id");
  GJO::Trait *gjoTrait = gjoTree->traitByParentIdAndName(gjoForm->id(), "Id");
  if ( !gjoTrait )
  {
    delete newForm;
    return NULL;
  }
  data = gjoTrait->value();
  newForm->setId(data.toInt());

  //data = gjoTree->trait(gjoForm->id(), "Kind");
  gjoTrait = gjoTree->traitByParentIdAndName(gjoForm->id(), "Kind");
  if ( !gjoTrait )
  {
    delete gjoForm;
    return NULL;
  }
  data = gjoTrait->value();
  newForm->setKind(data.toInt());

  newForm->getTranslates()->insert(1, gjoTree->translation(gjoForm->translationId(), 1));
  newForm->getTranslates()->insert(2, gjoTree->translation(gjoForm->translationId(), 2)); 

  //�������� �������
  GJO::Script *gjoScript = gjoTree->scriptByParentId(gjoForm->id());
  if ( gjoScript )
  {    
    Script newScript(gjoScript->toXML());
    newScript.setGJOTreeId(gjoScript->id());
    newForm->setModule(newScript);
  }

  Script newScript(gjoTree->xmlView(gjoForm->id()));
  //newScript.setBody(gjoTree->xmlView(gjoForm->id()));
  newForm->setBody(newScript);

  return newForm;
}

void GJODocumentParser::scanDocument(Document *document)
{
  if ( !document ) return;

  PunyCode pc;

  //������ ��������� ������
  for ( int i = 0; i < document->getTableParts()->size(); i++ )
  {
    if ( document->getTableParts()->value(i)->getGJOTreeId() == -1 )
    {
      QString systemName = pc.encode(document->getTableParts()->value(i)->getTranslates()->value(1));
      document->getTableParts()->value(i)->setSystemName(systemName);

      appendTablePart(document->getGJOTreeId(), document->getTableParts()->value(i));         
    }
  }

  //������ ����������
  for ( int i = 0; i < document->getAccessories()->size(); i++ )
  {
    if ( document->getAccessories()->value(i)->getGJOTreeId() == -1 )
    {
      QString systemName = pc.encode(document->getAccessories()->value(i)->getTranslates()->value(1));
      document->getAccessories()->value(i)->setSystemName(systemName);

      if ( document->getAccessories()->value(i)->getTablePartId() == -1 )
      {
        appendProperty(document->getGJOTreeId(), document->getAccessories()->value(i));        
      }
      else
      {
        //������ �������������� ��������� ��������� ����� ���������
        for ( int j = 0; j < document->getTableParts()->size(); j++ )
        {
          if ( document->getAccessories()->value(i)->getTablePartId() == document->getTableParts()->value(j)->getId())
          {
            appendProperty(document->getTableParts()->value(j)->getGJOTreeId(), document->getAccessories()->value(i));           
          }
        }
      }
    }
  }

  //������ ����
  for ( int i = 0; i < document->getForms()->size(); i++ )
  {
    if ( document->getForms()->value(i)->getGJOTreeId() == -1 )
    {
      QString systemName = pc.encode(document->getForms()->value(i)->getTranslates()->value(1));
      document->getForms()->value(i)->setSystemName(systemName);

      appendForm(document->getGJOTreeId(), document->getForms()->value(i));
    }
  }
}

void GJODocumentParser::scanDocument(DocumentsJournal *document)
{
  if ( !document ) return;
  
  //������ ����
  for ( int i = 0; i < document->getForms()->size(); i++ )
  {
    if ( document->getForms()->value(i)->getGJOTreeId() == -1 )
    {
      appendForm(document->getGJOTreeId(), (*document->getForms())[i]);
    }
  }

  //������ ������ �� �������������� ���������  
  QMap<int, BaseDocument*>::iterator j = document->getRegisteredDocuments()->begin();
  while ( j != document->getRegisteredDocuments()->end() )
  {    
    int gjoId = appendLink(document->getGJOTreeId(), j.key())->id();

    //������������ �������������� ���������� ���������
    QList<Accessory*> *registeredAccessoriesList = document->getDocumentAccessories(j.key());
    for ( int i = 0; i < registeredAccessoriesList->size(); i++ )
    {        
      Accessory *curAcc = registeredAccessoriesList->value(i);        
      gjoTree->setPropertyValue(appendProperty(gjoId, curAcc)->id(), curAcc->getTranslates()->value(gjoTree->languageId()).toLocal8Bit().data());
    }
    
    j++;
  }
}

void GJODocumentParser::scanDocument(Register *document)
{
  if ( !document ) return;  

  //������ ������ ��������
  for ( int i = 0; i < document->getFacts()->size(); i++)
  {
    if ( document->getFacts()->value(i)->getGJOTreeId() == -1 )
    {
      appendProperty(document->getGJOTreeId(), (*document->getFacts())[i]);                
    }
  }

  //������ ��������� 
  QMap<int, BaseDocument*>::iterator j = document->getDimensions()->begin();
  while ( j != document->getDimensions()->end() )
  {
    appendLink(document->getGJOTreeId(), j.key());    
    
    j++;
  }
}

void GJODocumentParser::scanDocument(Enumeration *document)
{
  if ( !document ) return;  

  //������ ��������� ������������  
  for ( int i = 0; i < document->getItems()->size(); i++ )
  {
    EnumItem *curEI = document->getItems()->value(i);
    //����������� ����� ���������� �������� � ������ ���
    if ( curEI->getGJOTreeId() == -1 )
    {      
      GJO::Trait *gjoTrait = new GJO::Trait(0,
        curEI->getTranslates()->value(gjoTree->languageId()).toLocal8Bit().data(),
        gjoTree->makeTranslationId(),
        document->getGJOTreeId(),
        -1);

      gjoTree->appendTrait(gjoTrait);

      QMap<int, QString>::iterator j = curEI->getTranslates()->begin();
      while ( j != curEI->getTranslates()->end() )
      {
        //���� ������� ��� �������� �� ����������, �� �� ����������� � ������
        if ( j.key() != gjoTree->languageId() )
        {
          gjoTree->appendTranslation(gjoTrait->translationId(), j.key(), j.value().toLocal8Bit().data());
        }

        j++;
      }     
    }
  }
}

void GJODocumentParser::scanDocument(Constant *document)
{
  if ( !document ) return;  

  //������ ���� ���������
  int typeId = 0;
  QMap<int, int> accessoryTypesMatch;
  accessoryTypesMatch.insert(DATA_TYPE_NUMBER,   NumberDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_STRING,   StringDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_DATETIME, DateTimeDataTypeId);
  accessoryTypesMatch.insert(DATA_TYPE_BOOLEAN,  BooleanDataTypeId);  

  if ( document->getType() )
  {
    typeId = accessoryTypesMatch.value(document->getType()->getTypeKind());
  }

  QVariant data;
  data.setValue(typeId);
  GJO::Trait *gjoTrait = new GJO::Trait(0,
    "Type",
    gjoTree->makeTranslationId(),
    document->getGJOTreeId(),
    0);

  gjoTrait->setValue(data);
  gjoTree->appendTrait(gjoTrait);
}

void GJODocumentParser::scanDocument(DForm *document)
{
  if ( !document )
  {
    return;
  }

  //������ ����
  for ( int i = 0; i < document->getForms()->size(); i++ )
  {
    if ( document->getForms()->value(i)->getGJOTreeId() == -1 )
    {
      appendForm(document->getGJOTreeId(), document->getForms()->value(i));          
    }
  }
}

void GJODocumentParser::scanDocument(DTable *document)
{
  if ( !document )
  {
    return;
  }
 
  PunyCode pc;

  //������ ����������
  for ( int i = 0; i < document->getAccessories()->size(); i++ )
  {
    if ( document->getAccessories()->value(i)->getGJOTreeId() == -1 )
    {
      QString systemName = pc.encode(document->getAccessories()->value(i)->getTranslates()->value(1));
      document->getAccessories()->value(i)->setSystemName(systemName);      
      
      appendProperty(document->getGJOTreeId(), document->getAccessories()->value(i));    
    }
  }
}

void GJODocumentParser::scanDocument(DQuery *document)
{
  if ( !document )
  {
    return;
  }

  //������ ��������
  /*for ( int i = 0; i < document->getScripts()->size(); i++ )
  {
    if ( document->getScripts()->value(i)->getGJOTreeId() == -1 )
    {
      appendScript(document->getGJOTreeId(), document->getScripts()->value(i));          
    }
  }*/
}

void GJODocumentParser::scanDocument(DClassModule *document)
{
  if ( !document )
  {
    return;
  }

  //������ ��������
  for ( int i = 0; i < document->getScripts()->size(); i++ )
  {
    if ( document->getScripts()->value(i)->getGJOTreeId() == -1 )
    {
      appendScript(document->getGJOTreeId(), document->getScripts()->value(i));          
    }
  }
}

void GJODocumentParser::scanDocument(DGlobalThread *document)
{
  if ( !document )
  {
    return;
  }

  Script script(*document->getScript());

  int gjoId = gjoTree->makeModuleId();
  GJO::Module *newGlobalThreadModule = new GJO::Module(
    gjoId,
    script.getTranslates()->value(gjoTree->languageId()),
    gjoTree->makeTranslationId(),
    GlobalThreadTypeId);

  appendScript(gjoId, &script);  

  //���������� ��������� �������
  QMap<int, QString>::iterator j = script.getTranslates()->begin();
  while ( j != script.getTranslates()->end() )
  {
    //���� ������� ��� ������� �� ����������, �� �� ����������� � ������
    if ( j.key() != gjoTree->languageId() )
    {             
      gjoTree->appendTranslation(newGlobalThreadModule->translationId(), j.key(), j.value());
    }

    j++;
  }

  gjoTree->appendModule(newGlobalThreadModule);
}

int GJODocumentParser::scanDocument(DGlobalContainer *document)
{
  if ( !document )
  {
    return -1;
  }

  Script script(*document->getScript());

  int gjoId = gjoTree->makeModuleId();
  GJO::Module *newGlobalContainerModule = new GJO::Module(
    gjoId,
    script.getTranslates()->value(gjoTree->languageId()),
    gjoTree->makeTranslationId(),
    GlobalContainerTypeId);

  appendScript(gjoId, &script);  

  //���������� ��������� �������
  QMap<int, QString>::iterator j = script.getTranslates()->begin();
  while ( j != script.getTranslates()->end() )
  {
    //���� ������� ��� ������� �� ����������, �� �� ����������� � ������
    if ( j.key() != gjoTree->languageId() )
    {             
      gjoTree->appendTranslation(newGlobalContainerModule->translationId(), j.key(), j.value());
    }

    j++;
  }

  gjoTree->appendModule(newGlobalContainerModule);

  return gjoId; 
}

void GJODocumentParser::scanDocument(DTrigger *document)
{
}

void GJODocumentParser::scanDocument(DTimeManager *document)
{
}

void GJODocumentParser::loadDocumentContent(int gjoId, Document *document)
{
  if ( !document ) return;  

  GJO::PropertyList propList = gjoTree->properties(gjoId);
  for ( int i = 0; i < propList.size(); i++ )
  {
    GJO::Property *curGJOProperty = propList.value(i);
    if ( (curGJOProperty->typeId() != -1) && (curGJOProperty->kind() == GJO::AccessoryKind) )
    {          
      Accessory *newAcc = createAccessory(curGJOProperty);
      document->getAccessories()->append(newAcc);
    }
  }

  //�������� ��������� ������ ���������
  GJO::ObjectList objList = gjoTree->objects(gjoId);
  for ( int i = 0; i < objList.size(); i++ )
  {
    GJO::Object *gjoObj = objList.value(i);
    switch( gjoObj->kind() )
    {
      case GJO::TablePartKind:
      {            
        TablePart *newTP = new TablePart();

        QVariant data = gjoTree->traitByParentIdAndName(gjoObj->id(), "Id")->value();      
        newTP->setId(data.toInt());

        newTP->setGJOTreeId(gjoObj->id());
        newTP->setSystemName(((GJO::TablePart*)gjoObj)->databaseName());

        newTP->getTranslates()->insert(1, gjoTree->translation(gjoObj->translationId(), 1));
        newTP->getTranslates()->insert(2, gjoTree->translation(gjoObj->translationId(), 2));

        document->getTableParts()->append(newTP);            

        //��������� ������ ���������� ��
        GJO::PropertyList propList = gjoTree->properties(gjoObj->id());
        for ( int j = 0; j < propList.size(); j++ )
        {
          GJO::Property *curGJOAcc = propList.value(j);
          if ( curGJOAcc->typeId() != -1 )
          {
            Accessory *newAcc = createAccessory(curGJOAcc);
            //��������� �������������� ��������� ��
            newAcc->setTablePartId(data.toInt());
            document->getAccessories()->append(newAcc);                                      
          }
        }
      }
      break;

      case GJO::FormKind:
      {            
        Form *newForm = createForm(gjoObj);
        document->getForms()->append(newForm);        
      }
      break;

      default:;
    }
  }      
}

void GJODocumentParser::loadDocumentContent(int gjoId, DocumentsJournal *document)
{
  if ( !document ) return;  

  //�������� ��������� ������ ���������
  GJO::ObjectList objList = gjoTree->objects(gjoId);
  for ( int i = 0; i < objList.size(); i++ )
  {
    GJO::Object *gjoObj = objList.value(i);
    switch( gjoObj->kind() )
    {
      case GJO::FormKind:
      {
        Form *newForm = createForm(gjoObj);
        document->getForms()->append(newForm);        
      }
      break;     

      default:;
    }
  }

  //�������� ������
  GJO::TraitList traitList = gjoTree->traits(gjoId);
  for ( int i = 0; i < traitList.size(); i++ )
  {
    GJO::Trait *gjoTrait = traitList.value(i);
    switch( gjoTrait->kind() )
    {
      case GJO::LinkKind:
      {
        //������ ������
        QVariant data = gjoTree->traitByParentIdAndName(gjoTrait->id(), "Id")->value();      
        document->getRegisteredDocuments()->insert(data.toInt(), NULL);

        //��������� ������ ������� ������� �������
        GJO::PropertyList gjoPropList = gjoTree->properties(gjoTrait->id());
        QList<Accessory*> newAccList;
        for ( int j = 0; j < gjoPropList.size(); j++ )
        {
          Accessory *newAcc = createAccessory(gjoPropList.value(j));          
          newAccList.append(newAcc);          
        }

        document->getRegisteredAccessories()->insert(data.toInt(), newAccList);
      }
      break;

      default:;
    }      
  }
}

void GJODocumentParser::loadDocumentContent(int gjoId, Register *document)
{
  if ( !document ) return;
 
  //�������� ������� ���������
  GJO::PropertyList propList = gjoTree->properties(gjoId);
  for ( int i = 0; i < propList.size(); i++ )
  {
    GJO::Property *curGJOAcc = propList.value(i);
    if ( curGJOAcc->typeId() != -1 )
    {          
      Accessory *newAcc = createAccessory(curGJOAcc);
      document->getFacts()->append(newAcc);      
    }
  }  

  //�������� ������
  GJO::TraitList traitList = gjoTree->traits(gjoId);
  for ( int i = 0; i < traitList.size(); i++ )
  {
    GJO::Trait *gjoTrait = traitList.value(i);
    switch( gjoTrait->kind() )
    {
      case GJO::LinkKind:
      {
        //������ ������
        QVariant data = gjoTree->traitByParentIdAndName(gjoTrait->id(), "Id")->value();       
        document->getDimensions()->insert(data.toInt(), NULL);       
      }
      break;

      default:;
    }      
  }
}

void GJODocumentParser::loadDocumentContent(int gjoId, Enumeration *document)
{
  if ( !document ) return;
  
  //�������� ��������� ������ ���������
  GJO::TraitList traitList = gjoTree->traits(gjoId);
  for ( int i = 0; i < traitList.size(); i++ )
  {
    GJO::Trait *curTrait = traitList.value(i);
    if ( curTrait->typeId() == -1 )
    {
      EnumItem *newEI = new EnumItem();
      newEI->getTranslates()->clear();
      newEI->getTranslates()->insert(1, gjoTree->translation(curTrait->translationId(), 1).toLocal8Bit().data());
      newEI->getTranslates()->insert(2, gjoTree->translation(curTrait->translationId(), 2).toLocal8Bit().data());
      document->getItems()->append(newEI);
    }
  } 
}

void GJODocumentParser::loadDocumentContent(int gjoId, Constant *document)
{
  if ( !document ) return;
  
  //�������� ��������� ������ ���������
  QVariant data = gjoTree->traitByParentIdAndName(gjoId, "Type")->value();
  switch( data.toInt() )
  {
    case NumberDataTypeId:
    {
      document->setType(new Number());
    }
    break;

    case StringDataTypeId:
    {
      document->setType(new String());
    }
    break;

    case DateTimeDataTypeId:
    {
      document->setType(new DateTime());
    }
    break;

    case BooleanDataTypeId:
    {
      document->setType(new Logical());
    }
    break;

    default:;
  } 
}

void GJODocumentParser::loadDocumentContent(int gjoId, DForm *document)
{
  if ( !document ) return;
 
  GJO::ObjectList objList = gjoTree->objects(gjoId);
  for ( int i = 0; i < objList.size(); i++ )
  {
    GJO::Object *gjoObj = objList.value(i);
    switch( gjoObj->kind() )
    {
      case GJO::FormKind:
      {            
        Form *newForm = createForm(gjoObj);
        document->getForms()->append(newForm);        
      }
      break;

      default:;
    }
  }      
}

void GJODocumentParser::loadDocumentContent(int gjoId, DTable *document)
{
  if ( !document ) return;

  GJO::PropertyList propList = gjoTree->properties(gjoId);
  for ( int i = 0; i < propList.size(); i++ )
  {
    GJO::Property *curGJOProperty = propList.value(i);
    if ( (curGJOProperty->typeId() != -1) && (curGJOProperty->kind() == GJO::AccessoryKind) )
    {          
      Accessory *newAcc = createAccessory(curGJOProperty);
      document->getAccessories()->append(newAcc);
    }
  }  
}

void GJODocumentParser::loadDocumentContent(int gjoId, DQuery *document)
{
  if ( !document ) return;  

  GJO::Script *gjoScript = gjoTree->scriptByParentId(gjoId);

  if ( !gjoScript )
  {
    return;
  }

  Script *newScript = new Script(gjoScript->script());
  newScript->setGJOTreeId(gjoScript->id());

  document->getScripts()->append(newScript);  
}

void GJODocumentParser::loadDocumentContent(int gjoId, DClassModule *document)
{
  if ( !document ) return;  

  GJO::Script *gjoScript = gjoTree->scriptByParentId(gjoId);

  if ( !gjoScript )
  {
    return;
  }

  Script *newScript = new Script(gjoScript->script());
  newScript->setGJOTreeId(gjoScript->id());

  document->getScripts()->append(newScript);
}

void GJODocumentParser::loadDocumentContent(int gjoId, DGlobalThread *document)
{
  if ( !document ) return;

  //GJO::Module *gjoModule = gjoTree->moduleById(gjoId);

  //if ( gjoModule )
  //{
  //  //�������� ���������
  //  document->getTranslates()->insert(1, gjoTree->translation(gjoModule->translationId(), 1));
  //  document->getTranslates()->insert(2, gjoTree->translation(gjoModule->translationId(), 2));
  //} 
}

void GJODocumentParser::loadDocumentContent(int gjoId, DGlobalContainer *document)
{
  if ( !document ) return;

  //GJO::Module *gjoModule = gjoTree->moduleById(gjoId);  

  ////�������� ���������
  //document->getTranslates()->insert(1, gjoTree->translation(gjoModule->translationId(), 1));
  //document->getTranslates()->insert(2, gjoTree->translation(gjoModule->translationId(), 2));  
}

void GJODocumentParser::loadDocumentContent(int gjoId, DTrigger *document)
{
  if ( !document ) return;  

  /*GJO::Script *gjoScript = gjoTree->scriptByParentId(gjoId);

  if ( !gjoScript )
  {
    return;
  }*/
}

void GJODocumentParser::loadDocumentContent(int gjoId, DTimeManager *document)
{
  if ( !document ) return;  

  /*GJO::Script *gjoScript = gjoTree->scriptByParentId(gjoId);

  if ( !gjoScript )
  {
    return;
  }*/
}
