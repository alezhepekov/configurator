#include "column.h"

using namespace ConfiguratorObjects::Documents;
using namespace ConfiguratorObjects::SQL;

Column::Column(const QString &columnName, const Accessory &value, const Index &indexValue)
{
  name = columnName;
  accessory = value;  
  index = indexValue;
}

Column::~Column()
{  
}

QString Column::getName()
{
  return name;
}

void Column::setName(const QString& newName)
{
  name = newName;
}

Accessory * Column::getAccessory()
{
  return &accessory;
}

void Column::setAccessory(const Accessory &newAccessory)
{
  accessory = newAccessory;
}

void Column::setIndex(const Index &newIndex)
{
  index = newIndex;
}

Index * Column::getIndex()
{
  return &index;
}
