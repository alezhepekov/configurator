/***********************************************************************
* Module:   dbcontrol.h
* Author:   LGP
* Modified: 27 ��� 2007 �.
* Purpose:  ����� �������� �������������� � �� ���������� ������������
***********************************************************************/

#ifndef DB_CONTROL_H
#define DB_CONTROL_H

#include <QString>
#include <QStringList>
#include <QList>
#include <QMap>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>
#include <QFile>
#include <QTextStream>
#include <QVariant>
#include <iostream>

#include "documents/document.h"
#include "documents/dictionary.h"
#include "documents/report.h"
#include "documents/documentsjournal.h"
#include "documents/register.h"
#include "documents/enumeration.h"
#include "documents/constant.h"
#include "documents/dform.h"
#include "documents/dtable.h"
#include "documents/dquery.h"
#include "documents/dclassmodule.h"
#include "documents/dglobalthread.h"
#include "documents/dglobalcontainer.h"
#include "documents/dclassmodule.h"
#include "documents/dtrigger.h"
#include "documents/dtimemanager.h"

#include "configurationheader.h"

namespace ConfiguratorObjects
{
  const int NO_ERRORS = 0x0000;

  class DBControl: public QObject
  {
    //��������� �����������
    ConnectionInfo dbConnectionInfo;

    //������� ���� ����������
    QSqlDatabase currentDatabase;

    //��������� ��������� ������ ��������� � ��
    QString lastError;

  public:
    DBControl(); 
    ~DBControl();

    //����� ��������� ����������� � ������� ���� � �������� ��
    bool createDatabase(ConnectionInfo& );

    //����� ��������� ����������� � ��
    bool connect(ConnectionInfo &);

  private:
    //����� ���������� ���������� ������� �������
    static int recordCount(QSqlQuery &query, const QString& tableName);

  public:
    int getRecordCount(const QString &tableName);
    static int getRecordCount(QSqlDatabase &database, const QString &tableName);

  private:
    //����� ���������� ������������ �������� ������������������
    static int sequenceMaxValue(QSqlQuery &query, const QString &tableName);

  public:
    int getSequenceMaxValue(const QString &tableName);
    static int getSequenceMaxValue(QSqlDatabase &database, const QString &tableName);

  private:
    //����� ���������� ���������� ������� �������
    static int fillSequence(QSqlQuery &query, const QString& tableName);

  public:  
    //����� ��������� ������� �������� ����������� ��� �������� �������
    bool fillIndex(const QString &tableName);
    static bool fillIndex(QSqlDatabase &database, const QString &tableName);  

    //����� ���������� �������� �� ��������������
    Documents::BaseDocument *getDocument(int);

    //����� ��������� ������ ��������� � ������� ��������� ��������������� ���������   
    bool setDocument(Documents::BaseDocument*); 

    //����� ��������� �������� �������� �� ��������������
    bool deleteDocument(int);

    //����� ������� ������ ���� ���������� 
    bool clear();

    //����� ��������� ���������� �������� ������ ��� ��������� � �������� ���������������
    bool addArchiveDocument(int, const QString&);

    //����� ���������� ��� �������� ������ ��� ��������� � �������� ���������������
    QStringList getArchiveDocuments(int);

    //����� ���������� ��������� �����������
    ConnectionInfo getConnectionInfo();

    //����� ���������� ��������� �� ������ �����������
    QSqlDatabase* getDatabase();

    //����� ���������� ��������� �� ������ �������� �������
    QList<Documents::Script*> getConfigurationScripts();

    //����� ������������� ������ ��������
    bool setConfigurationScripts(const QList<Documents::Script*> &scriptList);

    //����� ���������� ��������� ������������
    ConfigurationHeader getHeader();

    //����� ������������� ���������
    bool setHeader(const ConfigurationHeader &newHeader);

    //����� ���������� ��������� ��������� ���������� �������� ��������� � ���� ������ 
    QString getLastError() const;

  private:
    //����� ���������� � ������� translates �������� ������� �
    //���������� ������ � ������ ��������� ����������
    //����� ������������ ������������� �������� � ������� translates
    bool appendToDatabase(const QMap<int, QString> *translates, int &);

    //����� ���������� � ������� Accessories ��������� �������
    bool appendToDatabase(const QList<Documents::Accessory*> *accessories, const int ownerId);

    //����� ���������� � ������� TableParts ��������� ����� �������
    bool appendToDatabase(const QList<Documents::TablePart*> *tableParts, const int ownerId);

    //����� ���������� � ������� Froms ����� �������
    bool appendToDatabase(const QList<Documents::Form*> *forms, const int ownerId);

    //����� ���������� � ������� Scripts ������� ��� ��������� ���������
    bool appendToDatabase(const QList<Documents::Script*> *scripts, const int ownerId);

    //����� ��������� �������� ��������� ��� ��������� ��������� ���������������
    bool removeTranslates(int);

    //����� ��������� �������� ���������� ��� ��������� ��������� ���������������
    bool removeAccessories(int);

    //����� ��������� �������� ��������� ������ ��� ��������� ��������� ���������������
    bool removeTableParts(int);

    //����� ��������� �������� ���� ��� ��������� ��������� ���������������
    bool removeForms(int);

    //����� ��������� �������� �������� ��� ��������� ��������� ���������������
    bool removeScripts(int ownerId);

    //����� ��������� �������� ��������� � ������������ ���������������
    bool takeFromDatabase(QMap<int, QString> *translates, const int translatesId);

    //����� ��������� �������� ���������� ������������� ���������
    bool takeFromDatabase(QList<Documents::Accessory*> *accessories, const int ownerId);

    //����� ��������� �������� ��������� ������ ������������� ���������
    bool takeFromDatabase(QList<Documents::TablePart*> *tableParts, const int ownerId);

    //����� ��������� �������� ���� ������������� ���������
    bool takeFromDatabase(QList<Documents::Form*> *forms, const int ownerId);

    bool takeFromDatabase(QList<Documents::Script*> *scripts, const int ownerId);

    //����� ��������� �������� ��������� ������ ��������� ���� "��������"  
    bool deleteContentForDocumentDocument(int);

    //����� ��������� �������� ��������� ������ ��������� ���� "����������" 
    bool deleteContentForDocumentDictionary(int);

    //����� ��������� �������� ��������� ������ ��������� ���� "�����"
    bool deleteContentForDocumentReport(int);

    //����� ��������� �������� ��������� ������ ��������� ���� "����������������"
    bool deleteContentForDocumentDocumentsJournal(int);

    //����� ��������� �������� ��������� ������ ��������� ���� "�������"
    bool deleteContentForDocumentRegister(int);

    //����� ��������� �������� ��������� ������ ��������� ���� "������������"
    bool deleteContentForDocumentEnumeration(int);

    //����� ��������� �������� ��������� ������ ��������� ���� "���������"
    bool deleteContentForDocumentConstant(int);

    //����� ��������� �������� ��������� ������ ��������� ���� "�����"
    bool deleteContentForDocumentForm(int);

    bool deleteContentForDocumentTable(int docId);
    bool deleteContentForDocumentQuery(int docId);
    bool deleteContentForDocumentClassModule(int docId);
    bool deleteContentForDocumentGlobalThread(int docId);
    bool deleteContentForDocumentGlobalContainer(int docId);
    bool deleteContentForDocumentTrigger(int docId);
    bool deleteContentForDocumentTimeManager(int docId);    

    //����� ��������� ���������� ��������� ������ ��������� ���� "��������"
    bool appendContentForDocument(Documents::Document*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "����������"
    bool appendContentForDocument(Documents::Dictionary*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "�����"
    bool appendContentForDocument(Documents::Report*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "����������������"
    bool appendContentForDocument(Documents::DocumentsJournal*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "�������"
    bool appendContentForDocument(Documents::Register*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "������������"
    bool appendContentForDocument(Documents::Enumeration*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "���������"
    bool appendContentForDocument(Documents::Constant*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "�����"
    bool appendContentForDocument(Documents::DForm*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "�������"
    bool appendContentForDocument(Documents::DTable*);

    //����� ��������� ���������� ��������� ������ ��������� ���� "������"
    bool appendContentForDocument(Documents::DQuery*);

    bool appendContentForDocument(Documents::DClassModule*);
    bool appendContentForDocument(Documents::DGlobalThread*);
    bool appendContentForDocument(Documents::DGlobalContainer*);
    bool appendContentForDocument(Documents::DTrigger*);
    bool appendContentForDocument(Documents::DTimeManager*);

    //����� ��������� �������� ������� �������� ���������
    bool loadContentForBaseDocument(Documents::BaseDocument *document);

    //����� ��������� �������� ��������� ���� "��������" �� ��
    bool loadContentForDocument(Documents::Document*);

    //����� ��������� �������� ��������� ���� "����������" �� ��
    bool loadContentForDocument(Documents::Dictionary*);

    //����� ��������� �������� ��������� ���� "�����" �� ��
    bool loadContentForDocument(Documents::Report*);

    //����� ��������� �������� ��������� ���� "����������������" �� ��
    bool loadContentForDocument(Documents::DocumentsJournal*);

    //����� ��������� �������� ��������� ���� "�������" �� ��
    bool loadContentForDocument(Documents::Register*);

    //����� ��������� �������� ��������� ���� "������������" �� ��
    bool loadContentForDocument(Documents::Enumeration*);

    //����� ��������� �������� ��������� ���� "���������" �� ��
    bool loadContentForDocument(Documents::Constant*);

    //����� ��������� �������� ��������� ���� "�����" �� ��
    bool loadContentForDocument(Documents::DForm*);

    bool loadContentForDocument(Documents::DTable*);
    bool loadContentForDocument(Documents::DQuery*);
    bool loadContentForDocument(Documents::DClassModule*);
    bool loadContentForDocument(Documents::DGlobalThread*);
    bool loadContentForDocument(Documents::DGlobalContainer*);
    bool loadContentForDocument(Documents::DTrigger*);
    bool loadContentForDocument(Documents::DTimeManager*);
  };
}

#endif
