#include "datamanager.h"

#include <iostream>

using namespace ConfiguratorObjects;

DataManager::DataManager()
{
}

DataManager::~DataManager()
{
  disconnect();
}

bool DataManager::connect(const ConnectionInfo &cInfo)
{
  currentDatabase = QSqlDatabase::addDatabase("QPSQL", cInfo.dbName);
  
  currentDatabase.setHostName(cInfo.host);
  currentDatabase.setDatabaseName(cInfo.dbName);
  currentDatabase.setUserName(cInfo.userName);
  currentDatabase.setPassword(cInfo.password);

  return currentDatabase.open();
}

void DataManager::disconnect()
{
  if ( currentDatabase.isOpen() )
  {
    currentDatabase.close();
  }
}

bool DataManager::exec(const QString &sql, QDataStream &data)
{
  if ( !currentDatabase.isOpen() )
  {
    return false;
  }

  QSqlQuery query(currentDatabase);
  query.prepare(sql);

  if ( !query.exec() )
  {
    lastError = query.lastError().text();
    std::cout << QString("!!!ERROR: %1").arg(lastError).toLocal8Bit().data() << std::endl;
    return false;
  }
 
  outData(query, data);

  return true;
}
