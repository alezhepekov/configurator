/***********************************************************************
* Module:   table.h
* Author:   LGP
* Modified: 16 ����� 2007 �.
* Purpose:  ������������� ������� ��
***********************************************************************/

#ifndef TABLE_H
#define TABLE_H

#include <QString>
#include <QList>

#include "column.h"

namespace ConfiguratorObjects
{
  namespace SQL
  {
    class Table
    {
    public:
      Table(const QString &tableName);
      ~Table();

      QString getName();
      void setName(const QString& newName);

      void addColumn(Column *column);
      void removeColumn(const QString &name);

      QList<Column*> *getColumns();

    protected:
    private:
      QList<Column*> columns;
      QString name;
    };
  }
}

#endif
