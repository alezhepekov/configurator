/***********************************************************************
* Module:   configurationheader.h
* Author:   LGP
* Modified: 06 ������ 2007 �.
* Purpose:  ������������� ��������� ������������
***********************************************************************/

#ifndef CONFIGURATION_HEADER_H
#define CONFIGURATION_HEADER_H

#include <QString>
#include <QList>
#include <QDateTime>

#include "role.h"
#include "privilege.h"
#include "domxml.h"
#include "connectioninfo.h"

namespace ConfiguratorObjects
{
  class ConfigurationHeader: public Documents::DomXml
  {
    int id;

    QString name;

    QString author;

    QString version;    

    QDateTime creationDateTime;

    QDateTime lastModificationDateTime;

    QString lastModificationUserName;

    int userDatabaseId;

    QList<Role*> roles;

    QList<Privilege*> privileges;

  public:
    ConfigurationHeader();
    ConfigurationHeader(const ConfigurationHeader &other);
    ~ConfigurationHeader();

    ConfigurationHeader & ConfigurationHeader::operator= (const ConfigurationHeader &right);

    int getId() const;

    void setId(int newId);

    QString getName() const;

    void setName(const QString &newName);

    QString getAuthor() const;

    void setAuthor(const QString &newAuthor);

    QString getVersion() const;

    void setVersion(const QString &newVersion);

    QDateTime getCreationDateTime() const;

    void setCreationDateTime(const QDateTime &newCreationTime);    

    QDateTime getLastModificationDateTime() const;

    void setLastModificationDateTime(const QDateTime &newLastModificationTime);

    QString getLastModificationUserName() const;

    void setLastModificationUserName(const QString &userName);

    int getUserDatabaseId() const;

    void setUserDatabaseId(const int);

    const QList<Privilege*> *getPrivileges() const;

    QList<Privilege*> *getPrivileges();

    void setPriveleges(const QList<Privilege*> &newPrivileges);

    const QList<Role*> *getRoles() const;

    QList<Role*> *getRoles();

    void setRoles(const QList<Role*> &newRoles);

    //����� ��������� ������������ xml-�������������
    QString toXML();

    //����� ��������� ������������ xml-������������� � ���������� ��� � ���������� ���� xml-������
    void toXML(QDomDocument &, QDomElement &);

    //����� ��������� �������� �� xml-�������������
    bool fromXML(const QString&);

    //����� ��������� �������� �� xml-������������� � ���������� ���� xml-������
    void fromXML(const QDomDocument &, const QDomElement &);

  protected:
    void appendContentToXML(QDomDocument &, QDomElement &);

    friend QDataStream & operator<< (QDataStream &, const ConfigurationHeader &);
    friend QDataStream & operator>> (QDataStream &, ConfigurationHeader &);
  };

  QDataStream & operator<< (QDataStream &, const ConfigurationHeader &);
  QDataStream & operator>> (QDataStream &, ConfigurationHeader &);

  struct ConfigurationInfo
  {
    int id; 
    ConnectionInfo connectionInfo;    
    ConfigurationHeader header;
  };

  QDataStream & operator<< (QDataStream &, const ConfigurationInfo &);
  QDataStream & operator>> (QDataStream &, ConfigurationInfo &);   
}

Q_DECLARE_METATYPE(ConfiguratorObjects::ConfigurationInfo);

#endif
