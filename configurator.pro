######################################################################
##           Configurator project file (By LGP 07.03.2007)          ##
######################################################################

TEMPLATE = lib
LANGUAGE = C++

TARGET = configurator
DESTDIR = ./

CONFIG += qt warn_on debug staticlib rtti

DEPENDPATH += . Debug documents generatedfiles documents\types
INCLUDEPATH += . \
               ../../GJOTree/src \
               ../../ConfiguratorComponent/ConfigSettingsForm            

#LIBS += ../../GJOTree/src/gjotree.lib

# Input
HEADERS += abstractconfigurator.h \          
           configuration.h \
           configurator.h \
           dbcontrol.h \
           gjodocumentparser.h \         
           domxml.h \
           configurationlist.h \
           configurationlistform.h \
           newconfigurationform.h \
           configurationheader.h \
           role.h \
           privilege.h \
           punycode.h \
           table.h \
           column.h \
           index.h \
           scriptgenerator.h \
           documents/accessory.h \
           documents/basedocument.h \
           documents/constant.h \
           documents/dform.h \
           documents/dictionary.h \
           documents/document.h \
           documents/documentsjournal.h \
           documents/enumeration.h \
           documents/enumitem.h \
           documents/form.h \
           documents/objectlist.h \
           documents/register.h \
           documents/report.h \
           documents/script.h \
           documents/tablepart.h \
           documents/types/basetype.h \
           documents/types/tdatetime.h \
           documents/types/tfloat.h \
           documents/types/timage.h \
           documents/types/tinteger.h \
           documents/types/tlink.h \
           documents/types/tlogical.h \
           documents/types/tnumber.h \
           documents/types/tnumerator.h \
           documents/types/tstring.h

SOURCES += configuration.cpp \
           configurator.cpp \
           dbcontrol.cpp \
           gjodocumentparser.cpp \          
           domxml.cpp \
           configurationlist.cpp \
           configurationlistform.cpp \
           newconfigurationform.cpp \
           configurationheader.cpp \
           role.cpp \
           privilege.cpp \
           punycode.cpp \
           table.cpp \
           column.cpp \
           index.cpp \
           scriptgenerator.cpp \
           documents/accessory.cpp \
           documents/basedocument.cpp \
           documents/constant.cpp \
           documents/dform.cpp \
           documents/dictionary.cpp \
           documents/document.cpp \
           documents/documentsjournal.cpp \
           documents/enumeration.cpp \
           documents/enumitem.cpp \
           documents/form.cpp \
           documents/objectlist.cpp \
           documents/register.cpp \
           documents/report.cpp \
           documents/script.cpp \
           documents/tablepart.cpp \
           documents/types/basetype.cpp \
           documents/types/tdatetime.cpp \
           documents/types/tfloat.cpp \
           documents/types/timage.cpp \
           documents/types/tinteger.cpp \
           documents/types/tlink.cpp \
           documents/types/tlogical.cpp \
           documents/types/tnumber.cpp \
           documents/types/tnumerator.cpp \
           documents/types/tstring.cpp

FORMS = configurationlistform.ui newconfigurationform.ui
           
RESOURCES += configurator.qrc

CODECFORTR = CP1251

TRANSLATIONS = translates/configurator_en.ts \
               translates/configurator_ru.ts

