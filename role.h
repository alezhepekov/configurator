/***********************************************************************
* Module:   role.h
* Author:   LGP
* Modified: 06 ������ 2007 �.
* Purpose:  ������������� ���� ������������
***********************************************************************/

#ifndef ROLE_H
#define ROLE_H 1

#include <QString>
#include <QDataStream>

namespace ConfiguratorObjects
{
  class Role
  {
  public:
    Role();
    Role(const Role &);
    ~Role();

    /*inline*/ int getId() const;
    /*inline*/ void setId(int newId);

    /*inline*/ QString getExternalId() const;
    /*inline*/ void setExternalId(const QString &newExternalId);

    /*inline*/ QString getName() const;
    /*inline*/ void setName(const QString &newName);

  protected:
  private:
    int id;
   
    QString externalId;

    QString name;
  };

  QDataStream &operator<<(QDataStream &, const Role &);
  QDataStream &operator>>(QDataStream &, Role &);
}

#endif
