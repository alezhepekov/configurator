<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="ru">
<context>
    <name>ConfigurationListForm</name>
    <message>
        <location filename="../configurationlistform.cpp" line="241"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="64"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="231"/>
        <source>Id</source>
        <translation>Идентификатор</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="171"/>
        <source>Host</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="181"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="191"/>
        <source>Database name</source>
        <translation>Имя базы данных</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="201"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="211"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="222"/>
        <source>Header</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="251"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="261"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="271"/>
        <source>Creation date and time</source>
        <translation>Дата и время создания</translation>
    </message>
    <message>
        <location filename="../configurationlistform.ui" line="13"/>
        <source>Configuration choose</source>
        <translation>Выбор конфигурации</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="34"/>
        <source>Load configuration</source>
        <translation>Загрузить конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="17"/>
        <source>Configuration manager</source>
        <translation>Менеджер конфигураций</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="21"/>
        <source>Get configuration list</source>
        <translation>Получить список конфигураций</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="22"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="27"/>
        <source>New configuration</source>
        <translation>Новая конфигурация</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="28"/>
        <source>Create the new configuration</source>
        <translation>Создать новую конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="29"/>
        <source>Ctrl+Alt+Insert</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="35"/>
        <source>Load selected configuration</source>
        <translation>Загрузить выделенную конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="36"/>
        <source>Ctrl+Alt+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="41"/>
        <source>Remove configuration</source>
        <translation>Удалить конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="42"/>
        <source>Remove selected configuration</source>
        <translation>Удалить выделенную конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="43"/>
        <source>Ctrl+Alt+Delete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="63"/>
        <source>Object/Property</source>
        <translation>Объект/Свойство</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="332"/>
        <source>Configuration loaded</source>
        <translation>Конфигурация загружена</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="343"/>
        <source>New configuration command processing...</source>
        <translation>Обработка команды создания новой конфигурации...</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="352"/>
        <source>Get configuration list command processing...</source>
        <translation>Обработка команды получения списка конфигураций...</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="358"/>
        <source>Get configuration command processing...</source>
        <translation>Обработка команды получения конфигурации...</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="364"/>
        <source>Remove configuration command processing...</source>
        <translation>Обработка команды удаления конфигурации...</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="375"/>
        <source>Creating configuration failed !!!</source>
        <translation>Ошибка создания конфигурации !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="379"/>
        <source>Configuration created successfull !!!</source>
        <translation>Конфигурация создана успешно !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="389"/>
        <source>Recieving configuration list failed !!!</source>
        <translation>Ошибка получения списка конфигураций !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="393"/>
        <source>Configuration list recieved  successfull !!!</source>
        <translation>Список конфигураций получен успешно !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="407"/>
        <source>Loading configuration failed !!!</source>
        <translation>Ошибка загрузки конфигурации !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="411"/>
        <source>Configuration recieved  successfull !!!</source>
        <translation>Конфигурация загружена успешно !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="430"/>
        <source>Removing configuration failed !!!</source>
        <translation>Ошибка удаления конфигурации !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="434"/>
        <source>Configuration removed  successfull !!!</source>
        <translation>Конфигурация удалена успешно !!!</translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="125"/>
        <source>Configurations</source>
        <translation>Конфигурации</translation>
    </message>
</context>
<context>
    <name>Configurator</name>
    <message>
        <location filename="../configurator.cpp" line="29"/>
        <source>New configuration</source>
        <translation type="obsolete">Новая конфигурация</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="30"/>
        <source>Create the new configuration</source>
        <translation type="obsolete">Создать новую конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="36"/>
        <source>Open configuration</source>
        <translation type="obsolete">Открыть конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="37"/>
        <source>Open existing configuration</source>
        <translation type="obsolete">Открыть существующую конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="25"/>
        <source>Save configuration</source>
        <translation>Сохранить конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="26"/>
        <source>Save the current configuration</source>
        <translation>Сохранить текущую конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="32"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="33"/>
        <source>Settings of current configuration</source>
        <translation>Настройки текущей конфигурации</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="40"/>
        <source>Add configuration object</source>
        <translation>Добавить объект конфигурации</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="41"/>
        <source>Ctrl+Insert</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="47"/>
        <source>Remove configuration object</source>
        <translation>Удалить объект конфигурации</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="48"/>
        <source>Ctrl+Delete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="68"/>
        <source>Configurator</source>
        <translation>Конфигуратор</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1742"/>
        <source>Accessories</source>
        <translation>Реквизиты</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1574"/>
        <source>Script</source>
        <translation>Скрипт</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="398"/>
        <source>Dimension</source>
        <translation>Измерение</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1830"/>
        <source>Table parts</source>
        <translation>Табличные части</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1902"/>
        <source>Forms</source>
        <translation>Формы</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1961"/>
        <source>Dimensions</source>
        <translation>Измерения</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1996"/>
        <source>Facts</source>
        <translation>Факты</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="713"/>
        <source>Configuration</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1239"/>
        <source>Documents</source>
        <translation>Документы</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1255"/>
        <source>Dictionaries</source>
        <translation>Справочники</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1271"/>
        <source>Reports</source>
        <translation>Отчеты</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1303"/>
        <source>Registers</source>
        <translation>Регистры</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1357"/>
        <source>Document-forms</source>
        <translation>Документы-формы</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1319"/>
        <source>Enumerations</source>
        <translation>Перечисления</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1335"/>
        <source>Constants</source>
        <translation>Константы</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1140"/>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1141"/>
        <source>Save current configuration</source>
        <translation type="obsolete">Сохранить текущую конфигурацию</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1232"/>
        <source>Document</source>
        <translation>Документ</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1248"/>
        <source>Dictionary</source>
        <translation>Справочник</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1264"/>
        <source>Report</source>
        <translation>Отчет</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1279"/>
        <source>DocumentsJournal</source>
        <translation>ЖурналДокументов</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1296"/>
        <source>Register</source>
        <translation>Регистр</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1312"/>
        <source>Enumeration</source>
        <translation>Перечисление</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1328"/>
        <source>Constant</source>
        <translation>Константа</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1456"/>
        <source>Document-form</source>
        <translation type="obsolete">Документ-форма</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1369"/>
        <source>Accessory</source>
        <translation>Реквизит</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1426"/>
        <source>TablePart</source>
        <translation>ТабличнаяЧасть</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1459"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1539"/>
        <source>Fact</source>
        <translation>Факт</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1286"/>
        <source>Documents journals</source>
        <translation>Журналы документов</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="22"/>
        <source>Get configuration list</source>
        <translation type="obsolete">Получить список конфигураций</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="2037"/>
        <source>Scripts</source>
        <translation>Скрипты</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="19"/>
        <source>Configuration manager</source>
        <translation>Менеджер конфигураций</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="20"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="27"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1198"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1007"/>
        <source>Loading configuration failed</source>
        <translation>Ошибка загрузки конфигурации</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1189"/>
        <source>Saving configuration failed</source>
        <translation>Ошибка сохранения конфигурации</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1198"/>
        <source>Releasing configuration failed</source>
        <translation>Ошибка проведения конфигурации</translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1344"/>
        <source>DocumentForm</source>
        <translation>ДокументФорма</translation>
    </message>
</context>
<context>
    <name>NewConfigurationForm</name>
    <message>
        <location filename="../newconfigurationform.ui" line="13"/>
        <source>New configuration</source>
        <translation>Новая конфигурация</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="29"/>
        <source>Connection parameters</source>
        <translation>Параметры подключения</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="41"/>
        <source>Host</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="54"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="67"/>
        <source>Database name</source>
        <translation>Имя базы данных</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="90"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="103"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="149"/>
        <source>Header</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="161"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="184"/>
        <source>Creation data time</source>
        <translation>Дата и время создания</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="207"/>
        <source>ExternalId</source>
        <translation>Внешний ид</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="230"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="253"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
</context>
</TS>
