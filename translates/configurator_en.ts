<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="en">
<context>
    <name>ConfigurationListForm</name>
    <message>
        <location filename="../configurationlistform.cpp" line="241"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="64"/>
        <source>Value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="231"/>
        <source>Id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="171"/>
        <source>Host</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="181"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="191"/>
        <source>Database name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="201"/>
        <source>User</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="211"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="222"/>
        <source>Header</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="251"/>
        <source>Author</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="261"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="271"/>
        <source>Creation date and time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.ui" line="13"/>
        <source>Configuration choose</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="34"/>
        <source>Load configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="17"/>
        <source>Configuration manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="21"/>
        <source>Get configuration list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="22"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="27"/>
        <source>New configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="28"/>
        <source>Create the new configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="29"/>
        <source>Ctrl+Alt+Insert</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="35"/>
        <source>Load selected configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="36"/>
        <source>Ctrl+Alt+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="41"/>
        <source>Remove configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="42"/>
        <source>Remove selected configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="43"/>
        <source>Ctrl+Alt+Delete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="63"/>
        <source>Object/Property</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="332"/>
        <source>Configuration loaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="343"/>
        <source>New configuration command processing...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="352"/>
        <source>Get configuration list command processing...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="358"/>
        <source>Get configuration command processing...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="364"/>
        <source>Remove configuration command processing...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="375"/>
        <source>Creating configuration failed !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="379"/>
        <source>Configuration created successfull !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="389"/>
        <source>Recieving configuration list failed !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="393"/>
        <source>Configuration list recieved  successfull !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="407"/>
        <source>Loading configuration failed !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="411"/>
        <source>Configuration recieved  successfull !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="430"/>
        <source>Removing configuration failed !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="434"/>
        <source>Configuration removed  successfull !!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurationlistform.cpp" line="125"/>
        <source>Configurations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Configurator</name>
    <message>
        <location filename="../configurator.cpp" line="25"/>
        <source>Save configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="26"/>
        <source>Save the current configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="32"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="33"/>
        <source>Settings of current configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="40"/>
        <source>Add configuration object</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="41"/>
        <source>Ctrl+Insert</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="47"/>
        <source>Remove configuration object</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="48"/>
        <source>Ctrl+Delete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="68"/>
        <source>Configurator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1742"/>
        <source>Accessories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1574"/>
        <source>Script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="398"/>
        <source>Dimension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1830"/>
        <source>Table parts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1902"/>
        <source>Forms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1961"/>
        <source>Dimensions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1996"/>
        <source>Facts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="713"/>
        <source>Configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="2037"/>
        <source>Scripts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1239"/>
        <source>Documents</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1255"/>
        <source>Dictionaries</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1271"/>
        <source>Reports</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1286"/>
        <source>Documents journals</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1303"/>
        <source>Registers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1357"/>
        <source>Document-forms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1319"/>
        <source>Enumerations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1335"/>
        <source>Constants</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1232"/>
        <source>Document</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1248"/>
        <source>Dictionary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1264"/>
        <source>Report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1279"/>
        <source>DocumentsJournal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1296"/>
        <source>Register</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1312"/>
        <source>Enumeration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1328"/>
        <source>Constant</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1459"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1369"/>
        <source>Accessory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1426"/>
        <source>TablePart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1539"/>
        <source>Fact</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="19"/>
        <source>Configuration manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="20"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="27"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1198"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1007"/>
        <source>Loading configuration failed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1189"/>
        <source>Saving configuration failed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1198"/>
        <source>Releasing configuration failed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configurator.cpp" line="1344"/>
        <source>DocumentForm</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NewConfigurationForm</name>
    <message>
        <location filename="../newconfigurationform.ui" line="13"/>
        <source>New configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="29"/>
        <source>Connection parameters</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="41"/>
        <source>Host</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="54"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="67"/>
        <source>Database name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="90"/>
        <source>User</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="103"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="149"/>
        <source>Header</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="161"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="184"/>
        <source>Creation data time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="207"/>
        <source>ExternalId</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="230"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newconfigurationform.ui" line="253"/>
        <source>Author</source>
        <translation></translation>
    </message>
</context>
</TS>
