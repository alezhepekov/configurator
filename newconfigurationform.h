#ifndef NEW_CONFIGURATION_FORM_H
#define NEW_CONFIGURATION_FORM_H

#include <QDialog>
#include "generatedfiles/ui_newconfigurationform.h"
#include "configuration.h"
#include "userdatabaselist.h"

namespace ConfiguratorObjects
{
  class NewConfigurationForm : public QDialog, Ui_NewConfigurationForm
  {
    Q_OBJECT

  public:
    NewConfigurationForm(QWidget *parent = 0); 
    ~NewConfigurationForm();

    ConfigurationInfo getConfigurationInfo();

    void setUserDatabaseList(const UserDatabaseList &userDatabases); 
    void setDatabaseName(const QString &name);
    void setConfigurationName(const QString &name);

  protected:
    bool event(QEvent *);

  private:
    UserDatabaseList userDatabaseList;  
  };
}

#endif
