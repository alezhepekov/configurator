/***********************************************************************
* Module:   connectioninfo.h
* Author:   LGP
* Modified: 17 ������� 2007 �.
* Purpose:  ��������� � ����������� � ����������� � ��
***********************************************************************/

#ifndef CONNECTION_INFO_H
#define CONNECTION_INFO_H

#include <QString>
#include <QDataStream>

namespace ConfiguratorObjects
{ 
  struct ConnectionInfo
  {
    //��� ��� ip-����� ���� ����������� 
    QString host;

    //����
    QString port;

    //��� ���� ������
    QString dbName;  

    //��� ������������
    QString userName;

    //������
    QString password;   
  };

  QDataStream &operator<<(QDataStream &, const ConnectionInfo &);
  QDataStream &operator>>(QDataStream &, ConnectionInfo &);
}

#endif
