#ifndef ACCESS_CONTROL_H
#define ACCESS_CONTROL_H

#include <QtGlobal>
#include <QList>
#include <QFile>
#include <QDataStream>
#include <QString>

#include "matrix.h"

class AccessControl
{
public:
  AccessControl();    
  ~AccessControl();

  AccessControl & operator= (const AccessControl &op);

  //����� ���������� ������������� ���������� ��������
  quint64 getSubject(int);

  //����� ��������� ���������� �������� �������
  int addSubject(quint64, int);

  //����� ��������� ���������� �������� �������
  bool deleteSubject(int);

  //����� ���������� ������������� ���������� �������
  int getObject(int);
  
  //����� ��������� ���������� ������� �������
  int addObject(int);

  //����� ��������� ���������� ������� �������
  bool deleteObject(int);

  //����� ��������� ��������� ���������� ������� �������� � �������
  bool setPermission(int, int, int);

  //����� ���������� ���������� ������� �������� � �������
  int getPermission(int, int);  

  //����� ���������� ������ ��������������� ��������� �������
  QList<quint64>* getSubjectList();

  //����� ���������� ������ ��������������� �������� �������
  QList<int>* getObjectList();  

protected:
private:
  //������ ��������������� ��������� �������
  QList<quint64> accessSubjects;

  //������ ��������������� �������� �������
  QList<int> accessObjects;

  //������� ���� �������
  Matrix* accessMatrix;
};

#endif
