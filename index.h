/***********************************************************************
* Module:   index.h
* Author:   LGP
* Modified: 25 ����� 2007 �.
* Purpose:  ������������� ������� ������� ������� ��
***********************************************************************/

#ifndef INDEX_H
#define INDEX_H

#include <QString>

namespace ConfiguratorObjects
{
  namespace SQL
  {
    class Index
    {
    public:
      enum IndexKind
      {
        KindNone = 0,
        KindIndex = 1,
        KindPrimaryKey = 2,
        KindForeignKey = 3
      };

      Index();
      Index(enum IndexKind indexKind);
      ~Index();

      enum IndexKind getKind();
      void setKind(enum IndexKind newKind);

      QString getReferenceTableForForeignKey();
      void setReferenceTableForForeignKey(const QString &newReferenceTableForForeignKey);

      QString getReferenceColumnForForeignKey();
      void setReferenceColumnForForeignKey(const QString &newReferenceColumnForForeignKey);    

    protected:
    private:
      enum IndexKind kind;
      QString referenceTableForForeignKey;
      QString referenceColumnForForeignKey;
    };
  }
}

#endif
