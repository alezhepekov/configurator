#include "privilege.h"

using namespace ConfiguratorObjects;

Privilege::Privilege()
{
  id = -1;
}

Privilege::Privilege(const Privilege &other)
{
  id = other.id;
  externalId = other.externalId;
  name = other.name;
}

Privilege::~Privilege()
{
}

int Privilege::getId() const
{
  return id;
}

void Privilege::setId(int newId)
{
  id = newId;
}

QString Privilege::getExternalId() const
{
  return externalId;
}

void Privilege::setExternalId(const QString &newExternalId)
{
  externalId = newExternalId;
}

QString Privilege::getName() const
{
  return name;
}

void Privilege::setName(const QString &newName)
{
  name = newName;
}

namespace ConfiguratorObjects
{
  QDataStream &operator<<(QDataStream &out, const Privilege &privilege)
  {
    out << privilege.getId();
    out << privilege.getExternalId();
    out << privilege.getName();

    return out;
  }

  QDataStream &operator>>(QDataStream &in, Privilege &privilege)
  {
    int id;
    in >>id;
    privilege.setId(id);

    QString externalId;
    in >> externalId;
    privilege.setExternalId(externalId);

    QString name;
    in >> name;
    privilege.setName(name);

    return in;
  }
}
