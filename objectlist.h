/***********************************************************************
* Module:   objectslist.h
* Author:   LGP
* Modified: 29 ������� 2006 �.
* Purpose:  ����� ������������� ������ �������� ������������
***********************************************************************/

#ifndef OBJECT_LIST_H
#define OBJECT_LIST_H

#include "documents/document.h"
#include "documents/dictionary.h"
#include "documents/report.h"
#include "documents/documentsjournal.h"
#include "documents/register.h"
#include "documents/enumeration.h"
#include "documents/constant.h"
#include "documents/dform.h"
#include "documents/dtable.h"
#include "documents/dquery.h"
#include "documents/dclassmodule.h"
#include "documents/dglobalthread.h"
#include "documents/dglobalcontainer.h"
#include "documents/dtrigger.h"
#include "documents/dtimemanager.h"

#include "domxml.h"
#include <QList>

namespace ConfiguratorObjects
{
  class ObjectList: public Documents::DomXml
  {
    //������ �������� ������������
    QList<Documents::BaseObject*> objects;

  public:
    ObjectList();
    ObjectList(const ObjectList &other);
    ~ObjectList();

    ObjectList & ObjectList::operator= (const ObjectList &right);

    //����� ���������� ������ ��������
    const QList<Documents::BaseObject*> *getObjects() const;

    QList<Documents::BaseObject*> *getObjects();

    //����� ������������� ������ ��������
    void setObjects(const QList<Documents::BaseObject*> &);

    //����� ��������� ������������ xml-�������������
    QString toXML();

    //����� ��������� �������� �� xml-�������������
    bool fromXML(const QString&);

  protected:
    //����� ��������� �������� � ���������� �� � ��������� ��������
    void appendToXML(QDomDocument&, QDomElement&, const QMap<int, QString>*); 

    //����� ��������� �������� ��������� �� xml-���������
    void takeFromXML(const QDomElement&, QMap<int, QString>*);

    friend QDataStream &operator<<(QDataStream &, const ObjectList &);
    friend QDataStream &operator>>(QDataStream &, ObjectList &);
  };

  QDataStream &operator<<(QDataStream &out, const ObjectList &right);
  QDataStream &operator>>(QDataStream &in, ObjectList &right);
}

#endif
