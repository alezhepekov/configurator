#include "domxml.h"

using namespace ConfiguratorObjects::Documents;

DomXml::DomXml()
{
}

DomXml::~DomXml()
{
}

QDomElement DomXml::findElement(const QDomElement& ce, QStringList& paramPath)
{
  //�������� ������� ��������
  if (ce.isNull()||(paramPath.size() == 0))
  {    
    return QDomElement::QDomElement();   
  }

  //��������� ���� �������� �������� ������ � �������� ������
  if (!paramPath.first().compare(ce.tagName().toLower()))
  {
    //������ ���������
    if (paramPath.size() == 1)
    {
      //����� �������� ��������
      return ce;
    }
    else
    {
      //������ �� ���������
      if (ce.hasChildNodes())
      {
        //����� ���������� ��������� ����
        paramPath.removeFirst();
        return findElement(ce.firstChild().toElement(), paramPath);
      }
      else
      {
        //����� �������� ���� � ��������� ������ ����-�� ������ ��������
        QDomElement ne = ce.nextSibling().toElement();
        if (!ne.isNull())
        {
          return findElement(ne, paramPath);
        }
        else
        {            
          //������� ������� ���� � ������ ��������� �� ������
          return QDomElement::QDomElement();
        }
      }
    }
  }
  else
  {
    //����� �������� ���� � ��������� ������ ����-�� ������ ��������
    QDomElement ne = ce.nextSibling().toElement();
    if (!ne.isNull())
    {
      return findElement(ne, paramPath);
    }
    else
    {
      //������� ������� ���� � ������ ��������� �� ������
      return QDomElement::QDomElement();
    }
  }
}

QDomElement DomXml::getElement(const QDomDocument& doc, const QString& xPath)
{  
  //����� ������ ������ ������������������ ��������� ���� ����������� ������� ������
  QStringList path = xPath.toLower().split("/", QString::SkipEmptyParts);
  return findElement(doc.documentElement(), path);
}

QDomElement DomXml::getElement(const QDomElement& currentElement, const QString& xPath)
{
  QStringList path = xPath.toLower().split("/", QString::SkipEmptyParts);  
  return findElement(currentElement, path);
}
