/***********************************************************************
* Module:   configurationlist.h
* Author:   LGP
* Modified: 26 ������ 2007 �.
* Purpose:  ����� ������������� ������ ������������
***********************************************************************/

#ifndef CONFIGURATION_LIST_H
#define CONFIGURATION_LIST_H

#include "domxml.h"
#include "configurationheader.h"

namespace ConfiguratorObjects
{
  class ConfigurationList: public Documents::DomXml
  {
  public:
    ConfigurationList();
    ~ConfigurationList();

    const QList<ConfigurationInfo> *getConfigurations() const;
    QList<ConfigurationInfo> *getConfigurations();

    void setConfigurations(const QList<ConfigurationInfo> &);

    //����� ��������� ������������ xml-�������������
    QString toXML();

    //����� ��������� �������� ������ �� xml-�������������
    bool fromXML(const QString&);  

  protected:
  private:
    //������ ���������� � �������������
    QList<ConfigurationInfo> configurations;

    friend QDataStream &operator<<(QDataStream &, const ConfigurationList &);
    friend QDataStream &operator>>(QDataStream &, ConfigurationList &);
  };

  QDataStream &operator<<(QDataStream &, const ConfigurationList &);
  QDataStream &operator>>(QDataStream &, ConfigurationList &);
}

#endif
