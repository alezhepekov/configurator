#include "newconfigurationform.h"

#include "QRegExp"
#include "QRegExpValidator"

using namespace ConfiguratorObjects;

NewConfigurationForm::NewConfigurationForm(QWidget *parent)
:QDialog(parent, Qt::WindowTitleHint|Qt::WindowSystemMenuHint)
{  
  setupUi(this);

  QRegExp rx("[a-z][a-z0-9]*");
  QValidator *validator = new QRegExpValidator(rx, this);
  leDatabaseName->setValidator(validator); 

  leVersion->setText("1.0.0");
  dteCreationDateTime->setDateTime(QDateTime::currentDateTime()); 
}

NewConfigurationForm::~NewConfigurationForm()
{
}

bool NewConfigurationForm::event(QEvent *e)
{
  if ( e->type() == QEvent::LanguageChange )
    retranslateUi(this);

  return QWidget::event(e);
}

ConfigurationInfo NewConfigurationForm::getConfigurationInfo()
{
  ConfigurationInfo configInfo;
  configInfo.id = -1;

  //configInfo.connectionInfo.host     = cpHost->text();
  //configInfo.connectionInfo.port     = cpPort->text();
  configInfo.connectionInfo.dbName = leDatabaseName->text();
  //configInfo.connectionInfo.userName = cpUser->text();
  //configInfo.connectionInfo.password = cpPassword->text();
  
  configInfo.header.setId(-1); 
  configInfo.header.setName(leConfigurationName->text());
  configInfo.header.setAuthor(leAuthor->text());
  configInfo.header.setVersion(leVersion->text());
  configInfo.header.setCreationDateTime(dteCreationDateTime->dateTime());

  if ( cbUserDatabase->currentIndex() != -1 )
  {
    configInfo.header.setUserDatabaseId(cbUserDatabase->currentIndex() + 1);
  }  
 
  return configInfo;
}

void NewConfigurationForm::setUserDatabaseList(const UserDatabaseList &userDatabases)
{
  userDatabaseList = userDatabases;
  
  for ( QList<ConnectionInfo>::iterator i = userDatabaseList.getDatabases()->begin(); i != userDatabaseList.getDatabases()->end(); i++ )
  {  
    cbUserDatabase->addItem((*i).dbName);
  }

  cbUserDatabase->setCurrentIndex(-1);
}

void NewConfigurationForm::setDatabaseName(const QString &name)
{
  leDatabaseName->setText(name);
}

void NewConfigurationForm::setConfigurationName(const QString &name)
{
  leConfigurationName->setText(name);
}
