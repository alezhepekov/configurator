/***********************************************************************
* Module:   userdatabasemanager.h
* Author:   LGP
* Modified: 25 ���� 2007 �.
* Purpose:  �������� ������ ���������������� �� 
***********************************************************************/

#ifndef USER_DATABASE_MANAGER_H
#define USER_DATABASE_MANAGER_H

#include "datamanager.h"

namespace ConfiguratorObjects
{
  //typedef QMap<int, QList<QPair<QString, QVariant> > > DataBlock;

  class UserDatabaseManager: public DataManager
  {
    enum ParseKind
    {
      SelectClause,
      FromClause,
      WhereClause,
      UpdateClause,
      SetClause,
      InsertClause,
      ValuesClause,
      DeleteClause
    };

    static const int INVALID_PATH = 0x0001;

    QString debugOut;

  public:
    UserDatabaseManager();
    ~UserDatabaseManager();

    void putData1(DataBlock &data, const QString &s1);//Debug
    void putData(const QByteArray &data, const QString &s1);//Debug

    void selfTest();//Debug

    bool exec(const QString &sql, QDataStream &data);    

  protected:
    QString parsePath(const QString &path, const bool flag = false, const int k = 3);

    QString parsePathObjects(const QStringList &lexems, const ParseKind kind, const bool flag = false, const int k = 3);

    QString parseExpression(const QString &exp, const bool flag = false);

    void parseWhereLexem(QStringList &lexems, const bool flag = false);

    void parseFromLexem(QStringList &fromLexems, QStringList &selectLexems);

    void parseSelectLexem(QStringList &lexems, const int currentIndex);

    void parseSetLexem(QStringList &lexems, const bool flag = false);

    void parseUpdateLexem(QStringList &lexems, const int currentIndex);

    void parseIntoLexem(QStringList &lexems, const bool flag);

    void parseInsertLexem(QStringList &lexems, const int currentIndex);

    void parseFromLexem(QStringList &lexems, const bool flag);

    void parseDeleteLexem(QStringList &lexems, const int currentIndex);

    //����� ��������� ������ ������ ����� � ��������� ������ ���������
    //name - ��� ������� ������ ����� ���������
    //id   - ������������� ����������
    //out  - ����� �������� ������    
    int read(const QString &name, const int id, QDataStream &out);

    //����� ��������� ������ ������ ����� � ��������� ������ ���������
    //name - ��� ������� ������ ����� ���������
    //id   - ������������� ����������
    //in   - ����� ������� ������
    int write(const QString &name, const int id, QDataStream &in);

    //����� ��������� �������� ������ ����� � ��������� ������ ���������
    //name - ��� ������� ������ ����� ���������
    //id   - ������������� ����������   
    int erase(const QString &name, const int id);

    bool checkPath(const QString &path);

    bool insertDataIntoTable(const QString &tableName, const DataBlock &data);    
  };
}
  
#endif
