#include "newuserdatabaseform.h"

#include "QRegExp"
#include "QRegExpValidator"

using namespace ConfiguratorObjects;

NewUserDatabaseForm::NewUserDatabaseForm(QWidget *parent)
:QDialog(parent, Qt::WindowTitleHint|Qt::WindowSystemMenuHint)
{  
  setupUi(this);

  QRegExp rx("[a-z][a-z0-9]*");
  QValidator *validator = new QRegExpValidator(rx, this);
  leDatabaseName->setValidator(validator);

  /*udbHost->setText("127.0.0.1");
  udbUser->setText("root");
  udbPassword->setText("11111");*/
}

NewUserDatabaseForm::~NewUserDatabaseForm()
{
}

bool NewUserDatabaseForm::event(QEvent *e)
{
  if ( e->type() == QEvent::LanguageChange )
    retranslateUi(this);

  return QWidget::event(e);
}

ConnectionInfo NewUserDatabaseForm::getConnectionInfo()
{  
  ConnectionInfo cInfo;
  /*cInfo.host     = udbHost->text();
  cInfo.port     = udbPort->text();*/
  cInfo.dbName   = leDatabaseName->text();
 /* cInfo.userName = udbUser->text();
  cInfo.password = udbPassword->text();  */

  return cInfo;
}
