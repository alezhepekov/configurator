/***********************************************************************
* Module:   datamanager.h
* Author:   LGP
* Modified: 25 ���� 2007 �.
* Purpose:  �������� ������ ���������������� �� 
***********************************************************************/

#ifndef DATA_MANAGER_H
#define DATA_MANAGER_H

#include "dataexchangeinterface.h"
#include "connectioninfo.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>

#include <QMap>
#include <QList>
#include <QPair>

namespace ConfiguratorObjects
{
  /*typedef QMap<int, QList<QPair<QString, QVariant> > > DataBlock;*/

  class DataManager: public DataExchangeInterface
  {
  protected:
    QSqlDatabase currentDatabase;

  public:
    DataManager();
    ~DataManager();    

    bool connect(const ConnectionInfo &cInfo);

    void disconnect();

    bool exec(const QString &sql, QDataStream &data);    

  protected:
    /*int outData(QSqlQuery &query, QDataStream &out);

    int inData(QDataStream &in, DataBlock &values);*/
  };
}

#endif
