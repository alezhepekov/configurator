/***********************************************************************
* Module:   matrix.h
* Author:   LGP
* Modified: 25 ���� 2006 �.
* Purpose:  ����� �������������  �������
***********************************************************************/

#ifndef MATRIX_H
#define MATRIX_H

class Matrix
{
public:
  //����������� ������� �������� n x m ���������
  Matrix(int, int);
  
  Matrix(const Matrix& oldMatrix);
  ~Matrix();

  Matrix & Matrix::operator= (const Matrix &op);

  //����� ��������� ������� � �������
  int addColumn();

  //����� ������� ��������� ������� �� �������
  bool deleteColumn(int);

  //����� ��������� ������ � �������
  int addRow();

  //����� ������� ��������� ������ �� �������
  bool deleteRow(int);
    
  void setItem(int, int, int);

  //����� ���������� �������� ��������� ������ �������
  int getItem(int, int);

  //����� ��������� ��������� ����������� �������
  bool setDimension(int, int);

  //����� ���������� ���������� ����� �������
  int getRowCount();

  //����� ���������� ���������� �������� �������
  int getColCount();
  
  //����� ��������� ������� �������
  void clear();

  //�������� ����������� �������
  //Matrix operator= (Matrix op);

private:
  int rowCount, colCount;
  int *data;
};
#endif

